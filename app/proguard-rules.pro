# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
-keepclassmembers class com.mobifusion.android.ldoce5.Util.JavaScriptInterface {
   public *;
}
-ignorewarnings
#-keep public class com.mobifusion.android.ldoce5.Util$JavaScriptInterface
#-keep public class * implements com.mobifusion.android.ldoce5.Util$JavaScriptInterface
#-keepclassmembers class com.mobifusion.android.ldoce5.Util$JavaScriptInterface {
#    public String getLocalizedString(String key);
#    public boolean toggleFavorite(String jsonObject);
#    public boolean isWordFavorite(String id);
#    public void tappedWord(String clickedObject) throws JSONException;
#    public void tappedCrossrefto(String clickedObject) throws JSONException;
#    public void tappedThesref(String clickedObject)throws JSONException;
#    public void checkUsUkExampleSoundButton(String jsonObject);
#    public void infoButtonTapped(String jsonObject) throws JSONException;
#    public boolean isAdditionalInfoAvailable(String id);
#    public void setHeadcellHeight(String heightOfHeadCell)throws JSONException;
#    public void callForCopyToClipboard(String jsonObject) throws JSONException;
#    public void openImage(String detail);
#    public  void getSelectedWord(String selectedWord)throws JSONException;
#    public void callForAnalyticsTracking();
#}
