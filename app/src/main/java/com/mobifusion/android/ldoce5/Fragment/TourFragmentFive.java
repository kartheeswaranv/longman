package com.mobifusion.android.ldoce5.Fragment;


import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.mobifusion.android.ldoce5.R;
import com.mobifusion.android.ldoce5.Util.Singleton;

public class TourFragmentFive extends Fragment {

    public static final String PREFS_NAME = "LDOCE6PrefsFile";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.tour_fragment_five, container, false);
    }

    // ‎"\u200E" - This unicode Character is added by default -  In order to avoid Righ to Left in Arabic Language
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        SharedPreferences fontSizePreferences =  getActivity().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        int fontSizeValue = fontSizePreferences.getInt("FONT_VALUE", 0);
        //Up-to-date text
        TextView textView1=(TextView)getActivity().findViewById(R.id.tour_fragment_up_to_date);
        textView1.setText("\u200E"+getString(R.string.up_to_date_dictionary_content));
        textView1.setTypeface(Singleton.getMundoSansPro_Bold());
        textView1.setTextColor(Color.rgb(244, 219, 166));
        textView1.setTextSize(16+fontSizeValue);
        //Up-to-date content
        EditText textView2=(EditText)getActivity().findViewById(R.id.tour_fragment_up_to_date_content);
        textView2.setText("\n".concat("\u200E"+getString(R.string.tour_up_to_date_content)));
        textView2.setTypeface(Singleton.getMundoSansPro());
        textView2.setTextColor(Color.parseColor("#FFFFFF"));
        textView2.setTextSize(16+fontSizeValue);
    }
}
