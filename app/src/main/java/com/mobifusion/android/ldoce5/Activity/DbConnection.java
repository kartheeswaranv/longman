package com.mobifusion.android.ldoce5.Activity;

import android.database.sqlite.SQLiteDatabase;

import com.mobifusion.android.ldoce5.Util.Utils;

/**
 * Created by ramkumar on 24/03/15.
 */
public class DbConnection {


    public static SQLiteDatabase getDbConnection()
    {
        SQLiteDatabase db=null;
        try
        {
            //Data base connection
             db = SQLiteDatabase.openDatabase(Utils.CoreDBFilePathAndName, null, 0);

        }
        catch (Exception e)
        {
            System.out.println("LDOCE.sqlite File Not Found");
        }

            return db;
    }
   //User Db connection
    public static SQLiteDatabase getUserDbConnection()
    {

        //User DB Creation
        SQLiteDatabase userDbPath= null;
        userDbPath.openOrCreateDatabase(Utils.UserDBFileNameAndPath,null);
        //User DB Connection
        try
        {
            //Data base connection
            userDbPath = SQLiteDatabase.openDatabase(Utils.UserDBFileNameAndPath, null, 0);
        }
        catch (Exception e)
        {
            System.out.println("Path Not found");
        }
        return userDbPath;

    }
}
