package com.mobifusion.android.ldoce5.Util;

//import android.support.v4.app.Fragment;
//import android.support.v4.app.FragmentManager;
//import android.support.v4.app.FragmentStatePagerAdapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.mobifusion.android.ldoce5.Fragment.TourFragmentFive;
import com.mobifusion.android.ldoce5.Fragment.TourFragmentFour;
import com.mobifusion.android.ldoce5.Fragment.TourFragmentOne;
import com.mobifusion.android.ldoce5.Fragment.TourFragmentSix;
import com.mobifusion.android.ldoce5.Fragment.TourFragmentThree;
import com.mobifusion.android.ldoce5.Fragment.TourFragmentTwo;
import com.mobifusion.android.ldoce5.R;

/**
 * Created by ramkumar on 13/07/15.
 */
public class PagerAdapter extends FragmentStatePagerAdapter {

    public PagerAdapter(FragmentManager fm){
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch(position){
            case 0:
                    return new TourFragmentOne();
            case 1:
                    return new TourFragmentTwo();
            case 2:
                return new TourFragmentThree();
            case 3:
                    return new TourFragmentFour();
            case 4:
                return new TourFragmentFive();
            case 5:
                return new TourFragmentSix();
            default:
                break;

        }
        return null;
    }

    @Override
    public int getCount() {
        return 6;
    }
}
