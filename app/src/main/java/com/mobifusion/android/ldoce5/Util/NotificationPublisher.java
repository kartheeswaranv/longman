package com.mobifusion.android.ldoce5.Util;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import androidx.core.app.NotificationCompat;
import android.util.Log;

import com.mobifusion.android.ldoce5.Activity.DbHelper;
import com.mobifusion.android.ldoce5.Activity.WelcomeActivity;
import com.mobifusion.android.ldoce5.R;

/**
 * Created by ramkumar on 09/06/15.
 */
public class NotificationPublisher extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
           //Check whether Entry is present or not in UserDb-userNotification Table
           boolean isEntryPresent=DbHelper.checkNotificationEntry();
           if(isEntryPresent){
               //If entry is present in user notification table - check whether notification is or not for that entry
               boolean isNotificationSeen=DbHelper.checkIsNotifSeen();
               //If notification is already seen  -  do not create the Local notification
               if(isNotificationSeen){
                   Log.w("LDOCE::::","Notification not fired");
                   return;
               }
           }
           //Call for the notification to show
           createNotification(context);
        Log.w("LDOCE::::", "Notification fired");

    }
    public void createNotification(Context context){
        //Get the Pending Intent
        DbHelper db= new DbHelper();
        String title= String.format("%s\n%s\n",
                "LDOCE",
                "Word of the Day");
        String[] details=new String[3];
        //get the hwd and its ID for Local notification
        details=db.createAndInsertNotification(context);
        String hwdId=null,hwd=null;
        if(details!=null){
            hwdId=details[0];
            hwd=details[1];
        }
        if(hwd!=null) {
            //for Compatibility we are using NotificationCompat.Builder
            NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                    //If the Notification is Viewed or Swiped away we have to cancel the Notification
                    .setAutoCancel(true)
                            //title for Notifications
                    .setContentTitle("LDOCE - ".concat(context.getString(R.string.word_of_the_day)))
                            //Notification Body Content
                    .setContentText(hwd);
            //To show Notification in Expanded View in Lock Screen
            NotificationCompat.BigTextStyle style= new NotificationCompat.BigTextStyle();
            style.bigText(hwd);
            builder.setStyle(style);
            //icon for Notification
            builder.setSmallIcon(R.drawable.ic_launcher);
            //Setting Default sound for Notification
            builder.setDefaults(Notification.DEFAULT_ALL);
            // Insert into the notifications table
            DbHelper.insertNotificationTable(hwd, hwdId);
            Intent resultIntent = new Intent(context, WelcomeActivity.class);
            resultIntent.putExtra("Headword",hwd);
            resultIntent.putExtra("HeadwordIdIs", hwdId);
            System.out.println("Headword is" + hwd + "Headword Id is" + hwdId);
            // Because clicking the notification opens a new ("special") activity, there's
            // no need to create an artificial back stack.
            PendingIntent resultPendingIntent =
                    PendingIntent.getActivity(context.getApplicationContext(), 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            builder.setContentIntent(resultPendingIntent);
            NotificationManager manager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
            manager.notify(123456789,builder.build());
        }
    }
}
