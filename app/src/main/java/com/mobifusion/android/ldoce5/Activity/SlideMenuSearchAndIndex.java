package com.mobifusion.android.ldoce5.Activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.text.ClipboardManager;
import android.util.Log;
import android.view.ActionMode;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.Toast;

//import com.google.android.gms.analytics.GoogleAnalytics;
//import com.google.android.gms.analytics.HitBuilders;
//import com.google.android.gms.analytics.Logger;
//import com.google.android.gms.analytics.Tracker;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.mobifusion.android.ldoce5.Fragment.AboutFragment;
import com.mobifusion.android.ldoce5.Fragment.AcademicWordList;
import com.mobifusion.android.ldoce5.Fragment.BottomBarsFragment;
import com.mobifusion.android.ldoce5.Fragment.CoreVocabularyFragment;
import com.mobifusion.android.ldoce5.Fragment.DetailPageFragment;
import com.mobifusion.android.ldoce5.Fragment.FavoritesFragment;
import com.mobifusion.android.ldoce5.Fragment.HistoryFragment;
import com.mobifusion.android.ldoce5.Fragment.HomePageFragment;
import com.mobifusion.android.ldoce5.Fragment.IndexFragment;
import com.mobifusion.android.ldoce5.Fragment.SearchFragment;
import com.mobifusion.android.ldoce5.Fragment.SettingsFragment;
import com.mobifusion.android.ldoce5.Fragment.SlidingMenuFragment;
import com.mobifusion.android.ldoce5.R;
import com.mobifusion.android.ldoce5.Util.AlphabetListAdapter;
import com.mobifusion.android.ldoce5.Util.FirebaseWrapperScreens;
import com.mobifusion.android.ldoce5.Util.LDOCEApp;
import com.mobifusion.android.ldoce5.Util.Utils;
import com.mobifusion.android.ldoce5.model.IndexRowItem;
import com.mobifusion.android.ldoce5.model.RowWithHomnumForBookmark;
import com.mobifusion.android.ldoce5.model.SearchRowItem;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.zip.ZipInputStream;

import static com.mobifusion.android.ldoce5.Activity.WelcomeActivity.progress_bar_type;

public class SlideMenuSearchAndIndex extends BaseActivity implements View.OnClickListener,SlidingMenuFragment.OnFragmentInteractionListener,SearchFragment.OnSearchResultListener,BottomBarsFragment.OnTabChangeListener,DetailPageFragment.OnDetailPageLoadListener,CoreVocabularyFragment.OnCoreVocPageLoadListener,SettingsFragment.OnSettingPageRefreshListener{
    private final static String DETAIL_BACK_STACK="DETAIL_BACK_STACK";
    public static final String PREFS_NAME = "LDOCE6PrefsFile";
    private SlidingMenu slidingMenu ;
    private ActionMode mActionMode;
//    Tracker t;
    String fileToChange = File.separator+"sdcard"+File.separator+"Android"+File.separator+"obb"+ File.separator+"convertMp3ToDb";
    String rename = File.separator+"sdcard"+File.separator+"Android"+File.separator+"obb"+ File.separator+".media_files_228";
    String filePath = File.separator + "sdcard" + File.separator + "Android" + File.separator + "obb" + File.separator + "com.mobifusion.android.ldoce5" + File.separator + "main.228.com.mobifusion.android.ldoce5.obb";
    boolean check;
    boolean unZipFileSize = false;
    static boolean cancelButton = false;

    @Override
    public void onAttachFragment(Fragment fragment) {
        super.onAttachFragment(fragment);
    }

    @Override
    protected void onStart() {
        super.onStart();
        SharedPreferences sharedPreferences = getSharedPreferences("LDOCE6PrefsFile", MODE_PRIVATE);
        boolean isSearchTapped = sharedPreferences.getBoolean("SearchMenuTapped", false);
        if (isSearchTapped) {
            Fragment searchFragment = getSupportFragmentManager().findFragmentById(R.id.searchFragement);
            SearchView searchView = (SearchView) this.findViewById(R.id.search_bar);
            searchView.setFocusable(true);
            searchView.setIconified(false);
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.showSoftInputFromInputMethod(searchView.getWindowToken(), 0);
            searchView.requestFocus();
        }
        // Set the log level to verbose.
//        GoogleAnalytics.getInstance(this).getLogger().setLogLevel(Logger.LogLevel.VERBOSE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slide_menu_search_and_index);
        //Initialize the view elements
        SharedPreferences sharedPreferences = getSharedPreferences("LDOCE6PrefsFile", MODE_PRIVATE);
        String language = sharedPreferences.getString("LDOCELanguage", "en_GB");
        Utils.setApplicationLanguage(this, language);
        setUpView();
        //Get Headword and id from word of the day
        String hwd = getIntent().getStringExtra("Headword");
        String hwdId = getIntent().getStringExtra("HeadwordIdIs");

        if (isTablet(this)) {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager()
                    .beginTransaction();
            HomePageFragment homePageActivity = new HomePageFragment();
            fragmentTransaction.replace(R.id.detailPageFragment, homePageActivity);
            fragmentTransaction.commit();
        } else {

            //Setup side menu for phone
            FragmentTransaction fragmentTransaction = getSupportFragmentManager()
                    .beginTransaction();
            IndexFragment indexActivity = new IndexFragment();
            fragmentTransaction.replace(R.id.indexResultfragment, indexActivity);
            fragmentTransaction.commit();

            setupSideMenu();
            Button menuButton = (Button) findViewById(R.id.slideMenuButton);
            menuButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    InputMethodManager imm = (InputMethodManager) getApplication().getSystemService(Service.INPUT_METHOD_SERVICE);
                    if (imm.isActive()) {
                        imm.hideSoftInputFromWindow(slidingMenu.getWindowToken(), 0);
                    }

                    // Tracking for SlideMenu Button Click
                    // Get the tracker.
//                    t = ((LDOCEApp) getApplication()).getTracker(LDOCEApp.TrackerName.APP_TRACKER);
//                    // Build and send an Event.
//                    t.send(new HitBuilders.EventBuilder()
//                            .setCategory("expand_collapse_button_click")
//                            .setAction("expand_collapse_button_clicked")
//                            .setLabel("Expand/Collapse SlideMenuButton")
//                            .build());

                    Bundle params=new Bundle();
                    params.putString("eventCategory","expand_collapse_button_click");
                    params.putString("eventAction","expand_collapse_button_clicked");
                    params.putString("eventLabel","Expand/Collapse SlideMenuButton");
                    FirebaseWrapperScreens.EventLogs(params);
                    if (slidingMenu.isMenuShowing()) {
                        slidingMenu.toggle();
                    } else {
                        if (imm.isActive()) {
                            imm.hideSoftInputFromWindow(slidingMenu.getWindowToken(), 0);
                        }
                        //Toggle to slideMenu
                        slidingMenu.toggle(true);

                    }
                }
            });
        }

        if (hwd != null) {
            //Navigate to Detail Page for the Tapped Word
            Utils.navigateToDetailPage(this, hwd, hwdId, "0", "0");
            //Update the notification table
            DbHelper.updateNotificationTable(hwd, hwdId);
            getIntent().putExtra("Headword", "");
            getIntent().putExtra("HeadwordIdIs", "");
            Log.w("LDOCE6Check", "VIA Notification");
        }

        // show search results based on SaveSearch Word and Search on Clipboard word
        setUpSearchBar();

        // This is mainly used to request the FOcus in SearcBar when Search Menu is tapped
        boolean isSearchTapped = sharedPreferences.getBoolean("SearchMenuTapped", false);
        if (isSearchTapped) {
            Fragment searchFragment = getSupportFragmentManager().findFragmentById(R.id.searchFragement);
            SearchView searchView = (SearchView) this.findViewById(R.id.search_bar);
            searchView.setFocusable(true);
            searchView.setIconified(false);
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.showSoftInputFromInputMethod(searchView.getWindowToken(), 0);
            searchView.requestFocus();
        }
    }

    /**
     * This function is to set up side menu for phone
     */
    private void setupSideMenu()
    {
        slidingMenu = new SlidingMenu(this);
        slidingMenu.setMode(SlidingMenu.LEFT);
        slidingMenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
        slidingMenu.setShadowWidthRes(R.dimen.slidingmenu_shadow_width);
//        slidingMenu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
        slidingMenu.setFadeDegree(0.35f);
        slidingMenu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
        slidingMenu.setMenu(R.layout.slidingmenu);
        final Button closeButton = (Button)findViewById(R.id.closeMenuButton);
        Typeface longmanFont = Typeface.createFromAsset(getAssets(),"longman.ttf");


        //closeButton.setText(R.string.slide_menu_close_button);
        closeButton.setTypeface(longmanFont);

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(getApplicationContext(), SlideMenuSearchAndIndex.class);
                startActivity(intent);
            }
        });

        ImageView img = (ImageView)findViewById(R.id.mainLogo);
        img.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // This code maily used to - When Logo is Tapped then redirects to INdex Page
                Intent intent = new Intent();
                intent.setClass(getApplicationContext(), SlideMenuSearchAndIndex.class);
                startActivity(intent);
                setUpSearchBar();

//                Fragment searchFragment= getSupportFragmentManager().findFragmentById(R.id.searchFragement);
//                SearchView searchView= (SearchView)searchFragment.getView().findViewById(R.id.search_bar);
//                if (!searchView.isIconified()) {
//                    InputMethodManager inputMethodManager=(InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
//                    inputMethodManager.showSoftInputFromInputMethod(getWindow().getCurrentFocus().getWindowToken(),InputMethodManager.SHOW_FORCED);
//                }

            }
        });

        // Layout to make the whole top area clickable in Slide Menu Page for Phone
        RelativeLayout clickableLayout = (RelativeLayout) findViewById(R.id.clickableLayout);
        clickableLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // After Layout area is clicked - redirects to Index Page
                Intent intent = new Intent();
                intent.setClass(getApplicationContext(), SlideMenuSearchAndIndex.class);
                startActivity(intent);
                return true;
            }
        });

    }

    public boolean isTablet(Context context)
    {
        return getResources().getBoolean(R.bool.isTablet);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        //don't reload the current page when the orientation is changed

        super.onConfigurationChanged(newConfig);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_slide_menu_search_and_index, menu);
        return true;

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.ECLAIR
                && keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            // Take care of calling this method on earlier versions of
            // the platform where it doesn't exist.
            onBackPressed();
        }
        else if(!isTablet(this) && keyCode == KeyEvent.KEYCODE_MENU)
        {
            slidingMenu.toggle(true);
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onBackPressed() {
        //TODO: Looks like particular scenario missed.

        try
        {
            //Handling back button for search.
            final ListView searchListView = (ListView)findViewById(R.id.searchResultListView);
            Fragment searchFragment= getSupportFragmentManager().findFragmentById(R.id.searchFragement);
            SearchView searchView= (SearchView)searchFragment.getView().findViewById(R.id.search_bar);
            if (!searchView.isIconified()) {
                //Keyboard dismissal and removed the search focus
                searchListView.setVisibility(View.INVISIBLE);
                searchView.setQuery("", false);
                searchView.setIconified(true);
                searchView.setFocusable(false);
                return;
            }
            //for phone, check if slidemenu is opened, and close it when back first.
            if(!isTablet(this) && slidingMenu.isMenuShowing())
            {
                slidingMenu.toggle(true);
                return;
            }

            //Getting the current fragment displaying in R.id.indexResultfragment and check its class typ
            Fragment currentDisplayingFragment = (Fragment)getSupportFragmentManager().findFragmentById(R.id.indexResultfragment);
//            if (currentDisplayingFragment instanceof DetailPageFragment) {
//                DetailPageFragment frag = (DetailPageFragment) currentDisplayingFragment;
//                /*if((frag.mediaPlayer!=null) && frag.mediaPlayer.isPlaying() ) {
//                    frag.mediaPlayer.reset();
//                    frag.mediaPlayer.stop();
//                    frag.mediaPlayer.release();
//                    frag.mediaPlayer = null;
//                }*/
//            }

            if (currentDisplayingFragment instanceof IndexFragment)
            {
                //Current fragment is IndexFragment check for the parentbackbutton or tablet
                if (((IndexFragment) currentDisplayingFragment).parentActivityOnBackPressed()||isTablet(this))
                {
                    super.onBackPressed();
                }
            }
            else if(currentDisplayingFragment instanceof AcademicWordList){

                AcademicWordList academicWordList=(AcademicWordList)currentDisplayingFragment;
                boolean secondLevelToFirstLevel=academicWordList.onParentBackButtonPressed();
                if(secondLevelToFirstLevel)
                    return;
                else
                {
                    FragmentManager ft = getSupportFragmentManager();
                    ft.popBackStack();
                }

            }
            //Check if its table, if so it should perform normal back operation
            else if (isTablet(this))
            {
                Fragment detailFragment = (Fragment)getSupportFragmentManager().findFragmentById(R.id.detailPageFragment);
                if(detailFragment instanceof AcademicWordList){
                    AcademicWordList academicWordList=(AcademicWordList)detailFragment;
                    boolean secondLevelToFirstLevel=academicWordList.onParentBackButtonPressed();
                    if(secondLevelToFirstLevel)
                        return;
                }
                super.onBackPressed();
            }
            //Handle for other fragment type like History, favourite, Setting etc for phone
            else
            {
                FragmentManager ft = getSupportFragmentManager();
                ft.popBackStack();
            }


        } catch (Exception e)
        {
            System.out.println(e.getMessage());

        }

    }



    @Override
    public void onTabChanged(String tabId) {
        System.out.println("Tab Id is:" + tabId);
        //checking if tab1 is clicked or not
        //if tab2,tab3,tab4 are clicked - hide the listview if it is visible
        ListView searchlistView = (ListView) findViewById(R.id.searchResultListView);
        switch (tabId)
        {
            case "tab2":
                if(searchlistView.getVisibility()==View.VISIBLE)
                    searchlistView.setVisibility(View.INVISIBLE);
                break;
            case "tab3":
                if(searchlistView.getVisibility()==View.VISIBLE)
                    searchlistView.setVisibility(View.INVISIBLE);
                break;
            case "tab4":
                if(searchlistView.getVisibility()==View.VISIBLE)
                    searchlistView.setVisibility(View.INVISIBLE);
                break;
            case "tab1":
                setUpSearchBar();
                break;
            default:
                break;
        }
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
            setUpSearchBar();
    }

    @Override
    protected void onStop() {
        super.onStop();
        dismissKeyboard();
    }

    @Override
    protected void onPause() {
        super.onPause();
            if (isTablet(this)) {
                dismissKeyboard();
                setUpSearchBar();
            } else {
                Fragment searchFragment = getSupportFragmentManager().findFragmentById(R.id.searchFragement);
                Fragment fragment = searchFragment.getFragmentManager().findFragmentById(R.id.detailPageFragment);
                if (fragment instanceof DetailPageFragment || fragment instanceof IndexFragment) {
                    setUpSearchBar();
                }
            }

    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            if (checkForFile()) {
                //Shared preferences for bookmarks
                SharedPreferences preferences = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
                setUpSearchBar();

                //check if bookmark is already imported
                boolean isFavTableUpdated = preferences.getBoolean("FavTableUpdated", false);
                if (!isFavTableUpdated) {
                    updateFavoriteTable();
                }
                boolean imported = preferences.getBoolean("importedBookmarks", false);
                if (!imported) {
                    //import bookmark and show popup
                    final String PREF_FILE_NAME = "PEARSONDictPreferences";
                    SharedPreferences sharedPreferences = getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
                    String values = sharedPreferences.getString("BOOKMARKS", "");
                    if (!values.isEmpty())
                        importBookmarkAndShowPopup();
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putBoolean("importedBookmarks", true);
                    editor.apply();
                }

                if (preferences.getBoolean("Word-of-the-Day", true) && imported) {
                    boolean isEntryPresent = DbHelper.checkNotificationEntry();
                    if (isEntryPresent) {
                        //check whether Notification is seen or not
                        boolean isNotifSeen = DbHelper.checkIsNotifSeen();
                        if (!isNotifSeen) {
                            WelcomeActivity.clearNotification(this);
                            WordOfTheDayPopUp wordOfTheDayPopUp = new WordOfTheDayPopUp(this);
                            wordOfTheDayPopUp.show();
                            Log.w("LDOCE6Check", "VIA Popup");
                        }
                    } else {
                        DbHelper db = new DbHelper();
                        db.createAndInsertNotification(this);
                        //WelcomeActivity.clearNotification(this);
                        WordOfTheDayPopUp wordOfTheDayPopUp = new WordOfTheDayPopUp(this);
                        wordOfTheDayPopUp.show();
                        Log.w("LDOCE6Check", "VIA Popup");
                    }
                }
                SearchView searchView = (SearchView) this.findViewById(R.id.search_bar);
                //When app returns from background absorbs the behaviour of the focus and enable the keyboard
                if (!searchView.isIconified()) {
                    searchView.requestFocus();
                    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
                    //dismissKeyboard();
                }
                if (!isTablet(getApplicationContext())) {
                    if (slidingMenu.isMenuShowing()) {
                        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                    } else {
                        dismissKeyboard();
                    }
                }
                check = false;
            } else {
                initializeDownloadUI();
            }
        }
        catch (Exception e) {
            Log.e("File Check", e.getMessage());
        }
    }
    /*
        * This function is to initialize all the icons labels and text inside the current view.
        * calling this function should set all elements in the view with localization.
        * */
    public void setUpView()
    {
            //Setting OnTouchListener for search result ListView
            final Fragment searchFragment = getSupportFragmentManager().findFragmentById(R.id.searchFragement);
            final SearchView searchBar = (SearchView) searchFragment.getView().findViewById(R.id.search_bar);
            final ListView searchListView = (ListView) findViewById(R.id.searchResultListView);
            searchListView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(searchListView.getWindowToken(), 0);
                    return false;
                }
            });
            searchListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    //getting the clicked alphabet
                    AlphabetListAdapter adapter = (AlphabetListAdapter) searchListView.getAdapter();
                    IndexRowItem item = (IndexRowItem) adapter.getItem(position);
                    String clickedWord = item.getHwd();

                    androidx.fragment.app.FragmentManager fragmentManager = searchFragment.getFragmentManager();
                    if (fragmentManager.getBackStackEntryCount() > 0) {
                        //Get the detail fragment to handle back stack
                        Fragment fragment = searchFragment.getFragmentManager().findFragmentById(R.id.detailPageFragment);
                        Boolean isDetailPage = (fragment instanceof DetailPageFragment) ? true : false;
                        if (isDetailPage) {
                            DetailPageFragment detailPageFragment = (DetailPageFragment) fragment;
                            String previousWord = detailPageFragment.headword;
                            //Compare clicked word with previous word and if it equals do nothing
                            if (previousWord.equals(clickedWord))
                                return;
                            else
                                //Navigate to detail page
                                if (navigateToDetailPage(item) && !isTablet(getApplicationContext())) {
                                    if (!searchBar.isIconified()) {
                                        //Keyboard dismissal and removed the search focus
                                        searchListView.setVisibility(View.INVISIBLE);
                                        searchBar.setQuery("", false);
                                        searchBar.setIconified(true);
                                        searchBar.setFocusable(false);
                                    }
                                }
                        }
                        //Navigate to detail page
                        else if (navigateToDetailPage(item) && !isTablet(getApplicationContext())) {
                            if (!searchBar.isIconified()) {
                                //Keyboard dismissal and removed the search focus
                                searchListView.setVisibility(View.INVISIBLE);
                                searchBar.setQuery("", false);
                                searchBar.setIconified(true);
                                searchBar.setFocusable(false);
                            }
                        }
                    }
                    //Navigate to detail page
                    else {
                        if (navigateToDetailPage(item) && !isTablet(getApplicationContext())) {
                            if (!searchBar.isIconified()) {
                                //Keyboard dismissal and removed the search focus
                                searchListView.setVisibility(View.INVISIBLE);
                                searchBar.setQuery("", false);
                                searchBar.setIconified(true);
                                searchBar.setFocusable(false);
                            }
                        }
                    }
                }
            });

            //
            Typeface longmanFont = Typeface.createFromAsset(getAssets(), "longman.ttf");
            //checking if it is Tablet
            if (isTablet(getApplicationContext()))

            {
                //Setting icon for settings, about, expand and collapse button
                Button settingButton = (Button) findViewById(R.id.settings_btn);
                settingButton.setTypeface(longmanFont);
                settingButton.setOnClickListener(this);

                Button aboutButton = (Button) findViewById(R.id.about_btn);
                aboutButton.setTypeface(longmanFont);
                aboutButton.setOnClickListener(this);

                Button exapandCollapseButton = (Button) findViewById(R.id.expand_collapse_btn);
                exapandCollapseButton.setTypeface(longmanFont);
                exapandCollapseButton.setOnClickListener(this);
            } else {
                Button slideMenuButton = (Button) findViewById(R.id.slideMenuButton);
                slideMenuButton.setTypeface(longmanFont);

            }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.about_btn:
                aboutButtonTapped();
                break;
            case R.id.settings_btn:
                settingsButtonTapped();
                break;
            case R.id.expand_collapse_btn:
                expandCollapsButtonTapped();
                break;
        }
    }
    /**
    This function is called when the about button (?) in tab is tapped
    It navigate to a new screen about page
    */
    public void aboutButtonTapped()
    {
        //checking if about already seen
        FragmentManager fragmentManager=getSupportFragmentManager();
        Fragment currentFragment=fragmentManager.findFragmentById(R.id.detailPageFragment);
        if(currentFragment instanceof AboutFragment){
            //removing about and popping back to previous screen
            fragmentManager.popBackStack("AboutStack", FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
        else {
            //Adding about page to stack and navigating to about
            FragmentTransaction fragmentTransaction=getSupportFragmentManager().beginTransaction();
            AboutFragment aboutFragment=new AboutFragment();
            //Removing all pages with tag "AboutStack" from the stack
            fragmentManager.popBackStack("AboutStack", FragmentManager.POP_BACK_STACK_INCLUSIVE);
            //Adding about page to stack with 'AboutStack' tag
            fragmentTransaction.replace(R.id.detailPageFragment,aboutFragment).addToBackStack("AboutStack");
            fragmentTransaction.commit();
        }


    }
    /*
    This function is called when the setting button (*) in tab is tapped
    It navigate to a new screen settings page
    */

    public void settingsButtonTapped()
    {
       //checking if settings page is already exist
       FragmentManager fragmentManager=getSupportFragmentManager();
       Fragment currentFragment=fragmentManager.findFragmentById(R.id.detailPageFragment);
       if(currentFragment instanceof SettingsFragment)
       {
           //Settings page already there,so removing the page and moving back to previous screen
           fragmentManager.popBackStack("SettingsStack", FragmentManager.POP_BACK_STACK_INCLUSIVE);
       }
        else {
           FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
           SettingsFragment settingsFragment = new SettingsFragment();
           //Removing the all pages with 'SettingStack' from stack
           fragmentManager.popBackStack("SettingsStack", FragmentManager.POP_BACK_STACK_INCLUSIVE);
           //adding settings page to backstack with 'settingstag'
           fragmentTransaction.replace(R.id.detailPageFragment, settingsFragment).addToBackStack("SettingsStack");
           fragmentTransaction.commit();
       }
    }

     /*
    This function is called when the expand button (<->) in tab is tapped
    this will expand the detail view into full screen and hide the index section
    */
    public void expandCollapsButtonTapped()
    {
        Animation a;
        //Getting the relative layout of detail page
        RelativeLayout relativeLayout = (RelativeLayout)findViewById(R.id.detailPageLayout);
        //Getting the parameters into the linear layout
        LinearLayout.LayoutParams layoutParams=(LinearLayout.LayoutParams) relativeLayout.getLayoutParams();
        //checking if the detail page is full screen or not
        if (layoutParams.weight>0) {
            //detail page is not full screen, so setting the weight as 0 to make it fullscreen (last parameter is weight)
            a = new ExpandAnimation(30,0f,relativeLayout);
            //relativeLayout.setLayoutParams(new TableLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT, 0));
        }
        else{
            //detail page is in full screen, so setting the weight as 30 to divide the screen into 30:70 (last parameter is weight)
            a = new ExpandAnimation(0, 30,relativeLayout);
            //relativeLayout.setLayoutParams(new TableLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT, 30));
        }
        a.setDuration(400);
        relativeLayout.startAnimation(a);

    }

//This is mainly used to Embed the Set items(search results) into the ListView
    @Override
    public void onSearchResult(Set result) {

        //SearchListView - to show search results
        ListView searchListView = (ListView)findViewById(R.id.searchResultListView);
        searchListView.setVisibility(View.VISIBLE);
        AlphabetListAdapter alphabetListAdapter=new AlphabetListAdapter();

        List<AlphabetListAdapter.Row> list= new ArrayList<>();
        //Transfer all the set Items to a List, and the List is Embedded in the ListView through ListAdapter
        for(Object item : result)
        {
            list.add((SearchRowItem)item);
        }
        alphabetListAdapter.setRows(list);
        searchListView.setAdapter(alphabetListAdapter);


    }

    @Override
    public void onSearchClose(Boolean isClosed) {
        ListView searchListView = (ListView)findViewById(R.id.searchResultListView);
        if (searchListView.getVisibility() == View.VISIBLE)
            searchListView.setVisibility(View.INVISIBLE);
    }

    // function used to redirects to Index Page, when Search button is clicked
    public void searchButtonClicked(View view) {
        // redirect to IndexPage
        Intent intent = new Intent();
        intent.setClass(getApplicationContext(), SlideMenuSearchAndIndex.class);
        startActivity(intent);
        setUpSearchBar();
    }

    @Override
    public void detailPageLoaded(Boolean isPageLoaded) {
        System.out.println("Loaded");
        //Check the device
        //Based on the device, trigger the even to respective pages(Tablet-History,Favourites,setting : Phone-Setting page)
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (Utils.isTablet(this))
        {
            //Tablet

            //Get the active real tab content
            Fragment fragment = fragmentManager.findFragmentById(R.id.realtabcontent);
            if (fragment instanceof IndexFragment)
            {

            }else if (fragment instanceof CoreVocabularyFragment)
            {

            }
            else if (fragment instanceof HistoryFragment)
            {
                //calling fragments method to refresh the History page
                ((HistoryFragment)fragment).refreshHistoryPage();
            }
        }else
        {
            //Phone
            //Get the active real tab content
            Fragment fragment = fragmentManager.findFragmentById(R.id.indexResultfragment);
            if (fragment instanceof HistoryFragment)
            {
                //calling fragments method to refresh the History page
                ((HistoryFragment)fragment).refreshHistoryPage();
            }
        }
    }

    @Override
    public void detailPageInterrupted(Boolean isInterrupted) {
        System.out.println("Interrupted");
    }

    @Override
    public void coreVocPageLoaded(Boolean isPageLoaded) {
        System.out.println(" Core Voc Page Loaded");
    }

    @Override
    public void coreVocPageInterrupted(Boolean isInterrupted) {
        System.out.println("Core Voc Page Interrupted");
    }

    @Override
    public void onFragmentInteraction(String selectedMenu) {
        //Getting the clicked item id from the slide menu
        //Checking if slide menu is open or close
        if (slidingMenu.isMenuShowing()) {
            //close side menu
            slidingMenu.toggle();
        } else {
            slidingMenu.toggle(true);
        }
        //TODO: Find if search fragment presents or not and if it presents means, it needs to close
        //Getting the fragment manager to show new fragment
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.indexResultfragment);
        //Should hide the Search result while switching the tabs
        ListView searchListView = (ListView)findViewById(R.id.searchResultListView);
        if (searchListView.getVisibility()==View.VISIBLE)
            searchListView.setVisibility(View.INVISIBLE);

        if (fragment != null) {

             FragmentManager fm = getSupportFragmentManager();
             //Getting the fragment transaction
             FragmentTransaction ft = fm.beginTransaction();
             //checking the clicked item id to see which page to be shown
             switch (selectedMenu) {
                 case "favorites":
                     //navigate to favorite page
                     ft.replace(R.id.indexResultfragment, new FavoritesFragment());
                     break;
                 case "history":
                     //navigate to history page
                     ft.replace(R.id.indexResultfragment, new HistoryFragment());
                     break;
                 case "core_vocabulary":
                     //navigate to core vocabulary page
                     ft.replace(R.id.indexResultfragment, new CoreVocabularyFragment());
                     break;
                 case "about_this_app":
                     //navigate to about section
                     ft.replace(R.id.indexResultfragment, new AboutFragment());
                     break;
                 case "settings":
                     //navigate to settings page
                     ft.replace(R.id.indexResultfragment, new SettingsFragment());
                     break;
                 case "academic_word_list":
                     //navigate to settings page
                     ft.replace(R.id.indexResultfragment, new AcademicWordList());
                     break;
                 case "search":
                     //navigate to index page
                      SharedPreferences searchButtonTappedPreferences =  this.getSharedPreferences("LDOCE6PrefsFile", Context.MODE_PRIVATE);
                      SharedPreferences.Editor searchEditor = searchButtonTappedPreferences.edit();
                     searchEditor.putBoolean("SearchMenuTapped",true);
                     searchEditor.apply();
                     searchButtonClicked(null);
                     SearchView searchView = (SearchView) this.findViewById(R.id.search_bar);
                     searchView.setFocusable(true);
                     searchView.setIconified(false);
                     enableKeyboard();
                     return;
             }

             //Check for previous fragment and add to stack
             if (!selectedMenu.equals(Utils.getMenuName(fragment.getClass().getName()))) {
                     ft.addToBackStack("menu");
                     //Commit the changes
                     ft.commit();
             }
         }
         else {
             slidingMenu.toggle();
         }

    }

    @Override
    public void refreshMenuItems() {
        //Check for device - if its tablet refresh the right hand side views
        FragmentManager fragmentManager = getSupportFragmentManager();
        if( Utils.isTablet(this))
        {
            Fragment currentFragment = fragmentManager.findFragmentById(R.id.realtabcontent);
            Log.w("LDOCE6", currentFragment.getClass().toString());
            if (currentFragment instanceof HistoryFragment)
            {
                ((HistoryFragment)currentFragment).refreshUIElements();
            }else if (currentFragment instanceof FavoritesFragment)
            {
                ((FavoritesFragment)currentFragment).refreshUIElements();
            }else if (currentFragment instanceof CoreVocabularyFragment)
            {
                ((CoreVocabularyFragment)currentFragment).refreshUIElements();
            }
            else if (currentFragment instanceof IndexFragment)
            {
                ((IndexFragment)currentFragment).refreshUIElements();
            }
        }else
       {
           //Handle case for phone
           //Refresh the side menu's slide content
           Fragment currentFragment = fragmentManager.findFragmentById(R.id.slidingmenu);
           ((SlidingMenuFragment)currentFragment).populateSlideMenuContent(((SlidingMenuFragment) currentFragment).generateContentForSlideMenu());
       }

        // To affect the font size in search bar when the font size is changed in settings page
        // Common for both tablet and phone
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.searchFragement);
        if (fragment instanceof SearchFragment) {
            ((SearchFragment)fragment).refreshUIElements();
        }

    }

    @Override
    public void onSavaSearchWordStateChanged(boolean state) {
      // if (state)
          // showWord();
    }


    private class ExpandAnimation extends Animation {

        private final float mStartWeight;
        private final float mDeltaWeight;
        private RelativeLayout mContent;
        public ExpandAnimation(float startWeight, float endWeight, RelativeLayout mContent) {
            mStartWeight = startWeight;
            mDeltaWeight = endWeight - startWeight;
            this.mContent = mContent;
        }

        @Override
        protected void applyTransformation(float interpolatedTime, Transformation t) {
            LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) mContent.getLayoutParams();
            lp.weight = (mStartWeight + (mDeltaWeight * interpolatedTime));
            mContent.setLayoutParams(lp);
        }

        @Override
        public boolean willChangeBounds() {
            return true;
        }
    }
    public boolean navigateToDetailPage(IndexRowItem item)
    {
        return Utils.navigateToDetailPage(this,item.getHwd(),item.getHwdId(),"","");
    }


    public SlidingMenu getSlidingMenu() {
        return slidingMenu;
    }

    public void setSlidingMenu(SlidingMenu slidingMenu) {
        this.slidingMenu = slidingMenu;
    }

    @Override
    public void detailPageWordFavorited(RowWithHomnumForBookmark wordInfo,boolean isFavorited) {

        FragmentManager fragmentManager = getSupportFragmentManager();
        if (Utils.isTablet(this)) {
            //Get the active real tab content
            Fragment fragment = fragmentManager.findFragmentById(R.id.realtabcontent);
            if (fragment instanceof FavoritesFragment) {
                //calling fragments method to refresh the Favorites page
                ((FavoritesFragment) fragment).refreshFavouritesEntryList(wordInfo,isFavorited);

            }
        }
    }

    @Override
    public void detailPageWordTapping(String tappedWord) {
        Toast.makeText(this,tappedWord,Toast.LENGTH_SHORT).show();
    }

    // Setting clipboard word to Searchbar
    public void showClipBoardWord(){

        //Get the recent word from the clipboard and assign to the search bar
        ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
        CharSequence recentClipwordText=clipboard.getText();
        String trimmedWord=null;
        if(recentClipwordText!=null && recentClipwordText.length() > 0){
            trimmedWord = removelastClipboardCharacter((String) recentClipwordText);
        }else{
            // there is no clipboard word - dismiss the keyboard
        }
        boolean isSaveSearchOn = checkisSaveSearchWordOn();

        // Clipboard word is not null and have to paste it in Searchbar
        if(trimmedWord!=null){
            if(isTablet(this)){
                ListView searchListView = (ListView)findViewById(R.id.searchResultListView);
                if(searchListView.getVisibility()==View.INVISIBLE)
                    searchListView.setVisibility(View.VISIBLE);
                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.detailPageFragment);
                if (fragment instanceof HomePageFragment || fragment instanceof SettingsFragment||fragment instanceof DetailPageFragment||fragment instanceof AboutFragment) {
                    SearchView search = (SearchView) findViewById(R.id.search_bar);
                    search.setQuery(recentClipwordText, false);
                    search.setIconified(false);
                    search.setFocusable(true);
                }
            }else{
                    SearchView search = (SearchView) findViewById(R.id.search_bar);
                    search.setQuery(trimmedWord, false);
                    search.setIconified(false);
                    search.setFocusable(true);
            }
        }else{
            showLastSearchedWord();
            // scenario may be the copied clipboard word is ' ', dismiss keyboard
        }
    }

    // function is used to remove last character space in the Clipboard word
    public String removelastClipboardCharacter(String clipboardWord){
        if(clipboardWord!=null && clipboardWord.length() > 0 && clipboardWord.charAt(clipboardWord.length()-1)==' '){
            clipboardWord = clipboardWord.substring (0, clipboardWord.length()-1);
            return clipboardWord;
        }
        return  clipboardWord;
    }

    // function used to set last searched word in the searchbar
    public void showLastSearchedWord(){
        boolean isSaveSearchWordOn = checkisSaveSearchWordOn();
        if(isSaveSearchWordOn){
            // get the latest history word from history table
            String latestHistoryWord = DbHelper.getLatestHistoryHwd();
            if(latestHistoryWord!=null){

                // get the searchView to paste the last searched word
                SearchView search = (SearchView) findViewById(R.id.search_bar);
                if(isTablet(this)){
                    ListView searchListView = (ListView)findViewById(R.id.searchResultListView);
                    if(searchListView.getVisibility()==View.INVISIBLE)
                        searchListView.setVisibility(View.VISIBLE);
                    Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.detailPageFragment);
                    if (fragment instanceof HomePageFragment || fragment instanceof SettingsFragment||fragment instanceof DetailPageFragment||fragment instanceof AboutFragment) {
                        search.setQuery(latestHistoryWord, false);
                        search.setIconified(false);
                        search.setFocusable(true);
                    }
                }else{
                    Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.indexResultfragment);
                    if (fragment instanceof IndexFragment || fragment instanceof DetailPageFragment) {
                        search.setQuery(latestHistoryWord, false);
                        search.setIconified(false);
                        search.setFocusable(true);
                    }
                }
            }
        }
    }

    // function used to dismiss the keyboard if it is not needed
    public void dismissKeyboard() {
        //If keyboard is in visible state means, First we need to dismiss the keyboard and show the slide menu
        InputMethodManager inputKeyboard = (InputMethodManager) getSystemService(
                Context.INPUT_METHOD_SERVICE);
        try{
            //Check whether keyboard service is visible
            if (inputKeyboard.isAcceptingText()) {
                //Hiding keyboard
                inputKeyboard.hideSoftInputFromWindow(slidingMenu.getWindowToken(), 0);
            }
        }catch (Exception e){
            System.out.print("Keyboard Dismiss issue");
        }

    }

    // function used to enable the keyboard if it is needed
    public void enableKeyboard(){
        // If keyboard is invisible state - make it visible
        InputMethodManager inputKeyboard = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if(!inputKeyboard.isAcceptingText()){
            // enable the keyboard
            inputKeyboard.showSoftInputFromInputMethod(slidingMenu.getWindowToken(), 0);
        }
    }

    // function used to check SaveSearch button is On or not
    public boolean checkisSaveSearchWordOn(){
        boolean isSaveSearchOn = false;
        //get the SharedPreference Value -  get the Value from Settings Page Whether button is Switched on or off
        SharedPreferences sharedPreferences = getSharedPreferences("LDOCE6PrefsFile",MODE_PRIVATE);
        //check whether SaveSearchWord Switch is on or Off
        isSaveSearchOn=sharedPreferences.getBoolean("SaveSearchWord", false);
        if(isSaveSearchOn){
            isSaveSearchOn = true;
            return isSaveSearchOn;
        }
        return isSaveSearchOn;
    }

    // function used to check Clipboard button is On or not
    public boolean checkisSearchOnClipboardOn(){
        boolean isSearchOnClipboardOn = false;
        // get the Shared preference value
        SharedPreferences sharedPreferences =  getSharedPreferences("LDOCE6PrefsFile", MODE_PRIVATE);
        isSearchOnClipboardOn=sharedPreferences.getBoolean("Search-on-clipboard", false);
        if(isSearchOnClipboardOn){
            isSearchOnClipboardOn = true;
            return isSearchOnClipboardOn;
        }
        return  isSearchOnClipboardOn;
    }

    // function mainly used to setup the searchbar based on Save search word and search on clipboard word
    public void setUpSearchBar(){
        boolean isSearchOnClipboardOn = checkisSearchOnClipboardOn();
        boolean isSaveSearchWordOn = checkisSaveSearchWordOn();
        if(isSearchOnClipboardOn){
            showClipBoardWord();
        }else if (isSaveSearchWordOn){
            showLastSearchedWord();
        }else{
//            SearchView searchView = (SearchView) this.findViewById(R.id.search_bar);
//            if(isTablet(this)){
//                searchView.setIconified(true);
//                searchView.setFocusable(false);
//                searchView.clearFocus();
//            }else{
//                searchView.setIconified(true);
//                searchView.setFocusable(false);
//                searchView.clearFocus();
//            }
            // if needed enable the keyboard
            //enableKeyboard();
        }
    }



    @Override
    public void onActionModeStarted(ActionMode mode) {
        super.onActionModeStarted(mode);
        //Assigning to the local variable
        //Used to call finish()
        mActionMode = mode;
        //Getting the menu and menu count
        /*Check if the menu has any item named "Web search", if its present we should consider
        it as it was invoked from webview.
        */
        //Replace the default menu with our custom context menu (only if the context menu is called via webview)
        Menu menu = mode.getMenu();
        int count = mode.getMenu().size();
        for (int i =0;i<count;i++)
        {
            MenuItem menuItem = menu.getItem(i);
            System.out.println(menuItem.getTitle());
            String str = String.valueOf(menuItem.getTitle());
            //Check for the presence of the web search menu item
            //If present clear the menu
            if (str.equalsIgnoreCase(getString(R.string.web_search))||str.equalsIgnoreCase(getString(R.string.web_search_old)))
            {
                menu.clear();
                mode.getMenuInflater().inflate(R.menu.menu_web_context, menu);
                break;
            }
        }
    }

    /**
     * Custom function for copy menu in context menu
     * @param item
     * context menu item
     */
    public void onCopyClick(MenuItem item) {
        executeTextSelection("copy");
        mActionMode.finish();
    }

    /**
     * Custom function for search menu in context menu
     * @param item
     * context menu item
     */
    public void onSearchClick(MenuItem item) {
        executeTextSelection("search");
        mActionMode.finish();
    }

    /***
     * call getSelectedText() in detail page fragment
     * @param action either search or copy
     */
    void executeTextSelection(String action)
    {
        Fragment currentDisplayingFragment;
        if (Utils.isTablet(this))
            currentDisplayingFragment = (Fragment)getSupportFragmentManager().findFragmentById(R.id.detailPageFragment);
        else
            currentDisplayingFragment = (Fragment)getSupportFragmentManager().findFragmentById(R.id.indexResultfragment);
        if (currentDisplayingFragment instanceof DetailPageFragment)
            ((DetailPageFragment)currentDisplayingFragment).performSearchOrCopy(action);
    }

    //Used to display the currently showing Tour Page and show the screen behind Page if Skip button is Pressed
    public void removeTourAndTutorial(){
        FragmentManager fragmentManager = getSupportFragmentManager();
        if(Utils.isTablet(this))
        {
            //If Skip button or Close Button is Pressed form About This App->Tutorial
            //we have to show the screen which is behind in Tour Page
            TourBaseFragment tourBaseFragment = (TourBaseFragment) fragmentManager.findFragmentById(R.id.tour_base_lay);
            if (tourBaseFragment != null) {
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.remove(tourBaseFragment).commit();
            }
        }
        else {
            TourBaseFragment tourBaseFragment = (TourBaseFragment) fragmentManager.findFragmentById(R.id.tour_base_layout);
            if (tourBaseFragment != null) {
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.remove(tourBaseFragment).commit();
            }
        }
    }

    private void importBookmarkAndShowPopup() {
        //showing importing bookmark popup
        final ProgressDialog pdialog;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            pdialog = new ProgressDialog(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            pdialog = new ProgressDialog(this);
        }
        pdialog.setCancelable(false);
        pdialog.setTitle(getString(R.string.favorites));
        pdialog.setMessage(getString(R.string.importing_bookmarks));
        pdialog.setIndeterminate(true);
        pdialog.setCanceledOnTouchOutside(false);
        pdialog.show();
        android.os.Handler handler=new android.os.Handler();
        //call function to add bookmarks
        Utils.getBookmarkFromXML(this);
        final Context that=this;
        //showing successful with ok button
        handler.postDelayed(new Runnable() {
            public void run() {
                AlertDialog.Builder alertDialogBuilder;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    alertDialogBuilder = new AlertDialog.Builder(that, android.R.style.Theme_Material_Light_Dialog_Alert);
                } else {
                    alertDialogBuilder = new AlertDialog.Builder(that);
                }
                // set title
                alertDialogBuilder.setTitle(getString(R.string.imported_success));

                // set dialog message
                alertDialogBuilder
                        .setMessage(getString(R.string.bookmark_favorite))
                        .setCancelable(false)
                        .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // if this button is clicked, close
                                // current activity
                                dialog.cancel();
                            }
                        })

                ;

                // create alert dialog
                Dialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                pdialog.dismiss();
            }
        }, 3000);

    }

    private void updateFavoriteTable() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "dd/MM/yyyy", Locale.getDefault());
        Date date = new Date();
        String currentDate=dateFormat.format(date);
        SQLiteDatabase db = DbConnection.getUserDbConnection();
        String updateQuery=String.format("ALTER TABLE favorites ADD COLUMN entryDate DATETIME default '"+currentDate+"'");
        db.execSQL(updateQuery);
        db.close();
        final String PREF_FILE_NAME_LDOCE = "LDOCE6PrefsFile";
        SharedPreferences preferencesLDOCE = getApplicationContext().getSharedPreferences(PREF_FILE_NAME_LDOCE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferencesLDOCE.edit();
        editor.putBoolean("FavTableUpdated", true);
        // Commit the edits!
        editor.apply();
    }
    AlertDialog.Builder adb;
    public void initializeDownloadUI() {
        setContentView(R.layout.obbdownloader_main);
        final String file_url = "http://eltapps.pearson.com/ldoce6app/ldoce6_obb/main.228.com.mobifusion.android.ldoce5.obb";
        adb = new AlertDialog.Builder(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            adb = new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            adb = new AlertDialog.Builder(this);
        }
        boolean isTablet = getResources().getBoolean(R.bool.isTablet);
        //checking whether the device is tablet or phone
        if (isTablet) {
            adb.setTitle(R.string.media_files);
            adb.setCancelable(false).setMessage("Media files missing, click OK to download them again");
            adb.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Boolean internet = isInternetConnection();
                    Log.d("internet connection", internet.toString());
                    if (internet) {
                        new DownloadFileFromURL().execute(file_url);
                        dialog.dismiss();
                    } else {
                        Toast.makeText(getApplicationContext(), "Please check for internet connection", Toast.LENGTH_LONG).show();
                        adb.show();
                    }
                }
            });
            adb.setNegativeButton("Download later", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Log.d("slide", "slide");
                    finish();
                }
            });
        } else {
            adb.setTitle(R.string.media_files);
            adb.setCancelable(false)
                    .setMessage("Media files missing, click OK to download them again");
            adb.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {

                    Boolean internet = isInternetConnection();
                    Log.d("internet connection", internet.toString());
                    if (internet) {
                        new DownloadFileFromURL().execute(file_url);
                        dialog.dismiss();
                    } else {
                        Toast.makeText(getApplicationContext(), "Please check for internet connection", Toast.LENGTH_LONG).show();
                        adb.show();
                    }
                }
            });
            adb.setNegativeButton("Download later", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    cancelButton = true;
                    Log.d("slide", "slide");
                    finish();
                }
            });
            if (cancelButton) {
                cancelButton = false;
                finish();
            } else {
//                adb.show();
            }
        }
    }
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case progress_bar_type:
                pDialog = new ProgressDialog(this);
                pDialog.setMessage("Downloading file. Please wait...");
                pDialog.setIndeterminate(false);
                pDialog.setMax(100);
                pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                pDialog.setCancelable(false);
                pDialog.setButton(DialogInterface.BUTTON_POSITIVE,"Go to App", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                       ProgressBar progressBar = new ProgressBar(SlideMenuSearchAndIndex.this, null, android.R.attr.progressBarStyleSmall);
                        progressBar.setVisibility(View.VISIBLE);
                        dialogInterface.dismiss();
//                        File mediaFile = new File(File.separator + "storage" +File.separator+"emulated"+File.separator+"0" + File.separator + "Android" + File.separator + "obb" + File.separator + ".media_files_228" );
//                        new SlideMenuSearchAndIndex.DirectoryCleaner(mediaFile).clean();
//                        mediaFile.delete();
                        progressBar.setVisibility(View.GONE);
                        Intent intent =new Intent(SlideMenuSearchAndIndex.this,SlideMenuSearchAndIndex.class);
                        startActivity(intent);
                    }
                });
//                pDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//
//                        File dir = new File(File.separator + "sdcard" + File.separator + "Android" + File.separator + "obb" + File.separator + "com.mobifusion.android.ldoce5" + File.separator + "main.228.com.mobifusion.android.ldoce5.obb");
//                        new DirectoryCleaner(dir).clean();
//                        dir.delete();
//                        dialog.dismiss();
//                        finish();
//                    }
//                });
                pDialog.show();
                return pDialog;
            default:
                return null;
        }
    }
    public class DirectoryCleaner {
        private final File mFile;

        public DirectoryCleaner(File file) {
            mFile = file;
        }

        public void clean() {
            if (null == mFile || !mFile.exists() || !mFile.isDirectory()) return;
            for (File file : mFile.listFiles()) {
                delete(file);
            }
        }

        private void delete(File file) {
            if (file.isDirectory()) {
                for (File child : file.listFiles()) {
                    delete(child);
                }
            }
            file.delete();

        }
    }
    public boolean isInternetConnection(){
        ConnectivityManager cn=(ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo nf=cn.getActiveNetworkInfo();
        boolean connection=false;
        if(nf != null && nf.isConnected()==true )
        {
            connection=true;
            Toast.makeText(this, "Network Available", Toast.LENGTH_LONG).show();}
        else
        {
            connection=false;
            Toast.makeText(this, "Network Not Available", Toast.LENGTH_LONG).show();
        }
        return connection;
    }

    class DownloadFileFromURL extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog(progress_bar_type);
        }

        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                URL url = new URL(f_url[0]);

                URLConnection conection = url.openConnection();
                conection.connect();
                int lenghtOfFile = conection.getContentLength();
                InputStream input = new BufferedInputStream(url.openStream(), 8192);
                String file=File.separator + "sdcard" + File.separator + "Android" + File.separator + "obb" + File.separator + "com.mobifusion.android.ldoce5" + File.separator;
                OutputStream output = null;
                File dir=new File(file);
                if (dir.exists() && dir.isDirectory()) {
                    check = true;
                    Log.d("directory","yes");
                    output = new FileOutputStream("/storage/emulated/0/Android/obb/com.mobifusion.android.ldoce5/main.228.com.mobifusion.android.ldoce5.obb");
                }
                else {
                    File directory = new File(file);
                    if(directory.mkdir()) {
                        System.out.println("Directory created");
                        output = new FileOutputStream("/storage/emulated/0/Android/obb/com.mobifusion.android.ldoce5/main.228.com.mobifusion.android.ldoce5.obb");
                    }
                    else
                    {
                        System.out.println("Directory not created");
                    }
                    Log.d("directory","no");
                }
//                OutputStream output = new FileOutputStream("/storage/emulated/0/main.228.com.mobifusion.android.ldoce5.obb");
                //OutputStream output = new FileOutputStream("/storage/emulated/0/Android/obb/com.mobifusion.android.ldoce5/main.228.com.mobifusion.android.ldoce5.obb");
                byte data[] = new byte[1024];
                long total = 0;
                while ((count = input.read(data)) != -1) {
                    total += count;
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));
                    output.write(data, 0, count);
                }
                output.flush();
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }
            return null;
        }
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            pDialog.setProgress(Integer.parseInt(progress[0]));
//            whenGetFolderSizeRecursive_thenCorrect();

        }
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded
            dismissDialog(progress_bar_type);
//            Intent intent = new Intent(SlideMenuSearchAndIndex.this, SlideMenuSearchAndIndex.class);
//
//            startActivity(intent);

        }
    }

//    public void whenGetFolderSizeRecursive_thenCorrect() {
//
//        long expectedSize = 498672710;
//
//        File file1 = new File(fileToChange);
//        File file2 = new File(rename);
//        boolean success = file1.renameTo(file2);
//        if(success) {
//            File folder = new File(File.separator + "sdcard" + File.separator + "Android" + File.separator + "obb" + File.separator + ".media_files_228");
//            long size = getFolderSize(folder);
//
//            if (expectedSize == size) {
//                unZipFileSize = true;
//            } else {
//                folder.delete();
//                initializeDownloadUI();
//            }
//        }
//    }
//    private long getFolderSize(File folder) {
//        long length = 0;
//        File[] files = folder.listFiles();
//
//        int count = files.length;
//
//        for (int i = 0; i < count; i++) {
//            if (files[i].isFile()) {
//                length += files[i].length();
//            }
//            else {
//                length += getFolderSize(files[i]);
//            }
//        }
//        return length;
//    }

    boolean checkForFile(){
        if (!check) {
            try {
                FileInputStream fin = new FileInputStream(filePath);
                ZipInputStream zin = new ZipInputStream(fin);
                if (!zin.toString().isEmpty()) {
                    check = true;
                }
                zin.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return check;
    }


}
