package com.mobifusion.android.ldoce5.model;

/**
 * Created by sureshchandrababu on 27/03/15.
 */
public class SearchRowItem extends IndexRowItem {
    private String hwd;
    private String hwdId;
    private String homnum;
    private boolean isFrequent;

    public SearchRowItem(String hwd,String hwdId, boolean isFrequent)
    {
        this.hwd=hwd;
        this.hwdId=hwdId;
        this.isFrequent=isFrequent;
    }

    public String getHwd() {
        return hwd;
    }

    public void setHwd(String hwd) {
        this.hwd = hwd;
    }

    public String getHwdId() {
        return hwdId;
    }

    public void setHwdId(String hwdId) {
        this.hwdId = hwdId;
    }

    public boolean getIsFrequent() {
        return isFrequent;
    }


    public void setIsFrequent(boolean isFrequent) {
        this.isFrequent = isFrequent;
    }
    public String getHomnum() { return homnum; }

    public void setHomnum(String homnum) {this.homnum = homnum;}


    //Used to avoid Duplicates - checks for the Word in Set, if it is there- then it will not inset in the Set
    @Override
    public boolean equals(Object o) {
        if(o instanceof SearchRowItem)
        {
            SearchRowItem row = (SearchRowItem)o;
            return row.hwd.equals(this.hwd);
        }
        return false;

    }
    //Objects Internally
    @Override
    public int hashCode() {
        return this.hwd.hashCode();
    }
}
