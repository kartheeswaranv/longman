package com.mobifusion.android.ldoce5.model;

/**
 * Created by Bazi on 09/03/15.
 *
 * This class is used to store an item in the sidebar.it store a string,
 * its starting position and ending position in the list
 * */
public class SideIndexBarItem {
    public String text;
    public int start;
    public int end;

    public SideIndexBarItem(String text, int start, int end) {
        this.text = text;
        this.start = start;
        this.end = end;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getEnd() {
        return end;
    }

    public void setEnd(int end) {
        this.end = end;
    }

}
