package com.mobifusion.android.ldoce5.Fragment;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;
import android.widget.TextView;

import com.mobifusion.android.ldoce5.R;


public class BottomBarsFragment extends Fragment {
    private FragmentTabHost mTabHost;
    private FragmentActivity myContext;
    private OnTabChangeListener mListener;
    Typeface longmanFont;


    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.activity_bottom_bars,container,false);
//        mTabHost=(FragmentTabHost)findViewById(android.R.id.tabhost);
////        mTabHost.setup(getFragmentManager(),R.id.realtabcontent);
//        mTabHost.setup(this,getSupportFragmentManager(), realtabcontent);
//        mTabHost.addTab(mTabHost.newTabSpec("tab1").setIndicator("Core Vocabulary"),
//                CoreVocabularyFragment.class, null);
//        mTabHost.addTab(mTabHost.newTabSpec("tab2").setIndicator("History"),
//                HistoryFragment.class, null);
//        mTabHost.addTab(mTabHost.newTabSpec("tab3").setIndicator("Favourites"),
//                FavoritesFragment.class, null);
//        mTabHost.addTab(mTabHost.newTabSpec("tab4").setIndicator("Index"),
//                IndexFragment.class, null);
        return v;
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mTabHost=(FragmentTabHost)getView().findViewById(R.id.tabhost);
        mTabHost.setup(getActivity().getApplicationContext(), getFragmentManager(), R.id.realtabcontent);
        Typeface longmanFont= Typeface.createFromAsset(getActivity().getAssets(), "longman.ttf");
        mTabHost.addTab(mTabHost.newTabSpec("tab1").setIndicator("\uf15d"),
                IndexFragment.class, null);
        mTabHost.addTab(mTabHost.newTabSpec("tab2").setIndicator("\uf212"),
                FavoritesFragment.class, null);
        mTabHost.addTab(mTabHost.newTabSpec("tab3").setIndicator("\uf081"),
                CoreVocabularyFragment.class, null);
        mTabHost.addTab(mTabHost.newTabSpec("tab4").setIndicator("\uf1da"),
                HistoryFragment.class, null);
        //mTabHost.getTabWidget().setDividerDrawable(R.color.white);
        for(int i=0;i<mTabHost.getTabWidget().getChildCount();i++)
        {
            final TextView tv = (TextView) mTabHost.getTabWidget().getChildAt(i).findViewById(android.R.id.title); //Unselected Tabs
            tv.setTextColor(Color.parseColor("#ffffff"));
            tv.setTextSize(20.0f);
            tv.setTypeface(longmanFont);
            /*mTabHost.getTabWidget().getChildAt(i).setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                   String tabid = String.valueOf(mTabHost.getCurrentTab());
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        mListener.onTabChanged(tabid);
                    }
                    return false;
                }
            });*/
        }
        mTabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {

                mListener.onTabChanged(tabId);
            }

        });
    }

    /**
     * Called when a fragment is first attached to its activity.
     * {@link #onCreate(android.os.Bundle)} will be called after this.
     *
     * @param activity
     */
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnTabChangeListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnTabChangeListener");
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public interface OnTabChangeListener
    {
        public void onTabChanged(String tabId);
    }


}
