package com.mobifusion.android.ldoce5.Fragment;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Message;
import android.os.Parcelable;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.util.Log;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.TextView;

//import com.google.android.gms.analytics.GoogleAnalytics;
//import com.google.android.gms.analytics.HitBuilders;
//import com.google.android.gms.analytics.Tracker;
import com.mobifusion.android.ldoce5.Activity.DbConnection;
import com.mobifusion.android.ldoce5.Activity.SlideMenuSearchAndIndex;
import com.mobifusion.android.ldoce5.R;
import com.mobifusion.android.ldoce5.Util.AlphabetListAdapter;
import com.mobifusion.android.ldoce5.Util.FirebaseWrapperScreens;
import com.mobifusion.android.ldoce5.Util.LDOCEApp;
import com.mobifusion.android.ldoce5.Util.Singleton;
import com.mobifusion.android.ldoce5.Util.Utils;
import com.mobifusion.android.ldoce5.model.IndexTextView;
import com.mobifusion.android.ldoce5.model.RowWithHomnum;
import com.mobifusion.android.ldoce5.model.SideIndexBarItem;

import java.util.ArrayList;
import java.util.List;

import static android.view.View.INVISIBLE;

public class CoreVocabularyFragment extends Fragment  implements AdapterView.OnItemSelectedListener , View.OnTouchListener{


    //Declaring the variables
    private GestureDetector mGestureDetector;
    private static float sideIndexX;
    private static float sideIndexY;
    public OnCoreVocPageLoadListener mListener ;
    public static final String PREFS_NAME = "LDOCE6PrefsFile";

    //ProgressBar pb_core_voc;
    //ListView listView;
    //AlphabetListAdapter adapter;
    //List<AlphabetListAdapter.Row> rows;
    int filterId;
    ArrayList<RowWithHomnum> filteredList;
    Spinner filterSpinner;
    //Save for list view
    Parcelable currentState ;
    //Core Vocabulary Threading
    android.os.Handler handler;
    Thread coreVocabularyThread;
    //Tracker for Google Analytics
//    Tracker t;

    //SlideBar gesture detection
    class SideIndexGestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        //Detecting the swipe on the slide bar
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {

            sideIndexX = sideIndexX - distanceX;
            sideIndexY = sideIndexY - distanceY;

            if (sideIndexX >= 0 && sideIndexY >= 0) {
                //Navigating to the clicked section
                LinearLayout sideIndex = getSideIndexBar();
                navigateToClickedSectionOnIndexBar(sideIndex);
                IndexTextView indexTV = getTextViewAtPos(getSideIndexItemPositionFromY(sideIndexY,sideIndex),sideIndex);
                if(indexTV!=null)
                    processFloatIndicator(e2,indexTV.getText().toString());
            }

            return super.onScroll(e1, e2, distanceX, distanceY);
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.activity_core_vocabulary,null);
        v.setOnTouchListener(this);
        return v;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (mGestureDetector.onTouchEvent(event)) {
            return true;
        } else {
            return false;
        }
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnCoreVocPageLoadListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
        // Get the Tracker and Initialise it
//        t = ((LDOCEApp) getActivity().getApplication()).getTracker(
//                LDOCEApp.TrackerName.APP_TRACKER);
//        t.setScreenName("High Frequency Page");
        FirebaseWrapperScreens.ScreenViewEvent("High Frequency Page");

//        t.send(new HitBuilders.AppViewBuilder().build());
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final ProgressBar pb_core_voc=(ProgressBar) getActivity().findViewById(R.id.pb_core_voc);
        pb_core_voc.getIndeterminateDrawable().setColorFilter(Color.rgb(103, 144, 177), PorterDuff.Mode.MULTIPLY);
        final ListView listView = (ListView)getActivity().findViewById(R.id.core_voc_result_list);
        final AlphabetListAdapter adapter = new AlphabetListAdapter();
        final List<AlphabetListAdapter.Row> rows = new ArrayList();
        filterSpinner = (Spinner)getActivity().findViewById(R.id.core_voc_filter_spinner);

        // Handler to invoke the CoreVocabulary Thread
        handler = new android.os.Handler(){
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                    for(RowWithHomnum row : (ArrayList<RowWithHomnum>) msg.obj)
                    {
                        rows.add(row);
                    }
                    adapter.setRows(rows);
                    listView.setAdapter(adapter);
                    generateContentForSideIndex((ArrayList<RowWithHomnum>) msg.obj);
                    pb_core_voc.setVisibility(View.INVISIBLE);
                    mListener.coreVocPageLoaded(true);
                }
        };

        // Main thread to show the progress bar
        coreVocabularyThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    pb_core_voc.setVisibility(View.VISIBLE);
                    //get the filtered items based on the filterId
                    filteredList = getRows(filterId);
                    Message msg = new Message();
                    msg.obj = filteredList;
                    handler.sendMessage(msg);
                }catch (Exception e){
                    System.out.println("Problem in Core Vocabulary Thread");
                }
            }
        });
        coreVocabularyThread.start();
    }

    @Override
    public void onStart() {
        super.onStart();
        // Sending Hit to GA as Start of new Activity
//        GoogleAnalytics.getInstance(getActivity()).reportActivityStart(getActivity());
        final ListView listView = (ListView)getActivity().findViewById(R.id.core_voc_result_list);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //getting the clicked alphabet
                AlphabetListAdapter adapter = (AlphabetListAdapter) listView.getAdapter();
                RowWithHomnum item = (RowWithHomnum) adapter.getItem(position);
                item.setIsFrequent(true);
                String clickedWord = item.getHwd();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                if (fragmentManager.getBackStackEntryCount() > 0) {
                    //Get the detail fragment to handle back stack
                    Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.detailPageFragment);
                    Boolean isDetailPage = (fragment instanceof DetailPageFragment) ? true : false;
                    if (isDetailPage) {
                        DetailPageFragment detailPageFragment = (DetailPageFragment) fragment;
                        String previous = detailPageFragment.headword;
                        //Compare clicked word with previous word and if it equals do nothing
                        if (previous.equals(clickedWord)) {
                            if (detailPageFragment.homNum != item.getHomnum()) {
                                detailPageFragment.homNum = item.getHomnum();
                                detailPageFragment.navigateToEntryWithHomnum();
                            } else
                                return;
                        } else
                            //Navigate to detail page
                            Utils.navigateToDetailPage(getActivity(), item.getHwd(), item.getHwdId(), item.getHomnum(), "");
                    } else
                        //Navigate to detail page
                        Utils.navigateToDetailPage(getActivity(), item.getHwd(), item.getHwdId(), item.getHomnum(), "");
                } else {
                    //Navigate to detail page
                    Utils.navigateToDetailPage(getActivity(), item.getHwd(), item.getHwdId(), item.getHomnum(), "");
                }
            }
        });
        // Method to call to style the title header
        styleTitleHeader();
        // Method to invoke the custom spinner
        styleSpinner();

        //Setting scrollListener for tableview
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                //Setting ListView State
                currentState = listView.onSaveInstanceState();

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }

        });
        if (currentState!=null )
        {
            //If last word not null, which means we are in second level index.
            //Navigate to second level index with last word

            listView.onRestoreInstanceState(currentState);

        }

    }


    /**
     * Called when the Fragment is no longer started.  This is generally
     * tied to {@link Activity Activity.onStop} of the containing
     * Activity's lifecycle.
     */
    @Override
    public void onStop() {
        super.onStop();
        // Sending Hit to GA as End of Activity
//        GoogleAnalytics.getInstance(getActivity()).reportActivityStop(getActivity());
        ListView listView = (ListView)getActivity().findViewById(R.id.core_voc_result_list);
        currentState = listView.onSaveInstanceState();
    }

    // Method to style the Spinner
    public void styleSpinner(){
        SharedPreferences fontSizePreferences =  getActivity().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        final int fontSizeValue = fontSizePreferences.getInt("FONT_VALUE",0);
        //Create the adapter for the Spinner.
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),R.layout.ldoce_spinner_white,R.id.spinner_textview,getCoreVocabFilterItems()){
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                ArrayList<String> filterList = getCoreVocabFilterItems();
                String key = filterList.get(position);
                TextView textView = (TextView)v.findViewById(R.id.spinner_textview);
                textView.setTypeface(Singleton.getMundoSansPro_Medium());
                if (fontSizeValue==0)
                    textView.setTextSize(18);
                if (fontSizeValue==2)
                    textView.setTextSize(18+1);
                if (fontSizeValue==4)
                    textView.setTextSize(18+2);
                if (fontSizeValue==8)
                    textView.setTextSize(18+4);
                String value = getString(getResources().getIdentifier(key, "string", getActivity().getPackageName()));
                textView.setText(value);
                return v;
            }

            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                LayoutInflater inflater=(LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View row=inflater.inflate(R.layout.ldoce_spinner_dropdown_item, parent, false);
                TextView textView=(TextView)row.findViewById(R.id.spinner_dropdown_textview);

                ArrayList<String> filterList = getCoreVocabFilterItems();
                String key = filterList.get(position);
                textView.setTypeface(Singleton.getMundoSansPro());
                textView.setTextSize(18+fontSizeValue);
                String value = getString(getResources().getIdentifier(key, "string", getActivity().getPackageName()));
                textView.setText(value);
                return row;
            }

        };

        filterSpinner.setAdapter(dataAdapter);
        filterSpinner.setSelection(filterId);
        filterSpinner.setOnItemSelectedListener(this);
        filterSpinner.setOnTouchListener(spinnerOnTouch);
        mGestureDetector = new GestureDetector(getActivity(), new SideIndexGestureListener());
    }


    private View.OnTouchListener spinnerOnTouch = new View.OnTouchListener() {
        public boolean onTouch(View v, MotionEvent event) {

            //Search View
//            final ListView searchListView = (ListView) getActivity().findViewById(R.id.searchResultListView);
            Fragment searchFragment= getFragmentManager().findFragmentById(R.id.searchFragement);
            SearchView searchView= (SearchView)searchFragment.getActivity().findViewById(R.id.search_bar);
            searchView.setFocusable(false);
//            SharedPreferences sharedPreferences = getActivity().getSharedPreferences("LDOCE6PrefsFile", Context.MODE_PRIVATE);
//            boolean isSaveSearchOn=sharedPreferences.getBoolean("SaveSearchWord", false);
//            boolean isSearchOnClipboardOn=sharedPreferences.getBoolean("Search-on-clipboard", false);
//            if (!searchView.isIconified() && !isSaveSearchOn && !isSearchOnClipboardOn) {
//            //Keyboard dismissal and removed the search focus
//            searchListView.setVisibility(INVISIBLE);
//            searchView.setQuery("", false);
//            searchView.setIconified(true);
//            searchView.setFocusable(false);
//        }
            return false;
        }
    };

    // Method to invoke the Custom Header View
    public void styleTitleHeader(){
        SharedPreferences fontSizePreferences =  getActivity().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        int fontSizeValue = fontSizePreferences.getInt("FONT_VALUE",0);
        TextView titleText = (TextView) getActivity().findViewById(R.id.core_voc_title);
        titleText.setTypeface(Singleton.getMundoSansPro());
        titleText.setText(getString(R.string.core_vocabulary));
        titleText.setTextSize(20+fontSizeValue);
        TextView floating_tv =(TextView)getActivity().findViewById(R.id.tv_floating_indicator);
        floating_tv.setTypeface(Singleton.getMundoSansPro());

    }

    /***
     * Updates the ListView with the filtered results. By default gets the whole list.
     * @param filterId ranges from 0 to 11
     */
    void updateList(final int filterId)
    {
        final AlphabetListAdapter adapter = new AlphabetListAdapter();
        final List<AlphabetListAdapter.Row> rows = new ArrayList();
        final ListView listView = (ListView)getActivity().findViewById(R.id.core_voc_result_list);

        // Handler to invoke the Update List Thread
        final android.os.Handler handler = new android.os.Handler(){
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                for(RowWithHomnum row : (ArrayList<RowWithHomnum>) msg.obj)
                {
                    rows.add(row);
                }
                adapter.setRows(rows);
                listView.setAdapter(adapter);
                if (currentState!=null )
                {
                    //If last word not null, which means we are in second level index.
                    //Navigate to second level index with last word

                    listView.onRestoreInstanceState(currentState);

                }
                generateContentForSideIndex((ArrayList<RowWithHomnum>) msg.obj);
            }
        };
        // Main thread
        Thread updateListThread = new Thread(new Runnable() {
            @Override
            public void run() {
                //get the filtered items based on the filterId
                ArrayList<RowWithHomnum> filteredList = getRows(filterId);
                Message msg = new Message();
                msg.obj = filteredList;
                handler.sendMessage(msg);
            }
        });
        updateListThread.start();
    }

    /***
     * To get the filtered results as a list, when provided with a filter id
     * @param filterId ranges from 0 to 11
     * @return Returns the list of filtered RowWithHomnum items.
     */
    public ArrayList<RowWithHomnum> getRows(int filterId){
        SQLiteDatabase db;
        String query;
        DbConnection dbPath = new DbConnection();
        db = dbPath.getDbConnection();
        ArrayList<RowWithHomnum> list=new ArrayList<>();

        if(db!=null)
        {
            //Run the query to get the results.
            query = getQueryForFilter(filterId);
            Cursor cur = db.rawQuery(query, null);
            while(cur.moveToNext())
            {
                //Move through the cursor and add to the the result list.
                list.add(new RowWithHomnum(cur.getString(1),cur.getString(0),cur.getString(2)));

            }
            cur.close();
        }
        db.close();

        return list;
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    /***
     * Creates the SELECT query for the frequency_acad table based on the filter type (i.e. filterId)
     * @param filterId ranges from 0 to 11
     * @return Returns the query string.
     */
    public String getQueryForFilter(int filterId)
    {
        String baseQuery = "select * from frequency_acad";
        switch (filterId) {
            case 0:
                baseQuery += " where level!=''";
                break;
            case 1:
                baseQuery += " where level like '3'";
                break;
            case 2:
                baseQuery += " where level like '2'";
                break;
            case 3:
                baseQuery += " where level like '1'";
                break;
            case 4:
                baseQuery += " where freq_s like 'S%'";
                break;
            case 5:
                baseQuery += " where freq_s like 'S1'";
                break;
            case 6:
                baseQuery += " where freq_s like 'S2'";
                break;
            case 7:
                baseQuery += " where freq_s like 'S3'";
                break;
            case 8:
                baseQuery += " where freq_w like 'W%'";
                break;
            case 9:
                baseQuery += " where freq_w like 'W1'";
                break;
            case 10:
                baseQuery += " where freq_w like 'W2'";
                break;
            case 11:
                baseQuery += " where freq_w like 'W3'";
                break;
        }
        return baseQuery;
    }

    /***
     * Returns the list of filter items with resource key strings, for the Spinner/Dropdown, so that they can be localized.
     * @return Returns list of string for the dropdown / spinner.
     */
    ArrayList<String> getCoreVocabFilterItems(){
        ArrayList<String> list = new ArrayList<>();
        list.add("all_nine_thousand_words");
        list.add("high_frequency");
        list.add("medium_frequency");
        list.add("lower_frequency");
        list.add("all_top_spoken_words");
        list.add("top_one_thousand_spoken_words");
        list.add("top_two_thousand_spoken_words");
        list.add("top_three_thousand_spoken_words");
        list.add("all_top_written_words");
        list.add("top_one_thousand_written_words");
        list.add("top_two_thousand_written_words");
        list.add("top_three_thousand_written_words");
        return list;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        //when selection is changed, update the ListView.
        filterId = position;
        updateList(filterId);
        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

    }

    public  void generateContentForSideIndex(ArrayList<RowWithHomnum> arrayList)
    {
        List<SideIndexBarItem> list=new ArrayList<>();
        String currentCharacter="";
        String previousCharacter="";
        int start=0;
        int end=0;
        previousCharacter=arrayList.get(0).getHwd().substring(0,1);
        for(int i=0;i<arrayList.size();i++)
        {
            RowWithHomnum rowWithHomnum=arrayList.get(i);
            //taking first character
            currentCharacter= rowWithHomnum.getHwd().substring(0,1);
            //checking if it is a new item which we dint add to array
            if(!currentCharacter.equalsIgnoreCase(previousCharacter))
            {
               //add to side index array
                end=i;
               list.add(new SideIndexBarItem(previousCharacter.toUpperCase(),start,end));
                start=end;
                previousCharacter=currentCharacter;
            }

            if(i==arrayList.size()-1)
            {
                end=i;
                list.add(new SideIndexBarItem(previousCharacter.toUpperCase(),start,end));
            }

        }
        //Drawing side indexbar on view
        populateSideIndexBar(list);
    }
    //This function generate the side IndexBar using TextView.
    public void populateSideIndexBar(List<SideIndexBarItem> indexBarContent) {

        //Initializing variables
        LinearLayout sideIndex = null;
        if (getActivity()!=null)
        sideIndex = (LinearLayout)getActivity().findViewById(R.id.coreSideIndex);
        //Removing the all views before created.
        try {
            sideIndex.removeAllViews();
            int indexListSize = indexBarContent.size();
            if (indexListSize < 1) {
                return;
            }
            //calculating the maximum height
            int indexMaxSize = (int) Math.floor(sideIndex.getHeight() / 20);
            int tmpIndexListSize = indexListSize;
            while (tmpIndexListSize > indexMaxSize) {
                tmpIndexListSize = tmpIndexListSize / 2;
            }
            double delta;
            if (tmpIndexListSize > 0) {
                delta = indexListSize / tmpIndexListSize;
            } else {
                delta = 1;
            }
            //IndexText view is inherited class from text view to store the starting position also
            IndexTextView tmpTV;
            for (double i = 1; i <= indexListSize; i = i + delta) {
                SideIndexBarItem tmpIndexItem = indexBarContent.get((int) i - 1);
                String tmpLetter = tmpIndexItem.getText().toString();
                //Styling the indexbar items
                tmpTV = new IndexTextView(getActivity());
                tmpTV.startPosition=(int)tmpIndexItem.getStart();
                tmpTV.setText(tmpLetter);
                tmpTV.setGravity(Gravity.CENTER);
                tmpTV.setTextSize(14);
                tmpTV.setTypeface(Singleton.getMundoSansPro_Bold());
                tmpTV.setTextColor(Color.parseColor("#ffffff"));
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1);
                tmpTV.setLayoutParams(params);
                sideIndex.addView(tmpTV);
            }

            Integer sideIndexHeight = sideIndex.getHeight();
            //Onclick event for slide menu
            sideIndex.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    // now you know coordinates of touch
                    sideIndexX = event.getX();
                    sideIndexY = event.getY();

                    // and can display a proper item
                    LinearLayout sideIndex = getSideIndexBar();
                    navigateToClickedSectionOnIndexBar(sideIndex);
                    IndexTextView indexTV = getTextViewAtPos(getSideIndexItemPositionFromY(sideIndexY,sideIndex),sideIndex);
                    if(indexTV!=null)
                        processFloatIndicator(event,indexTV.getText().toString());
                    return true;
                }
            });
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }



    //This function navigate the list view into the clicked position.it find the clicked item based on the clicked position
    //move to the starting position of that element in the list
    public void navigateToClickedSectionOnIndexBar(LinearLayout sideIndex) {

        try {
            // getting the size of index bar (number of elements will be equals to number of subviews :P)
            int indexContentSize = sideIndex.getChildCount();

            // compute the item index for given event position belongs to
            int itemPosition = getSideIndexItemPositionFromY(sideIndexY, sideIndex);

            // get the item (we can do it since we know item index)
            if (itemPosition < indexContentSize && itemPosition >=0) {
                IndexTextView tmp = getTextViewAtPos(itemPosition, sideIndex);
                //scrolling to the position
                ListView listView = (ListView)getView().findViewById(R.id.core_voc_result_list);
                listView.setSelection(tmp.startPosition);
            }
        }
        catch (NullPointerException ex){
            Log.e("Error in SideIndex", ex.getMessage().toString());
        }
    }
    LinearLayout getSideIndexBar(){
        LinearLayout sideIndex = (LinearLayout) getView().findViewById(R.id.coreSideIndex);
        return sideIndex;
    }
    int getSideIndexItemPositionFromY(float yPosition, LinearLayout sideIndex){
        // getting the height of index bar
        Integer sideIndexHeight = sideIndex.getHeight();
        // getting the size of index bar (number of elements will be equals to number of subviews :P)
        int indexContentSize=sideIndex.getChildCount();
        // compute number of pixels for every side index item
        double pixelPerIndexItem = (double) sideIndexHeight / indexContentSize;

        // compute the item index for given event position belongs to
        int itemPosition = (int) (yPosition / pixelPerIndexItem);
        return itemPosition;
    }

    IndexTextView getTextViewAtPos(int itemPos,LinearLayout sideIndex){
        IndexTextView tv = (IndexTextView)sideIndex.getChildAt(itemPos);
        return tv;
    }

    void processFloatIndicator(final MotionEvent event,String textToSet){
        final FrameLayout floating_indicator = (FrameLayout)getActivity().findViewById(R.id.fl_floating_indicator);
        if(event.getAction() == MotionEvent.ACTION_DOWN || event.getAction() == MotionEvent.ACTION_MOVE) {
            floating_indicator.setVisibility(View.VISIBLE);
            TextView tv = (TextView)floating_indicator.findViewById(R.id.tv_floating_indicator);
            tv.setText(textToSet);
            floating_indicator.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (event.getAction() != MotionEvent.ACTION_DOWN)
                        floating_indicator.setVisibility(View.INVISIBLE);
                }
            }, 1000);
        }
        else
            floating_indicator.setVisibility(INVISIBLE);
    }

    public interface OnCoreVocPageLoadListener
    {
        /**
         * callback function to notify the listener once the core vocabulary page is loaded
         * @param :isPageLoaded
         */

        public void coreVocPageLoaded(Boolean isPageLoaded);

        /**
         * callback function to notify the listener if core voc page is interrupted
         * @param : isInterrupted
         */

        public void coreVocPageInterrupted(Boolean isInterrupted);

    }

    @Override
    public void onPause() {
        super.onPause();
        if(coreVocabularyThread.isAlive()){
            handler.removeCallbacksAndMessages(coreVocabularyThread);
        }
    }
    /**
     * refresh the UI element from settings page
     * Used in language change, Font size increase
     */
    public void refreshUIElements()
    {
        styleTitleHeader();
        styleSpinner();
//        TextView noHistoryTextView = (TextView)getActivity().findViewById(R.id.tv_empty_view);
//        noHistoryTextView.setText(getString(R.string.no_favorites));
//        TextView historyTitle = (TextView)getActivity().findViewById(R.id.favorites_title);
//        historyTitle.setText(getString(R.string.favorites));

    }

    @Override
    public void onResume() {
        SharedPreferences sharedPreferences =  getActivity().getSharedPreferences("LDOCE6PrefsFile", Context.MODE_PRIVATE);
        boolean isSearchOnClipboardOn=sharedPreferences.getBoolean("Search-on-clipboard", false);
        boolean isSaveSearchWordOn=sharedPreferences.getBoolean("SaveSearchWord",false);
        if(Utils.isTablet(getActivity())){

        if(isSaveSearchWordOn && !isSearchOnClipboardOn){
            ((SlideMenuSearchAndIndex)getActivity()).showLastSearchedWord();
        }
        }
//        else {
//            if(!isSaveSearchWordOn && !isSearchOnClipboardOn) {
//                ((SlideMenuSearchAndIndex) getActivity()).showWord();
//            }
//        }
        super.onResume();

    }
}
