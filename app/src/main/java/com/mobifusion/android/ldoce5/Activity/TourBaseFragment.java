package com.mobifusion.android.ldoce5.Activity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
//import androidx.viewpager.widget.ViewPager;
//import androidx.core.view.ViewPager;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

//import com.google.android.gms.analytics.GoogleAnalytics;
//import com.google.android.gms.analytics.HitBuilders;
//import com.google.android.gms.analytics.Tracker;
import com.mobifusion.android.ldoce5.R;
import com.mobifusion.android.ldoce5.Util.FirebaseWrapperScreens;
import com.mobifusion.android.ldoce5.Util.LDOCEApp;
import com.mobifusion.android.ldoce5.Util.PagerAdapter;
import com.mobifusion.android.ldoce5.Util.Singleton;

import java.util.ArrayList;
import java.util.List;
import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;


public class TourBaseFragment extends Fragment implements ViewPager.OnPageChangeListener,View.OnClickListener {
    ViewPager pager;
    List<Button> list;
    public Activity parent;
    //flag is used to identify Start Tour or Next is to be displayed in Next Button
    int flag = 1;
//    Tracker t;
    public static final String PREFS_NAME = "LDOCE6PrefsFile";
    private static final int REQUEST_CODE_ASK_PERMISSIONS = 123;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_tour_base_fragment, null);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (Build.VERSION.SDK_INT >= 23) {
            // Marshmallow+
            checkPermission();
        } else {
            // Pre-Marshmallow
        }

        //Adding Buttons below the tour content into a list
        list = new ArrayList<>();
        list.add((Button) getActivity().findViewById(R.id.Button1));
        list.add((Button) getActivity().findViewById(R.id.Button2));
        list.add((Button) getActivity().findViewById(R.id.Button3));
        list.add((Button) getActivity().findViewById(R.id.Button4));
        list.add((Button) getActivity().findViewById(R.id.Button5));
        list.add((Button) getActivity().findViewById(R.id.Button6));

        //Initial button in default selected state
        Button button0 = list.get(0);
        button0.setBackgroundResource(R.drawable.shape_selected);

        //Setting the onClick listner for all buttons
        for (Button btn : list) {
            btn.setTag("Pager Button");
            btn.setOnClickListener(this);
        }
        //Initializing the pager
        pager = (ViewPager) getActivity().findViewById(R.id.tour_base);
        final PagerAdapter page = new PagerAdapter(getActivity().getSupportFragmentManager());
        pager.setAdapter(page);
        pager.setOnPageChangeListener(this);

        final Button skipButton = (Button) getActivity().findViewById(R.id.skipButton);
        final Button nextButton = (Button) getActivity().findViewById(R.id.nextButton);
        skipButton.setTypeface(Singleton.getMundoSansPro());
        nextButton.setTypeface(Singleton.getMundoSansPro());
         /*If Skip Button is Clicked
            1. get the Current View Pager Position If it is == 5 then move the View Pager to start Page
            2. If View Pager is showing Last Page  and Button is Clicked then remove the Tour Fragment and shoe About Page
        */
        skipButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int count = pager.getCurrentItem();
//                t = ((LDOCEApp) getActivity().getApplicationContext()).getTracker(
//                        LDOCEApp.TrackerName.APP_TRACKER);
                if (count == 5) {
                    //restart the View Pager to First Page
                    pager.setCurrentItem(0);
//                    t.setScreenName("Tour and Tutorial Page");
                    //removing for now as per QA suggestion
//                    FirebaseWrapperScreens.ScreenViewEvent("Tour and Tutorial Page");
                    Bundle params=new Bundle();

                    params.putString("eventCategory","tour_page_events");
                    params.putString("eventAction","page_actions");
                    params.putString("eventLabel","restartButtonTapped");
                    FirebaseWrapperScreens.EventLogs(params);

//                    t.send(new HitBuilders.EventBuilder().setCategory("tour_page_events").setAction("page_actions").setLabel("restartButtonTapped").build());
                } else {
                    //remove the Tout and Tutorial Fragment and show the About Page Fragment
                    removeTourAndTutorial();
                    //removing for now as per QA suggestion
//                    FirebaseWrapperScreens.ScreenViewEvent("Tour and Tutorial Page");
//                    t.setScreenName("Tour and Tutorial Page");
                    Bundle params=new Bundle();
                    params.putString("eventCategory","tour_page_events");
                    params.putString("eventAction","page_actions");
                    params.putString("eventLabel","skipButtonTapped");
                    FirebaseWrapperScreens.EventLogs(params);

//
//                    t.send(new HitBuilders.EventBuilder().setCategory("tour_page_events").setAction("page_actions").setLabel("closeButtonTapped").build());
                }
            }
        });
       /*If Next Button is Clicked
            1. get the Current View Pager Position If it is < 5 then move the View Pager to Next Page
            2. If View Pager is showing Last Page  and Button is Clicked then remove the Tour Fragment and shoe About Page
        */
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int count = pager.getCurrentItem();
//                t = ((LDOCEApp) getActivity().getApplicationContext()).getTracker(
//                        LDOCEApp.TrackerName.APP_TRACKER);
                //If Next Button or StatrtTour is clicked move to next Tour Page
                if ((nextButton.getText() == getString(R.string.next)) || (nextButton.getText() == getString(R.string.start_tour))) {
                    //Move the View Pager to Next Page of Display
                    pager.setCurrentItem(count + 1, true);
//                    t.setScreenName("Tour and Tutorial Page");

                    //removing for now as per QA suggestion
//                    FirebaseWrapperScreens.ScreenViewEvent("Tour and Tutorial Page");

                    Bundle params=new Bundle();
                    params.putString("eventCategory","tour_page_events");
                    params.putString("eventAction","page_actions");
                    params.putString("eventLabel","nextPageSwiped");
                    FirebaseWrapperScreens.EventLogs(params);


//                    t.send(new HitBuilders.EventBuilder().setCategory("tour_page_events").setAction("page_actions").setLabel("nextPageSwiped").build());
                } else {
                    //remove the Tout and Tutorial Fragment and show the About Page Fragment
                    removeTourAndTutorial();
//                    t.setScreenName("Tour and Tutorial Page");
                    //removing for now as per QA suggestion
//                    FirebaseWrapperScreens.ScreenViewEvent("Tour and Tutorial Page");

                    Bundle params=new Bundle();
                    params.putString("eventCategory","tour_page_events");
                    params.putString("eventAction","page_actions");
                    params.putString("eventLabel","closeButtonTapped");
                    FirebaseWrapperScreens.EventLogs(params);
//                    t.send(new HitBuilders.EventBuilder().setCategory("tour_page_events").setAction("page_actions").setLabel("closeButtonTapped").build());
                }
            }
        });
    }

    @Override
    public void onPageSelected(int position) {
        //changing the state of buttons based scrolling pages
        for (int i = 0; i < list.size(); i++) {
            Button button = list.get(i);
            if (i == position)
                button.setBackgroundResource(R.drawable.shape_selected);
            else
                button.setBackgroundResource(R.drawable.shape);
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        SharedPreferences fontSizePreferences = getActivity().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        int fontSizeValue = fontSizePreferences.getInt("FONT_VALUE", 0);

        //get the SkipButton from Layout
        Button skipButton = (Button) getActivity().findViewById(R.id.skipButton);
        //get the Next button from Layout
        Button nextButton = (Button) getActivity().findViewById(R.id.nextButton);


        /*
           If View Pager's Position is 5(last) -  then change the Name of Buttons
        */
        if (position == 5) {
            // set the Name nextButton to Close
            nextButton.setText(R.string.close);
            nextButton.setTextSize(14 + fontSizeValue);
            //set the Name skipButton to Restart
            skipButton.setText(R.string.restart);
            nextButton.setTextSize(14 + fontSizeValue);
        }
        //First time When Tour Page is Opened - Next Button should be named as "Start Tour"
        //If the Position is 0 and First Time Tour Page is viewing
        else if (position == 0 && flag == 1) {
            //Show the button name as Start TOur
            nextButton.setText(R.string.start_tour);
            nextButton.setTextSize(14 + fontSizeValue);
            //Show the skip button
            skipButton.setText(R.string.skip);
            skipButton.setTextSize(14 + fontSizeValue);
            //set the Flag as 0 to display Button name Next
            flag = 0;
        } else {
            //IF Tour is Viewing for second time and position is 0
            //Show the Button Name as Next
            if (position == 0 && flag == 0) {
                nextButton.setText(R.string.next);
                nextButton.setTextSize(14 + fontSizeValue);
            }
            //Without considering Flag if Position is between 1 and 4 Button name is "Next"
            else if (position > 0 && position <= 4) {
                nextButton.setText(R.string.next);
                nextButton.setTextSize(14 + fontSizeValue);
            }
            //retain the same name of Button as it is
            skipButton.setText(R.string.skip);
            skipButton.setTextSize(14 + fontSizeValue);
        }
    }

    @Override
    public void onClick(View v) {
        int count = pager.getCurrentItem();
        //Function to scroll to corresponding pages on clicking the pager buttons
        Button clickedButton = (Button) v;
        if (clickedButton.getTag().equals("Pager Button")) {
            int i = list.indexOf(clickedButton);
            pager.setCurrentItem(i, true);
        }
    }

    public void removeTourAndTutorial() {
        //Remove tour and tutorial in welcome activity
        if (parent instanceof WelcomeActivity) {
            WelcomeActivity welcomeActivityParent = (WelcomeActivity) parent;
            welcomeActivityParent.removeTourAndTutorial();
        }
        //Remove tour and tutorial in slidemenusearchandindex activity
        else if (parent instanceof SlideMenuSearchAndIndex) {
            SlideMenuSearchAndIndex slideMenuSearchAndIndexParent = (SlideMenuSearchAndIndex) parent;
            slideMenuSearchAndIndexParent.removeTourAndTutorial();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        // Sending Hit to GA as new Activity
//        GoogleAnalytics.getInstance(getActivity()).reportActivityStart(getActivity());
//        // Sending Sub Pages Hit
//        t = ((LDOCEApp) getActivity().getApplication()).getTracker(
//                LDOCEApp.TrackerName.APP_TRACKER);
//        t.setScreenName("Tour and Tutorial Page");
        FirebaseWrapperScreens.ScreenViewEvent("Tour and Tutorial Page");
//        t.send(new HitBuilders.AppViewBuilder().build());
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void checkPermission() {
        int hasWriteContactsPermission = getActivity().checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION);
        if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
            if (!shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) {

                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        REQUEST_CODE_ASK_PERMISSIONS);
            }
            return;
        }
        requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                REQUEST_CODE_ASK_PERMISSIONS);
    }


    @Override
    public void onStop() {
        super.onStop();
        // Sending Hit to GA as End of Activity
//        GoogleAnalytics.getInstance(getActivity()).reportActivityStop(getActivity());
    }
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
//                    Toast.makeText(getActivity(), R.string.you_have_shared_your_location, Toast.LENGTH_SHORT)
//                            .show();
                } else {
                    // Permission Denied
//                    Toast.makeText(getActivity(), R.string.you_have_denied_access_to_your_location, Toast.LENGTH_SHORT)
//                            .show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

}



