package com.mobifusion.android.ldoce5.Fragment;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import androidx.fragment.app.Fragment;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

//import com.google.android.gms.analytics.GoogleAnalytics;
//import com.google.android.gms.analytics.HitBuilders;
//import com.google.android.gms.analytics.Tracker;
import com.mobifusion.android.ldoce5.R;
import com.mobifusion.android.ldoce5.Util.AlphabetListAdapter;
import com.mobifusion.android.ldoce5.Util.FirebaseWrapperScreens;
import com.mobifusion.android.ldoce5.Util.LDOCEApp;
import com.mobifusion.android.ldoce5.Util.Singleton;
import com.mobifusion.android.ldoce5.Util.Utils;
import com.mobifusion.android.ldoce5.model.RowWithFrequency;

import java.util.List;

    /**
    *  Created by Bazi on 30/06/15
    */

public class AboutSubFragment extends Fragment implements ListView.OnItemClickListener {

    public static final String PREFS_NAME = "LDOCE6PrefsFile";
//    Tracker t;
    List<AlphabetListAdapter.Row> items;
    int title;
    String pageName;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.activity_about_sub_fragment,null);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        styleElements();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onStart() {
        super.onStart();
        // Sending Hit to GA as new Activity
//        GoogleAnalytics.getInstance(getActivity()).reportActivityStart(getActivity());
//        // Sending Sub Pages Hit
//        t = ((LDOCEApp) getActivity().getApplication()).getTracker(
//                LDOCEApp.TrackerName.APP_TRACKER);
//        t.setScreenName(pageName);
//        FirebaseWrapperScreens.ScreenViewEvent(pageName);

//        t.send(new HitBuilders.AppViewBuilder().build());
    }

    @Override
    public void onStop() {
        super.onStop();
        // Sending Hit to GA as End of Activity
//        GoogleAnalytics.getInstance(getActivity()).reportActivityStop(getActivity());
    }

    public void styleElements() {
        // Set the Detail page title font title to MundoSansPro
        SharedPreferences fontSizePreferences =  getActivity().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        int fontSizeValue = fontSizePreferences.getInt("FONT_VALUE", 0);
        TextView aboutPageTitle = (TextView) getActivity().findViewById(R.id.about_this_subapp_title);
        aboutPageTitle.setTypeface(Singleton.getMundoSansPro());
        aboutPageTitle.setText(getString(title));
        aboutPageTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP,20+fontSizeValue);

        ListView listView=(ListView)getActivity().findViewById(R.id.about_sub_list);
        //load the items into list which is passed by previous page
        AlphabetListAdapter adapter=new AlphabetListAdapter();
        List<AlphabetListAdapter.Row> list=items;
        adapter.setRows(list);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        navigateToNextPage((RowWithFrequency)items.get(position));
    }

    /**
     * to load the html page of specific item selected, the resource id is used to identify the page
     * @param item the resource id ans page name is in the item
     */
    public void navigateToNextPage(RowWithFrequency item){
        int title=Integer.parseInt(item.getHwdId());
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        HtmlPage htmlPage = new HtmlPage();
        htmlPage.title=title;
        htmlPage.offline=true;
        htmlPage.pageName=getPageName(title);
        if(Utils.isTablet(getActivity().getApplicationContext()))
            fragmentTransaction.replace(R.id.detailPageFragment, htmlPage).addToBackStack("HtmlPage");
        else
            fragmentTransaction.replace(R.id.indexResultfragment, htmlPage).addToBackStack("HtmlPage");
        fragmentTransaction.commit();
    }

    String getPageName(int title){
        String name="";
        switch (title){
            case R.string.foreword:
                name="Foreword Page";
                break;
            case R.string.introduction:
                name="Introduction Page";
                break;
            case R.string.international_phonetic_alphabet:
                name="Pronunciation Page";
                break;
            case R.string.special_signs:
                name="Special Signs Page";
                break;
            case R.string.short_forms:
                name="Short Forms Page";
                break;
            case R.string.labels:
                name="Labels Page";
                break;
            case R.string.grammar_codes:
                name="Grammar Codes Page";
                break;
            case R.string.patterns:
                name="Patterns Page";
                break;
        }
        return name;
    }
}
