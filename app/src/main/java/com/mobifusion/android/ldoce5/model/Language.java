package com.mobifusion.android.ldoce5.model;

/**
 * Created by MuraliKDharan on 17/03/15.
 */
public class Language {

    private String languageName;
    private String languageCode;
    public Language(String name, String code)
    {
        languageName = name;
        languageCode = code;
    }

    public String getLanguageName()
    {
        return languageName;
    }
    public String getLanguageCode()
    {
        return languageCode;
    }

    @Override
    public String toString()
    {
        return languageName;
    }

}
