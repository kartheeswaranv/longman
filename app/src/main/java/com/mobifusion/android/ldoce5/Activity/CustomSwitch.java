package com.mobifusion.android.ldoce5.Activity;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Switch;

import java.lang.reflect.Field;

/**
 * Created by Manikandan on 06/07/15.
 */
public class CustomSwitch extends Switch {

    public CustomSwitch(Context context) {
        super(context);
    }

    public CustomSwitch(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomSwitch(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    @Override
    public void requestLayout() {
        try {
            Field mOnLayout = Switch.class.getDeclaredField("mOnLayout");
            mOnLayout.setAccessible(true);
            mOnLayout.set(this, null);
            Field mOffLayout = Switch.class.getDeclaredField("mOffLayout");
            mOffLayout.setAccessible(true);
            mOffLayout.set(this, null);
        } catch (Exception x) {
           // Log.e(x.getMessage(),);
        }
        super.requestLayout();
    }
}
