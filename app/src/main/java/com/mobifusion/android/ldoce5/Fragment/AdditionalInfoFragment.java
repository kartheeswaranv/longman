package com.mobifusion.android.ldoce5.Fragment;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;

//import com.google.android.gms.analytics.GoogleAnalytics;
//import com.google.android.gms.analytics.HitBuilders;
//import com.google.android.gms.analytics.Tracker;
import com.mobifusion.android.ldoce5.Activity.DbConnection;
import com.mobifusion.android.ldoce5.R;
import com.mobifusion.android.ldoce5.Util.FirebaseWrapperScreens;
import com.mobifusion.android.ldoce5.Util.LDOCEApp;
import com.mobifusion.android.ldoce5.Util.Singleton;
import com.mobifusion.android.ldoce5.Util.Utils;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;


public class AdditionalInfoFragment extends Fragment {

    public static final String PREFS_NAME = "LDOCE6PrefsFile";
    //The page to be loaded
    int itemId;
    String hwdID;
    String webViewContent;
    DetailPageFragment parent;
//    Tracker t;
    int fontSizeValue;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_additional_info,null);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        styleElements();
        generateContentForAdditionalInfo();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Get the Tracker and Initialise it
//        t = ((LDOCEApp) getActivity().getApplication()).getTracker(
//                LDOCEApp.TrackerName.APP_TRACKER);
//        t.setScreenName(getPageTitle());
        FirebaseWrapperScreens.ScreenViewEvent(getPageTitle());

//        t.send(new HitBuilders.AppViewBuilder().build());
    }

    @Override
    public void onStart() {
        super.onStart();
        // Sending Hit to GA as Start of new Activity
//        GoogleAnalytics.getInstance(getActivity()).reportActivityStart(getActivity());
    }

    @Override
    public void onStop() {
        super.onStop();
        // Sending Hit to GA as End of Activity
//        GoogleAnalytics.getInstance(getActivity()).reportActivityStop(getActivity());
    }
    //design header for phone and tablet
    public void styleElements(){
        final SharedPreferences fontSizePreferences =  getActivity().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        fontSizeValue = fontSizePreferences.getInt("FONT_VALUE", 0);
        TextView caption=(TextView)getActivity().findViewById(R.id.caption);
        caption.setText(getTitle());
        caption.setTextSize(20 + fontSizeValue);
        caption.setTypeface(Singleton.getMundoSansPro());
        if(Utils.isTablet(getActivity().getApplicationContext())){
            caption.setTextColor(Color.rgb(244,219,166));
            Button closeButton=(Button)getActivity().findViewById(R.id.closeButton);
            closeButton.setTypeface(Singleton.getLongman());
            closeButton.setVisibility(View.VISIBLE);
            closeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Close the additional info pop-up on clicking close button
                    parent.removeDummyCellAndCloseAdditionalInfo();
                }
            });
        }
    }
    //generate content on additional info based on menu selection
    public void generateContentForAdditionalInfo (){

        switch (itemId){
            case 1001:
                generateContentForWordOrigin();
                break;
            case 1002 :
                generateContentForVerbTable();
                break;
            case 1003:
                generateContentForCollocation();
                break;
            case 1004:
                generateContentForThesaurus();
                break;
        }
    }
    //get title based on menu item selected
    public String getTitle(){
        switch (itemId){
            case 1001:
                return getString(R.string.word_origin);
            case 1002 :
                return getString(R.string.verb_table);
            case 1003:
                return getString(R.string.collocations);
            case 1004:
                return getString(R.string.thesaurus);
        }
        return "";
    }

    //get page name for tracking
    public String getPageTitle(){
        switch (itemId){
            case 1001:
                return "General Info Page";
            case 1002 :
                return "Verb Table Page";
            case 1003:
                return "Collocations Page";
            case 1004:
                return "Thesaurus Page";
        }
        return "";
    }

    //code to generate content for word origin
    void generateContentForWordOrigin(){
        webViewContent="";
        String wordOriginStyle=String.format("<html>\n" +
                "    <head>\n" +
                "        <style>\n" +
                "body\n" +
                "{\n" +
                "    font-family:\"MundoSansPro\";\n" +
                "    color:#275078;\n" +
                "    font-size:18px;\n" +
                "}\n" +
                "\n" +
                "SENSE\n" +
                "{\n" +
                "\tfont-family: \"MundoSansPro\";\n" +
                "\tdisplay: block;\n" +
                "}\n" +
                "\n" +
                "sensenum\n" +
                "{\n" +
                "\tfont-family: \"MundoSansPro-Bold\";\n" +
                "\tfont-size:18px;\n" +
                "}\n" +
                "\n" +
                "ORIGIN\n" +
                "{\n" +
                "\tfont-family: \"MundoSansPro-Italic\";\n" +
                "\tfont-size:18px;\n" +
                "\n" +
                "}\n" +
                "CENTURY\n" +
                "{\n" +
                "\tfont-family: \"MundoSansPro\";\n" +
                "\tfont-size:18px;\n" +
                "\n" +
                "}\n" +
                "hey\n" +
                "{\n" +
                "\tfont-family:'MundoSansPro-Bold';\n" +
                "\tfont-size:18px;\n" +
                "\tdisplay: block;\n" +
                "\tcolor:#275078;\n" +
                "\n" +
                "}\n" +
                "LANG\n" +
                "{\n" +
                "\tfont-family: \"MundoSansPro\";\n" +
                "\tfont-size:18px;\n" +
                "}\n" +
                "TRAN\n" +
                "{\n" +
                "\tfont-family:'MundoSansPro-Bold';\n" +
                "\tfont-size:18px;\n" +
                "}\n" +
                "Ref,REFHWD,REFHOMNUM,REFSENSENUM\n" +
                "{\n" +
                "\tfont-family:'MundoSansPro';\n" +
                "\tfont-size:18px;\n" +
                "}\n" +
                "refhom\n" +
                "{\n" +
                "\tvertical-align : super;\n" +
                "\t\tfont-size:16px;\n" +
                "\t\tfont-family:\"MundoSansPro\";\n" +
                "}\n" +
                "HWD\n" +
                "{\n" +
                "\tfont-family:'MundoSansPro-Bold';\n" +
                "\tfont-size:18px;\n" +
                "}\n" +

                "Origintitle\n" +
                "{\n" +
                "\tfont-family:'MundoSansPro-Bold';\n" +
                "\tfont-size:18px;\n" +
                "\tdisplay:block;\n"+
                "}\n" +
                "@font-face {\n" +
                " font-family: 'MundoSansPro';\n" +
                " src: url('../MundoSansPro.otf')\n" +
                "}\n" +
                "@font-face {\n" +
                " font-family: 'MundoSansPro-Bold';\n" +
                " src: url('../MundoSansPro-Bold.otf')\n" +
                "}\n" +
                "@font-face {\n" +
                " font-family: 'MundoSansPro-Medium';\n" +
                " src: url('../MundoSansPro-Medium.otf')\n" +
                "}\n" +
                "@font-face {\n" +
                " font-family: 'MundoSansPro-MediumItalic';\n" +
                " src: url('../MundoSansPro-MediumItalic.otf')\n" +
                "}\n" +
                "@font-face {\n" +
                " font-family: 'MundoSansPro-BoldItalic';\n" +
                " src: url('../MundoSansPro-BoldItalic.otf')\n" +
                "}\n" +
                "@font-face {\n" +
                " font-family: 'MundoSansPro-Italic';\n" +
                " src: url('../MundoSansPro-Italic.otf')\n" +
                "}\n" +
                "@font-face {\n" +
                " font-family: 'MundoSansPro-Light';\n" +
                " src: url('../MundoSansPro-Light.otf')\n" +
                "}\n" +
                "@font-face {\n" +
                " font-family: 'MundoSansPro-LightItalic';\n" +
                " src: url('../MundoSansPro-LightItalic.otf')\n" +
                "}\n" +
                "@font-face {\n" +
                " font-family: 'CharisSILR';\n" +
                " src: url('../CharisSILR.ttf')\n" +
                "}\n" +
                "\n" +
                "@font-face {\n" +
                " font-family: 'CharisSILI';\n" +
                " src: url('../CharisSILI.ttf')\n" +
                "            }\n" +
                "        </style>\n" +
                "    </head>\n" +
                "    <body>");


        SQLiteDatabase db= DbConnection.getDbConnection();
        if(db!=null) {
            // Run the query to get the details of the hwd through the id.
            String id = hwdID;
            //Giving ? to pass arguments the DB query
            String query = String.format("select sense,hwd from etymology where id = ?");
            //passing the arguments values to the query on execution using string arguments
            Cursor cur = db.rawQuery(query, new String[]{id});
            while (cur.moveToNext())
            {
                wordOriginStyle+="<hwd>"+cur.getString(1)+"</hwd>";
                wordOriginStyle+="<BR/><BR/>";
                wordOriginStyle+="<Origintitle>Origin:</Origintitle>";
                wordOriginStyle+=cur.getString(0);

            }

            wordOriginStyle.concat("</body>\n" +
                    "</html>");
            webViewContent=wordOriginStyle;
            populateContentInWebView();
        }
    }
    //code to generate content for verb table
    void generateContentForVerbTable(){
        webViewContent="";
        String verbTableStyle=String.format("<html>\n" +
                "    <head>\n" +
                "        <style>\n" +
                "            body\n" +
                "            {\n" +
                "                font-family:'MundoSansPro';\n" +
                "                background:#ECF3F8;\n" +
                "                color:#275078;\n" +
                "                font-size:12pt;\n" +
                "            }\n" +
                "\n" +
                "            .popverbs\n" +
                "            {\n" +
                "                display:none;\n" +
                "            }\n" +
                "            .lemma\n" +
                "            {\n" +
                "                font-family:'MundoSansPro-Medium';\n" +
                "                margin-left:10px;\n" +
                "            }\n" +
                "            table\n" +
                "            {\n" +
                "                border:thin solid #F1E0B9;\n" +
                "                border-collapse: collapse;\n" +
                "                margin:10px 10px;\n" +
                "                width:95%%;\n" +
                "                background: #FCFAF6;\n" +
                "                font-size:12pt;\n" +
                "            }\n" +
                "            table tr\n" +
                "            {\n" +
                "                border:thin solid #F1E0B9;\n" +
                "            }\n" +
                "            table tr td\n" +
                "            {\n" +
                "                border:thin solid #F1E0B9;\n" +
                "                padding:5;\n" +
                "                width: 30%%;" +
                "            }\n" +
                "            table tr td.header\n" +
                "            {\n" +
                "                font-family:'MundoSansPro-Medium';\n" +
                "                background:#7FA4C1;\n" +
                "                color:#F1E0B9;\n" +
                "                border:thin solid #7FA4C1;\n" +
                "            }\n" +
                "            .verb_form,.col1\n" +
                "            {\n" +
                "                font-family:'MundoSansPro-Medium';\n" +
                "            }\n" +
                "@font-face {\n" +
                " font-family: 'MundoSansPro';\n" +
                " src: url('../MundoSansPro.otf')\n" +
                "}\n" +
                "@font-face {\n" +
                " font-family: 'MundoSansPro-Bold';\n" +
                " src: url('../MundoSansPro-Bold.otf')\n" +
                "}\n" +
                "@font-face {\n" +
                " font-family: 'MundoSansPro-Medium';\n" +
                " src: url('../MundoSansPro-Medium.otf')\n" +
                "}\n" +
                "@font-face {\n" +
                " font-family: 'MundoSansPro-MediumItalic';\n" +
                " src: url('../MundoSansPro-MediumItalic.otf')\n" +
                "}\n" +
                "@font-face {\n" +
                " font-family: 'MundoSansPro-BoldItalic';\n" +
                " src: url('../MundoSansPro-BoldItalic.otf')\n" +
                "}\n" +
                "@font-face {\n" +
                " font-family: 'MundoSansPro-Italic';\n" +
                " src: url('../MundoSansPro-Italic.otf')\n" +
                "}\n" +
                "@font-face {\n" +
                " font-family: 'MundoSansPro-Light';\n" +
                " src: url('../MundoSansPro-Light.otf')\n" +
                "}\n" +
                "@font-face {\n" +
                " font-family: 'MundoSansPro-LightItalic';\n" +
                " src: url('../MundoSansPro-LightItalic.otf')\n" +
                "}\n" +
                "@font-face {\n" +
                " font-family: 'CharisSILR';\n" +
                " src: url('../CharisSILR.ttf')\n" +
                "}\n" +
                "\n" +
                "@font-face {\n" +
                " font-family: 'CharisSILI';\n" +
                " src: url('../CharisSILI.ttf')\n" +
                "            }\n" +
                "        </style>\n" +
                "    </head>\n" +
                "    <body>");

        SQLiteDatabase db= DbConnection.getDbConnection();
        if(db!=null) {
            // Run the query to get the details of the hwd through the id.
            String id = hwdID;
            //Giving ? to pass arguments the DB query
            String query = String.format("select data from verbtables where id = ?");
            //passing the arguments values to the query on execution using string arguments
            Cursor cur = db.rawQuery(query, new String[]{id});

            while (cur.moveToNext())
            {
                verbTableStyle+=cur.getString(0);
            }

            verbTableStyle.concat("</body>\n" +
                    "</html>");
            webViewContent=verbTableStyle;
            populateContentInWebView();
        }
    }
    //code to generate content for collocation
    void generateContentForCollocation() {
        webViewContent = "";
        String strXML = "";
        String data="";
        SQLiteDatabase db = DbConnection.getDbConnection();
        if (db != null) {
            // Run the query to get the details of the hwd through the id.
            String id = hwdID;
            //Giving ? to pass arguments the DB query
            String query = String.format("select collocations from collocations where id = ?");
            //passing the arguments values to the query on execution using string arguments
            Cursor cur = db.rawQuery(query, new String[]{id});
            while (cur.moveToNext()) {
                //Removing
                 data = cur.getString(0);

            }
            //Replace the <HEADING> Content Sense with - Meaning
            String patternString = "(<HEADING>)(Sense )(.)(</HEADING>)";
            // to search case-insensitive search
            Pattern pattern = Pattern.compile(patternString,Pattern.CASE_INSENSITIVE);
            //finding matches in the result String for the given regex
            Matcher m = pattern.matcher(data);
            try{
                while(m.find()){
                    //Replace the group 2 Value Sense 1 with Meaning 1
                    data=data.replaceAll(m.group(2),"Meaning ");
                }

            }catch (Exception e){
                e.printStackTrace();
            }

            //Add a new Line between a new Meaning and ErrorBox
            String patt="((</ErrorBox>)(</ColloBox>)(<ColloBox>))";
            Pattern pattern2=Pattern.compile(patt,Pattern.CASE_INSENSITIVE);
            Matcher m2=pattern2.matcher(data);
            try{
                //If new meaning comes after ErrorBox -  add a new Line
                while(m2.find()){
                        data=data.replace(m2.group(3),"<addNewLine></addNewLine>"+m2.group(3));
                    }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
            strXML ="<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n" +
                        "<?xml-stylesheet type=\"text/xsl\" href=\"collocation_stylesheet.xsl\"?>" + data;
            String strXSLT = getFileContent("collocation_stylesheet.xsl");
            String html=StaticTransform(strXSLT, strXML);
            if(!html.isEmpty()) {
                webViewContent = html;
                populateContentInWebView();
            }
    }

    //code to generate content for thesaurus
    void generateContentForThesaurus() {
        webViewContent = "";
        String thesaurusXML = String.format("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n" +
                "<?xml-stylesheet type=\"text/xsl\" href=\"thesaurus_stylesheet.xsl\"?>");

        SQLiteDatabase db = DbConnection.getDbConnection();
        if (db != null) {
            // Run the query to get the details of the hwd through the id.
            String id = hwdID;
            //Giving ? to pass arguments the DB query
            String query ="select thesaurus from thesaurus where id = ? OR thesaurus like '%\\<Thesaurus>\\<ThesBox id=\"+id+\"%'";
            //passing the arguments values to the query on execution using string arguments
            Cursor cur = db.rawQuery(query, new String[]{id});
            if(cur.getCount()>0){
                while (cur.moveToNext()) {
                    thesaurusXML += cur.getString(0);
                }
            }
            //Replace the <HEADING> Content Sense with - Meaning
            String patternString = "(<HEADING>)(Sense )(.)(</HEADING>)";
            // to search case-insensitive search
            Pattern pattern = Pattern.compile(patternString,Pattern.CASE_INSENSITIVE);
            //finding matches in the result String for the given regex
            Matcher m = pattern.matcher(thesaurusXML);
            try{
                while(m.find()){
                    //Replace the group 2 Value Sense 1 with Meaning 1
                    thesaurusXML=thesaurusXML.replaceAll(m.group(2),"Meaning ");
                }

            }catch (Exception e){
                e.printStackTrace();
            }
            String strXSLT = getFileContent("thesaurus_stylesheet.xsl");
            String html=StaticTransform(strXSLT, thesaurusXML);
            webViewContent=html;
            populateContentInWebView();
        }
    }
    //populate the content on webview
    void populateContentInWebView(){

        WebView webView = (WebView) getActivity().findViewById(R.id.wv_additionalInfo);

        if (webView!=null) {
            WebSettings webViewSettings = webView.getSettings();
            if (fontSizeValue==0)
                webViewSettings.setTextZoom(webViewSettings.getTextZoom()+0);
            if (fontSizeValue==2)
                webViewSettings.setTextZoom(webViewSettings.getTextZoom()+5);
            if (fontSizeValue==4)
                webViewSettings.setTextZoom(webViewSettings.getTextZoom()+9);
            if (fontSizeValue==8)
                webViewSettings.setTextZoom(webViewSettings.getTextZoom()+17);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WebView.setWebContentsDebuggingEnabled(true);
        }
        if (webView!=null) {
            webView.getSettings().setJavaScriptEnabled(true);
            webView.loadDataWithBaseURL("file:///android_asset/www/", webViewContent, "text/html", "UTF-8", null);
        }

    }
    /**
    * Render Xml and XSLT and return corresponding HTML with style
     * @param strXsl XSTL style as String
     * @param strXml XML to be formated as String
     *               @return HTML with style
    */
    public static String StaticTransform(String strXsl, String strXml) {
        String html = "";

        try {

            InputStream ds = null;
            ds = new ByteArrayInputStream(strXml.getBytes("UTF-8"));

            Source xmlSource = new StreamSource(ds);

            InputStream xs = new ByteArrayInputStream(strXsl.getBytes("UTF-8"));
            Source xsltSource = new StreamSource(xs);

            StringWriter writer = new StringWriter();
            Result result = new StreamResult(writer);
            TransformerFactory tFactory = TransformerFactory.newInstance();
            Transformer transformer = tFactory.newTransformer(xsltSource);
            transformer.transform(xmlSource, result);

            html = writer.toString();

            ds.close();
            xs.close();

            xmlSource = null;
            xsltSource = null;

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerFactoryConfigurationError e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return html;
    }

    /**
     * Read a stylesheet file from res/raw... and return its content as string
     * @param fileId the Resource id of the file
     */
    private String GetStyleSheet(int fileId) {
        String strXsl = null;

        InputStream raw = getResources().openRawResource(fileId);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        int size = 0;
        // Read the entire resource into a local byte buffer.
        byte[] buffer = new byte[1024];
        try {
            while ((size = raw.read(buffer, 0, 1024)) >= 0) {
                outputStream.write(buffer, 0, size);
            }
            raw.close();
            strXsl = outputStream.toString();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return strXsl;
    }

    private String getFileContent(String fileName){
        StringBuilder buf=new StringBuilder();
        InputStream json= null;
        String content="";
        try {
            json = getActivity().getAssets().open("www/stylesheet/"+fileName);
            BufferedReader in= null;
            in = new BufferedReader(new InputStreamReader(json, "UTF-8"));
            String str;
            while ((str=in.readLine()) != null) {
                buf.append(str);
            }
            content=buf.toString();
            in.close();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return content;
    }
}
