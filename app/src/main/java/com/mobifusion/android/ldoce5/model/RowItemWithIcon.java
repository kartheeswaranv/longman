package com.mobifusion.android.ldoce5.model;

/**
 * Created by Bazi on 13/03/15.
 */
public class RowItemWithIcon extends IndexRowItem {
    private String itemId;
    private String iconCode;
    private int StringCode;

    /**
     *This class is used to store a slide index item
     *
     * @param itemId used to identify an item
     * @param iconCode the icon code used in longman fon
     */
    public RowItemWithIcon(String itemId, String iconCode,int StringCode) {
        this.itemId = itemId;
        this.iconCode = iconCode;
        this.StringCode = StringCode;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getIconCode() {
        return iconCode;
    }

    public void setIconCode(String iconCode) {
        this.iconCode = iconCode;
    }

    public int getStringCode() {
        return StringCode;
    }

    public void setStringCode(int stringCode) {
        this.StringCode = stringCode;
    }
}
