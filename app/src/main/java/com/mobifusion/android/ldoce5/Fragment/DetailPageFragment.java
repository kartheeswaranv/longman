package com.mobifusion.android.ldoce5.Fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff.Mode;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
//import android.support.annotation.Nullable;
//import android.support.v4.app.Fragment;
//import android.support.v4.app.FragmentManager;
//import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

//import com.google.android.gms.analytics.GoogleAnalytics;
//import com.google.android.gms.analytics.HitBuilders;
//import com.google.android.gms.analytics.Tracker;
import com.mobifusion.android.ldoce5.Activity.DbConnection;
import com.mobifusion.android.ldoce5.R;
import com.mobifusion.android.ldoce5.Util.AlphabetListAdapter;
import com.mobifusion.android.ldoce5.Util.FirebaseWrapperScreens;
import com.mobifusion.android.ldoce5.Util.JavaScriptInterface;
import com.mobifusion.android.ldoce5.Util.LDOCEApp;
import com.mobifusion.android.ldoce5.Util.Utils;
import com.mobifusion.android.ldoce5.Util.XSLTransformHelper;
import com.mobifusion.android.ldoce5.model.AdditionalInfoItem;
import com.mobifusion.android.ldoce5.model.RowWithHomnumForBookmark;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.xml.transform.TransformerException;

import static android.content.Context.CLIPBOARD_SERVICE;


public class DetailPageFragment extends Fragment implements OnCompletionListener,MediaPlayer.OnErrorListener {
    MediaPlayer mediaPlayer;
    public OnDetailPageLoadListener mListener;
    public Boolean isLoaded = false;
    String idToPass;
    String buttonToPass;
    public int headCellHeight;
    public String headword;
    public String hwdID;
    public String homNum;
    public String senseNum;
    Thread displayThread;
    Handler handler;
    public boolean thesRedirect;
    public boolean sendBack;
    public String imageName;
    ProgressBar pb_detail;
//    Tracker t;
    public int scrollYpostion;
    public static final String PREFS_NAME = "LDOCE6PrefsFile";
    boolean isRedirected;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_detail_page_fragment, null);
//        mediaPlayer = new MediaPlayer();
        return v;

    }

    /**
     * This function initialize the values to detail page.
     *
     * @param headword
     * @param hwdID
     * @param homNum
     * @param senseNum
     */
    public void setDetailPageArguments(String headword, String hwdID, String homNum, String senseNum) {
        this.headword = headword;
        this.hwdID = hwdID;
        this.homNum = homNum;
        this.senseNum = senseNum;
        this.imageName="";
        setUpVariables();
    }
    public void setDetailPageArguments(String headword,String hwdID,String homNum,String senseNum,boolean thesRedirect,int headCellHeight){
        this.headword=headword;
        this.hwdID=hwdID;
        this.homNum=homNum;
        this.senseNum=senseNum;
        this.thesRedirect=thesRedirect;
        this.headCellHeight=headCellHeight;
        setUpVariables();
    }
    /**
     * This function set head word if it is navigated from index, search or history page.
     * Example : set hwd= 'walk' when 'walked' is clicked from search (searchinflection)
     */
    void setUpVariables() {
        if (homNum.equalsIgnoreCase("")) {
            SQLiteDatabase sqLiteDatabase = DbConnection.getDbConnection();
            Cursor cursor = sqLiteDatabase.rawQuery("select hwd from core where id = ?", new String[]{hwdID});
            if (cursor.getCount() > 0) {
                while (cursor.moveToNext()) {
                    headword = cursor.getString(0);
                }
            }
            sqLiteDatabase.close();
        }
        isRedirected=false;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnDetailPageLoadListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
        // Get the Tracker and Initialise it
//        t = ((LDOCEApp) getActivity().getApplication()).getTracker(
//                LDOCEApp.TrackerName.APP_TRACKER);
//        t.setScreenName("Word Description Page");
        FirebaseWrapperScreens.ScreenViewEvent("Word Description Page");

//        t.send(new HitBuilders.AppViewBuilder().build());
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

            try {
                imageName = getImageDetailFromDB(headword);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        pb_detail = (ProgressBar) getActivity().findViewById(R.id.pb_detail);
        pb_detail.getIndeterminateDrawable().setColorFilter(Color.rgb(103, 144, 177), Mode.MULTIPLY);
        final WebView webView = (WebView) getActivity().findViewById(R.id.wv_headCell);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WebView.setWebContentsDebuggingEnabled(true);
        }
        if (webView != null) {
            webView.getSettings().setJavaScriptEnabled(true);
            webView.addJavascriptInterface(new JavaScriptInterface(getResources(), getActivity(), this), "JavaScriptInterface");
            webView.setWebViewClient(new myWebClient());
            WebSettings webViewSettings = webView.getSettings();
            SharedPreferences fontSizePreferences =  getActivity().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
            int fontSizeValue = fontSizePreferences.getInt("FONT_VALUE", 0);
            if (fontSizeValue==0)
                webViewSettings.setTextZoom(webViewSettings.getTextZoom()+1);
            if (fontSizeValue==2)
                webViewSettings.setTextZoom(webViewSettings.getTextZoom()+3);
            if (fontSizeValue==4)
                webViewSettings.setTextZoom(webViewSettings.getTextZoom()+7);
            if (fontSizeValue==8)
                webViewSettings.setTextZoom(webViewSettings.getTextZoom()+15);
        }

        // Handler to Update the UI
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);

                try {
                    //Update the History Table for current tapped word - ordering the History Table
                    updateHistoryTable(headword,hwdID);
                    //checks whether Detail Page is still in Loading state or not
                    isLoaded = true;
                        if (webView!=null) {
                            String msgToLoad=String.valueOf(msg.obj);
                            if(String.valueOf(msg.obj).contains("&lt;expandHWD/&gt;")){
                                System.out.println("Replace the String");
                                String hwd=headword;
                                Log.d("hwd", hwd);
                                if(hwd!=null){
                                    msgToLoad=msgToLoad.replaceAll("&lt;expandHWD/&gt;",hwd+" ");
                                }
                            }
                            //putting the homnum and sense value value
                            if(String.valueOf(msg.obj).contains("replaceHomnum") && !isRedirected){
                                msgToLoad=msgToLoad.replace("replaceHomnum",homNum);
                                isRedirected=true;
                            }
                            if(String.valueOf(msg.obj).contains("replaceSensenum")){
                                msgToLoad=msgToLoad.replace("replaceSensenum",senseNum);
                            }
                            if(String.valueOf(msg.obj).contains("replaceId")){
                                msgToLoad=msgToLoad.replace("replaceId","");
                            }
                            //replaceImageDetails
                            if(String.valueOf(msg.obj).contains("replaceImageDetails")){
                                msgToLoad=msgToLoad.replace("replaceImageDetails",imageName);
                            }
                            if(thesRedirect) {
                                //while Loading - check this ia tablet
                                // If this is tablet -  we have to calculate the Headcell height to show thesaurus content
                                if(Utils.isTablet(getActivity())) {
                                    //check in mastexsltablet -  for a variable ThesNotTapped
                                    if (String.valueOf(msg.obj).contains("ThesNotTapped")) {
                                        msgToLoad = msgToLoad.replace("ThesNotTapped", "ThesTapped");
                                        msgToLoad = msgToLoad.replace("thesHeadwrdId", hwdID);
                                    }
                                }
                            }
                            webView.loadDataWithBaseURL("file:///android_asset/www/", msgToLoad, "text/html", "UTF-8", null);
                            webView.setScrollY(scrollYpostion);
                        }


                } catch (NullPointerException e) {
                    e.printStackTrace();
                    Log.e("DetailPage", "Page didn't load fully and error is " + e.getMessage());

                }

            }
        };

        //Create a separate Thread for content to display in detail page -  This will run in Background Process,without blocking UI
        displayThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String contentsToDisplay = null;
                    contentsToDisplay = displayInfo();
                    Message msg = new Message();
                    msg.obj = contentsToDisplay;
                    if (contentsToDisplay != null) {
                        handler.sendMessage(msg);
                   }
                } catch (Throwable t) {
                    Log.v("Content Display Thread", "Thread Exception" + t);
                    mListener.detailPageInterrupted(true);
                }
            }
        });

        displayThread.start();
        mListener.detailPageLoaded(true);
        //Check if it is phone -  navigate to AdditionalInfo -  default for phone headcellheight is 0and thesaurus ItemId is 1004
        if(!Utils.isTablet(getActivity()) && (thesRedirect)){
            navigateToAdditionalInfo(hwdID,0,1004);
            thesRedirect=false;
        }
    }



    public class myWebClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // TODO Auto-generated method stub
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // TODO Auto-generated method stub
            pb_detail.setVisibility(View.VISIBLE);
            view.loadUrl(url);
            return true;

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            // TODO Auto-generated method stub
            super.onPageFinished(view, url);
            try {
                if (pb_detail != null)
                    pb_detail.setVisibility(View.GONE);
            } catch (NullPointerException e) {
                Log.e("DetailPage", "Page didn't load fully and error is" + e.getMessage());
            }

        }
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public String displayInfo() {
        // Create a DB Connection
        SQLiteDatabase db;
        String results = "";
        String query;
        DbConnection dbPath = new DbConnection();
        db = dbPath.getDbConnection();
        // Query the DB for getting the Head cell contents
        if (db != null) {
            // Run the query to get the details of the hwd through the id.
            String id = hwdID;
            //Giving ? to pass arguments the DB query
            query = String.format("select core from core where hwd = (select hwd from core where id = ?)");
            Log.d("head word", headword);

            //passing the arguments values to the query on execution using string arguments
            Cursor cur = db.rawQuery(query, new String[]{id});

            while (cur.moveToNext()) {
                results += cur.getString(0);
            }
        }

        try {
            String xlsToLoad = (Utils.isTablet(getActivity())) ? "href=\"masterxsltablet.xsl\"" : "href=\"masterxslphone.xsl\"";
            String enclosingMeta = "<?xml-stylesheet type=\"text/xsl\"" + xlsToLoad + "  ?>" +
                    "<dict>" +
                    "    <dictionary  xmlns:h=\"http://www.w3.org/1999/xhtml\" xmlns:e=\"urn:IDMEE\" xmlns:d=\"urn:LDOCE\" xmlns:p=\"urn:LDOCE\" xmlns=\"urn:LDOCE\"" +
                    "        xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"" +
                    "        xsi:schemaLocation=\"urn:DPS2-metadata \">%s</dictionary>" +
                    "</dict>";
            results = String.format(enclosingMeta, results);
            results = XSLTransformHelper.getHTMLForXML(results, getActivity());
            //to remove new Line the String
            results = results.replaceAll("\n", "");
            //to remove tab space from the String
            results = results.replaceAll("\t", "");
//            /*pattern to remove a space(if closing tag followed by a space)
//            Three groups in the Pattern:
//            group 1: to match every closing tag in the result String
//            group 2: to match space or null characters
//            group 3: to match opening tag followed by above groups
//            */
//            String patternString="(<\\/.+?>)(\\s*)(?=(<?))";
//            Pattern pattern= Pattern.compile(patternString);
//            //finding matches in the result String for the given regex
//            Matcher matcher= pattern.matcher(results);
//            //this string is used for space calculation(to remove space after each iteration in our result String)
//            int removalCharCount = 0;
//            //to iterate every match
//            while(matcher.find()){
//                int start=matcher.start(2);
//                int end=matcher.end(2);
//                String group3=matcher.group(3);
//                //if group 2 value is null or Empty - do no remove space
//                if(!group3.isEmpty() && start!=end){
//                    //get the stating position od group 2 and ending position of group -  remove those space in result String
//                    results= new StringBuilder(results).replace(matcher.start(2)-removalCharCount,matcher.end(2)-removalCharCount,"").toString();
//                    removalCharCount += end-start;
//                }
//            }
        } catch (TransformerException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        db.close();
        return results;
    }

    /**
     * This function invoked when app is navigated to detail page.it accept a argument the word detail and
     * store it in history table with current timestamp. if the word is already in history table,it updates
     * the timestamp
     *
     * @param headword head word
     * @param hwdID    word id
     */
    public void updateHistoryTable(String headword, String hwdID) {
        SQLiteDatabase db;
        String query;
        db = DbConnection.getUserDbConnection();
        SQLiteDatabase coreDb = DbConnection.getDbConnection();
        // Query the DB for checking if the word is already exist
        Boolean isExist = false;
        if (db != null) {
            query = String.format("select * from history where id = ? OR hwd = ? ");
            //passing the arguments values to the query on execution using string arguments
            Cursor cur = db.rawQuery(query, new String[]{hwdID, headword});
            while (cur.moveToNext()) {
                isExist = true;
                break;
            }
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String currentTimeStamp = dateFormat.format(new Date()); // Find todays date
            long timestamp = System.currentTimeMillis() * 1000;
            //The word is already in history table,update the timestamp to new time
            if (isExist) {
                ContentValues values = new ContentValues();
                values.put("timestamp", timestamp);
                db.update("history", values, "id = ? or hwd = ?", new String[]{hwdID, headword});

            } else {
                //checking if the word is frequent or not
                Cursor cursor = coreDb.rawQuery("select * from frequency_acad where hwd = ? AND (freq_s <> '' OR freq_w <> '' OR level <> '')", new String[]{headword});
                boolean isFrequent = false;

                if (cursor.getCount() > 0) {
                    isFrequent = true;
                    Log.d("frequency check","freq_actions");

                }

                //word is not exist in history,so inserting the word with necessary details
                ContentValues values = new ContentValues();
                values.put("id", hwdID);
                values.put("hwd", headword);
                values.put("pos", "");
                values.put("timestamp", timestamp);
                values.put("frequent", isFrequent ? "1" : "");
                db.insert("history", null, values);
                //check for only tablet
                if (getActivity() != null) {
                    boolean isTablet = (getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) >= (Configuration.SCREENLAYOUT_SIZE_LARGE);
                    if (isTablet) {
                        FragmentManager fragmentManager = this.getFragmentManager();
                        //Get the active real tab content
                        Fragment fragment = fragmentManager.findFragmentById(R.id.realtabcontent);
                        Log.d("head word", "real tab");

                        //Check for the history fragment instance
//                        if (fragment instanceof HistoryFragment) {
//                            //Call the updateHistoryEntryList method
//                            ((HistoryFragment) fragment).updateHistoryEntryList();
//                        }
                    }
                }
            }
        }
        coreDb.close();
        db.close();
    }

    /**
     * Callback interface for detail page
     */
    public interface OnDetailPageLoadListener {
        /**
         * callback function to notify the listener once the detail page is loaded
         *
         * @param isPageLoaded - tells is page loaded or not
         */
        public void detailPageLoaded(Boolean isPageLoaded);


        /**
         * callback function to notify the listener if detail page is interrupted
         *
         * @param isInterrupted
         */
        public void detailPageInterrupted(Boolean isInterrupted);

        public void detailPageWordFavorited(RowWithHomnumForBookmark wordInfo, boolean isFavorite);

        public void detailPageWordTapping(String tappedWord);
    }

    public void playSound(String id, String buttonClicked, Context context) {

            DbConnection dbPath = new DbConnection();
            SQLiteDatabase db = dbPath.getDbConnection();
            boolean isInternetOn = false;
            boolean isPlayed = false;
            if(Utils.isTablet(getContext().getApplicationContext())){
                try {
                    Thread.sleep(700);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        // Check if the current existence of DB is not null
            if (db != null && !id.equalsIgnoreCase("")) {
                String fileName = null;
                Uri url = null;

                //Check for UK English
                if (buttonClicked.equals("uk")) {
                    // Tracking for SlideMenu Button Click
//                    t = ((LDOCEApp) getActivity().getApplication()).getTracker(LDOCEApp.TrackerName.APP_TRACKER);
//                    // Set the screen as Detail Page
//                    t.setScreenName("Word Description Page");
                    FirebaseWrapperScreens.ScreenViewEvent("Word Description Page");
                    Bundle params=new Bundle();
                    params.putString("eventCategory","audio_button_action");
                    params.putString("eventAction","uk_button_clicked");
                    params.putString("eventLabel","UKbuttonTapped");
                    FirebaseWrapperScreens.EventLogs(params);
//                    // Build and send an Event.
//                    t.send(new HitBuilders.EventBuilder()
//                            .setCategory("audio_button_action")
//                            .setAction("uk_button_click")
//                            .setLabel("UKbuttonTapped")
//                            .build());
                    // Query to get the records from uksound table
                    String query = String.format("select * from uksound where id=?");
                    Cursor cur = db.rawQuery(query, new String[]{id});
                    //Check id presents in the table
                    while (cur.moveToNext()) {
                        fileName = cur.getString(cur.getColumnIndex("filename"));
                    }
                    if (Environment.getExternalStorageState()
                            .equals(Environment.MEDIA_MOUNTED)) {
                        String filePath = File.separator + "sdcard" + File.separator + "Android" + File.separator + "obb" + File.separator + ".media_files_228" + File.separator + "ldoce6_hwd_gb" + File.separator + fileName;
                        try {
                            FileInputStream fis = new FileInputStream(filePath);
                            String entry = null;
                            while ((entry = fis.toString()) != null) {
                                if (!entry.toString().isEmpty()) {
                                    File Mytemp = File.createTempFile("TCL", "mp3", getActivity().getCacheDir());
                                    Mytemp.deleteOnExit();
                                    FileOutputStream fos = new FileOutputStream(Mytemp);
                                    for (int c = fis.read(); c != -1; c = fis.read()) {
                                        try {
                                            fos.write(c);
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                    mediaPlayer = new MediaPlayer();
                                    fos.close();
                                    FileInputStream MyFile = new FileInputStream(Mytemp);
                                    mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                                    mediaPlayer.setDataSource(MyFile.getFD());
                                    mediaPlayer.prepare();
                                    mediaPlayer.start();
                                    mediaPlayer.setOnCompletionListener(this);
                                    mediaPlayer.setOnErrorListener(this);
                                } else {
                                    Toast.makeText(getContext(), R.string.unable_to_locate_media_files, Toast.LENGTH_LONG).show();
                                }
                            }
                            fis.close();

                        } catch (IOException e) {
                            e.printStackTrace();

                        } finally {
                            if (mediaPlayer != null && !mediaPlayer.isPlaying()) {
                                mediaPlayer.stop();
//                                mediaPlayer = null;
                                mediaPlayer.release();
                            }
                        }
                    }

                    if (fileName != null)
                        url = Uri.parse("http://eltapps.pearson.com/ldoce6app/ldoce6_hwd_gb/");
                }

                //Check for US English
                else if (buttonClicked.equals("us")) {
//                    t = ((LDOCEApp) getActivity().getApplication()).getTracker(LDOCEApp.TrackerName.APP_TRACKER);
//                    // Set the screen as Detail Page
//                    t.setScreenName("Word Description Page");
//                    // Build and send an Event.
//                    t.send(new HitBuilders.EventBuilder()
//                            .setCategory("audio_button_action")
//                            .setAction("us_button_click")
//                            .setLabel("USbuttonTapped")
//                            .build());

                    FirebaseWrapperScreens.ScreenViewEvent("Word Description Page");
                    Bundle params=new Bundle();
                    params.putString("eventCategory","audio_button_action");
                    params.putString("eventAction","us_button_clicked");
                    params.putString("eventLabel","USbuttonTapped");
                    FirebaseWrapperScreens.EventLogs(params);
                    // Query to get the records from ussound table
                    String query = String.format("select * from ussound where id=?");
                    Cursor cur = db.rawQuery(query, new String[]{id});
                    //Check id presents in the table
                    while (cur.moveToNext()) {
                        //Assign the filename
                        fileName = cur.getString(cur.getColumnIndex("filename"));
                    }
                    if (Environment.getExternalStorageState()
                            .equals(Environment.MEDIA_MOUNTED)) {
                        String filePath = File.separator + "sdcard" + File.separator + "Android" + File.separator + "obb" + File.separator + ".media_files_228" + File.separator + "ldoce6_hwd_us" + File.separator + fileName;
                        try {
                            FileInputStream fis = new FileInputStream(filePath);
                            String zipFileName = "convertMp3ToDb" + File.separator + "ldoce6_hwd_us" + File.separator + fileName;
                            String entry = null;
                            while ((entry = fis.toString()) != null) {
                                if (!entry.toString().isEmpty()) {
                                    File Mytemp = File.createTempFile("TCL", "mp3", getActivity().getCacheDir());
                                    Mytemp.deleteOnExit();
                                    FileOutputStream fos = new FileOutputStream(Mytemp);
                                    for (int c = fis.read(); c != -1; c = fis.read()) {
                                        try {
                                            fos.write(c);
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                    mediaPlayer = new MediaPlayer();
                                    fos.close();
                                    FileInputStream MyFile = new FileInputStream(Mytemp);
                                    mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                                    mediaPlayer.setDataSource(MyFile.getFD());
                                    mediaPlayer.prepare();
                                    mediaPlayer.start();
                                    mediaPlayer.setOnCompletionListener(this);
                                    mediaPlayer.setOnErrorListener(this);
                                } else {
                                    Toast.makeText(getContext(), R.string.unable_to_locate_media_files, Toast.LENGTH_LONG).show();
                                }
                            }
                            fis.close();

                        } catch (IOException e) {
                            e.printStackTrace();

                        } finally {
                            if (mediaPlayer != null && !mediaPlayer.isPlaying()) {
                                mediaPlayer.stop();
//                                mediaPlayer = null;
                                mediaPlayer.release();
                            }
                        }
                    }
                    if (fileName != null)
                        url = Uri.parse("http://eltapps.pearson.com/ldoce6app/ldoce6_hwd_us/");
                } else if (buttonClicked.equals("example")) {
                    //fetch the filename from exampleSound table from database
                    String query = String.format("select * from exasound where id=?");
                    Cursor cur = db.rawQuery(query, new String[]{id});
                    while (cur.moveToNext()) {
                        fileName = cur.getString(cur.getColumnIndex("filename"));
                    }

                    if (fileName != null)
                        url = Uri.parse("http://eltapps.pearson.com/ldoce6app/ldoce6_exa_pron/");
//                        Toast.makeText(context,R.string.sorry_audio_is_not_available,Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(getContext(), R.string.unable_to_locate_media_files, Toast.LENGTH_LONG).show();

                }

                /*Get the file path url and convert to string
                    * Concatenate the filename with the file path url
                    * Initialise the mediaPlayer and assign the data source with the file path url*/
                if (url != null) {
                    String filePath = url.toString();
                    filePath = filePath.concat(fileName);
                    idToPass = id;
                    buttonToPass = buttonClicked;
                    //Call for media player
                    playerStart(filePath);
                } else {
                    callToWebView(id, buttonClicked);
                }

                // Close the DB Connection
                db.close();
            } else {
                callToWebView(id, buttonClicked);
            }

    }



    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        callToWebView();
        return false;
    }


    @Override
    public void onCompletion(MediaPlayer mp) {
        /*Stop the loader and make the US or UK button back to normal state
        call the restoreSoundButtonState in main.js */
        Log.v("Completed", "Audio played successfully");
        //Call for webView without params
        mediaPlayer = null;
        callToWebView();
    }

    private void callToWebView() {
        callToWebView(idToPass, buttonToPass);
    }

    //Method to call webView to update the button state
    public void callToWebView(String idToPass, String buttonToPass) {
        //Call to webView
        final WebView webview = (WebView) getActivity().findViewById(R.id.wv_headCell);
        final JSONObject jsonObject = new JSONObject();
        try {
            //Add idToPass and buttonToPass to json object
            jsonObject.put("id", idToPass);
            jsonObject.put("btn", buttonToPass);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        //Run in a separate thread
        if (webview != null)
            webview.post(new Runnable() {
                @Override
                public void run() {
                    // Call the jQuery function from java code with the jsonObject
                    webview.loadUrl("javascript:restoreSoundButtonState(" + jsonObject.toString() + ")");
                }
            });
    }

    //function is used to scroll to a particular homnum entry in detail page
    public void navigateToEntryWithHomnum() {
        WebView webview = (WebView) getActivity().findViewById(R.id.wv_headCell);
        if (webview != null) {
            String function = "javascript:navigateToHomnumOrSense('" + homNum + "')";
            webview.loadUrl(function);
        }
    }

    //Media player call
    private void playerStart(String filePath) {
        //Make audio manager in stream music
        if (mediaPlayer == null)
            mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mediaPlayer.setOnCompletionListener(this);
        mediaPlayer.setOnErrorListener(this);
        try {
            //Set url to data source
            mediaPlayer.setDataSource(filePath);
            //Prepare the player
            mediaPlayer.prepare();
            //Play the audio
            mediaPlayer.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    //Handle scenarios for back, home button pressed and fragment cancelled
    @Override
    public void onPause() {
        super.onPause();
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            callToWebView();
            mediaPlayer.stop();

//            mediaPlayer.reset();
//            mediaPlayer.release();
//            mediaPlayer = null;
            //callToWebView();
        }
        mediaPlayer = null;
        if (displayThread.isAlive()) {
            handler.removeCallbacksAndMessages(displayThread);
        }
    }

    public void openFloatingMenu(final String id, final int headHeight) {
        //Override method to floating contextual menu
        //Get the webview
        WebView webview = (WebView) getActivity().findViewById(R.id.wv_headCell);
        //Call for oncreate context menu
        registerForContextMenu(webview);
        boolean isWordOrigin = false, isVerbTable = false, isCollocation = false, isThesaurus = false;
        //Adding heading to the action sheet
        idToPass = id;
        headCellHeight = headHeight;
        //Adding menu dynamically to the action sheet
        DbConnection dbPath = new DbConnection();
        SQLiteDatabase db = dbPath.getDbConnection();
        if (db != null && !id.equalsIgnoreCase("")) {
            //Word origin
            String query = String.format("select * from etymology where id=?");
            Cursor cur = db.rawQuery(query, new String[]{id});
            //Check id presents in the table
            while (cur.moveToNext()) {
                //Check for id
                isWordOrigin = true;
            }
            //Verb Table
            query = String.format("select * from verbtables  where id=?");
            cur = db.rawQuery(query, new String[]{id});
            //Check id presents in the table
            while (cur.moveToNext()) {
                //Check for id
                isVerbTable = true;
            }
            //Collocations
            query = String.format("select * from collocations where id=?");
            cur = db.rawQuery(query, new String[]{id});
            //Check id presents in the table
            while (cur.moveToNext()) {
                //Check for id
                isCollocation = true;
            }
            //Thesaurus
            query = String.format("select * from thesaurus where id=?");
            cur = db.rawQuery(query, new String[]{id});
            //Check id presents in the table
            while (cur.moveToNext()) {
                //Check for id
                isThesaurus = true;
            }
        }
        db.close();
        //Check the condition and add items to the action sheet
        final List<AlphabetListAdapter.Row> list = new ArrayList<>();

        if (isWordOrigin) {
            AdditionalInfoItem infoItem=new AdditionalInfoItem();
            infoItem.text=getString(R.string.word_origin);
            infoItem.id=1001;
            list.add(infoItem);
        }
        if (isVerbTable) {
            AdditionalInfoItem infoItem = new AdditionalInfoItem();
            infoItem.text = "\u200E"+getString(R.string.verb_table);
            infoItem.id = 1002;
            list.add(infoItem);
        }
        if (isCollocation) {
            AdditionalInfoItem infoItem = new AdditionalInfoItem();
            infoItem.text = getString(R.string.collocations);
            infoItem.id = 1003;
            list.add(infoItem);
        }
        if (isThesaurus) {
            AdditionalInfoItem infoItem = new AdditionalInfoItem();
            infoItem.text = getString(R.string.thesaurus);
            infoItem.id = 1004;
            list.add(infoItem);
        }
        AlphabetListAdapter adapter = new AlphabetListAdapter();
        adapter.setRows(list);

        AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(getActivity(), android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(getActivity());
        }
        builder.setTitle(getString(R.string.additional_content));
        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                AdditionalInfoItem item = (AdditionalInfoItem) list.get(which);
                navigateToAdditionalInfo(idToPass, headCellHeight, item.id);
                final WebView webView = (WebView) getActivity().findViewById(R.id.wv_headCell);
                if (webView != null) {
                    webView.post(new Runnable() {
                        @Override
                        public void run() {
                            webView.loadUrl("javascript:addDummyDivAndScrollToTop('" + idToPass + "')");
                        }
                    });
                }
            }
        });

        builder.setNegativeButton(R.string.cancel,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alertDialog=builder.create();
        ListView listView = alertDialog.getListView();
        listView.setFooterDividersEnabled(false);
        if(list.size()>0)
        alertDialog.show();
    }

    //Method to call webView to update the button state
    public void callToWebViewForAdditionalInfo(String idToPass, String buttonToPass) {
        //Call to webView
        final WebView webview = (WebView) getActivity().findViewById(R.id.wv_headCell);
        final JSONObject jsonObject = new JSONObject();
        try {
            //Add idToPass and buttonToPass to json object
            jsonObject.put("id", idToPass);
            jsonObject.put("btn", buttonToPass);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        //Run in a separate thread
        if (webview != null)
            webview.post(new Runnable() {
                @Override
                public void run() {
                    // Call the jQuery function from java code with the jsonObject
                    webview.loadUrl("javascript:addDivTagForAdditionalInfo(" + jsonObject.toString() + ")");
                }
            });
    }

    //navigate to additional info pop-up
    public void navigateToAdditionalInfo(String id, int headCellHeight, int itemId) {
        //Removing if additional info aready exist
        removeDummyCellAndCloseAdditionalInfo();
        //Getting fragment manager and fragment transaction
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        AdditionalInfoFragment additionalInfoFragment = new AdditionalInfoFragment();
        additionalInfoFragment.itemId = itemId;
        additionalInfoFragment.hwdID = id;
        additionalInfoFragment.parent = this;
        if (Utils.isTablet(getActivity())) {
            //handle for tablet
            //Getting the frame
            // and setting height
            final FrameLayout additionalInfoLayout = (FrameLayout) getActivity().findViewById(R.id.additionalInfoFragment);
            final RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) additionalInfoLayout.getLayoutParams();
            //

            int curentHeight = getView().getHeight() - (Utils.convertPxToDp(getActivity().getApplicationContext(), headCellHeight));
            layoutParams.height = curentHeight - 20;
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    additionalInfoLayout.setLayoutParams(layoutParams);
                }
            });
            fragmentTransaction.replace(R.id.additionalInfoFragment, additionalInfoFragment, "AdditionalInfoFragment");
            fragmentTransaction.addToBackStack(String.valueOf(additionalInfoFragment));
            fragmentTransaction.commit();
        } else {
            //handle for phone
            fragmentTransaction.replace(R.id.indexResultfragment, additionalInfoFragment).addToBackStack(String.valueOf(additionalInfoFragment)).commit();
        }
    }

    //remove the dummy cell in webview and close the additional info pop-up for tablet
    public void removeDummyCellAndCloseAdditionalInfo() {

        final AdditionalInfoFragment fragment;
        fragment = (AdditionalInfoFragment) getFragmentManager().findFragmentByTag("AdditionalInfoFragment");
        if (fragment != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.remove(fragment).commit();
                }
            });
            final WebView webView = (WebView) getActivity().findViewById(R.id.wv_headCell);
            if(webView != null) {
                webView.post(new Runnable() {
                    @Override
                    public void run() {
                        webView.loadUrl("javascript:removeDummyDiv();");
                    }
                });
            }
        }

    }


    public String getImageDetailFromDB(String hwd) throws JSONException {
            //DbConnection
            DbConnection dbPath = new DbConnection();
            SQLiteDatabase db = dbPath.getDbConnection();
            //Assigning variables
            JSONArray jsonArray = new JSONArray();
            //Check id not equal to null
            if (db != null && !hwd.equalsIgnoreCase("")) {
                //Getting all the images associated to hwd
                String query = String.format("select id,filename from pics where id in (select id from core where hwd=?)");
                Cursor cur = db.rawQuery(query, new String[]{hwd});
                while (cur.moveToNext()) {
                    //fetching and storing all details in JSON to pass to webview
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("id", cur.getString(0));
                    jsonObject.put("fileName", "file:///android_asset/thumbnail/".concat(cur.getString(1)));
                    jsonArray.put(jsonObject);
                }
            }
            String ImageDetails = jsonArray.toString();
            return ImageDetails;


    }

    public void redirectToThes(int headcellHeight){
        //When Thesref is tapped -  navigate to Thesaurus page for the tapped entry
        navigateToAdditionalInfo(hwdID, headcellHeight, 1004);
        //to avoid scroll on Detail Page(back to Thesaurus Page)
        final WebView webview = (WebView) getActivity().findViewById(R.id.wv_headCell);
        webview.post(new Runnable() {
            @Override
            public void run() {
                webview.loadUrl("javascript:addDummyDivAndScrollToTop('" + hwdID + "')");
            }
        });
            thesRedirect=false;
    }

    public void  getHeadCellHeight (){
        final WebView webview = (WebView) getActivity().findViewById(R.id.wv_headCell);
        //if(webview!=null)
        //webview.loadUrl("javascript:getHeadcellHeight('"+hwdID+"')");
        webview.post(new Runnable() {
            @Override
            public void run() {
                webview.loadUrl("javascript:getHeadcellHeight('" + hwdID + "')");
            }
        });

        //webview and call JSinterfacefunction
        //JavaScript:getHeadCellheight(hwdId)
       // webview.loadUrl("javascript:getHeadCellheight(" + hwdID + ")");
    }
    public void copyToClipBoard(String hwd,Context context){
        //Copy to clipboard code
            ClipboardManager clipboardManager = (ClipboardManager) context.getSystemService(CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText("HeadWord", hwd);
            clipboardManager.setPrimaryClip(clip);
    }
    @Override
    public void onStart() {
        super.onStart();
        //Remove any additional info if open for tab
        if(Utils.isTablet(getActivity())) {
            removeDummyCellAndCloseAdditionalInfo();
        }
        // Sending Hit to GA as Start of new Activity
//        GoogleAnalytics.getInstance(getActivity()).reportActivityStart(getActivity());
    }

    @Override
    public void onStop() {
        super.onStop();
        // Sending Hit to GA as End of Activity
//        GoogleAnalytics.getInstance(getActivity()).reportActivityStop(getActivity());
        WebView webview = (WebView)getActivity() .findViewById(R.id.wv_headCell);
        if (webview!=null)
            scrollYpostion = webview.getScrollY();
    }

    /**
     * performs search or copy
     * It will execute the funtion in JavaScript and JSInterface will handle the action
     * @param action action to be performed
     */
    public void performSearchOrCopy(String action)

    {
        WebView webview = (WebView)getActivity() .findViewById(R.id.wv_headCell);
            // to call the jQuery function from java code
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(action);
            if(webview!=null)
                webview.loadUrl("javascript:getSelectedString(" + jsonArray.toString() + ")");
    }


}
