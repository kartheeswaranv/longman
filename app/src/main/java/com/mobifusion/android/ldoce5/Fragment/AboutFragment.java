package com.mobifusion.android.ldoce5.Fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

//import com.google.android.gms.analytics.GoogleAnalytics;
//import com.google.android.gms.analytics.HitBuilders;
//import com.google.android.gms.analytics.Tracker;
import com.mobifusion.android.ldoce5.Activity.TourBaseFragment;
import com.mobifusion.android.ldoce5.R;
import com.mobifusion.android.ldoce5.Util.AlphabetListAdapter;
import com.mobifusion.android.ldoce5.Util.FirebaseWrapperScreens;
import com.mobifusion.android.ldoce5.Util.LDOCEApp;
import com.mobifusion.android.ldoce5.Util.Singleton;
import com.mobifusion.android.ldoce5.Util.Utils;
import com.mobifusion.android.ldoce5.model.RowWithFrequency;

import java.util.ArrayList;
import java.util.List;

    /**
     *  Created by Bazi on 30/06/15
     */

public class AboutFragment extends Fragment implements ListView.OnItemClickListener {

    public static final String PREFS_NAME = "LDOCE6PrefsFile";
//    Tracker t;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.activity_about_fragment,null);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //Initializing the view
        styleElements();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Get the Tracker and Initialise it
//        t = ((LDOCEApp) getActivity().getApplication()).getTracker(
//                LDOCEApp.TrackerName.APP_TRACKER);
//        t.setScreenName("About this App Page");
//
        FirebaseWrapperScreens.ScreenViewEvent("About this App Page");
//        t.send(new HitBuilders.AppViewBuilder().build());
    }

    @Override
    public void onStart() {
        super.onStart();
        // Sending Hit to GA as Start of new Activity
//        GoogleAnalytics.getInstance(getActivity()).reportActivityStart(getActivity());
    }

    @Override
    public void onStop() {
        super.onStop();
        // Sending Hit to GA as End of Activity
//        GoogleAnalytics.getInstance(getActivity()).reportActivityStop(getActivity());
    }

    public void styleElements() {
        // Set the About page title font title to MundoSansPro
        TextView aboutPageTitle = (TextView) getActivity().findViewById(R.id.about_this_app_title);
        aboutPageTitle.setTypeface(Singleton.getMundoSansPro());
        SharedPreferences fontSizePreferences =  getActivity().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        int fontSizeValue = fontSizePreferences.getInt("FONT_VALUE", 0);
        aboutPageTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP,20+fontSizeValue);
        //populating the content for about page
        ListView listView=(ListView)getActivity().findViewById(R.id.about_list);

        AlphabetListAdapter adapter=new AlphabetListAdapter();
        List<AlphabetListAdapter.Row> list=getMenuItems();
        adapter.setRows(list);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);
    }

    /**
     * This function is called to generate content for About section
     * @return A list of Items with ROW data model
     */
    public List<AlphabetListAdapter.Row> getMenuItems() {

        if(Utils.isTablet(getActivity().getApplicationContext())) {

            List<AlphabetListAdapter.Row> menuItems = new ArrayList<>();
            menuItems.add(new RowWithFrequency("\u200E"+getString(R.string.welcome_to_ldoce), "", false));
            menuItems.add(new RowWithFrequency("\u200E"+getString(R.string.pronunciation_table), "", false));
            menuItems.add(new RowWithFrequency("\u200E"+getString(R.string.short_forms_and_labels), "", false));
            menuItems.add(new RowWithFrequency("\u200E"+getString(R.string.grammar_codes_patterns), "", false));
            menuItems.add(new RowWithFrequency("\u200E"+getString(R.string.academic_word_list), "", false));
            menuItems.add(new RowWithFrequency("\u200E"+getString(R.string.tutorial), "", false));
            //Note: Need to add FAQ means: Remove the below commented code and change the case numbers in navigateToNextPage() method
            //menuItems.add(new RowWithFrequency(getString(R.string.frequently_asked_questions),"",false));
            menuItems.add(new RowWithFrequency("\u200E"+getString(R.string.email_support), "", false));
            menuItems.add(new RowWithFrequency("\u200E"+getString(R.string.tell_a_friend), "", false));
            menuItems.add(new RowWithFrequency("\u200E"+getString(R.string.copyright_notices), "", false));
            menuItems.add(new RowWithFrequency("\u200E"+getString(R.string.acknowledgements), "", false));
            return menuItems;

        }
        else{
            List<AlphabetListAdapter.Row> menuItems = new ArrayList<>();
            menuItems.add(new RowWithFrequency("\u200E"+getString(R.string.welcome_to_ldoce), "", false));
            menuItems.add(new RowWithFrequency("\u200E"+getString(R.string.pronunciation_table), "", false));
            menuItems.add(new RowWithFrequency("\u200E"+getString(R.string.short_forms_and_labels), "", false));
            menuItems.add(new RowWithFrequency("\u200E"+getString(R.string.grammar_codes_patterns), "", false));
            //menuItems.add(new RowWithFrequency(getString(R.string.academic_word_list), "", false));
            menuItems.add(new RowWithFrequency("\u200E"+getString(R.string.tutorial), "", false));
            //Note: Need to add FAQ means: Remove the below commented code and change the case numbers in navigateToNextPage() method
            //menuItems.add(new RowWithFrequency(getString(R.string.frequently_asked_questions),"",false));
            menuItems.add(new RowWithFrequency("\u200E"+getString(R.string.email_support), "", false));
            menuItems.add(new RowWithFrequency("\u200E"+getString(R.string.tell_a_friend), "", false));
            menuItems.add(new RowWithFrequency("\u200E"+getString(R.string.copyright_notices), "", false));
            menuItems.add(new RowWithFrequency("\u200E"+getString(R.string.acknowledgements), "", false));
            return menuItems;

        }

    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        navigateToNextPage(position);
    }

    /**
     * This function decide wher to navigate on clicking items from about menu
     * @param position
     * ‎"\u200E" - This unicode Character is added by default -  In order to avoid Righ to Left in Arabic Language
     */
    public void navigateToNextPage(int position){
        List<AlphabetListAdapter.Row> subItems=new ArrayList<>();
        if(Utils.isTablet(getActivity().getApplicationContext())) {
            switch (position) {
                //welcome to longman
                case 0:
                    //Navigating to submenu with its string and string resource id
                    subItems.add(new RowWithFrequency("\u200E"+getString(R.string.foreword), String.valueOf(R.string.foreword), false));
                    subItems.add(new RowWithFrequency("\u200E"+getString(R.string.introduction), String.valueOf(R.string.introduction), false));
                    navigateToSubPage(subItems, R.string.welcome_to_ldoce, "Welcome to Longman Page");
//                    FirebaseWrapperScreens.ScreenViewEvent("Welcome to Longman Page");

                    break;
                //pronounciation table
                case 1:
                    //Navigating to submenu with its string and string resource id
//                    subItems.add(new RowWithFrequency("\u200E"+getString(R.string.international_phonetic_alphabet), String.valueOf(R.string.international_phonetic_alphabet), false));
//                    subItems.add(new RowWithFrequency("\u200E"+getString(R.string.special_signs), String.valueOf(R.string.special_signs), false));
//                    navigateToSubPage(subItems, R.string.pronunciation_table, "International Phonetic Alphabet Page");
                    navigateHtmlPage(R.string.pronunciation_table, "Pronunciation Page");
//                    FirebaseWrapperScreens.ScreenViewEvent("International Phonetic Alphabet Page");


                    break;
                //short form and label
                case 2:
                    //Navigating to submenu with its string and string resource id
                    subItems.add(new RowWithFrequency("\u200E"+getString(R.string.short_forms), String.valueOf(R.string.short_forms), false));
                    subItems.add(new RowWithFrequency("\u200E"+getString(R.string.labels), String.valueOf(R.string.labels), false));
                    navigateToSubPage(subItems, R.string.short_forms_and_labels, "ShortForms Page");
                    break;
                //grammer codes
                case 3:
                    //Navigating to submenu with its string and string resource id
                    subItems.add(new RowWithFrequency("\u200E"+getString(R.string.grammar_codes), String.valueOf(R.string.grammar_codes), false));
                    subItems.add(new RowWithFrequency("\u200E"+getString(R.string.patterns), String.valueOf(R.string.patterns), false));
                    navigateToSubPage(subItems, R.string.grammar_codes_patterns, "Grammarcodes Page");
                    break;
                //academic words
            case 4:
                FirebaseWrapperScreens.ScreenViewEvent("Academic Word List Page");
                navigateToAcademicList();
//                t.setScreenName("Academic Word List Page");
//                t.send(new HitBuilders.AppViewBuilder().build());
                break;
//            //tutorials
                case 5:
//                    t.setScreenName("Tutorial Page");
//                    t.send(new HitBuilders.AppViewBuilder().build());
//                    FirebaseWrapperScreens.ScreenViewEvent("Tour and Tutorial Page");
                    //function to take cares and displays Tutorial Pages
                    navigateToTourPage();
                    break;
//            //faq
//            case 6:
//                //navigating to html page to loead online URL
//                navigateHtmlPage(R.string.frequently_asked_questions, "FAQ and Support Page");
//                break;
                //Email support
                case 6:
                    FirebaseWrapperScreens.ScreenViewEvent("Email Support Page");
                    //compose a support mail
                    composeSupportMail();

//                    t.setScreenName("Email Support Page");
//                    t.send(new HitBuilders.AppViewBuilder().build());
                    break;
                //Tell a friend
                case 7:
                    FirebaseWrapperScreens.ScreenViewEvent("Tell A Friend Page");
                    //compose tell a friend mail
                    composeTellAFriendMail();
//                    t.setScreenName("Tell A Friend Page");

//                    t.send(new HitBuilders.AppViewBuilder().build());
                    break;
                //copyright
                case 8:
//                    FirebaseWrapperScreens.ScreenViewEvent("Copyright Notices Page");
                    //load html page using the resource id
                    navigateHtmlPage(R.string.copyright_notices, "Copyright Page");

                    break;
                //Acknowledge
                case 9:
                    //load html page using the resource id
                    navigateHtmlPage(R.string.acknowledgements, "Acknowledgements Page");
//                    FirebaseWrapperScreens.ScreenViewEvent("Acknowledgements Page");

                    break;
            }
        }
        else
        {
            switch (position) {
                //welcome to longman
                case 0:
                    //Navigating to submenu with its string and string resource id
                    subItems.add(new RowWithFrequency("\u200E"+getString(R.string.foreword), String.valueOf(R.string.foreword), false));
                    subItems.add(new RowWithFrequency("\u200E"+getString(R.string.introduction), String.valueOf(R.string.introduction), false));
                    navigateToSubPage(subItems, R.string.welcome_to_ldoce, "Welcome to Longman Page");
//                    FirebaseWrapperScreens.ScreenViewEvent("Welcome to Longman Page");

                    break;
                //pronounciation table
                case 1:
                    //Navigating to submenu with its string and string resource id
//                    subItems.add(new RowWithFrequency("\u200E"+getString(R.string.international_phonetic_alphabet), String.valueOf(R.string.international_phonetic_alphabet), false));
//                    subItems.add(new RowWithFrequency("\u200E"+getString(R.string.special_signs), String.valueOf(R.string.special_signs), false));
//                    navigateToSubPage(subItems, R.string.pronunciation_table, "International Phonetic Alphabet Page");
//                    FirebaseWrapperScreens.ScreenViewEvent("International Phonetic Alphabet Page");
                    navigateHtmlPage(R.string.pronunciation_table, "Pronunciation Page");
                    break;
                //short form and label
                case 2:
                    //Navigating to submenu with its string and string resource id
                    subItems.add(new RowWithFrequency("\u200E"+getString(R.string.short_forms), String.valueOf(R.string.short_forms), false));
                    subItems.add(new RowWithFrequency("\u200E"+getString(R.string.labels), String.valueOf(R.string.labels), false));
//                    FirebaseWrapperScreens.ScreenViewEvent("Short Forms Page");

                    navigateToSubPage(subItems, R.string.short_forms_and_labels, "ShortForms Page");
                    break;
                //grammer codes
                case 3:
                    //Navigating to submenu with its string and string resource id
                    subItems.add(new RowWithFrequency("\u200E"+getString(R.string.grammar_codes), String.valueOf(R.string.grammar_codes), false));
                    subItems.add(new RowWithFrequency("\u200E"+getString(R.string.patterns), String.valueOf(R.string.patterns), false));
//                    FirebaseWrapperScreens.ScreenViewEvent("Grammar Codes Page");
                    navigateToSubPage(subItems, R.string.grammar_codes_patterns, "Grammarcodes Page");
                    break;
                //academic words
//                case 4:
//                    navigateToAcademicList();
//                    t.setScreenName("Academic Word List Page");

//                    t.send(new HitBuilders.AppViewBuilder().build());
//                    break;
//            //tutorials
                case 4:
//                    t.setScreenName("Tutorial Page");
//                    t.send(new HitBuilders.AppViewBuilder().build());
//                    FirebaseWrapperScreens.ScreenViewEvent("Tour and Tutorial Page");

                    //function to take cares and displays Tutorial Pages
                    navigateToTourPage();
                    break;
//            //faq
//            case 6:
//                //navigating to html page to loead online URL
//                navigateHtmlPage(R.string.frequently_asked_questions, "FAQ and Support Page");
//                break;
                //Email support
                case 5:
                    FirebaseWrapperScreens.ScreenViewEvent("Email Support Page");
                    //compose a support mail
                    composeSupportMail();
//                    t.setScreenName("Email Support Page");

//                    t.send(new HitBuilders.AppViewBuilder().build());
                    break;
                //Tell a friend
                case 6:
                    FirebaseWrapperScreens.ScreenViewEvent("Tell A Friend Page");
                    //compose tell a friend mail
                    composeTellAFriendMail();
//                    t.setScreenName("Tell A Friend Page");

//                    t.send(new HitBuilders.AppViewBuilder().build());
                    break;
                //copyright
                case 7:
                    //load html page using the resource id
//                    FirebaseWrapperScreens.ScreenViewEvent("Copyright Notices Page");

                    navigateHtmlPage(R.string.copyright_notices, "Copyright Page");
                    break;
                //Acknowledge
                case 8:
                    //load html page using the resource id
                    navigateHtmlPage(R.string.acknowledgements, "Acknowledgements Page");
//                    FirebaseWrapperScreens.ScreenViewEvent("Acknowledgements Page");

                    break;
            }

        }
    }

    /**
     *  This function will compose a tell friend mail with predefined mail body with subject
     */
    private void composeTellAFriendMail() {

        Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
        emailIntent.setType("plain/text");

        emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, getString(R.string.try_the_new_ldoce_app));
        emailIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.mail_body));
        emailIntent.setType("plain/text");
        startActivity(Intent.createChooser(emailIntent, "Send your email in:"));
    }
    /**
     *  This function will compose a support mail with predefined to address with subject
     */
    private void composeSupportMail() {
        String mailContent= "\n\n\n\n\n\n"+"-LDOCE6";
        Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
        emailIntent.setType("plain/text");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{"support@pearsonelt.com"});
        emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Attn : Support-LDOCE");
        emailIntent.putExtra(Intent.EXTRA_TEXT, mailContent);
        emailIntent.setType("plain/text");
        startActivity(Intent.createChooser(emailIntent, "Send your email in:"));
    }

    public void navigateToSubPage(List<AlphabetListAdapter.Row> subItems,int title,String pageName){

        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        AboutSubFragment aboutSubFragment = new AboutSubFragment();
        aboutSubFragment.items=subItems;
        aboutSubFragment.title=title;
        aboutSubFragment.pageName=pageName;

        if(Utils.isTablet(getActivity().getApplicationContext()))
            fragmentTransaction.replace(R.id.detailPageFragment, aboutSubFragment).addToBackStack("AboutSubPage");
        else
            fragmentTransaction.replace(R.id.indexResultfragment, aboutSubFragment).addToBackStack("AboutSubPage");
        fragmentTransaction.commit();

    }

    /**
     * to navigate to academic word list
     */
    public void navigateToAcademicList(){

        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        AcademicWordList academicWordList = new AcademicWordList();
        if(Utils.isTablet(getActivity().getApplicationContext()))
            fragmentTransaction.replace(R.id.detailPageFragment, academicWordList).addToBackStack("AcademicWordList");
        else
            fragmentTransaction.replace(R.id.indexResultfragment, academicWordList).addToBackStack("AcademicWordList");
        fragmentTransaction.commit();
    }

        //Function calls when Tutorial is Tapped from About this App Section
        //Usage: Overlay the Tour and Tutorial Page with the Behind Screen
        public void navigateToTourPage(){
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            //TourBase Fragment is the Main base fragment for all the Tour Pages
            TourBaseFragment tutorialBaseFragment = new TourBaseFragment();
            tutorialBaseFragment.parent=getActivity();
            //If clicked it from Tablets
            if (Utils.isTablet(getActivity().getApplicationContext())) {
                fragmentTransaction.replace(R.id.tour_base_lay, tutorialBaseFragment).addToBackStack("Tour and Tutorial Page");
            }
            //If clicked it from Phone
            else
                fragmentTransaction.replace(R.id.tour_base_layout, tutorialBaseFragment).addToBackStack("Tour and Tutorial Page");
            fragmentTransaction.commit();
        }

    /**
     * to load a html page with specific resource string id
     * @param title
     */
    public void navigateHtmlPage(int title,String pageName){

        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        HtmlPage htmlPage = new HtmlPage();
        htmlPage.title=title;
        htmlPage.pageName=pageName;
        //to check the page is online or offline
        if(title==R.string.frequently_asked_questions)
            htmlPage.offline=false;
        else
            htmlPage.offline=true;
        if(Utils.isTablet(getActivity().getApplicationContext())){
            fragmentTransaction.replace(R.id.detailPageFragment, htmlPage).addToBackStack("HtmlPage");
        }
        else {
            fragmentTransaction.replace(R.id.indexResultfragment, htmlPage).addToBackStack("HtmlPage");
        }
        fragmentTransaction.commit();

    }
}
