package com.mobifusion.android.ldoce5.Fragment;


import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

//import com.google.android.gms.analytics.GoogleAnalytics;
//import com.google.android.gms.analytics.HitBuilders;
//import com.google.android.gms.analytics.Tracker;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.mobifusion.android.ldoce5.Activity.CustomSwitch;
import com.mobifusion.android.ldoce5.Activity.WelcomeActivity;
import com.mobifusion.android.ldoce5.R;
import com.mobifusion.android.ldoce5.Util.FirebaseWrapperScreens;
import com.mobifusion.android.ldoce5.Util.LDOCEApp;
import com.mobifusion.android.ldoce5.Util.Singleton;
import com.mobifusion.android.ldoce5.Util.Utils;
import com.mobifusion.android.ldoce5.model.Language;

import java.util.ArrayList;

//import static com.mobifusion.android.ldoce5.Activity.LicenseCheck.mFirebaseAnalytics;


public class SettingsFragment extends Fragment implements AdapterView.OnItemSelectedListener {
    FirebaseAnalytics mFirebaseAnalytics;
    public static final String PREFS_NAME = "LDOCE6PrefsFile";
    public OnSettingPageRefreshListener mListener;
    Spinner languageSpinner;
    ArrayList<Language> languages;
//    Tracker t;
    int pro = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.activity_settings,null);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        styleElements();
    }

    @Override
    public void onStart() {
        super.onStart();

        //
        final SharedPreferences seekBarPreferences =  getActivity().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        final SharedPreferences.Editor seekBarEditor = seekBarPreferences.edit();
        final int seekBarValue = seekBarPreferences.getInt("SeekBarValue", 0);

        //
        final SharedPreferences fontSizePreferences =  getActivity().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        final SharedPreferences.Editor fontSizeEditor = fontSizePreferences.edit();
        final int fontSizeValue = fontSizePreferences.getInt("FONT_VALUE", 0);

        // Sending Hit to GA as Start of new Activity
//        GoogleAnalytics.getInstance(getActivity()).reportActivityStart(getActivity());
//
//        // Get the Tracker and Initialise it
//        t = ((LDOCEApp) getActivity().getApplication()).getTracker(
//                LDOCEApp.TrackerName.APP_TRACKER);
//        t.setScreenName("Settings Page");
        FirebaseWrapperScreens.ScreenViewEvent("Settings Page");

//        Bundle screenViewParams= new Bundle();
//        screenViewParams.putString("screenName","Settings page");
//        mFirebaseAnalytics.logEvent("screenview",screenViewParams);


//        t.send(new HitBuilders.AppViewBuilder().build());

        final SharedPreferences preferences = getActivity().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = preferences.edit();
        styleSpinner();
        final SeekBar sb_font_size=(SeekBar)getActivity().findViewById(R.id.sb_font_size);
        sb_font_size.setMax(3);
        sb_font_size.setProgress(seekBarValue);
        sb_font_size.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                //Progress - Value that User has selected
                pro=progress;	//we can use the progress value of pro as anywhere
                switch (pro) {
                    case 0:
                        fontSizeEditor.putInt("FONT_VALUE", 0);
                        fontSizeEditor.apply();
                        seekBarEditor.putInt("SeekBarValue", 0);
                        seekBarEditor.apply();
                        styleElements();
                        styleSpinner();
                        break;
                    case 1:
                        fontSizeEditor.putInt("FONT_VALUE", 2);
                        fontSizeEditor.apply();
                        seekBarEditor.putInt("SeekBarValue", 1);
                        seekBarEditor.apply();
                        styleElements();
                        styleSpinner();
                        break;
                    case 2:
                        fontSizeEditor.putInt("FONT_VALUE", 4);
                        fontSizeEditor.apply();
                        seekBarEditor.putInt("SeekBarValue", 2);
                        seekBarEditor.apply();
                        styleElements();
                        styleSpinner();
                        break;
                    case 3:
                        fontSizeEditor.putInt("FONT_VALUE", 8);
                        fontSizeEditor.apply();
                        seekBarEditor.putInt("SeekBarValue", 3);
                        seekBarEditor.apply();
                        styleElements();
                        styleSpinner();
                        break;
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        //word of the day
        final Switch swWordOfTheDay = (Switch)getActivity().findViewById(R.id.sw_word_of_the_day);
        swWordOfTheDay.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (swWordOfTheDay.isChecked()) {
                    editor.putBoolean("Word-of-the-Day", true);
                    editor.apply();
                    WelcomeActivity.getNotification(getActivity());
                    Bundle params=new Bundle();
                    params.putString("eventCategory","settings_page_buttons_click");
                    params.putString("eventAction","wordoftheday_on_button_toggled");
                    params.putString("eventLabel","Word of the day is on");
                    FirebaseWrapperScreens.EventLogs(params);
//                    t.send(new HitBuilders.EventBuilder().setCategory("settings_page_buttons_click").setAction("wordoftheday_on_button_toggled").setLabel("Word of the day is on").build());
                } else {
                    // Toast.makeText(getActivity(), "Word-of-the-Day is Disabled", Toast.LENGTH_SHORT).show();
                    editor.putBoolean("Word-of-the-Day", false);
                    editor.apply();
                    WelcomeActivity.cancelNotification(getActivity());

                    Bundle params=new Bundle();
                    params.putString("eventCategory","settings_page_buttons_click");
                    params.putString("eventAction","wordoftheday_off_button_toggled");
                    params.putString("eventLabel","Word of the day is off");
                    FirebaseWrapperScreens.EventLogs(params);
//                    t.send(new HitBuilders.EventBuilder().setCategory("settings_page_buttons_click").setAction("wordoftheday_off_button_toggled").setLabel("Word of the day is off").build());
                }
                }
            });
            swWordOfTheDay.setChecked(preferences.getBoolean("Word-of-the-Day", true));

        //Search on clipboard
        final Switch swSearchOnClipboard = (Switch)getActivity().findViewById(R.id.sw_search_on_clipboard);
        swSearchOnClipboard.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (swSearchOnClipboard.isChecked()) {
                        editor.putBoolean("Search-on-clipboard", true);
                        editor.apply();

                        Bundle params=new Bundle();
                        params.putString("eventCategory","settings_page_buttons_click");
                        params.putString("eventAction","search_clipboard_button_clicked");
                        params.putString("eventLabel","Search on Clipboard is on");
                        FirebaseWrapperScreens.EventLogs(params);
//                        t.send(new HitBuilders.EventBuilder().setCategory("settings_page_buttons_click").setAction("search_clipboard_button_clicked").setLabel("Search on Clipboard is on").build());
                    } else {
                        editor.putBoolean("Search-on-clipboard", false);
                        editor.apply();

                        Bundle params=new Bundle();
                        params.putString("eventCategory","settings_page_buttons_click");
                        params.putString("eventAction","search_clipboard_button_clicked");
                        params.putString("eventLabel","Search on Clipboard is off");
                        FirebaseWrapperScreens.EventLogs(params);
//                        t.send(new HitBuilders.EventBuilder().setCategory("settings_page_buttons_click").setAction("search_clipboard_button_clicked").setLabel("Search on Clipboard is off").build());
                    }
                }
            });
            //Default value set it as false
            swSearchOnClipboard.setChecked(preferences.getBoolean("Search-on-clipboard", false));

        //SaveSearch word
        final Switch swSaveSearchWord=(Switch)getActivity().findViewById(R.id.sw_save_search_word);
        swSaveSearchWord.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (swSaveSearchWord.isChecked()) {
                    editor.putBoolean("SaveSearchWord", true);
                    editor.apply();
                    mListener.onSavaSearchWordStateChanged(true);
                    Bundle params=new Bundle();
                    params.putString("eventCategory","settings_page_buttons_click");
                    params.putString("eventAction","save_search_button_clicked");
                    params.putString("eventLabel","Save Search Button is on");
                    FirebaseWrapperScreens.EventLogs(params);
//                    t.send(new HitBuilders.EventBuilder().setCategory("settings_page_buttons_click").setAction("save_search_button_clicked").setLabel("Save Search Button is on").build());
                } else {
                    editor.putBoolean("SaveSearchWord", false);
                    editor.apply();
                    mListener.onSavaSearchWordStateChanged(false);
                    Bundle params=new Bundle();
                    params.putString("eventCategory","settings_page_buttons_click");
                    params.putString("eventAction","save_search_button_clicked");
                    params.putString("eventLabel","Save Search Button is off");
                    FirebaseWrapperScreens.EventLogs(params);

//                    t.send(new HitBuilders.EventBuilder().setCategory("settings_page_buttons_click").setAction("save_search_button_clicked").setLabel("Save Search Button is off").build());
                }
            }
        });
        swSaveSearchWord.setChecked(preferences.getBoolean("SaveSearchWord", false));
    }

    public void styleSpinner(){
        final SharedPreferences preferences = getActivity().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        final SharedPreferences fontSizePreferences =  getActivity().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        final int fontSizeValue = fontSizePreferences.getInt("FONT_VALUE", 0);
        //Adding items to the Language Spinner
        languageSpinner = (Spinner)getActivity().findViewById(R.id.sp_language_spinner);
        languages = getLanguages();
        // Giving values for Spinner
        ArrayAdapter<Language> dataAdapter = new ArrayAdapter<Language>(getActivity(),R.layout.ldoce_spinner_white,R.id.spinner_textview,languages){
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v =super.getView(position, convertView, parent);
                TextView spinnerTextView = (TextView)v.findViewById(R.id.spinner_textview);
                spinnerTextView.setTypeface(Singleton.getMundoSansPro());
                    if (fontSizeValue==0)
                        spinnerTextView.setTextSize(14);
                    if (fontSizeValue==2)
                        spinnerTextView.setTextSize(14+1);
                    if (fontSizeValue==4)
                        spinnerTextView.setTextSize(14+2);
                    if (fontSizeValue==8)
                        spinnerTextView.setTextSize(14+3);
                return v;
            }

            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                LayoutInflater inflater=(LayoutInflater) getActivity().getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View row=inflater.inflate(R.layout.ldoce_spinner_dropdown_item, parent, false);
                TextView label=(TextView)row.findViewById(R.id.spinner_dropdown_textview);
                label.setText(languages.get(position).getLanguageName());
                label.setTypeface(Singleton.getMundoSansPro());

                label.setTextSize(14+fontSizeValue);
                return row;
            }
        };
        dataAdapter.setDropDownViewResource(R.layout.ldoce_spinner_dropdown_item);
        languageSpinner.setAdapter(dataAdapter);
        languageSpinner.setOnItemSelectedListener(this);
        Language language = getLanguageByCode(preferences.getString("LDOCELanguage", "en_GB"));
        Log.w("LDOCE6:", language.getLanguageCode());
        languageSpinner.setSelection(getLanguagePositionByCode(language.getLanguageCode()));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void styleElements() {
        //Styling the UI Elements
        TextView tv_word_of_the_day = (TextView) getActivity().findViewById(R.id.tv_word_of_the_day);
        TextView tv_search_clipboard = (TextView) getActivity().findViewById(R.id.tv_search_on_clipboard);
        TextView tv_desc_search_clipboard = (TextView) getActivity().findViewById(R.id.tv_desc_search_on_clipboard);
        TextView tv_save_search = (TextView) getActivity().findViewById(R.id.tv_save_search_word);
        TextView tv_desc_save_search = (TextView) getActivity().findViewById(R.id.tv_desc_save_search_word);
        TextView tv_font_size = (TextView) getActivity().findViewById(R.id.tv_font_size);
        TextView tv_settings_choose_lang = (TextView) getActivity().findViewById(R.id.tv_settings_choose_lang);
        TextView settingTitle = (TextView)getActivity().findViewById(R.id.settings_title);
        CustomSwitch swWordOfTheDay=(CustomSwitch)getActivity().findViewById(R.id.sw_word_of_the_day);
        CustomSwitch swSaveSearchWord=(CustomSwitch)getActivity().findViewById(R.id.sw_save_search_word);
        CustomSwitch swSearchOnClipboard=(CustomSwitch)getActivity().findViewById(R.id.sw_search_on_clipboard);



        tv_word_of_the_day.setTypeface(Singleton.getMundoSansPro());
        tv_search_clipboard.setTypeface(Singleton.getMundoSansPro());
        tv_desc_search_clipboard.setTypeface(Singleton.getMundoSansPro());
        tv_save_search.setTypeface(Singleton.getMundoSansPro());
        tv_desc_save_search.setTypeface(Singleton.getMundoSansPro());
        tv_font_size.setTypeface(Singleton.getMundoSansPro());
        tv_settings_choose_lang.setTypeface(Singleton.getMundoSansPro());
        settingTitle.setTypeface(Singleton.getMundoSansPro());
        swWordOfTheDay.setTypeface(Singleton.getMundoSansPro());
        swSaveSearchWord.setTypeface(Singleton.getMundoSansPro());
        swSearchOnClipboard.setTypeface(Singleton.getMundoSansPro());

        tv_word_of_the_day.setText(getActivity().getString(R.string.word_of_the_day));
        tv_search_clipboard.setText(getActivity().getString(R.string.search_on_clipboard));
        tv_desc_search_clipboard.setText(getActivity().getString(R.string.places_clipboard_text_on_to_the_search_bar_on_launch));
        tv_save_search.setText(getActivity().getString(R.string.save_search_word));
        tv_desc_save_search.setText(getActivity().getString(R.string.places_last_viewed_word_on_to_search_on_home_page));
        tv_font_size.setText(getActivity().getString(R.string.font_size));
        tv_settings_choose_lang.setText(getActivity().getString(R.string.language));
        settingTitle.setText(getActivity().getString(R.string.settings));

        swWordOfTheDay.setTextOn(getString(R.string.on));
        swWordOfTheDay.setTextOff(getString(R.string.off));

        swSaveSearchWord.setTextOn(getString(R.string.on));
        swSaveSearchWord.setTextOff(getString(R.string.off));

        swSearchOnClipboard.setTextOn(getString(R.string.on));
        swSearchOnClipboard.setTextOff(getString(R.string.off));

        SharedPreferences fontSizePreferences =  getActivity().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        int fontSizeValue = fontSizePreferences.getInt("FONT_VALUE",0);
            tv_word_of_the_day.setTextSize(18+fontSizeValue);
            tv_search_clipboard.setTextSize(18+fontSizeValue);
            tv_desc_search_clipboard.setTextSize(14+fontSizeValue);
            tv_save_search.setTextSize(18+fontSizeValue);
            tv_desc_save_search.setTextSize(14+fontSizeValue);
            tv_font_size.setTextSize(18+fontSizeValue);
            tv_settings_choose_lang.setTextSize(18+fontSizeValue);
            settingTitle.setTextSize(20+fontSizeValue);

    }
    // Adding supported languages to list which added to the spinner
    public ArrayList<Language> getLanguages()
    {
        ArrayList<Language> languages = new ArrayList<Language>();
        languages.add(new Language("English (UK)","en_GB"));
        languages.add(new Language("English (US)","en_US"));
        languages.add(new Language("لعربية","ar_EG"));
        languages.add(new Language("中国（简体）","zh_CN"));
        languages.add(new Language("한국의","ko_KR"));
        languages.add(new Language("日本の","ja_JP"));
        languages.add(new Language("português","pt_PT"));
        languages.add(new Language("pусский","ru_RU"));
        return languages;
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Log.w("LDOCE6::::", String.valueOf(position));
        String langCode =((Language)parent.getItemAtPosition(position)).getLanguageCode();

        setLocale(langCode);
        styleElements();
        Bundle params=new Bundle();
        params.putString("eventCategory","settings_page_buttons_click");
        params.putString("eventAction","dict_lang_button_clicked");
        params.putString("eventLabel",langCode);
        FirebaseWrapperScreens.EventLogs(params);
        mListener.refreshMenuItems();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    // App default language setting by the option selected in Spinner
    public void setLocale(String lang) {
        Utils.setApplicationLanguage(getActivity(), lang);
        SharedPreferences settings = getActivity().getSharedPreferences(PREFS_NAME, Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("LDOCELanguage", lang);
        Log.d("Lang ", lang);
        // Commit the edits!
        editor.apply();

    }
    // Checking the default language to Spinner array values and returning to Spinner
    public Language getLanguageByCode (String code)
    {
        ArrayList<Language> languages = getLanguages();
        Language temp=languages.get(0);
        for(int i=0;i<languages.size();i++)
        {
            if(code.equals(languages.get(i).getLanguageCode()))
            {
                temp = languages.get(i);
                break;
            }
        }
        return temp;
    }
    // Checking the default language to Spinner array values and returning to Spinner
    public int getLanguagePositionByCode (String code)
    {
        ArrayList<Language> languages = getLanguages();
        Language temp=languages.get(0);
        int j=0;
        for(int i=0;i<languages.size();i++)
        {
            if(code.equals(languages.get(i).getLanguageCode()))
            {
                temp = languages.get(i);
                j=i;
                break;
            }
        }
        return j;
    }
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnSettingPageRefreshListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }
    /**
     * Callback interface for settings page
     */
    public interface OnSettingPageRefreshListener
    {
        /***
         * To refresh the menu item content for tablet alone
         */
        void refreshMenuItems();
        void onSavaSearchWordStateChanged(boolean state);

    }

    @Override
    public void onStop() {
        super.onStop();
        // Sending Hit to GA as End of Activity
//        GoogleAnalytics.getInstance(getActivity()).reportActivityStop(getActivity());
    }

    @Override
    public void onResume() {
        super.onResume();
        //If keyboard is in visible state means, First we need to dismiss the keyboard and show the slide menu
        InputMethodManager inputKeyboard = (InputMethodManager) getActivity().getSystemService(
                Context.INPUT_METHOD_SERVICE);
        try{
            //Check whether keyboard service is visible
            if (inputKeyboard.isAcceptingText()) {
                //Hiding keyboard
                inputKeyboard.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
            }
        }catch (Exception e){
            System.out.print("Keyboard Dismiss issue");
        }
    }
}
