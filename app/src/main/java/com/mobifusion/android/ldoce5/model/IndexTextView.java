package com.mobifusion.android.ldoce5.model;

import android.content.Context;
import android.widget.TextView;

/**
 * Created by Bazi on 09/03/15.
 *
 * This textview is used to display the elements in the sidebar.it is extended from texview to store the starting
 * position of that section in the list also.
 * */

public class IndexTextView extends TextView{

    public int startPosition;
    public IndexTextView(Context context) {
        super(context);
    }
}