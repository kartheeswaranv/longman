package com.mobifusion.android.ldoce5.Activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import androidx.fragment.app.FragmentActivity;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.mobifusion.android.ldoce5.R;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.zip.ZipInputStream;

import static com.mobifusion.android.ldoce5.Activity.WelcomeActivity.progress_bar_type;


public class BaseActivity extends FragmentActivity {

    public static final String PREFS_NAME = "LDOCE6PrefsFile";
    public ProgressDialog pDialog;
    Integer WRITE_EXST = 1;
    Integer READ_EXST = 2;
    String filePath = File.separator + "sdcard" + File.separator + "Android" + File.separator + "obb" + File.separator + "com.mobifusion.android.ldoce5" + File.separator + "main.228.com.mobifusion.android.ldoce5.obb";
    boolean check;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_base, menu);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();

            // This Shared Preferences mainly used to check whether Search in the Main Menu is tapped or not
            SharedPreferences searchButtonTappedPreferences = this.getSharedPreferences("LDOCE6PrefsFile", Context.MODE_PRIVATE);
            SharedPreferences.Editor searchEditor = searchButtonTappedPreferences.edit();
            searchEditor.putBoolean("SearchMenuTapped", false);
            searchEditor.apply();
            boolean isTablet = getResources().getBoolean(R.bool.isTablet);
            if (isTablet) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
            } else {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
            }

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //Alert for obb check
    public void initializeDownloadUI() {
        setContentView(R.layout.obbdownloader_main);
        final String file_url = "http://eltapps.pearson.com/ldoce6app/ldoce6_obb/main.228.com.mobifusion.android.ldoce5.obb";

        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            adb = new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            adb = new AlertDialog.Builder(this);
        }

        adb.setTitle(R.string.media_files);


        adb.setCancelable(false)
                .setMessage("Media files missing, click OK to download them again");

        adb.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                new DownloadFileFromURL().execute(file_url);
                dialog.dismiss();
                //finish();
            }
        });
        adb.setNegativeButton("Download later", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Log.d("base","base");
                dialog.dismiss();
                finish();
            }
        });
        adb.show();

    }
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case progress_bar_type:
                pDialog = new ProgressDialog(this);
                pDialog.setMessage("Downloading file. Please wait...");
                pDialog.setIndeterminate(false);
                pDialog.setMax(100);
                pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                pDialog.setCancelable(false);
                pDialog.setButton(DialogInterface.BUTTON_POSITIVE,"Go to App", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
//                        File mediaFile = new File(File.separator + "storage" +File.separator+"emulated"+File.separator+"0" + File.separator + "Android" + File.separator + "obb" + File.separator + ".media_files_228" );
//                        new BaseActivity.DirectoryCleaner(mediaFile).clean();
//                        mediaFile.delete();
                        Intent intent =new Intent(BaseActivity.this,SlideMenuSearchAndIndex.class);
                        startActivity(intent);
                    }
                });

                pDialog.show();
                return pDialog;
            default:
                return null;
        }
    }
    public class DirectoryCleaner {
        private final File mFile;

        public DirectoryCleaner(File file) {
            mFile = file;
        }

        public void clean() {
            if (null == mFile || !mFile.exists() || !mFile.isDirectory()) return;
            for (File file : mFile.listFiles()) {
                delete(file);
            }
        }

        private void delete(File file) {
            if (file.isDirectory()) {
                for (File child : file.listFiles()) {
                    delete(child);
                }
            }
            file.delete();

        }
    }
    class DownloadFileFromURL extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog(progress_bar_type);
        }

        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                URL url = new URL(f_url[0]);

                URLConnection conection = url.openConnection();
                conection.connect();
                int lenghtOfFile = conection.getContentLength();
                InputStream input = new BufferedInputStream(url.openStream(), 8192);
                OutputStream output = new FileOutputStream("/storage/emulated/0/main.228.com.mobifusion.android.ldoce5.obb");
                //OutputStream output = new FileOutputStream("/storage/emulated/0/Android/obb/com.mobifusion.android.ldoce5/main.228.com.mobifusion.android.ldoce5.obb");
                byte data[] = new byte[1024];
                long total = 0;
                while ((count = input.read(data)) != -1) {
                    total += count;
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));
                    output.write(data, 0, count);
                }
                output.flush();
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }
            return null;
        }
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            pDialog.setProgress(Integer.parseInt(progress[0]));
        }
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded
            dismissDialog(progress_bar_type);
        }
    }
//check for obb
    boolean checkForFile(){
        if (!check) {
            try {
                FileInputStream fin = new FileInputStream(filePath);
                ZipInputStream zin = new ZipInputStream(fin);
                if (!zin.toString().isEmpty()) {
                    check = true;
                }
                zin.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return check;
    }
}