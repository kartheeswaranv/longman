package com.mobifusion.android.ldoce5.Fragment;

import android.app.Activity;
import androidx.fragment.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.mobifusion.android.ldoce5.R;
import com.mobifusion.android.ldoce5.Util.AlphabetListAdapter;
import com.mobifusion.android.ldoce5.model.RowItemWithIcon;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SlidingMenuFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SlidingMenuFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SlidingMenuFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SlidingMenuFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SlidingMenuFragment newInstance(String param1, String param2) {
        SlidingMenuFragment fragment = new SlidingMenuFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public SlidingMenuFragment() {
        // Required empty public constructor
    }

    /**
     * Called when the fragment's activity has been created and this
     * fragment's view hierarchy instantiated.  It can be used to do final
     * initialization once these pieces are in place, such as retrieving
     * views or restoring state.  It is also useful for fragments that use
     * {@link #setRetainInstance(boolean)} to retain their instance,
     * as this callback tells the fragment when it is fully associated with
     * the new activity instance.  This is called after {@link #onCreateView}
     * and before {@link #onViewStateRestored(android.os.Bundle)}.
     *
     * @param savedInstanceState If the fragment is being re-created from
     *                           a previous saved state, this is the state.
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);
        List<RowItemWithIcon> list = generateContentForSlideMenu();
        populateSlideMenuContent(list);

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);


        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sliding_menu, container, false);
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(String selectedMenu);
    }

    /**
     * This function is called to generate content for the slide menu items with its icon
     * @return a list of elements of RowItemWithIcon to show in slide menu
     */
    public List<RowItemWithIcon> generateContentForSlideMenu()
    {
        //Initialize array to add elements to slide menu
        List<RowItemWithIcon> list=new ArrayList<>();
        //Initialize the value for each items :
        // 1. an id to identify element on onClick
        // 2. localized title for the slide item
        // 3. icon code for the slide item in longman font
        //TODO:set font face and size

        list.add(new RowItemWithIcon("search","\uf002",R.string.search));
        list.add(new RowItemWithIcon("favorites","\uf212",R.string.bookmarks));
        list.add(new RowItemWithIcon("history","\uf017",R.string.history));
        list.add(new RowItemWithIcon("core_vocabulary","\uf081",R.string.core_vocabulary));
        list.add(new RowItemWithIcon("academic_word_list","\uf19d",R.string.academic));
        list.add(new RowItemWithIcon("about_this_app","\uf05a",R.string.about_this_app));
        list.add(new RowItemWithIcon("settings","\uf013",R.string.settings));
        return list;
    }

    /**
     * This Function populate the slide menu and its content using a custom adapter called AlphabeticListAdapter
     * @param list : content to be shown in Slide Menu
     *
     */
    public void populateSlideMenuContent(List<RowItemWithIcon> list)
    {
        //Adapter to populate list
        AlphabetListAdapter adapter= new AlphabetListAdapter() ;
        //Row is a base class
        List<AlphabetListAdapter.Row> rows = new ArrayList<AlphabetListAdapter.Row>();

        //Adding each element to the list
        for (RowItemWithIcon item : list)
        {
            // Add other headwords from the DB to the list
            rows.add(item);

        }
        //setting the adpater
        adapter.setRows(rows);

        //getting the list view to populate content
        final ListView listView = (ListView) getView().findViewById(R.id.listItems);
        // ListView Item Click Listener
        listView.setItemsCanFocus(true);
        listView.setFocusableInTouchMode(true);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                // ListView Clicked item value
                RowItemWithIcon clickedItem    = (RowItemWithIcon) listView.getItemAtPosition(position);

                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    //Sending the clicked item's id to identify the item
                    mListener.onFragmentInteraction(clickedItem.getItemId());
                }
            }

        });

        listView.setAdapter(adapter);

    }
}
