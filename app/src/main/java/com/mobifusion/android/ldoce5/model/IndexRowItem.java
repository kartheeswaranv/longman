package com.mobifusion.android.ldoce5.model;

import com.mobifusion.android.ldoce5.Util.AlphabetListAdapter;

/**
 * Created by Bazi on 06/03/15.
 */
public class IndexRowItem extends AlphabetListAdapter.Row {
    private String hwd;
    private String hwdId;
    private boolean isFrequent;
    public boolean isSelected;

    public IndexRowItem() {
    }

    public IndexRowItem(String hwd,String hwdId, boolean isFrequent)
    {
        this.hwd=hwd;
        this.hwdId=hwdId;
        this.isFrequent=isFrequent;
    }
    public IndexRowItem(String hwd,String hwdId, boolean isFrequent, boolean isSelected)
    {
        this.hwd=hwd;
        this.hwdId=hwdId;
        this.isFrequent=isFrequent;
        this.isSelected=isSelected;
    }
    public String getHwd() {
        return hwd;
    }

    public void setHwd(String hwd) {
        this.hwd = hwd;
    }

    public String getHwdId() {
        return hwdId;
    }

    public void setHwdId(String hwdId) {
        this.hwdId = hwdId;
    }

    public boolean getIsFrequent() {
        return isFrequent;
    }

    public void setIsFrequent(boolean isFrequent) {
        this.isFrequent = isFrequent;
    }

    public boolean getIsSelected() {
        return isSelected;
    }

    public void setIsSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    //Used to avoid Duplicates - checks for the Word in Set, if it is there- then it will not inset in the Set
    @Override
    public boolean equals(Object o) {
        if(o instanceof IndexRowItem)
        {
            IndexRowItem row = (IndexRowItem)o;
            return row.hwd.equals(this.hwd);
        }
        return false;

    }
    //Objects Internally
    @Override
    public int hashCode() {
        return this.hwd.hashCode();
    }
}
