package com.mobifusion.android.ldoce5.Util;


import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
//import android.support.v4.app.Fragment;
//import android.support.v4.app.FragmentActivity;
//import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.widget.SearchView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;

//import com.google.android.gms.analytics.HitBuilders;
//import com.google.android.gms.analytics.Tracker;
import com.mobifusion.android.ldoce5.Activity.DbConnection;
import com.mobifusion.android.ldoce5.Activity.DbHelper;
import com.mobifusion.android.ldoce5.Activity.ImagePopUp;
import com.mobifusion.android.ldoce5.Activity.SlideMenuSearchAndIndex;
import com.mobifusion.android.ldoce5.Fragment.DetailPageFragment;
import com.mobifusion.android.ldoce5.Fragment.ImageFragment;
import com.mobifusion.android.ldoce5.R;
import com.mobifusion.android.ldoce5.model.RowWithHomnumForBookmark;
import com.mobifusion.android.ldoce5.model.SearchRowItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by Bazi on 13/04/15.
 */
public  class JavaScriptInterface  {
//    Tracker t;
    private Resources resources;
    private Context context;
    private DetailPageFragment parentFragment;
    public JavaScriptInterface(Resources resources,Context context,DetailPageFragment detailPageFragment) {
        this.resources = resources;
        this.context=context;
        this.parentFragment=detailPageFragment;
    }
    @JavascriptInterface
    public String getLocalizedString(String key){
        int id=resources.getIdentifier(key,"string","com.mobifusion.android.ldoce5");
        String localized=resources.getString(id);
        return resources.getString(id);
    }

    /**
     * This function accept a JSON object from the favorite and return the state of favorite button(Selected/not selected)
     * @param jsonObject : details including id,hwd,homnum,frequent info of entry
     * @return true if word is favourite false if word is not favorite
     */
    @JavascriptInterface
    public boolean toggleFavorite(String jsonObject){

        boolean isToggle=false;
        try {
            //getting the object from the javascript
            JSONObject wordInfo = new JSONObject(jsonObject);
            boolean isFrequent=wordInfo.getString("frequent").equalsIgnoreCase("1")?true:false;
            SimpleDateFormat dateFormat = new SimpleDateFormat(
                    "dd/MM/yyyy", Locale.getDefault());
            Date date = new Date();
            String currentDate=dateFormat.format(date);
            RowWithHomnumForBookmark wordItem=new RowWithHomnumForBookmark(wordInfo.getString("hwd"),wordInfo.getString("id"),wordInfo.getString("homnum"),isFrequent,currentDate);
            if(wordInfo!=null)
            {
                //fetching the id
                String id=wordInfo.getString("id");
                // Fetch the hwd
                String hwd=wordInfo.getString("hwd");
                //checking if the word is already in favorites
                if(Utils.isFavorite(id)){
                    // Track
//                    t = ((LDOCEApp)parentFragment.getActivity().getApplication()).getTracker(LDOCEApp.TrackerName.APP_TRACKER);
                    // Set the screen as Detail Page
//                    t.setScreenName("Word Description Page");
                    // Build and send an Event.
//                    t.send(new HitBuilders.EventBuilder()
//                            .setCategory("favorite_button_action")
//                            .setAction("word_un_fav")
//                            .setLabel(hwd)
//                            .build());
                    Bundle params=new Bundle();
                    params.putString("eventCategory","bookmark_page_actions");
                    params.putString("eventAction","bookmark_word_un-favorited");
                    params.putString("eventLabel",hwd);
                    FirebaseWrapperScreens.EventLogs(params);

                    //if yes
                    //delete entry from favorite table
                    boolean success = Utils.deleteItemFromFavorite(new String[]{id});
//                    Toast.makeText(context,R.string.the_word_has_been_removed_from_your_bookmarks,Toast.LENGTH_SHORT).show();
                    //Return false if deletion is success to remove the favourite state in detail page
                   isToggle=success?false:true;
                }
                else{
                    //insert entry from favorite table
                    isToggle=Utils.insertItemToFavourite(wordInfo);
                    if(isToggle){
                        // Track
//                        t = ((LDOCEApp)parentFragment.getActivity().getApplication()).getTracker(LDOCEApp.TrackerName.APP_TRACKER);
//                        // Set the screen as Detail Page
//                        t.setScreenName("Word Description Page");
//                        // Build and send an Event.
//                        t.send(new HitBuilders.EventBuilder()
//                                .setCategory("favorite_button_action")
//                                .setAction("word_fav")
//                                .setLabel(hwd)
//                                .build());


                        Bundle params=new Bundle();
                        params.putString("eventCategory","bookmark_page_actions");
                        params.putString("eventAction","bookmark_word_favorited");
                        params.putString("eventLabel",hwd);
                        FirebaseWrapperScreens.EventLogs(params);
                        //Show the toast that word is added to favorite
                        Toast.makeText(context,resources.getString(R.string.the_word_has_been_added_to_bookmarks),Toast.LENGTH_SHORT).show();
                        SlideMenuSearchAndIndex slideMenuSearchAndIndex= (SlideMenuSearchAndIndex) context;
                    }
                }
                //refreshing favorite page on tablet
                parentFragment.mListener.detailPageWordFavorited(wordItem,isToggle);
            }

        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        //return status to show
        return isToggle;
    }

    /**
     * To check a word is favorite or not
     * @param id id of the word entry
     * @return true if word is favorite , false if the word is not favorite
     */
    @JavascriptInterface
    public boolean isWordFavorite(String id){
        //checking if the word if favorite or not
        return Utils.isFavorite(id);
    }

    /***
     * To get the clicked word for the webview
     * @param clickedObject is selected word for webview
     */
    @JavascriptInterface
    public void tappedWord(String clickedObject
    ) throws JSONException {
        Log.w("TappedWord", clickedObject);
        //Extract each and every item from String object
        JSONObject js= new JSONObject(clickedObject);
        //getting HWD
        String hwd=js.getString("text");
        //getting sense number -  for navigation
        String sensenum=js.getString("sensenum");
        //getting the Entry number
        String homnum=js.getString("homnum");
        Log.d("sense", sensenum+" homnum" + homnum+ " hwd" +hwd);

        //check whether the tapped Word is present inside DB or not
        boolean isHwdAvailable=Utils.checkHwd(hwd);
        if(isHwdAvailable){
            //take hwdId for corressponding hwd in DB
            String hwdId=Utils.getHwdId(hwd,homnum);
            if(!hwdId.isEmpty())
                //Pass all the retrieved items -  id,hwd,homnum and sensenum to navigate to detail page
            Utils.navigateToDetailPage((FragmentActivity) context,hwd,hwdId,homnum,sensenum);
        }
        //check the tapped word is present inside <SearchInflections> table - if so retrieve and navigate to Detail Page
        else
        {
            //Retrieve the hwd, hwdId from SearcInflections
            List<SearchRowItem> searchInflectionsResultList=DbHelper.getSearchInflectionHWD(hwd);
            if(searchInflectionsResultList.size()>0)
            {
                //getting HwdId
                String headwordID=searchInflectionsResultList.get(0).getHwdId();
                //getting hwd
                String headword=searchInflectionsResultList.get(0).getHwd();
                if(!headwordID.isEmpty() && !headword.isEmpty()){
                    Utils.navigateToDetailPage((FragmentActivity) context,headword,headwordID,homnum,sensenum);
                }
            }
        }

        //Toast.makeText(context,clickedObject,Toast.LENGTH_SHORT).show();
    }

    @JavascriptInterface
    public void phrasalVerbScroll(String clickedObject) throws JSONException{
        JSONObject js= new JSONObject(clickedObject);
        //getting HWD
        String hwd=js.getString("text");
    }
    /***
     *
     * @param clickedObject
     */
    @JavascriptInterface
    public void tappedCrossrefto(String clickedObject) throws JSONException {
        Log.w("Crossrefto",clickedObject);
        //Extract each and every item from String object
        JSONObject js= new JSONObject(clickedObject);
        //getting HWD
        String hwd=js.getString("text");
        //getting sense number -  for navigation
        String sensenum=js.getString("sensenum");
        //getting the Entry number
        String homnum=js.getString("homnum");
        //checking whether the tapped word is available in DB or not, if available navigate to Detail Page
        boolean isHwdAvailable=Utils.checkHwd(hwd);
        if(isHwdAvailable){
            //Fetch Id from DB and navigate to Detail Page
            String hwdId=Utils.getHwdId(hwd, homnum);
            Log.d("frequency check","hwdavail");

            if(!hwdId.isEmpty()) {
                Utils.navigateToDetailPage((FragmentActivity) this.context, hwd, hwdId, homnum, sensenum);
            }
            //check the tapped word is present inside <SearchInflections> table - if so retrieve and navigate to Detail Page
            else
            {
                //Retrieve the hwd, hwdId from SearcInflections
                List<SearchRowItem> searchInflectionsResultList=DbHelper.getSearchInflectionHWD(hwd);
                Log.d("frequency check","hwdavailinflect");

                if(searchInflectionsResultList.size()>0)
                {
                    //getting HwdId
                    String headwordID=searchInflectionsResultList.get(0).getHwdId();
                    //getting hwd
                    String headword=searchInflectionsResultList.get(0).getHwd();
                    if(!headwordID.isEmpty() && !headword.isEmpty()){
                        Utils.navigateToDetailPage((FragmentActivity) context,headword,headwordID,homnum,sensenum);
                    }
                }
            }
        }

        //Toast.makeText(context,clickedObject,Toast.LENGTH_SHORT).show();
    }

    /***
     *
     * @param clickedObject
     */
    @JavascriptInterface
    public void tappedThesref(String clickedObject)throws JSONException
    {
        JSONObject js= new JSONObject(clickedObject);
        Log.d("frequency check","tappedthesref");

        String hwd=js.getString("text");
        //getting sense number -  for navigation
        String sensenum=js.getString("sensenum");
        //getting the Entry number
        String homnum=js.getString("homnum");
        //getting cellHeight
        int cellHeight=js.getInt("cellHeight");
        boolean isHwdAvailable=Utils.checkHwd(hwd);
        if(isHwdAvailable){
            //take hwdId for corressponding hwd in DB
            String hwdId=Utils.getHwdId(hwd, homnum);
            Log.d("js page","done");
            if(!hwdId.isEmpty())
                //Pass all the retrieved items -  id,hwd,homnum and sensenum to navigate to detail page
                //context.detailPageFragment.navigateToAdditionalInfo(hwdID,cellHeight,1004);

                Utils.detailPageNavigation((FragmentActivity) context, hwd, hwdId, homnum, sensenum, 0);
        }
    }

    @JavascriptInterface
    public void checkUsUkExampleSoundButton(String jsonObject){

        try {
            //getting the object from the javascript
           JSONObject wordInfo = new JSONObject(jsonObject);
            if(wordInfo!=null)
            {
                //fetching the id
                String id=wordInfo.getString("id");
                String button= wordInfo.getString("buttonChoose");
                System.out.println("Pressed Button is:"+button);

//                boolean isInternetOn=false;
                //check for internet connection
                //Getting the connectivity service
//                ConnectivityManager cm =
//                        (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
//                NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
//                boolean isInternetOn = activeNetwork != null &&
//                        activeNetwork.isConnectedOrConnecting();

//                ConnectivityManager check = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
//                if (check != null) {
//                    //Check all network information
//                    NetworkInfo[] info = check.getAllNetworkInfo();
//                    if (info != null)
//                        for (int i = 0; i < info.length; i++)
//                            if (info[i].getState() == NetworkInfo.State.CONNECTED) {
//                                isInternetOn = true;
//                            }
//                }
                //checking if the id is not equal to null
                try {
                    if(!id.equals("")){
                        //Call to play the sound
                        parentFragment.playSound(id,button,context);
                    }
                    else{
                        Log.d("frequency check","web");

                        parentFragment.callToWebView(id,button);
                    }
                }
                catch (Exception e){
                    Log.e("Js",e.getMessage());
                }

            }
        }
        catch (JSONException e) {
            Log.e("checkukus",e.getMessage());
        }
    }
    @JavascriptInterface
    public void exSound(String jsonObject){
        try {
            //getting the object from the javascript
            JSONObject wordInfo = new JSONObject(jsonObject);
            if(wordInfo!=null)
            {
                //fetching the id
                String id=wordInfo.getString("id");
                String button= wordInfo.getString("buttonChoose");
                System.out.println("Pressed Button is:"+button);
//        boolean isInternetOn=false;
                ConnectivityManager cm =
                        (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                boolean isInternetOn = activeNetwork != null &&
                        activeNetwork.isConnectedOrConnecting();

                ConnectivityManager check = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                if (check != null) {
                    //Check all network information
                    NetworkInfo[] info = check.getAllNetworkInfo();
                    if (info != null)
                        for (int i = 0; i < info.length; i++)
                            if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                                isInternetOn = true;
                            }
                }
                if(isInternetOn){
                    //Call to play the sound
                    parentFragment.playSound(id,button,context);
                }
                else
                {
                    Toast.makeText(context,R.string.sorry_audio_is_not_available,Toast.LENGTH_SHORT).show();
                    parentFragment.callToWebView(id,button);
                }
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @JavascriptInterface
    public void infoButtonTapped(String jsonObject) throws JSONException {
        String id = null,button=null;
        int headCellHeight=0;
        JSONObject wordInfo = new JSONObject(jsonObject);
        if(wordInfo!=null) {
            //fetching the id
             id = wordInfo.getString("id");
            headCellHeight=wordInfo.getInt("headCellHeight");
        }
        parentFragment.openFloatingMenu(id,headCellHeight);
    }

    @JavascriptInterface
    // Check USsound file name is available in Database, raw sound file in the extracted Folder
    public boolean isUSSoundAvailable(String id){
        boolean isAudioAvailable = false;
        System.out.println("US Audio ID  is:"+id);
        if(!id.isEmpty()){
            DbConnection dbPath = new DbConnection();
            SQLiteDatabase db = dbPath.getDbConnection();
            if(db!=null && !id.equalsIgnoreCase("") ){
                String query = String.format("select * from ussound where id=?");
                Cursor cur = db.rawQuery(query, new String[]{id});
                // check if the filename exists in the Database
                while(cur.moveToNext()){
                    String fileName = cur.getString(cur.getColumnIndex("filename"));
                    if(fileName!=null){
                        String filePath = File.separator + "sdcard" + File.separator + "Android" + File.separator + "obb" + File.separator + ".media_files_228" + File.separator + "ldoce6_hwd_us" + File.separator + fileName;
                        File audioFile = new File(filePath);
                        // check if the file is exists in the Extracted media Folder
                        if(audioFile.exists()){
                            isAudioAvailable = true;
                        }else{
                            isAudioAvailable = false;
                        }
                    }else {
                        isAudioAvailable = false;
                    }
                }
            }
            db.close();
        }
        return  isAudioAvailable;
    }

    @JavascriptInterface
    // Check UKsound File is available and raw mp3 audio is available
    public boolean isUKSoundAvailable(String id){
        boolean isAudioAvailable = false;
        System.out.println("UK Audio ID  is:"+id);
        if(!id.isEmpty()){
            DbConnection dbPath = new DbConnection();
            SQLiteDatabase db = dbPath.getDbConnection();
            if(db!=null && !id.equalsIgnoreCase("") ){
                String query = String.format("select * from uksound where id=?");
                Cursor cur = db.rawQuery(query, new String[]{id});
                while(cur.moveToNext()){
                    String fileName = cur.getString(cur.getColumnIndex("filename"));
                    // check if the file is available in the database table
                    if(fileName!=null){
                        String filePath = File.separator + "sdcard" + File.separator + "Android" + File.separator + "obb" + File.separator + ".media_files_228" + File.separator + "ldoce6_hwd_gb" + File.separator + fileName;
                        File audioFile = new File(filePath);
                        // If .mp3 is not available in the Extracted media Folder- disable the state of audio button
                        if(audioFile.exists()){
                            isAudioAvailable = true;
                        }else{
                            isAudioAvailable = false;
                        }
                    }else {
                        isAudioAvailable = false;
                    }
                }
            }
            db.close();
        }
        return  isAudioAvailable;
    }
    @JavascriptInterface
    public boolean isExtractedFilesAvailable(){
        boolean isMediaAvailable = false;

            String filePath = File.separator + "sdcard" + File.separator + "Android" + File.separator + "obb" + File.separator + ".media_files_228";
            File file = new File(filePath);
            if(file.exists()) {
                isMediaAvailable = true;
            }
        return isMediaAvailable;


    }

    @JavascriptInterface
    public boolean isAdditionalInfoAvailable(String id){
        boolean isContentAvailable=false;
        if(!id.isEmpty()){
            System.out.println("Selected id is:"+id);
            DbConnection dbPath = new DbConnection();
            SQLiteDatabase db = dbPath.getDbConnection();
            if (db != null && !id.equalsIgnoreCase("")) {
                //Word origin
                String query = String.format("select * from etymology where id=?");
                Cursor cur = db.rawQuery(query, new String[]{id});
                //Check id presents in the table
                while (cur.moveToNext()) {
                    //Check for id
                    isContentAvailable= true;
                    return isContentAvailable;
                }
                //Verb Table
                query = String.format("select * from verbtables  where id=?");
                cur = db.rawQuery(query, new String[]{id});
                //Check id presents in the table
                while (cur.moveToNext()) {
                    //Check for id
                    isContentAvailable= true;
                    return isContentAvailable;
                }
                //Collocations
                query = String.format("select * from collocations where id=?");
                cur = db.rawQuery(query, new String[]{id});
                //Check id presents in the table
                while (cur.moveToNext()) {
                    //Check for id
                    isContentAvailable= true;
                    return isContentAvailable;
                }
                //Thesaurus
                query = String.format("select * from thesaurus where id=?");
                cur = db.rawQuery(query, new String[]{id});
                //Check id presents in the table
                while (cur.moveToNext()) {
                    //Check for id
                    isContentAvailable= true;
                    return isContentAvailable;
                }
            }
            db.close();
        }
        return isContentAvailable;
    }

    //setParentheadHeight(JSOn)
    //{ parent.height= argument value
    @JavascriptInterface
    public void setHeadcellHeight(String heightOfHeadCell)throws JSONException {

        JSONObject js= new JSONObject(heightOfHeadCell);
        float height= Float.parseFloat(js.getString("headHeight"));
        System.out.println("Calculated Height is:"+height);
        Log.w("LDOCE HEADCELL HEIGHT:", String.valueOf(height));
        int x =(int)height;
        double y= Math.floor(x);
        parentFragment.headCellHeight= (int) y;
        parentFragment.sendBack=true;
        parentFragment.redirectToThes((int)y);

    }

    @JavascriptInterface
    public void callForCopyToClipboard(String jsonObject) throws JSONException {
        try {
            //getting the object from the javascript
            JSONObject wordInfo = new JSONObject(jsonObject);
            if (wordInfo != null) {
                //fetching the hwd
                String hwd = wordInfo.getString("hwd");
                //Call to Detailpage for copy to clipboard
                DetailPageFragment detailPageFragment;
                parentFragment.copyToClipBoard(hwd, context);
                Toast.makeText(context, context.getString(R.string.copied_to_clipboard), Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @JavascriptInterface
    public void openImage(String detail) {
        try {
            //getting the object from the javascript
            JSONObject fileInfo = new JSONObject(detail);
            if (fileInfo != null) {
                //fetching the hwdID and image name
                String hwdId = fileInfo.getString("id");
                String fileName = fileInfo.getString("fileName");
                if (!hwdId.equalsIgnoreCase("") && !fileName.equalsIgnoreCase("")) {

                    if (Utils.isTablet(context)) {
                        //check for the extracted media file
                        String mediaFile = File.separator + "sdcard" + File.separator + "Android" + File.separator + "obb" + File.separator + ".media_files_228";
                        File check = new File(mediaFile);
                        if(check.exists()) {
                            ImagePopUp imagePopUp = new ImagePopUp(context);
                            imagePopUp.fileName = fileName;
                            imagePopUp.show();
                        }
                        else
                        {
                            Toast.makeText(context,R.string.unable_to_locate_media_files,Toast.LENGTH_SHORT).show();

                        }
                    } else {
                        SlideMenuSearchAndIndex slideMenuSearchAndIndex = (SlideMenuSearchAndIndex) context;
                        androidx.fragment.app.FragmentManager fragmentManager = slideMenuSearchAndIndex.getSupportFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        //check for extracted media file
                        String mediaFile = File.separator + "sdcard" + File.separator + "Android" + File.separator + "obb" + File.separator + ".media_files_228";
                        File check = new File(mediaFile);
                        if(check.exists()) {
                            ImageFragment imageFragment = new ImageFragment();
                            imageFragment.fileName = fileName;
                            imageFragment.id = hwdId;
                            fragmentTransaction.replace(R.id.indexResultfragment, imageFragment);
                            fragmentTransaction.addToBackStack("ImageFragment");
                            fragmentTransaction.commit();
                        }
                        else
                        {
                            Toast.makeText(context,R.string.unable_to_locate_media_files,Toast.LENGTH_SHORT).show();

                        }

                    }

                }
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }
    @JavascriptInterface
    public String PhrasalVerb()
    {
        String phrasalEntry="";
        if(DbHelper.phrasalverbHwd=="")
        {
            Log.d("db helper jsintrfac","null");

        }else {
            Log.d("db helper jsintrfac",DbHelper.phrasalverbHwd);
             phrasalEntry=DbHelper.phrasalverbHwd;
            DbHelper.phrasalverbHwd="";

        }
return phrasalEntry;

    }


    @JavascriptInterface
    public  void getSelectedWord(String selectedWord)throws JSONException {
        JSONObject js= new JSONObject(selectedWord);
        String word = js.getString("selectedString");
        JSONArray action = js.getJSONArray("actionToPerform");
        if (action.getString(0).equalsIgnoreCase("copy"))
            copy(word);
        else
            search(word);
    }

    /**
     * Copy to android's clipboard
     * @param word word to be copied
     */
    private void copy(String word) {
        android.content.ClipboardManager clipboard = (android.content.ClipboardManager)context.getSystemService(Context.CLIPBOARD_SERVICE);
        android.content.ClipData clip = android.content.ClipData.newPlainText("Word to Copy", word);
        clipboard.setPrimaryClip(clip);
        Toast.makeText(context,context.getString(R.string.copied_to_clipboard),Toast.LENGTH_SHORT).show();
//        t = ((LDOCEApp)context.getApplicationContext()).getTracker(
//                LDOCEApp.TrackerName.APP_TRACKER);
//        t.setScreenName("Copy to clipboard");
        Bundle params=new Bundle();
        params.putString("eventCategory","copy_to_clipboard");
        params.putString("eventAction","copytoclipboard_events");
        params.putString("eventLabel","Copy_contextual_Clicked");
        FirebaseWrapperScreens.EventLogs(params);
//        t.send(new HitBuilders.EventBuilder().setCategory("copy_to_clipboard").setAction("copytoclipboard_events").setLabel("Copy_contextual_Clicked").build());
    }

    /***
     * Place the selected text in the searchbar
     * @param word word to be search
     */
    private void search(final String word){
        Fragment searchFragment= ((SlideMenuSearchAndIndex)context).getSupportFragmentManager().findFragmentById(R.id.searchFragement);
        final SearchView searchView= (SearchView)searchFragment.getActivity().findViewById(R.id.search_bar);
        searchView.post(new Runnable() {
            @Override
            public void run() {
                searchView.setQuery(word, true);
                searchView.setIconified(false);
            }
        }) ;
    }

    public void callForAnalyticsTracking() {
        Log.d("frequency check es","freq_actions");

        Bundle params=new Bundle();
        params.putString("eventCategory","freq_button_action");
        params.putString("eventAction","freq_button_clicked");
        params.putString("eventLabel","frequencyButtonTapped");
        FirebaseWrapperScreens.EventLogs(params);
//        t = ((LDOCEApp)parentFragment.getActivity().getApplication()).getTracker(LDOCEApp.TrackerName.APP_TRACKER);
//        // Set the screen as Detail Page
//        t.setScreenName("Word Description Page");
//        // Build and send an Event.
//        t.send(new HitBuilders.EventBuilder()
//                .setCategory("freq_button_action")
//                .setAction("freq_button_clicked")
//                .setLabel("frequencyButtonTapped")
//                .build());
    }


}
