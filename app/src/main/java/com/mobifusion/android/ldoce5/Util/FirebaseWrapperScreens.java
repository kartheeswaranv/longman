package com.mobifusion.android.ldoce5.Util;

import android.os.Bundle;

import static com.mobifusion.android.ldoce5.Activity.LicenseCheck.mFirebaseAnalytics;
public class FirebaseWrapperScreens {

    public static void ScreenViewEvent(String screenName)
    {
        Bundle screenViewParams= new Bundle();
        screenViewParams.putString("screenName",screenName);
//        mFirebaseAnalytics.logEvent("screenview",screenViewParams);
    }


    public static void EventLogs(Bundle params )
    {
        mFirebaseAnalytics.logEvent("longman_events", params);
    }
}
