package com.mobifusion.android.ldoce5.Util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.mobifusion.android.ldoce5.Activity.WelcomeActivity;

/**
 * Created by sureshchandrababu on 25/06/15.
 */
public class NotificationClearance extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.w("LDOCE::::","CLEARANCE CALLED");
        WelcomeActivity.clearNotification(context);
    }
}
