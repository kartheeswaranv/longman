package com.mobifusion.android.ldoce5.model;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Bazi on 08/03/15.
 */
/**
* This class is used to store first level or second level index object completely. It has three object.
 * 1. indexViewContent is an list of all elements in the index list with 'IndexRowItem' type.
 * 2. sideIndexBarContent : elements in the side Index Bar, with SideIndexBarItems type.
 * 3. sectionContent : different section titles and its starting location in list
*
* */
public class IndexContent {

    public List<IndexRowItem> indexViewContent;
    public List<SideIndexBarItem>   sideIndexBarContent;
    public HashMap<String,Integer> sectionContent;

    public IndexContent()
    {

    }

    public IndexContent(List<IndexRowItem> indexViewContent, List<SideIndexBarItem> sideIndexBarContent, HashMap<String, Integer> sectionContent) {
        this.indexViewContent = indexViewContent;
        this.sideIndexBarContent = sideIndexBarContent;
        this.sectionContent = sectionContent;
    }
}