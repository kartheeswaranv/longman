package com.mobifusion.android.ldoce5.Activity;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StatFs;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

//import com.google.android.gms.analytics.GoogleAnalytics;
//import com.google.android.gms.analytics.HitBuilders;
//import com.google.android.gms.analytics.Tracker;

import com.mobifusion.android.ldoce5.R;
import com.mobifusion.android.ldoce5.Util.Decompress;
import com.mobifusion.android.ldoce5.Util.FirebaseWrapperScreens;
import com.mobifusion.android.ldoce5.Util.LDOCEApp;
import com.mobifusion.android.ldoce5.Util.NotificationClearance;
import com.mobifusion.android.ldoce5.Util.NotificationPublisher;
import com.mobifusion.android.ldoce5.Util.OnAsyncTaskCompleted;
import com.mobifusion.android.ldoce5.Util.Singleton;
import com.mobifusion.android.ldoce5.Util.Utils;
import com.mobifusion.android.ldoce5.model.Language;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;


public class WelcomeActivity extends BaseActivity implements AdapterView.OnItemSelectedListener {

    public static final String PREFS_NAME = "LDOCE6PrefsFile";
    public SharedPreferences settings;
    public ProgressDialog pDialog;
    public Button btnShowProgress;
    public static final int progress_bar_type = 0;
    TextView tv_welcome;
    TextView tv_choose_lang;
    TextView tv_you_can_change;
    Spinner language_spinner;
    Button start_Button;
    ProgressBar progressBar;
    AsyncTask<Void,Integer,Integer> task;
    ArrayList<Language> langs;
    Locale myLocale;
    int memorySize = 500,memoryRequired;
//    Tracker t;
    private ProgressBar mPB;
    private int STORAGE_PERMISSION_CODE = 23;
    private int SPLASH_SCREEN = 10000;
    static boolean cancelButton = false;
    boolean check;
    String filePath = File.separator + "sdcard" + File.separator + "Android" + File.separator + "obb" + File.separator + "com.mobifusion.android.ldoce5" + File.separator + "main.228.com.mobifusion.android.ldoce5.obb";
    String location = File.separator+"sdcard"+File.separator+"Android"+File.separator+"obb"+ File.separator;
    String fileToChange = File.separator+"sdcard"+File.separator+"Android"+File.separator+"obb"+ File.separator+"convertMp3ToDb";
    String rename = File.separator+"sdcard"+File.separator+"Android"+File.separator+"obb"+ File.separator+".media_files_228";
    String ukCheck = File.separator + "sdcard" + File.separator + "Android" + File.separator + "obb" + File.separator + ".media_files_228" + File.separator + "ldoce6_hwd_gb";
    String usCheck = File.separator + "sdcard" + File.separator + "Android" + File.separator + "obb" + File.separator + ".media_files_228" + File.separator + "ldoce6_hwd_us";
    String picCheck = File.separator + "sdcard" + File.separator + "Android" + File.separator + "obb" + File.separator + ".media_files_228" + File.separator + "ldoce6pics";
    boolean unZipFileSize = false;
    String extractFilePath = File.separator + "sdcard" + File.separator + "Android" + File.separator + "obb" + File.separator + ".media_files_228";
    File extractFile = new File(extractFilePath);
    File ukExtractFile = new File(ukCheck);
    File usExtractFile = new File (usCheck);
    File picExtractFile = new File (picCheck);

    private boolean mDBExtractRunning = false;
    private boolean mMediaExtractRunning = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        tv_welcome = (TextView) findViewById(R.id.tv_welcome);
        tv_choose_lang = (TextView) findViewById(R.id.tv_choose_lang);
        tv_you_can_change = (TextView) findViewById(R.id.tv_you_can_change);
        start_Button = (Button) findViewById(R.id.Start_Button);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.getIndeterminateDrawable().setColorFilter(0xffffffff, android.graphics.PorterDuff.Mode.SRC_ATOP);

        //creating Singleton instance for Font resource.
        Singleton.getInstance(this);

        //Initialise the tracker
//        t = ((LDOCEApp) getApplication()).getTracker(
//                LDOCEApp.TrackerName.APP_TRACKER);
//        t.setScreenName("Welcome Screen");
//
        FirebaseWrapperScreens.ScreenViewEvent("Welcome Page");
//        // Sending Hit to GA When new Hits are tracked
//        t.send(new HitBuilders.AppViewBuilder().build());

        //Getting DBExtracted status and hasLaunched once status from user default


        //NOTE: To update your DB, Increment the 'DbOverrideVersion' value in ANDROID MANIFEST
        //It will impose user to extract DB again
        //getting the DB version from the manifest and current version from application

    }
    AlertDialog.Builder adb;
    public void initializeDownloadUI() {
        setContentView(R.layout.obbdownloader_main);
        final String file_url = "http://eltapps.pearson.com/ldoce6app/ldoce6_obb/main.228.com.mobifusion.android.ldoce5.obb";
        adb = new AlertDialog.Builder(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            adb = new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            adb = new AlertDialog.Builder(this);
        }
        boolean isTablet = getResources().getBoolean(R.bool.isTablet);
        //checking whether the device is tablet or phone
        if (isTablet) {
            adb.setTitle(R.string.media_files);
            adb.setCancelable(false).setMessage("Media files missing, click OK to download them again");
            adb.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Boolean internet = isInternetConnection();
                    Log.d("internet connection", internet.toString());
                    if (internet) {
                        new DownloadFileFromURL().execute(file_url);
                        dialog.dismiss();
                    } else {
                        Toast.makeText(getApplicationContext(), "Please check for internet connection", Toast.LENGTH_LONG).show();
                        adb.show();
                    }
                }
            });
            adb.setNegativeButton("Download later", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
            adb.show();
        } else {
            adb.setTitle(R.string.media_files);
            adb.setCancelable(false)
                    .setMessage("Media files missing, click OK to download them again");
            adb.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {

                    Boolean internet = isInternetConnection();
                    Log.d("internet connection", internet.toString());
                    if (internet) {
                        new DownloadFileFromURL().execute(file_url);
                        dialog.dismiss();
                    } else {
                        Toast.makeText(getApplicationContext(), "Please check for internet connection", Toast.LENGTH_LONG).show();
                        adb.show();
                    }
                    adb.show();
                }
            });
            adb.setNegativeButton("Download later", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    cancelButton = true;
                    Log.d("slide", "slide");
                    finish();
                }
            });
            if (cancelButton) {
                cancelButton = false;
                finish();
            } else {
                adb.show();
            }
        }
    }
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case progress_bar_type:
                pDialog = new ProgressDialog(this);
                //Download will get started
                pDialog.setMessage("Downloading file. Please wait...");
                pDialog.setIndeterminate(false);
                pDialog.setMax(100);
                pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                pDialog.setCancelable(false);
                //Adding a goto app button to redirect to app with offline dictionary

                pDialog.setButton(DialogInterface.BUTTON_POSITIVE,"Go to App", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        progressBar.setVisibility(View.VISIBLE);
                        dialogInterface.dismiss();
//                        File mediaFile = new File(File.separator + "storage" +File.separator+"emulated"+File.separator+"0" + File.separator + "Android" + File.separator + "obb" + File.separator + ".media_files_228" );
//                        new DirectoryCleaner(mediaFile).clean();
//                        mediaFile.delete();
                        Intent intent =new Intent(WelcomeActivity.this,SlideMenuSearchAndIndex.class);
                        startActivity(intent);
                    }
                });
//                //Adding a cancel button to stop the download process
//                pDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        //Deleting the downloaded file if the download is cancelled
//
//                        File dir = new File(File.separator + "storage" +File.separator+"emulated"+File.separator+"0" + File.separator + "Android" + File.separator + "obb" + File.separator + "com.mobifusion.android.ldoce5" + File.separator);
//                        new DirectoryCleaner(dir).clean();
//                        dir.delete();
//                        File folderLoc = new File(File.separator + "storage" +File.separator+"emulated"+File.separator+"0" + File.separator + "Android" + File.separator + "obb" + File.separator + "com.mobifusion.android.ldoce5" );
//                       new DirectoryCleaner(folderLoc).clean();
//                        folderLoc.delete();
//                        dialog.dismiss();
//                        //Exit the application after download is cancelled
//                        finish();
//                    }
//                });
                pDialog.show();
                return pDialog;
            default:
                return null;
        }
    }
    public class DirectoryCleaner {
        private final File mFile;

        public DirectoryCleaner(File file) {
            mFile = file;
        }

        public void clean() {
            if (null == mFile || !mFile.exists() || !mFile.isDirectory()) return;
            for (File file : mFile.listFiles()) {
                delete(file);
            }
        }

        private void delete(File file) {
            if (file.isDirectory()) {
                for (File child : file.listFiles()) {
                    delete(child);
                }
            }
            file.delete();

        }
    }
    class DownloadFileFromURL extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog(progress_bar_type);
        }

        @Override
        protected String doInBackground(String... f_url) {
            int count;


            try {
                URL url = new URL(f_url[0]);

                URLConnection conection = url.openConnection();
                conection.connect();
                int lenghtOfFile = conection.getContentLength();
                InputStream input = new BufferedInputStream(url.openStream(), 8192);
                String file=File.separator + "sdcard" + File.separator + "Android" + File.separator + "obb" + File.separator + "com.mobifusion.android.ldoce5" + File.separator;
                OutputStream output = null;
                File dir=new File(file);
                    if (dir.exists() && dir.isDirectory()) {
                        check = true;
                        Log.d("directory","yes");
                         output = new FileOutputStream("/storage/emulated/0/Android/obb/com.mobifusion.android.ldoce5/main.228.com.mobifusion.android.ldoce5.obb");
                    }
                else {
                        File directory = new File(file);
                        if(directory.mkdir()) {
                            System.out.println("Directory created");
                            output = new FileOutputStream("/storage/emulated/0/Android/obb/com.mobifusion.android.ldoce5/main.228.com.mobifusion.android.ldoce5.obb");
                        }
                        else
                        {
                            System.out.println("Directory not created");
                        }
                        Log.d("directory","no");
                    }
//                OutputStream output = new FileOutputStream("/storage/emulated/0/main.228.com.mobifusion.android.ldoce5.obb");
                //OutputStream output = new FileOutputStream("/storage/emulated/0/Android/obb/com.mobifusion.android.ldoce5/main.228.com.mobifusion.android.ldoce5.obb");
                byte data[] = new byte[1024];
                long total = 0;
                while ((count = input.read(data)) != -1) {
                    total += count;
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));
                    output.write(data, 0, count);
                }
                output.flush();
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }
            return null;
        }
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            pDialog.setProgress(Integer.parseInt(progress[0]));
        }
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded
            dismissDialog(progress_bar_type);
            Intent intent = new Intent();
            intent.setClass(WelcomeActivity.this, SlideMenuSearchAndIndex.class);
            startActivity(intent);
        }
    }

    /***
     * To call the Notification function
     */
    public boolean isInternetConnection(){
        ConnectivityManager cn=(ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo nf=cn.getActiveNetworkInfo();
        boolean connection=false;
        if(nf != null && nf.isConnected()==true )
        {
            connection=true;
//            Toast.makeText(this, "Network Available", Toast.LENGTH_LONG).show();
}
        else
        {
            connection=false;

        }
    return connection;
    }
    void callNotification()
    {
        getNotification(this);
        scheduledNotificationClearance(this);
    }

    // Giving values to the array and thus used by spinner
    public ArrayList<Language> addLanguages()
    {
        ArrayList<Language> languages = new ArrayList<Language>();
        languages.add(new Language("English (UK)","en_GB"));
        languages.add(new Language("English (US)","en_US"));
        languages.add(new Language("لعربية","ar_EG"));
        languages.add(new Language("中国（简体）","zh_CN"));
        languages.add(new Language("한국의","ko_KR"));
        languages.add(new Language("日本の","ja_JP"));
        languages.add(new Language("português","pt_PT"));
        languages.add(new Language("pусский", "ru_RU"));
        return languages;
    }

    // Checking the default language to Spinner array values and returning to Spinner
    public Language getLanguageByCode (String code)
    {
        Language temp=langs.get(0);
        for(int i=0;i<langs.size();i++)
        {
            if(code.equals(langs.get(i).getLanguageCode()))
            {
                temp = langs.get(i);
                break;
            }
        }
        return temp;
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        //don't reload the current page when the orientation is changed

        super.onConfigurationChanged(newConfig);
    }


    public static boolean isTablet(Context context)
    {
        return (context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_welcome, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // Reloading the page when an option is selected in the spinner
    public void redraw()
    {
        //Assigning the text to the labels
        tv_welcome.setText(getString(R.string.welcome));
        tv_choose_lang.setText(getString(R.string.choose_your_language));
        start_Button.setText(getString(R.string.start));
        //check for task not equal to null and if the task is cancelled
        if( task!=null && task.isCancelled()) {
            //Replace X with memoryRequired
            String replacedString = Integer.toString((int) (memoryRequired));
            replacedString=getString(R.string.please_free_up_X_space).replace("X", replacedString);
            tv_you_can_change.setText(replacedString);
            System.out.println("Task is cancelled due to in-sufficient memory");
        }
        else {
            if (task == null) {
                tv_you_can_change.setText(R.string.you_can_change_these_settings_at_any_time);
                tv_you_can_change.setVisibility(View.VISIBLE);
                System.out.println("Task is Null");
            }
            else {
                tv_you_can_change.setText(getApplication().getResources().getString(R.string.extracting_database) + "  " + "0%");
                tv_you_can_change.setVisibility(View.VISIBLE);
                System.out.println("DB is Extracting again after Cancellation");
            }
        }
        if (start_Button.getVisibility() == View.VISIBLE){
            tv_you_can_change.setText(R.string.you_can_change_these_settings_at_any_time);
            tv_you_can_change.setVisibility(View.VISIBLE);
            System.out.println("DB is Extracted");
        }
    }

    /***
     * Creates and Schedules the notification
     * With the help of alarm manager
     * @param context
     */
    public static void getNotification(Context context){
        long scheduledTime = 0;
        //AlarmManager to get the system service
        AlarmManager alarmMgr = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent notificationPublisherIntent = new Intent(context,NotificationPublisher.class);
        PendingIntent alarmIntent = PendingIntent.getBroadcast(context, 6154, notificationPublisherIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        // Get the current Hours,Minutes,Seconds
        Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        int seconds = calendar.get(Calendar.SECOND);
        //Check for current time exceed 7:59:59 am
        if (hour<=7&&minute<=59&&seconds<=59)
        {
            //Before 8am
            //Calculate remaining hour till 8am
            hour = 7-hour; //to get remaining hours

        }else
        {
            //after 8am
            //Calculate remaining hour till 12 am midnight and add 8 hours to calculated hour
            hour = (23-hour)+8;
        }
        //converting minutes to seconds and then converting to milliseconds
        minute = 59-minute; //to get remaining minutes
        seconds = 59-seconds; //to get remaining minutes
        //Converting to milliseconds
        scheduledTime =((hour*60*60)+(minute*60)+seconds+10)*1000;
        // Set the alarm to start at approximately 8:00 a.m.
        // With setInexactRepeating(), you have to use one of the AlarmManager interval
        // constants--in this case, AlarmManager.INTERVAL_DAY.
        alarmMgr.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis() + scheduledTime,
                AlarmManager.INTERVAL_DAY, alarmIntent);
        Log.d("LDOCE", "Sceduled Time:" + (scheduledTime + calendar.getTimeInMillis()) + "");
    }

    /***
     * Cancel the already scheduled notification
     * When the word of the day of the switch is disabled
     * @param context
     */
    public static void cancelNotification(Context context){
        Intent notificationPublisherIntent = new Intent(context,NotificationPublisher.class);
        PendingIntent alarmIntent = PendingIntent.getBroadcast(context, 6154, notificationPublisherIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        //Cancel notificationClearacne service
        Intent notificationClearanceIntent = new Intent(context,NotificationPublisher.class);
        PendingIntent clearanceIntent = PendingIntent.getBroadcast(context, 6155, notificationClearanceIntent, PendingIntent.FLAG_CANCEL_CURRENT);


        AlarmManager alarmMgr = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        alarmMgr.cancel(alarmIntent);
        alarmMgr.cancel(clearanceIntent);
        alarmIntent.cancel();
        clearanceIntent.cancel();
        Log.v("Word-of-the-Day", "cancelling notification");
    }

    /***
     *Schdeule broadcast at 23 hour 59 minute to clear fired notifications
     * To be schedule along with 8 am notification.
     * @param context
     */
    public static void scheduledNotificationClearance(Context context)
    {
        AlarmManager alarmMgr = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent notificationPublisherIntent = new Intent(context,NotificationClearance.class);
        PendingIntent alarmIntent = PendingIntent.getBroadcast(context, 6155, notificationPublisherIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        // Get the current Hours,Minutes,Seconds
        Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        int seconds = calendar.get(Calendar.SECOND);
        hour = (23-hour);
        //converting minutes to seconds and then converting to milliseconds
        minute = 59-minute; //to get remaining minutes
        seconds = 59-seconds; //to get remaining minutes
        //Converting to milliseconds
        long scheduledTime =((hour*60*60)+(minute*60)+seconds)*1000;
        alarmMgr.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis() + scheduledTime,
                AlarmManager.INTERVAL_DAY, alarmIntent);
        Log.v("NOTIFICATION CLERANCE:", String.valueOf(scheduledTime+calendar.getTimeInMillis()));
    }

    public static void clearNotification(Context context){
        NotificationManager notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(123456789);
        Log.v("Word-of-the-Day", "Clearing Notification");
    }

    // App default language setting by the option selected in Spinner
    public void setLocale(String lang) {
        Utils.setApplicationLanguage(this,lang);
        settings = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("LDOCELanguage", lang);
        // Commit the edits!
        editor.apply();
        redraw();
    }

    @Override
    protected void onDestroy() {
        if(task!=null)
        {
            task.cancel(true);
        }
        super.onDestroy();
    }
    @Override
    public void onResume()
    {
        super.onResume();
        //check if the db file exists, if it exists, then do not extract.
        //also check if the overrideDB flag is set, if it is set, then we need to override the extraction process.
        File dir = this.getFilesDir();
        File dbFile = new File(dir,"ldoce.sqlite");
        settings = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        boolean dbExtracted = settings.getBoolean("dbExtracted", false);

        ApplicationInfo ai;
        int dbOverrideAppValue=0;
        int dbOverridePreferenceValue=0;
        try {
            ai = getApplicationContext().getPackageManager().getApplicationInfo(getApplicationContext().getPackageName(), PackageManager.GET_META_DATA);
            int value = ai.metaData.getInt("DbOverrideVersion", 0);
            dbOverrideAppValue=value;
            int value1 = settings.getInt("DbOverrideVersion", 0);
            dbOverridePreferenceValue=value1;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        if(dbOverrideAppValue>dbOverridePreferenceValue){
            dbExtracted=false;
        }

        if(!dbExtracted) {
            if (!mDBExtractRunning) {
                mDBExtractRunning = true;
                OnAsyncTaskCompleted callback = new OnAsyncTaskCompleted() {

                    @Override
                    public void onPreExecution(Context context, Boolean isStarted) {
                        progressBar.setVisibility(View.VISIBLE);
                        System.out.println("onPreExecution");
                    }

                    @Override
                    public void onDoInBackground(Context context, Boolean isStarted) {
                        // TODO: Write methods to handle background tasks during database extraction if needed.
                        System.out.println("onDoInBackground");
                    }

                    @Override
                    public void unzipExtractionFile(Context context, Boolean isStarted) {
                        // Used to update the textview while index creation is on progress
                        tv_you_can_change.setText(context.getResources().getString(R.string.extracting_unzip));
                        tv_you_can_change.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onProgressUpdation(Context context, Boolean isStarted, Integer... values) {
                        tv_you_can_change.setText(context.getResources().getString(R.string.extracting_database) + "  " + values[0] + "%");
                        tv_you_can_change.setVisibility(View.VISIBLE);
                        progressBar.setProgress(values[0]);
                    }

                    @Override
                    public void onIndexCreation(Context context, Boolean isStarted) {
                        // Used to update the textview while index creation is on progress
                        tv_you_can_change.setText(context.getResources().getString(R.string.creating_indexes_on_database));
                        tv_you_can_change.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onImportingBookmark(Context context, Boolean isStarted) {
                        tv_you_can_change.setText(context.getResources().getString(R.string.importing_bookmarks));
                        tv_you_can_change.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onPostExecution(Context context, Boolean isStarted) {
                        Log.v("Welcome Activity", "Extraction Completed Successfully");
                        tv_you_can_change.setText(context.getResources().getString(R.string.you_can_change_these_settings_at_any_time));
                        tv_you_can_change.setVisibility(View.VISIBLE);
                        // After the execution make the progress bar invisible
                        progressBar.setVisibility(View.INVISIBLE);
                        // After the execution make visible of start button
                        start_Button.setVisibility(View.VISIBLE);
                        // After the execution make language spinner visible
                        language_spinner.setVisibility(View.VISIBLE);
                        // After the execution make choose language label visible
                        tv_choose_lang.setVisibility(View.VISIBLE);
                        System.out.println("onPostExecution");
                        mDBExtractRunning = false;
                    }
                };

                task = Decompress.getInstance(this, callback);
                if (task != null) {
                    if (task.getStatus() == AsyncTask.Status.RUNNING)
                        Toast.makeText(this, " Extraction already in progress", Toast.LENGTH_SHORT);
                    else {
                        StatFs stat = new StatFs(Environment.getDataDirectory().getPath());
                        long bytesAvailable = (long) stat.getBlockSize() * (long) stat.getAvailableBlocks();
                        long megAvailable = bytesAvailable / (1024 * 1024);
                        if (megAvailable < memorySize) {
                            memoryRequired = (int) (memorySize - megAvailable);
                            task.cancel(true);
                        } else {
                            FragmentManager fragmentManager = getSupportFragmentManager();
                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                            TourBaseFragment tutorialBaseFragment = new TourBaseFragment();
                            tutorialBaseFragment.parent = this;
                            fragmentTransaction.replace(R.id.tour_base_layout, tutorialBaseFragment).addToBackStack("Tour and Tutorial Page");
                            fragmentTransaction.commit();
                            task.execute();
//                    if(!hasLaunchedOnce){
                            //Displaying tour and tutorial

                        }
                    }
                }
            }
        }
        else
        {
            //NOTE: No need to extract database, already extracted.

            //make start button visible.
            /*start_Button.setVisibility(View.VISIBLE);
            //make language spinner visible
            language_spinner.setVisibility(View.VISIBLE);
            //showing the choose language label
            tv_choose_lang.setVisibility(View.VISIBLE);*/

            try
            {
                SQLiteDatabase db = SQLiteDatabase.openOrCreateDatabase(Utils.CoreDBFilePathAndName, null);
            }
            catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
            }
        }
        //new Decompress((TextView)findViewById(R.id.textLabel),this).execute();
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        String langCode =((Language)adapterView.getItemAtPosition(i)).getLanguageCode();
        //Toast.makeText(adapterView.getContext(),langCode,Toast.LENGTH_SHORT).show();
        setLocale(langCode);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
    }

    @Override
    protected void onStart() {
        super.onStart();
        settings = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        boolean dbExtracted = settings.getBoolean("dbExtracted", false);
        boolean hasLaunchedOnce = settings.getBoolean("hasLaunchedOnce", false);
        ApplicationInfo ai;
        int dbOverrideAppValue=0;
        int dbOverridePreferenceValue=0;
        try {
            ai = getPackageManager().getApplicationInfo(getPackageName(), PackageManager.GET_META_DATA);
            int value = ai.metaData.getInt("DbOverrideVersion", 0);
            dbOverrideAppValue=value;
            int value1 = settings.getInt("DbOverrideVersion", 0);
            dbOverridePreferenceValue=value1;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        //checking if db is updated or not
        if(dbOverrideAppValue>dbOverridePreferenceValue){
            //if db is updated,Execute the extration process again
            dbExtracted=false;
        }
        if(dbExtracted && hasLaunchedOnce )//&& check
        {
            navigateToIndexPage();
        }
        else {
            // Getting text view and button ids

            // Assign MundoSansPro as Default FontType
            tv_welcome.setTypeface(Singleton.getMundoSansPro());
            tv_choose_lang.setTypeface(Singleton.getMundoSansPro());
            tv_you_can_change.setTypeface(Singleton.getMundoSansPro());
            start_Button.setTypeface(Singleton.getMundoSansPro());
            // Giving white color to Progress bar


            //Adding items to the Language Spinner
            language_spinner = (Spinner) findViewById(R.id.language_spinner);
            langs = addLanguages();
            // Giving values for Spinner
            ArrayAdapter<Language> dataAdapter = new ArrayAdapter<Language>(this, R.layout.ldoce_spinner, R.id.spinner_textview, langs) {
                @Override
                public View getView(int position, View convertView, ViewGroup parent) {
                    View v = super.getView(position, convertView, parent);
                    TextView spinnerTextView = (TextView) v.findViewById(R.id.spinner_textview);
                    spinnerTextView.setTypeface(Singleton.getMundoSansPro());
                    return v;
                }

                @Override
                public View getDropDownView(int position, View convertView, ViewGroup parent) {

                    LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View row = inflater.inflate(R.layout.ldoce_spinner_dropdown_item, parent, false);
                    TextView label = (TextView) row.findViewById(R.id.spinner_dropdown_textview);
                    label.setText(langs.get(position).getLanguageName());
                    label.setTypeface(Singleton.getMundoSansPro());
                    return row;
                }
            };
            dataAdapter.setDropDownViewResource(R.layout.ldoce_spinner_dropdown_item);
            language_spinner.setAdapter(dataAdapter);
            language_spinner.setOnItemSelectedListener(this);


            String currentLangCode;
            if (hasLaunchedOnce)
                currentLangCode = settings.getString("LDOCELanguage", "en_GB");
            else
                currentLangCode = getResources().getConfiguration().locale.getDefault().toString();
            // When the system locale is not null
            if (!currentLangCode.equals(null)) {
                int position = langs.indexOf(getLanguageByCode(currentLangCode));
                //int position = dataAdapter.getPosition(getLanguageByCode(currentLangCode));
                language_spinner.setSelection(position);
                setLocale(currentLangCode);
            }
//
            //dbExtracted = settings.getBoolean("dbExtracted", false);
            //Start button to navigate to first level
            //Intent from welcome to first level activity
            start_Button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Setting has launched once user default
                    // Calling the shortcut function to create the app shortcut
                    // addApplicationShortcut();
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putBoolean("hasLaunchedOnce", true);
                    editor.commit();
                    // Commit the edits!
                    // GetNotification();
                    SharedPreferences preferences = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
                    if (preferences.getBoolean("Word-of-the-Day", true)) {
                        callNotification();
                    }
                    editor.apply();

                    navigateToIndexPage();
                }

            });



        }
        // Sending Hit to GA as Start of Activity
//        GoogleAnalytics.getInstance(WelcomeActivity.this).reportActivityStart(this);

    }

    @Override
    protected void onStop() {
        super.onStop();
        // Sending Hit to GA as End of Activity
//        GoogleAnalytics.getInstance(WelcomeActivity.this).reportActivityStop(this);
    }

    /**
     *Method for Creating automatic shortcut after app getting installed
     */
    private void addApplicationShortcut() {
        //Adding shortcut for Index Activity
        Intent shortcutIntent = new Intent(getApplicationContext(), WelcomeActivity.class);
        shortcutIntent.setAction(Intent.ACTION_MAIN);
        Intent addIntent = new Intent();
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, getString(R.string.app_name));
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE, Intent.ShortcutIconResource.fromContext(getApplicationContext(),R.drawable.ic_launcher));
        addIntent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
        getApplicationContext().sendBroadcast(addIntent);
    }
    boolean checkForFile(){
        if (!check) {
            try {
                FileInputStream fin = new FileInputStream(filePath);
                ZipInputStream zin = new ZipInputStream(fin);
                if (!zin.toString().isEmpty()) {
                    check = true;
                }
                zin.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return check;
    }

    /**
     * METHOD TO NAVIGATE TO DETAIL TO PAGE
     */
    public void navigateToIndexPage(){
        String hwd = getIntent().getStringExtra("Headword");
        String hwdId = getIntent().getStringExtra("HeadwordIdIs");
        Intent intent = new Intent();
        intent.setClass(this, SlideMenuSearchAndIndex.class);
        intent.putExtra("Headword", hwd);
        intent.putExtra("HeadwordIdIs",hwdId);
        System.out.println("welcome Headword is" + hwd + " welcome Headword Id is" + hwdId);
//        startActivity(intent);
        try{
            if (extractFile.exists()) {
                if(ukExtractFile.exists() && usExtractFile.exists() && picExtractFile.exists()) {
                    try {
                        if (checkForFile()) {
                            startActivity(intent);
                            finish();
                        } else {
                            initializeDownloadUI();
                        }

                    } catch (Exception e) {
                        Log.e("File Check", e.getMessage());
                    }
                }
                else if(extractFile.exists()) {
                    deleteDir(extractFile);
                    checkSize();
                }
            }
            else
            {
                if(!mMediaExtractRunning) {
                    checkSize();
                }

            }

        }
        catch (Exception e) {
            Log.e("File Check", e.getMessage());
        }

    }

    //Remove tour and tutorial
    public void removeTourAndTutorial(){
        FragmentManager fragmentManager = getSupportFragmentManager();
        TourBaseFragment tourBaseFragment=(TourBaseFragment)fragmentManager.findFragmentById(R.id.tour_base_layout);
        if(tourBaseFragment!=null) {
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.remove(tourBaseFragment).commit();
        }
    }
    private long getFolderSize(File folder) {
        long length = 0;
        File[] files = folder.listFiles();

        int count = files.length;

        for (int i = 0; i < count; i++) {
            if (files[i].isFile()) {
                length += files[i].length();
            }
            else {
                length += getFolderSize(files[i]);
            }
        }
        return length;
    }

    public void whenGetFolderSizeRecursive_thenCorrect() {

        long expectedSize = 498672710;

        File file1 = new File(fileToChange);
        File file2 = new File(rename);
        boolean success = file1.renameTo(file2);
        if(success) {
            File folder = new File(File.separator + "sdcard" + File.separator + "Android" + File.separator + "obb" + File.separator + "media_files_228");
            long size = getFolderSize(folder);

            if (expectedSize == size) {
                unZipFileSize = true;
                File mediaFile=new File(File.separator + "sdcard" + File.separator + "Android" + File.separator + "obb" + File.separator + ".media_files_228");
                folder.renameTo(mediaFile);
            } else {
                folder.delete();
                initializeDownloadUI();
                checkSize();
            }
        }
    }
    public void checkSize(){

//        setContentView(R.layout.extract_media_file);

        StatFs statFs = new StatFs(Environment.getExternalStorageDirectory().getAbsolutePath());
        StatFs unSd = new StatFs(Environment.getRootDirectory().getAbsolutePath());
        long blockSize = statFs.getBlockSize();
        long totalSize = statFs.getBlockCount()*blockSize;
        long availableSize = statFs.getAvailableBlocks()*blockSize;
        long freeSize = statFs.getFreeBlocks()*blockSize;
        System.out.println("blockSize= "+blockSize);
        System.out.println("totalSize= "+totalSize);
        System.out.println("availableSize= "+availableSize);
        System.out.println("freeSize= "+freeSize );

        if (freeSize >= 1e+9) {
            mMediaExtractRunning = true;
            AsyncTaskRunner runner = new AsyncTaskRunner();
            runner.execute();
        } else {
//            Toast.makeText(context, R.string.insufficent_storage, Toast.LENGTH_LONG).show();
            dialogforExtract();
        }

    }
    public void unzip (){
        String filePath = File.separator+"sdcard"+File.separator+"Android"+File.separator+"obb"+File.separator+"com.mobifusion.android.ldoce5"+File.separator+"main.228.com.mobifusion.android.ldoce5.obb";
// Unzip the file and place it in another location
        try
        {
            FileInputStream fin = new FileInputStream(filePath);
            ZipInputStream zin = new ZipInputStream(fin);
            ZipEntry ze = null;
            while ((ze = zin.getNextEntry()) != null)
            {
                Log.v("Decompress", "Unzipping " + ze.getName());

                if(ze.isDirectory())
                {
                    dirChecker(ze.getName());
                }
                else
                {

                    FileOutputStream fout = new FileOutputStream(location + ze.getName());
                    BufferedOutputStream bout = new BufferedOutputStream(fout);
                    byte[] buffer = new byte[8192];
                    int len;
                    while ((len = zin.read(buffer)) != -1)
                    {
                        bout.write(buffer, 0, len);
//                        progressBar.setVisibility(View.VISIBLE);
//                        publishProgress(progress);
                    }
                    bout.close();
                    fout.close();
                    zin.closeEntry();

                }

            }
            whenGetFolderSizeRecursive_thenCorrect();
            zin.close();

        }
        catch(Exception e)
        {
            Log.e("Decompress", "unzip", e);
        }

    }
    private void dirChecker(String dir) {
        File f = new File(location + dir);

        if(!f.isDirectory()) {
            f.mkdirs();
        }
    }

    public void dialogforExtract(){

        AlertDialog.Builder adb = new AlertDialog.Builder(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            adb = new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            adb = new AlertDialog.Builder(this);
        }

        adb.setCancelable(false)
                .setMessage(
                        R.string.insufficent_storage);

        adb.setNegativeButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                finish();
            }
        });
        adb.show();

    }
    public boolean deleteDir(File dir) {
        tv_you_can_change.setText(getResources().getString(R.string.extracting_unzip));
        tv_you_can_change.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.VISIBLE);
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {

                boolean success = deleteDir
                        (new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        return dir.delete();
//        System.out.println("The directory is deleted.");
    }
    private class AsyncTaskRunner extends AsyncTask<Boolean, Boolean, Boolean> {


        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            mMediaExtractRunning = false;
            String hwd = getIntent().getStringExtra("Headword");
            String hwdId = getIntent().getStringExtra("HeadwordIdIs");
            Intent intent = new Intent();
            intent.setClass(WelcomeActivity.this, SlideMenuSearchAndIndex.class);
            intent.putExtra("Headword", hwd);
            intent.putExtra("HeadwordIdIs",hwdId);

            startActivity(intent);
            finish();
        }

        @Override
        protected Boolean doInBackground(Boolean... params) {
            unzip();
            return true;
        }

        /*
                 * (non-Javadoc)
                 *
                 * @see android.os.AsyncTask#onPreExecute()
                 */
        @Override
        protected void onPreExecute() {
            // Things to be done before execution of long running operation. For
            // example showing ProgessDialog
//            tv_you_can_change.setText(getResources().getString(R.string.extracting_unzip));
//            tv_you_can_change.setVisibility(View.VISIBLE);
//            progressBar.setVisibility(View.VISIBLE);
            Intent intent=new Intent(WelcomeActivity.this,SlideMenuSearchAndIndex.class);
            startActivity(intent);
        }

        /*
         * (non-Javadoc)
         *
         * @see android.os.AsyncTask#onProgressUpdate(Progress[])
         */

    }
}

