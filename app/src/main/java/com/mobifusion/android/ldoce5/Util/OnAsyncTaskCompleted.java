package com.mobifusion.android.ldoce5.Util;

import android.content.Context;

/**
 * Created by vthanigaimani on 1/18/15.
 */
public interface OnAsyncTaskCompleted {
    public void onPreExecution(Context context, Boolean isStarted);
    public void onDoInBackground(Context context, Boolean isStarted);
    public void onProgressUpdation(Context context, Boolean isStarted,Integer... values);
    public void unzipExtractionFile(Context context, Boolean isStarted);
    public void onIndexCreation(Context context, Boolean isStarted);
    public void onPostExecution(Context context, Boolean isStarted);
    public void onImportingBookmark(Context context, Boolean isStarted);
}
