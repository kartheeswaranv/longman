package com.mobifusion.android.ldoce5.Util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.mobifusion.android.ldoce5.Activity.WelcomeActivity;

/**
 * Created by sureshchandrababu on 17/06/15.
 */
public class NotificationBootReceiver extends BroadcastReceiver {
    public static final String PREFS_NAME = "LDOCE6PrefsFile";
    @Override
    public void onReceive(Context context, Intent intent) {
        SharedPreferences preferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        Log.w("LDOCE:BOOTRECEIVER:", (preferences.getBoolean("Word-of-the-Day", true) ? "ON" : "OFF"));

        if(preferences.getBoolean("Word-of-the-Day",true)) {
            WelcomeActivity.getNotification(context);
            //Re-schedule notification clearance service
            WelcomeActivity.scheduledNotificationClearance(context);
        }


    }
}
