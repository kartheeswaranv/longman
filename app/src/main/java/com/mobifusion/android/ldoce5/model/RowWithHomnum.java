package com.mobifusion.android.ldoce5.model;

/**
 * Created by vthanigaimani on 3/28/15.
 */
public class RowWithHomnum extends IndexRowItem  {
    private String hwd;
    private String hwdId;
    private String homnum;

    public RowWithHomnum(String hwd, String hwdId, String homnum) {
        this.hwd = hwd;
        this.hwdId = hwdId;
        this.homnum = homnum;
    }
    public RowWithHomnum(String hwd, String hwdId, String homnum,boolean frequnet) {
        this.hwd = hwd;
        this.hwdId = hwdId;
        this.homnum = homnum;
        super.setIsFrequent(frequnet);

    }
    public String getHomnum() {
        return homnum;
    }

    public String getHwd() {
        return hwd;

    }

    public String getHwdId() {
        return hwdId;
    }


    @Override
    public boolean equals(Object o) {
        RowWithHomnum obj=(RowWithHomnum)o;
        if(obj.getHwd().equalsIgnoreCase(this.hwd) && obj.getHomnum().equalsIgnoreCase(this.homnum) && obj.getHwdId().equalsIgnoreCase(this.hwdId))
            return true;
        return false;
    }
}
