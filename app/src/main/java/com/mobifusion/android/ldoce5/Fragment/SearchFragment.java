package com.mobifusion.android.ldoce5.Fragment;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
//import com.google.android.gms.analytics.HitBuilders;
//import com.google.android.gms.analytics.Tracker;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.mobifusion.android.ldoce5.Activity.DbHelper;
import com.mobifusion.android.ldoce5.Util.FirebaseWrapperScreens;
import com.mobifusion.android.ldoce5.Util.Utils;
import com.mobifusion.android.ldoce5.R;
import com.mobifusion.android.ldoce5.Util.AlphabetListAdapter;
import com.mobifusion.android.ldoce5.Util.LDOCEApp;
import com.mobifusion.android.ldoce5.Util.Singleton;
import com.mobifusion.android.ldoce5.model.SearchRowItem;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import static android.widget.SearchView.OnCloseListener;
import static android.widget.SearchView.OnQueryTextListener;



public class SearchFragment extends Fragment {

    SearchView searchView;

    private OnSearchResultListener mListener;
//    Tracker t;
    Timer timer;
    TimerTask timerTask;
    public static final String PREFS_NAME = "LDOCE6PrefsFile";
    Button buttonSearch;

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Get the tracker.
//    t = ((LDOCEApp) getActivity().getApplication()).getTracker(
//                LDOCEApp.TrackerName.APP_TRACKER);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_search, null);

        //Font size
        SharedPreferences fontSizePreferences =  getActivity().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        int fontSizeValue = fontSizePreferences.getInt("FONT_VALUE", 0);
        searchView=(SearchView)v.findViewById(R.id.search_bar);
        searchView.setOnQueryTextListener(searchOnQueryListener);
        searchView.setOnCloseListener(searchOnCloseListener);
        searchView.setOnSearchClickListener(searchOnClickListener);
        //Setting the font for searchview's text
        int id = searchView.getContext().getResources().getIdentifier("android:id/search_src_text", null, null);
        TextView textView = (TextView) searchView.findViewById(id);
        //Apply font size value
        textView.setTextSize(18+fontSizeValue);
        textView.setTextColor(Color.parseColor("#38678A"));
        textView.setTypeface(Singleton.getMundoSansPro_Medium());

        // To remove blue focus line
        int searchPlateId = searchView.getContext().getResources().getIdentifier("android:id/search_plate", null, null);
        View searchPlateView = searchView.findViewById(searchPlateId);
        if (searchPlateView != null) {
            searchPlateView.setBackgroundColor(Color.WHITE);
        }

        // Button for enabling touch on full searchBar
        buttonSearch = (Button)v.findViewById(R.id.button_Search);
        buttonSearch.setBackgroundColor(Color.TRANSPARENT);

            buttonSearch.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Triggering the touch to open
                    searchView.setIconified(false);
                }
            });

        return v;
    }


   private OnCloseListener searchOnCloseListener = new OnCloseListener() {
       @Override
       public boolean onClose() {
           mListener.onSearchClose(true);
           // Enabling the Button view when search is Stopped
           buttonSearch.setVisibility(View.VISIBLE);
           return false;
       }
   };


    public View.OnClickListener searchOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // Hiding the Button view when search is Stopped
            buttonSearch.setVisibility(View.GONE);
        }
    };

   public OnQueryTextListener searchOnQueryListener = new OnQueryTextListener() {

       // Handler to Update the UI
       final Handler handler = new Handler(){
           @Override
           public void handleMessage(Message msg) {
               super.handleMessage(msg);
               String searchText = ((SearchView)getActivity().findViewById(R.id.search_bar)).getQuery().toString();
               //while updating ListView -  check the SearchView text
               //When SearchView text is empty do not update the searchResults ListView
               if(!searchText.equals(""))
                   Log.d("results found ","querry text");

               mListener.onSearchResult((LinkedHashSet<SearchRowItem>)msg.obj);
           }
       };
       //Called when the user submits the query - clicking search in Keypad
       @Override
       public boolean onQueryTextSubmit(String query) {

           if(query!=null && !query.isEmpty() && TextUtils.getTrimmedLength(query)>0) {
               // When Keypad Search button is pressed - retrieve the search results and store it in a Set
               LinkedHashSet<SearchRowItem> results = searchRetrievalResults(query);
               if(!results.isEmpty()) {
                   // Convert the Set results into an array and access it
                   Object[] resultsArray = results.toArray();
                   // Take the first result - to be navigate to Detail Page
                   SearchRowItem item=(SearchRowItem)resultsArray[0];
                   // Get Hwd and HwdId for the first result
                   String firstElement = item.getHwd();
                   String firstElementId = item.getHwdId();

                   // If the result is no matches found and HwdId is Empty - Do not navigate to details page
                   if(firstElement != "No Matches Found" && firstElementId!="") {
                       FragmentManager fm = getFragmentManager();
                       // Check the BackStack COunt
                       if(fm.getBackStackEntryCount()>0) {
                           // Create an instance of Detail Page fragment
                           Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.detailPageFragment);
                          // DetailPageFragment fragment = (DetailPageFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.detailPageFragment);
                           // Main Point: have to check whether currently detail page fragment is visible or not
                           if(fragment!=null && fragment instanceof DetailPageFragment) {
                               // If currently detailsPage fragment is Visible, get the current displayed headword
                               DetailPageFragment detailPageFragment = (DetailPageFragment) fragment;
                               // Get the current displayed headword
                               String previousWord = detailPageFragment.headword;
                               if(Utils.isTablet(getActivity().getApplicationContext())) {
                                   // Check if the current clicked word and already displayed word is same - if same don't navigate
                                   if(!previousWord.equals(firstElement)){
                                       Utils.navigateToDetailPage(getActivity(), firstElement, firstElementId, "", "");
                                   }
                               }else {
                                   if(!previousWord.equals(firstElement)) {
                                       // If phone: hide the search results and display the detail page
                                       Utils.navigateToDetailPage(getActivity(), firstElement, firstElementId, "", "");
                                       final ListView searchListView = (ListView)getActivity().findViewById(R.id.searchResultListView);
                                       // iconify the searchBar after detail page navigation
                                       freezeSearchBar();
                                   }
                               }
                           }else {
                               if (Utils.isTablet(getActivity().getApplicationContext())) {
                                   Utils.navigateToDetailPage(getActivity(), firstElement, firstElementId, "", "");
                               } else {
                                   Utils.navigateToDetailPage(getActivity(), firstElement, firstElementId, "", "");
                                   final ListView searchListView = (ListView)getActivity().findViewById(R.id.searchResultListView);
                                   searchListView.setVisibility(View.INVISIBLE);
                                   freezeSearchBar();
                               }
                           }
                       }else {
                           if (Utils.isTablet(getActivity().getApplicationContext())) {
                               Utils.navigateToDetailPage(getActivity(), firstElement, firstElementId, "", "");
                           }else {
                               Utils.navigateToDetailPage(getActivity(), firstElement, firstElementId, "", "");
                               final ListView searchListView = (ListView)getActivity().findViewById(R.id.searchResultListView);
                               searchListView.setVisibility(View.INVISIBLE);
                           }
                       }
                   }

               }
           }
               return false;
       }

       // Function is mainly used to Iconfiy and clear the SearchBar Value
       public void freezeSearchBar(){
           final Fragment searchFragment= getActivity().getSupportFragmentManager().findFragmentById(R.id.searchFragement);
           final SearchView searchBar = (SearchView)searchFragment.getView().findViewById(R.id.search_bar);
           searchBar.setIconified(true);
           searchBar.setQuery("", false);
           searchBar.setFocusable(false);
           searchBar.setIconified(true);
       }


       //Called when the query text is changed by the user. - when user starts tying in SearchView
       @Override
       //If keyboard is in visible state means, First we need to dismiss the keyboard and show the slide menu
       public boolean onQueryTextChange(String newText) {

           // Hiding the Button view when search is started
           buttonSearch.setVisibility(View.GONE);

           SharedPreferences fontSizePreferences =  getActivity().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
           int fontSizeValue = fontSizePreferences.getInt("FONT_VALUE", 0);
           int id = searchView.getContext().getResources().getIdentifier("android:id/search_src_text", null, null);
           TextView textView = (TextView) searchView.findViewById(id);
           //Apply font size value
           textView.setTextSize(18+fontSizeValue);
           final String enteredText=newText;
           //check for search text is null
           if (newText.equals(""))
           {
               //If it is null, close the search and it takes to the previous page
               mListener.onSearchClose(true);
               return true;
           }

           // Create a separate Thread for Search -  This will run in Background Process,without blocking UI
           // Timer timer = new Timer();
            final Thread searchThread= new Thread(new Runnable() {
               @Override
               public void run() {
                   try {

                       System.out.println(enteredText);
                       Message msg = new Message();
                       //After the Results are Fetched -  UI needs to be updated
                       msg.obj = searchRetrievalResults(enteredText);

                       //send a message to Handler to update the UI
                       //msg.obj contains the result Set of Search Results
                       handler.sendMessage(msg);
                        }

                   catch (Throwable t )
                   {
                       // End the Search Thread
                       Log.i("SearchThreadException","Thread Exception"+t);
                   }
               }
           });

           // Start the thread task
           searchThread.start();
           // Stop a timer event
           stopTimer();
           // Start a timer event
           startTimer(enteredText);

           return false;
       }

   };

    // Method to start a timer event
    public void startTimer(final String enteredText){
       timer = new Timer();
       timerTask = new TimerTask() {
            @Override
            public void run() {
                sendASearchEvent(enteredText);
            }
        };
        timer.schedule(timerTask,500);
    }

    // Method to stop a timer event
    public void stopTimer() {
        if (timer != null)
        {
            timer.cancel();
            timer = null;
        }
    }

    // Method to send the searched word to GA
    public void sendASearchEvent(String enteredText){
        Log.w("Search Event: ",enteredText);
        Bundle params=new Bundle();
        params.putString("eventCategory","search");
        params.putString("eventAction","search_word");
        params.putString("eventLabel",enteredText);
        FirebaseWrapperScreens.EventLogs(params);

//        t.send(new HitBuilders.EventBuilder().setCategory("search").setAction("search_word").setLabel(enteredText).build());
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getActivity().getMenuInflater().inflate(R.menu.menu_search, menu);
        return true;
    }

    public LinkedHashSet<SearchRowItem> searchRetrievalResults(String enteredText)
        {
            //DbHelper: This File is used in Retrieving the result through Query
            DbHelper db= new DbHelper();
            //LinkedHashSet - does not allow Duplicates, and it maintains the Order in which it is entered
            LinkedHashSet<SearchRowItem> setForSearch=new LinkedHashSet<>();

            List<SearchRowItem> items=DbHelper.getSearchResult(enteredText);
            if(!items.isEmpty()) {
                for (SearchRowItem item : items) {
                    setForSearch.add(item);
                }
            }
            //pass all the results which is obtained from all 4 functions(excluding Duplicate Entries)
            //return setForSearch;
            //mListener.onSearchResult(setForSearch);

            if(items.isEmpty())
            {
                Resources res = getResources();
                String noResults = res.getString( R.string.no_matches_found);
                setForSearch.add(new SearchRowItem(noResults,"",false) );
                return setForSearch;
            }
         return setForSearch;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    /**
     * Called when a fragment is first attached to its activity.
     * {@link #onCreate(android.os.Bundle)} will be called after this.
     *
     * @param activity
     */
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnSearchResultListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnSearchResultListener");
        }
    }
    /*
     *Setting up Custom Listener for interaction between Activity and Fragment
     * OnSearchResult(List) sends search result to Main activity
     * OnSearchClose(Bool) sends search view is closed or not to Main Activity
     */
    public interface OnSearchResultListener
    {
        public void onSearchResult(Set result);
        public void onSearchClose(Boolean isClosed);
    }

    public void refreshUIElements(){
        SharedPreferences fontSizePreferences =  getActivity().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        int fontSizeValue = fontSizePreferences.getInt("FONT_VALUE", 0);
        searchView = (SearchView)getActivity().findViewById(R.id.search_bar);
        int id = searchView.getContext().getResources().getIdentifier("android:id/search_src_text", null, null);
        TextView textView = (TextView) searchView.findViewById(id);
        //Apply font size value
        textView.setTextSize(18 +fontSizeValue);
        ListView searchListView=(ListView)getActivity().findViewById(R.id.searchResultListView);
        if (searchListView!=null) {
            AlphabetListAdapter adapter = (AlphabetListAdapter) searchListView.getAdapter();
            if (adapter!=null)
            adapter.notifyDataSetChanged();
        }
    }
}
