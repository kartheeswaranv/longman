package com.mobifusion.android.ldoce5.Util;

import android.content.Context;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

/**
 * Created by vthanigaimani on 4/8/15.
 */
public class XSLTransformHelper  {
    public static String getHTMLForXML(String xml,Context ctx) throws TransformerException, IOException {
        Source xmlSource = new StreamSource(new StringReader(xml));
        Source xsltSource;
        if (Utils.isTablet(ctx))
            xsltSource = new StreamSource(ctx.getAssets().open("www/masterxsltablet.xsl"));
        else
            xsltSource = new StreamSource(ctx.getAssets().open("www/masterxslphone.xsl"));

        TransformerFactory tFactory = TransformerFactory.newInstance();
        Transformer transformer = tFactory.newTransformer(xsltSource);

        StringWriter writer = new StringWriter();
        StreamResult result = new StreamResult(writer);
        transformer.transform(xmlSource, result);
        String html = writer.toString();

        return html;
    }

}
