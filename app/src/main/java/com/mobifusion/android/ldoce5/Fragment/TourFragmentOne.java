package com.mobifusion.android.ldoce5.Fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.mobifusion.android.ldoce5.R;
import com.mobifusion.android.ldoce5.Util.Singleton;

/**
 * Created by ramkumar on 13/07/15.
 */
public class TourFragmentOne extends Fragment {

    public static final String PREFS_NAME = "LDOCE6PrefsFile";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.tour_fragment_one, null);
        return v;
    }

    // ‎"\u200E" - This unicode Character is added by default -  In order to avoid Righ to Left in Arabic Language
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        SharedPreferences fontSizePreferences =  getActivity().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        int fontSizeValue = fontSizePreferences.getInt("FONT_VALUE", 0);
        //Welcome  to
        TextView textView = (TextView) getActivity().findViewById(R.id.tour_fragment_welcome_to);
        textView.setText("\n".concat("\u200E"+getString(R.string.welcome_to)).concat("\n"));
        textView.setTypeface(Singleton.getMundoSansPro_Bold());
        textView.setTextColor(Color.rgb(244, 219, 166));
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP,16+fontSizeValue);
        //Longman Dictionary
        TextView textView1 = (TextView) getActivity().findViewById(R.id.tour_fragment_dictionary);
        textView1.setText("\u200E"+getString(R.string.longman_dictionary_of_contemporary_english_edition).concat("\n"));
        textView1.setTypeface(Singleton.getMundoSansPro());
        textView1.setTextColor(Color.rgb(244, 219, 166));
        textView1.setTextSize(TypedValue.COMPLEX_UNIT_SP,16+fontSizeValue);
        //Whats New
        TextView textView2 = (TextView) getActivity().findViewById(R.id.tour_fragment_whats_new);
        textView2.setText("\u200E"+getString(R.string.whats_new_in_ldoce).concat("\n"));
        textView2.setTypeface(Singleton.getMundoSansPro_Bold());
        textView2.setTextColor(Color.parseColor("#FFFFFF"));
        textView2.setTextSize(TypedValue.COMPLEX_UNIT_SP,16+fontSizeValue);
        //Bullet Points
        String bullet_points = "\t●     ".concat("\u200E"+getString(R.string.new_improved_user_interface)).concat("\n\n\t●     ").concat("\u200E"+getString(R.string.advanced_search_features)).concat("\n\n\t●     ").concat("\u200E"+getString(R.string.search_high_frequency_words)).concat("\n\n\t●     ").concat("\u200E"+getString(R.string.up_to_date_dictionary_content)).concat("\n\n\t●     ").concat("\u200E"+getString(R.string.expanded_information));
        EditText textView3 = (EditText) getActivity().findViewById(R.id.tour_fragment_bullet);
        textView3.setText(bullet_points);
        textView3.setTypeface(Singleton.getMundoSansPro());
        textView3.setTextColor(Color.parseColor("#FFFFFF"));
        textView3.setTextSize(TypedValue.COMPLEX_UNIT_SP,16+fontSizeValue);
    }
}
