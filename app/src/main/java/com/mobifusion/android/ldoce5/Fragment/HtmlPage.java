package com.mobifusion.android.ldoce5.Fragment;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;

//import com.google.android.gms.analytics.GoogleAnalytics;
//import com.google.android.gms.analytics.HitBuilders;
//import com.google.android.gms.analytics.Tracker;
import com.mobifusion.android.ldoce5.R;
import com.mobifusion.android.ldoce5.Util.FirebaseWrapperScreens;
import com.mobifusion.android.ldoce5.Util.LDOCEApp;
import com.mobifusion.android.ldoce5.Util.Singleton;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

/**
     *  Created by Bazi on 30/06/15
     */

public class HtmlPage extends Fragment{

    public static final String PREFS_NAME = "LDOCE6PrefsFile";
//    Tracker t;
    public int title;
    boolean offline;
    int fontSizeValue;
    String pageName;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.activity_html_page,null);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        styleElements();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
//        t = ((LDOCEApp) getActivity().getApplication()).getTracker(
//                LDOCEApp.TrackerName.APP_TRACKER);
//        t.setScreenName(pageName);
//        t.enableAutoActivityTracking(true);
        FirebaseWrapperScreens.ScreenViewEvent(pageName);
Log.e("copy right ", pageName);
//        t.send(new HitBuilders.AppViewBuilder().build());
    }

    @Override
    public void onStart() {
        super.onStart();
        // Sending Hit to GA as new Activity
//        GoogleAnalytics.getInstance(getActivity()).reportActivityStart(getActivity());
    }

    @Override
    public void onStop() {
        super.onStop();
        // Sending Hit to GA as End of Activity
//        GoogleAnalytics.getInstance(getActivity()).reportActivityStop(getActivity());
    }

    public void styleElements() {
        // Set the Detail page title font title to MundoSansPro
        SharedPreferences fontSizePreferences =  getActivity().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        fontSizeValue = fontSizePreferences.getInt("FONT_VALUE", 0);
        TextView htmlPageTitle = (TextView) getView().findViewById(R.id.html_title);
        htmlPageTitle.setTypeface(Singleton.getMundoSansPro());
        htmlPageTitle.setText(getString(title));
        htmlPageTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20 + fontSizeValue);
        if(offline){
            //load static page reside in app
            getFilePath(title);
        }
        else {
            //load a page with URL
            getURL(title);
        }
    }

    /**
     * this function with return file name to load based on the resource string id
     * @param title the string resource id
     */
    public void getFilePath(int title){
        String fileName="";
        switch (title){
            //
            case R.string.foreword:
                fileName="Foreword.html";
                break;
            case R.string.introduction:
                fileName="Introduction.html";
                break;
            case R.string.pronunciation_table:
                fileName="International Phonetic Alphabet.html";
                break;
            case R.string.special_signs:
                fileName="Specialsigns.html";
                break;
            case R.string.short_forms:
                fileName="ShortForms.html";
                break;
            case R.string.labels:
                fileName="Labels.html";
                break;
            case R.string.grammar_codes:
                fileName="Grammarcodes.html";
                break;
            case R.string.patterns:
                fileName="Patterns.html";
                break;
            case R.string.copyright_notices:
                fileName="Copyright.html";
                break;
            case R.string.acknowledgements:
                fileName="Acknowledgements.html";
                break;
        }
        populateWebViewWithFile(fileName);
    }

    /**
     * this function is to get Online URL to load in webview using string resource id
     * @param title resource string id
     */
    void getURL(int title){
        String url="";
        switch (title){
            case R.string.frequently_asked_questions:
                url="http://www.pearsonintlsupport.com/support247/mobile_app/index.html";
                break;
        }
        populateWebViewWithURL(url);
    }

    /**
     *  load webview with a html page using its name
     * @param fileName the name of the file to be loaded
     */
    public void populateWebViewWithFile(String fileName) {

        StringBuilder buf=new StringBuilder();
        InputStream json= null;
        String content="";
        try {
            json = getActivity().getAssets().open("www/html/"+fileName);
            BufferedReader in= null;
            in = new BufferedReader(new InputStreamReader(json, "UTF-8"));
            String str;
            while ((str=in.readLine()) != null) {
                buf.append(str);
            }
            content=buf.toString();
            in.close();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        content=content.replaceAll("</fontvalue>", String.valueOf(12 + fontSizeValue));
        content=content.replaceAll("</fontvalueNormal>", String.valueOf(12 + fontSizeValue));
        content=content.replaceAll("</fontvalueHeading>", String.valueOf(16 + fontSizeValue));
        content=content.replaceAll("</fontvalueSubHeading>", String.valueOf(10 + fontSizeValue));
        WebView webView=(WebView)getActivity().findViewById(R.id.wv_html_page);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadDataWithBaseURL("file:///android_asset/www/html/", content, "text/html", "utf-8", null);
        //webView.loadUrl("file:///android_asset/www/html/" + fileName);
        webView.setLongClickable(true);
        webView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return true;
            }
        });
    }

    /**
     *  load the webview with URL
     * @param url string URL
     */
    public void populateWebViewWithURL(String url){
        WebView webView=(WebView)getActivity().findViewById(R.id.wv_html_page);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(url);
    }
}
