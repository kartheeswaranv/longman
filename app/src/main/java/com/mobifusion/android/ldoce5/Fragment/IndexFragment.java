package com.mobifusion.android.ldoce5.Fragment;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcelable;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

//import com.google.android.gms.analytics.GoogleAnalytics;
//import com.google.android.gms.analytics.HitBuilders;
//import com.google.android.gms.analytics.Tracker;
import com.mobifusion.android.ldoce5.Activity.SlideMenuSearchAndIndex;
import com.mobifusion.android.ldoce5.Activity.WordOfTheDayPopUp;
import com.mobifusion.android.ldoce5.R;
import com.mobifusion.android.ldoce5.Util.AlphabetListAdapter;
import com.mobifusion.android.ldoce5.Util.FirebaseWrapperScreens;
import com.mobifusion.android.ldoce5.Util.LDOCEApp;
import com.mobifusion.android.ldoce5.Util.Singleton;
import com.mobifusion.android.ldoce5.Util.Utils;
import com.mobifusion.android.ldoce5.model.IndexContent;
import com.mobifusion.android.ldoce5.model.IndexRowItem;
import com.mobifusion.android.ldoce5.model.IndexTextView;
import com.mobifusion.android.ldoce5.model.SideIndexBarItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static android.widget.AbsListView.OnScrollListener;

public class IndexFragment extends androidx.fragment.app.ListFragment implements View.OnTouchListener{

    //Declaring the variables
    private GestureDetector mGestureDetector;
    private static float sideIndexX;
    private static float sideIndexY;
    public Parcelable currentState ;
    public Parcelable previousState ;
    String lastClickedWord;

//    Tracker t;

    class SideIndexGestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        //Detecting the swipe on the slide bar
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {

            sideIndexX = sideIndexX - distanceX;
            sideIndexY = sideIndexY - distanceY;

            if (sideIndexX >= 0 && sideIndexY >= 0) {
                //Navigating to the clicked section
                LinearLayout sideIndex = getSideIndexBar();
                navigateToClickedSectionOnIndexBar(sideIndex);
                IndexTextView indexTV = getTextViewAtPos(getSideIndexItemPositionFromY(sideIndexY,sideIndex),sideIndex);
                if(indexTV!=null)
                    processFloatIndicator(e2,indexTV.getText().toString());
            }

            return super.onScroll(e1, e2, distanceX, distanceY);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.activity_index, null);
        String hwd=null;
        //return super.onCreateView(inflater, container, savedInstanceState);
        v.setOnTouchListener(this);
        return v;
    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (mGestureDetector.onTouchEvent(event)) {
            Log.w("OnTouch","returned True");
            return true;
        } else {
            Log.w("OnTouch","returned false");
            return false;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        Singleton.getInstance(getActivity());
        // Sending Hit to GA as Start of new Activity
//        GoogleAnalytics.getInstance(getActivity()).reportActivityStart(getActivity());
        //Typeface longmanFont= Typeface.createFromAsset(getActivity().getAssets(), "longman.ttf");
        //Button slideMenuButton=(Button) getView().findViewById(R.id.slideMenuButton);
        //slideMenuButton.setTypeface(longmanFont);
        mGestureDetector = new GestureDetector(getActivity(), new SideIndexGestureListener());
        //Generating first level index
        IndexContent indexContent= generateContentForIndex("");
        //populating first level index in the view
        populateIndexAndSideBar(indexContent);
        getListView().setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getListView().getWindowToken(), 0);
                return false;
            }
        });

        //Setting scrollListener for tableview
        getListView().setOnScrollListener(new OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                //Setting ListView State
                currentState = getListView().onSaveInstanceState();

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }

        });
        //Checking currentstate and listview context, if both not null we wanna access the previous state
        if (currentState!=null && getListView()!=null )
        {
            //If last word not null, which means we are in second level index.
            //Navigate to second level index with last word
            if (lastClickedWord!=null)
            {
                IndexContent obj = generateContentForIndex(lastClickedWord);
                populateIndexAndSideBar(obj);
                //getListView().onRestoreInstanceState(previousState);
            }

                getListView().onRestoreInstanceState(currentState);

        }
        styleElements();
        //showWordPopUp();
    }

    void styleElements(){
        //Refactor to put all the styling code, instead of all over the place.
        TextView floating_tv =(TextView)getActivity().findViewById(R.id.tv_floating_indicator);
        floating_tv.setTypeface(Singleton.getMundoSansPro());
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.list_alphabet);
        //Using singleton creating object for that
    }

    //Handling back button press
    public boolean parentActivityOnBackPressed() {
        //On pressing the back button,if it is in second level, navigated to first level.
        //If it is in first level, app is closed.
        //To check if it is first level or second level, an item from the list is took and check if it has
        // an id. if not it is a first level element
        //taking the list
        ListView ls=(ListView)getActivity().findViewById(android.R.id.list);
        AlphabetListAdapter adapter=(AlphabetListAdapter)ls.getAdapter();
        //Getting the last item
        IndexRowItem item=(IndexRowItem) adapter.getItem(adapter.getCount()-1);
        //checking if it has ID
        if (!item.getHwdId().equalsIgnoreCase(""))
        {
            //App currently in second level index
            //generating first level index
            IndexContent obj = generateContentForIndex("");
            //populating first level index in view
            populateIndexAndSideBar(obj);
            if(previousState!=null) {
                //changing lastclickedworkd to null, restoring the previous state
                lastClickedWord = null;
                getListView().onRestoreInstanceState(previousState);
                //Change currentstate to previous state
                currentState = previousState;
            }
            //Change previous state to null to indicate we are in first level
            previousState = null;
            return false;
        }
        return true;
        /*else
        {
            //Navigating to welcome screen and closing app.
            Intent intent = new Intent(getActivity().getApplicationContext(), WelcomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("EXIT", true);
            startActivity(intent);
        }*/
    }
    /*
    This function is to populate array content for the first level or second level.
    It return a 'IndexContent' Object which comprises of three variables :
                -> Index list content,section details, side index bar content
    the argument 'clickedAlphabet' decide whether its first level or second level.
                -> if clickedAlphabet "" : First Level
                -> if not : Second Level
     */
    public IndexContent generateContentForIndex(String clickedAlphabet)
    {
        //Initialising variables to store values
        List<IndexRowItem> listItems = new ArrayList<>();
        List<SideIndexBarItem> sideIndexBarContent=new ArrayList<>();
        HashMap<String, Integer> sectionContent=new HashMap<String, Integer>();

        if (clickedAlphabet.equalsIgnoreCase(""))
        {
            //FirstLevel Index//

            //Store list view values
            listItems.add(new IndexRowItem("A","",false));
            listItems.add(new IndexRowItem("B","",false));
            listItems.add(new IndexRowItem("C","",false));
            listItems.add(new IndexRowItem("D","",false));
            listItems.add(new IndexRowItem("E","",false));
            listItems.add(new IndexRowItem("F","",false));
            listItems.add(new IndexRowItem("G","",false));
            listItems.add(new IndexRowItem("H","",false));
            listItems.add(new IndexRowItem("I","",false));
            listItems.add(new IndexRowItem("J","",false));
            listItems.add(new IndexRowItem("K","",false));
            listItems.add(new IndexRowItem("L","",false));
            listItems.add(new IndexRowItem("M","",false));
            listItems.add(new IndexRowItem("N","",false));
            listItems.add(new IndexRowItem("O","",false));
            listItems.add(new IndexRowItem("P","",false));
            listItems.add(new IndexRowItem("Q","",false));
            listItems.add(new IndexRowItem("R","",false));
            listItems.add(new IndexRowItem("S","",false));
            listItems.add(new IndexRowItem("T","",false));
            listItems.add(new IndexRowItem("U","",false));
            listItems.add(new IndexRowItem("V","",false));
            listItems.add(new IndexRowItem("W","",false));
            listItems.add(new IndexRowItem("X","",false));
            listItems.add(new IndexRowItem("Y","",false));
            listItems.add(new IndexRowItem("Z","",false));

            //Store side index bar content
            String[] alphabets={"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"};
            SideIndexBarItem temp;
            for(int i=0;i<alphabets.length;i++)
            {
                temp= new SideIndexBarItem(alphabets[i],i,i);
                sideIndexBarContent.add(temp);
                sectionContent.put(alphabets[i],i);
            }

        }
        else
        {
            //second level index
                //First Level Index Event track
                // Build and send an Event.
//            t.send(new HitBuilders.EventBuilder()
//                    .setCategory("index")
//                    .setAction("index_word_clicked")
//                    .setLabel("first_level_index_clicked")
//                    .build());

            Bundle params=new Bundle();
            params.putString("eventCategory","index");
            params.putString("eventAction","index_word_clicked");
            params.putString("eventLabel","first_level_index_clicked");
            FirebaseWrapperScreens.EventLogs(params);
            try {
                //Data base connection
                SQLiteDatabase db = SQLiteDatabase.openDatabase(Utils.CoreDBFilePathAndName, null, 0);
                String query;
                //checking if its a first #section
                if(clickedAlphabet.equalsIgnoreCase("#"))
                {
                    query = String.format("SELECT  DISTINCT(HWD),id,frequent from lookup where (SUBSTR (upper(HWD),1,1)NOT BETWEEN 'A' AND 'Z') group by hwd order by hwd  collate nocase");
                }
                else
                    query = String.format("select  hwd,id,MAX(frequent) from lookup where sortname like '%s%%' group by hwd order by sortname  collate nocase",clickedAlphabet);
                Cursor cur = db.rawQuery(query, null);
                if (cur.getCount() <= 0) {
                    Log.e("IndexFragment:", "Database empty");

                }
                else
                {
                    //traversing through all the items
                    listItems.add(new IndexRowItem("A-Z","##BackButton##",false));
                    while (cur.moveToNext())
                    {
                        String hwd=cur.getString(0);
                        String hwdId=cur.getString(1);
                        //Storing as IndexRowItem
                        listItems.add(new IndexRowItem(hwd,hwdId,cur.getString(2).equalsIgnoreCase("1")));
                    }
                }
                db.close();
            }
            catch (Exception e) {
              Log.e("IndexFragment:", e.getMessage());
            }
            //populating the side index bar using the plist and its count, It is stored as a SideIndexBarItem
            String currentCharacter,previousCharacter;
            previousCharacter="";
            currentCharacter="";
            int start=1;
            int end=1;
            SideIndexBarItem indexItem;
            //Adding 'A-Z' section to the indexbar and listview
            indexItem = new SideIndexBarItem("A-Z",0,0);
            //Adding element to the side index bar
            sideIndexBarContent.add(indexItem);
            sectionContent.put("A-Z",0);
            //Adding section title  and its starting position
            //Data base connection
            SQLiteDatabase db = SQLiteDatabase.openDatabase(Utils.CoreDBFilePathAndName, null, 0);
            String query="select REPLACE(SUBSTR(sortname,1,2),'_','') AS KEY from lookup where sortname like ? group by hwd order by sortname  collate nocase";

            Cursor cur = db.rawQuery(query,new String[]{clickedAlphabet+"%"});
            if (cur.getCount() <= 0) {
                Log.e("IndexFragment:", "Database empty");

            }
            else
            {
                //Adding section title  and its starting position
                int count=0;
                while (cur.moveToNext())
                {
                    //Sub index title like AA,AB,AC...
                    currentCharacter = cur.getString(0);
                    //Count for each section like AA,AB,AC....
                    count++;
                    //calculating the starting and ending location of each section(on clicking the side index bar).
                    if(!currentCharacter.equalsIgnoreCase(previousCharacter)){
                        //Since we are not using the end value,it is put as dummy. did not calculated its value
                        end=start=count;
                        indexItem = new SideIndexBarItem(currentCharacter.toUpperCase(),start,end);
                        previousCharacter=currentCharacter;
                        //Adding element to the side index bar
                        sideIndexBarContent.add(indexItem);
                    }
                }

            }
            db.close();
        }
        //return as a IndexContent object
        return new IndexContent(listItems,sideIndexBarContent,sectionContent);
    }
    //This function populate the headwords in the list and side index bar.it receives the list words to be shown as argument
    //and generate the side index bar based on that words.
    void populateIndexAndSideBar(IndexContent indexContentObject) {

        AlphabetListAdapter adapter= new AlphabetListAdapter() ;
        List<AlphabetListAdapter.Row> rows = new ArrayList<AlphabetListAdapter.Row>();
        List<IndexRowItem> indexContent=indexContentObject.indexViewContent;
        //Adding 'A-Z' section in list to go back to the previous list
        //rows.add(new AlphabetListAdapter.Item("A-Z"));
        //Adding each element to the list
        for (IndexRowItem item : indexContent)
        {
            // Add other headwords from the DB to the list
            rows.add(item);

        }
        //setting the adpater
        adapter.setRows(rows);
        setListAdapter(adapter);
        //populating side indexbar
        populateSideIndexBar(indexContentObject);
        //To avoid Shrinking the IndexBar on showing keyboard
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);

    }

    //This function generate the side IndexBar using TextView.
    public void populateSideIndexBar(final IndexContent indexContentObject) {

        //Initializing variables
        final List<SideIndexBarItem> indexBarContent=indexContentObject.sideIndexBarContent;
        HashMap<String,Integer> sectionContent=indexContentObject.sectionContent;
        LinearLayout sideIndex = (LinearLayout)getActivity().findViewById(R.id.sideIndex);
        //Removing the all views before created.
       try {
           sideIndex.removeAllViews();


        int indexListSize = indexBarContent.size();
        if (indexListSize < 1) {
            return;
        }
        //calculating the maximum height
        int indexMaxSize = (int) Math.floor(sideIndex.getHeight() / 20);
        int tmpIndexListSize = indexListSize;
        while (tmpIndexListSize > indexMaxSize) {
            tmpIndexListSize = tmpIndexListSize / 2;
        }
        double delta;
        if (tmpIndexListSize > 0) {
            delta = indexListSize / tmpIndexListSize;
        } else {
            delta = 1;
        }
        //IndexText view is inherited class from text view to store the starting position also
        IndexTextView tmpTV;
        for (double i = 1; i <= indexListSize; i = i + delta) {
            SideIndexBarItem tmpIndexItem = indexBarContent.get((int) i - 1);
            String tmpLetter = tmpIndexItem.getText().toString();
            //Styling the indexbar items
            tmpTV = new IndexTextView(getActivity());
            tmpTV.startPosition=(int)tmpIndexItem.getStart();
            tmpTV.setText(tmpLetter);
            tmpTV.setGravity(Gravity.CENTER);
            tmpTV.setTextSize(14);
            tmpTV.setTypeface(Singleton.getMundoSansPro_Bold());
            tmpTV.setTextColor(Color.parseColor("#ffffff"));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1);
            tmpTV.setLayoutParams(params);
            sideIndex.addView(tmpTV);
        }

        Integer sideIndexHeight = sideIndex.getHeight();
        //Onclick event for slide menu
        sideIndex.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // now you know coordinates of touch
                sideIndexX = event.getX();
                sideIndexY = event.getY();

                // and can display a proper item
                LinearLayout sideIndex = getSideIndexBar();
                navigateToClickedSectionOnIndexBar(sideIndex);
                IndexTextView indexTV = getTextViewAtPos(getSideIndexItemPositionFromY(sideIndexY,sideIndex),sideIndex);
                if(indexTV!=null)
                    processFloatIndicator(event,indexTV.getText().toString());

                return true;
            }
        });
       }catch (Exception e)
       {
           System.out.println(e.getMessage());
       }
    }

    //This function navigate the list view into the clicked position.it find the clicked item based on the clicked position
    //move to the starting position of that element in the list
    public void navigateToClickedSectionOnIndexBar(LinearLayout sideIndex) {

        try {
            // getting the size of index bar (number of elements will be equals to number of subviews :P)
            int indexContentSize = sideIndex.getChildCount();

            // compute the item index for given event position belongs to
            int itemPosition = getSideIndexItemPositionFromY(sideIndexY, sideIndex);
            IndexTextView tmp = getTextViewAtPos(itemPosition, sideIndex);
            // get the item (we can do it since we know item index)
            if (itemPosition < indexContentSize) {
                //scrolling to the position
                getListView().setSelection(tmp.startPosition);
                currentState = getListView().onSaveInstanceState();
            }
        }
        catch (NullPointerException ex){
            Log.e("Error in SideIndex", ex.getStackTrace().toString());
        }
    }
    LinearLayout getSideIndexBar(){
        LinearLayout sideIndex = (LinearLayout) getActivity().findViewById(R.id.sideIndex);
        return sideIndex;
    }
    int getSideIndexItemPositionFromY(float yPosition, LinearLayout sideIndex){
        // getting the height of index bar
        Integer sideIndexHeight = sideIndex.getHeight();
        // getting the size of index bar (number of elements will be equals to number of subviews :P)
        int indexContentSize=sideIndex.getChildCount();
        // compute number of pixels for every side index item
        double pixelPerIndexItem = (double) sideIndexHeight / indexContentSize;

        // compute the item index for given event position belongs to
        int itemPosition = (int) (yPosition / pixelPerIndexItem);
        return itemPosition;
    }

    IndexTextView getTextViewAtPos(int itemPos,LinearLayout sideIndex){
        IndexTextView tv = (IndexTextView)sideIndex.getChildAt(itemPos);
        return tv;
    }

    void processFloatIndicator(final MotionEvent event,String textToSet){
        final FrameLayout floating_indicator = (FrameLayout)getActivity().findViewById(R.id.fl_floating_indicator);
        if(event.getAction() == MotionEvent.ACTION_DOWN || event.getAction() == MotionEvent.ACTION_MOVE) {
            floating_indicator.setVisibility(View.VISIBLE);
            TextView tv = (TextView)floating_indicator.findViewById(R.id.tv_floating_indicator);
            tv.setText(textToSet);
            floating_indicator.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (event.getAction() != MotionEvent.ACTION_DOWN)
                        floating_indicator.setVisibility(View.INVISIBLE);
                }
            }, 1000);
        }
        else
            floating_indicator.setVisibility(View.INVISIBLE);
    }
    //When clicks on the list item
    //  -> if it is in first level : Navigate to second level of specific clicked letter
    //  -> if it is in second level index : Navigate to Definition Page
    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        // Get the tracker.
//        Tracker t = ((LDOCEApp) getActivity().getApplication()).getTracker(
//                LDOCEApp.TrackerName.APP_TRACKER);

        //getting the clicked alphabet
        AlphabetListAdapter adapter=(AlphabetListAdapter)l.getAdapter() ;
        IndexRowItem item = (IndexRowItem) adapter.getItem(position);
        String clickedWord= item.getHwd();

        //checking if clicked word is A-Z , Then navigate back to first level
        if(clickedWord.equals("A-Z"))
        {
            //populate first level
            IndexContent obj = generateContentForIndex("");
            populateIndexAndSideBar(obj);
            //Restore the previous state(ie first level state)
            if (previousState!=null) {
                currentState = previousState;
                getListView().onRestoreInstanceState(currentState);
                previousState = null;
            }
        }
        else
        {
            //check if the clicked word has id, then navigate to details page, else move to second level
            if (item.getHwdId().equalsIgnoreCase(""))
            {
                //Save current state (i.e first level index state) to previous state for back functionality
                previousState = (currentState == null) ? getListView().onSaveInstanceState():currentState ;
                //Populate the second level based on clicked word

                IndexContent obj = generateContentForIndex(clickedWord);
                populateIndexAndSideBar(obj);
                //save clicked word to last clicked word and save the state of list view
                lastClickedWord = clickedWord;
                currentState = getListView().onSaveInstanceState();
            }
            else
            {
                androidx.fragment.app.FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                if (fragmentManager.getBackStackEntryCount()>0)
                {
                    //Get the detailPage fragment to handle back stack
                    Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.detailPageFragment);
                    Boolean isDetailPage = (fragment instanceof DetailPageFragment)?true:false;
                    if (isDetailPage)
                    {
                        DetailPageFragment detailPageFragment = (DetailPageFragment)fragment;
                        String previous = detailPageFragment.headword;
                        //Check clicked word with previous word and if it equals do nothing
                        if (previous.equals(clickedWord))
                            return;
                        else
                            //Navigate to detail page
                            Utils.navigateToDetailPage(getActivity(),item.getHwd(),item.getHwdId(),"","");
                    }
                    else
                        //Navigate to detail page
                        Utils.navigateToDetailPage(getActivity(),item.getHwd(),item.getHwdId(),"","");
                }else
                {
                    //Navigate to detail page
                    Utils.navigateToDetailPage(getActivity(),item.getHwd(),item.getHwdId(),"","");
                }
            }
            //Second Level Index Event track
            // Build and send an Event.
//            t.send(new HitBuilders.EventBuilder()
//                    .setCategory("index")
//                    .setAction("index_word_clicked")
//                    .setLabel("second_level_index_clicked")
//                    .build());


            Bundle params=new Bundle();
            params.putString("eventCategory","index");
            params.putString("eventAction","index_word_clicked");
            params.putString("eventLabel","second_level_index_clicked");
            FirebaseWrapperScreens.EventLogs(params);

        }
    }

    void showWordPopUp(){
            WordOfTheDayPopUp wordOfTheDayPopUp=new WordOfTheDayPopUp(getActivity(),"u2fc098491a42200a.6e2b450a.11503f8242d.-172c");
            wordOfTheDayPopUp.setCancelable(false);
            wordOfTheDayPopUp.show();

    }
    @Override
    public void onResume() {
        SharedPreferences sharedPreferences =  getActivity().getSharedPreferences("LDOCE6PrefsFile", Context.MODE_PRIVATE);
        boolean isSearchOnClipboardOn=sharedPreferences.getBoolean("Search-on-clipboard", false);
        boolean isSaveSearchWordOn=sharedPreferences.getBoolean("SaveSearchWord",false);
//        if(Utils.isTablet(getActivity())){

            if(isSaveSearchWordOn && !isSearchOnClipboardOn){
                ((SlideMenuSearchAndIndex)getActivity()).showLastSearchedWord();
           }
//        }
//        else {
//            if(!isSaveSearchWordOn && !isSearchOnClipboardOn) {
//                ((SlideMenuSearchAndIndex) getActivity()).showWord();
//            }
//        }
        super.onResume();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Get the Tracker and Initialise it
//        t = ((LDOCEApp) getActivity().getApplication()).getTracker(
//                LDOCEApp.TrackerName.APP_TRACKER);
//        t.setScreenName("Index Page");
//        t.send(new HitBuilders.AppViewBuilder().build());
        FirebaseWrapperScreens.ScreenViewEvent("Index Page");

    }

    @Override
    public void onStop() {
        super.onStop();
        // Sending Hit to GA as End of Activity
//        GoogleAnalytics.getInstance(getActivity()).reportActivityStop(getActivity());
    }

    public void refreshUIElements(){
        styleElements();
        onStart();
    }
}

