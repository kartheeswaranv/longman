package com.mobifusion.android.ldoce5.Fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

//import com.google.android.gms.analytics.GoogleAnalytics;
//import com.google.android.gms.analytics.HitBuilders;
//import com.google.android.gms.analytics.Tracker;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.mobifusion.android.ldoce5.Activity.DbConnection;
import com.mobifusion.android.ldoce5.Activity.SlideMenuSearchAndIndex;
import com.mobifusion.android.ldoce5.R;
import com.mobifusion.android.ldoce5.Util.AlphabetListAdapter;
import com.mobifusion.android.ldoce5.Util.CustomTypefaceSpan;
import com.mobifusion.android.ldoce5.Util.FirebaseWrapperScreens;
import com.mobifusion.android.ldoce5.Util.LDOCEApp;
import com.mobifusion.android.ldoce5.Util.Singleton;
import com.mobifusion.android.ldoce5.Util.Utils;
import com.mobifusion.android.ldoce5.model.IndexRowItem;
import com.mobifusion.android.ldoce5.model.RowWithHomnumForBookmark;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;
import android.widget.PopupMenu;


import static android.view.View.INVISIBLE;


public class FavoritesFragment extends Fragment{
    //Variable to handle Contextual actionbar show / hide
    private ActionMode favoritesActionMode;
    private GestureDetector mGestureDetector;
//    Tracker t;
    public static final String PREFS_NAME = "LDOCE6PrefsFile";
    Menu mainMenu=null;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.activity_favorites,null);
        return v;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    public boolean ParentOnTouchEvent(MotionEvent event) {

        if (mGestureDetector.onTouchEvent(event)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Get the Tracker and Initialise it
//        t = ((LDOCEApp) getActivity().getApplication()).getTracker(
//                LDOCEApp.TrackerName.APP_TRACKER);
//        t.setScreenName("Favorites Page");
//        t.send(new HitBuilders.AppViewBuilder().build());
        FirebaseWrapperScreens.ScreenViewEvent("Bookmarks Page");

    }

    @Override
    public void onStart() {
        super.onStart();
        // Sending Hit to GA as Start of new Activity
//        GoogleAnalytics.getInstance(getActivity()).reportActivityStart(getActivity());
        //edit.putInt("favoriteLastFilter", 0);
        //edit.commit();

        // Get the ListView from the XML
        final ListView listView = (ListView)getActivity().findViewById(R.id.lv_favorites);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //getting the clicked alphabet
                AlphabetListAdapter adapter = (AlphabetListAdapter) listView.getAdapter();
                RowWithHomnumForBookmark item = (RowWithHomnumForBookmark) adapter.getItem(position);
                androidx.fragment.app.FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                //Check BackStack entry
                Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.detailPageFragment);
                //Get the detailPage fragment to handle back stack and check it is from detail page and check whether clicked word and previous word are equal
                if (fragmentManager.getBackStackEntryCount() > 0 ) {
                    //get the detail page fragment
                    Boolean isDetailPage = (fragment instanceof DetailPageFragment) ? true : false;
                    if (isDetailPage) {
                        DetailPageFragment detailPageFragment = (DetailPageFragment) fragment;
                        //Get the previous word check for the previous word
                        String previousWord = detailPageFragment.headword;
                         if (previousWord.equals(item.getHwd())){
                            if(detailPageFragment.homNum!=item.getHomnum()){
                                detailPageFragment.homNum=item.getHomnum();
                                detailPageFragment.navigateToEntryWithHomnum();
                            }
                             else
                                return;

                        }
                        else
                            //Navigate to detail page
                            Utils.navigateToDetailPage(getActivity(), item.getHwd(),item.getHwdId(),item.getHomnum(),"");
                    } else
                        //Navigate tot detail page
                        Utils.navigateToDetailPage(getActivity(), item.getHwd(),item.getHwdId(),item.getHomnum(),"");
                }
                //Navigate to detail page
                else
                    Utils.navigateToDetailPage(getActivity(), item.getHwd(),item.getHwdId(),item.getHomnum(),"");
            }
        });

        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        final AlphabetListAdapter listViewAdapter = (AlphabetListAdapter) listView.getAdapter();

        listView.setMultiChoiceModeListener(new ListView.MultiChoiceModeListener() {

            @Override
            public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
                /*
                  Scenario: When SearchView is in focus, and trying to Select an listView Item and Press Delete
                   1. Remove SearchView Focus
                   2. Make the SearchView to its normal position
                   3. Dismiss Keyboard if it is in enable state
                 */
                final ListView searchListView = (ListView) getActivity().findViewById(R.id.searchResultListView);
                Fragment searchFragment = getFragmentManager().findFragmentById(R.id.searchFragement);
                SearchView searchView = (SearchView) searchFragment.getView().findViewById(R.id.search_bar);
                if (!searchView.isIconified()) {
                    //Keyboard dismissal and remove the search focus
                    searchListView.setVisibility(INVISIBLE);
                    searchView.setQuery("", false);
                    searchView.setIconified(true);
                    searchView.setFocusable(false);
                }
                //get the count of checkedItems in Favorites ListView
                int checkedCount = listView.getCheckedItemCount();
                final Button menuButton=(Button)getActivity().findViewById(R.id.fav_Menu_button);
                //To display the number of Items selected in Action Bar
                if (checked) {
                    listViewAdapter.setSelection(position);
                } else{
                    listViewAdapter.removeSelectionAt(position);
                }
                //final Button menuButton=(Button)getActivity().findViewById(R.id.fav_Menu_button);
                mode.setTitle(checkedCount + "/" + listView.getCount());
                favoritesSelectAndUnSelect(listView);
            }

            private void favoritesSelectAndUnSelect(ListView list) {
                SparseBooleanArray checkedItems = list.getCheckedItemPositions();
                for (int i = 0; i < checkedItems.size(); i++) {
                    IndexRowItem checkedRow = null;
                    int checkedPosition = checkedItems.keyAt(i);
                    checkedRow = ((IndexRowItem) listViewAdapter.getItem(checkedPosition));
                    //isSelected is a property for favorites Item - to change the background color
                    checkedRow.isSelected = checkedItems.valueAt(i);
                }
            }

            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                final Button menuButton=(Button)getActivity().findViewById(R.id.fav_Menu_button);
                if (menuButton.isEnabled()) {
                    menuButton.setEnabled(false);
                    menuButton.setAlpha(0.3f);
                }
                MenuInflater inflater = mode.getMenuInflater();
                inflater.inflate(R.menu.menu_context, menu);
                favoritesActionMode = mode;
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                //Disabling swipe gesture for slidemenu for phone alone
                if (!Utils.isTablet(getActivity()))
                    ((SlideMenuSearchAndIndex) getActivity()).getSlidingMenu().setTouchModeAbove(SlidingMenu.TOUCHMODE_NONE);
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                // Respond to clicks on the actions in the CAB
                AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
                switch (item.getItemId()) {
                    case R.id.context_menu_delete:
                        //To retrieve the HWDid of corresponding selected rowItem in favorites ListView
                        List<String> ids = new ArrayList<String>();
                        String[] idArray = new String[ids.size()];
                        //This is mainly used to Store an array with Boolean Values -  it hte ListView Item is Checked, this array stores checkedItem position and bool value
                        SparseBooleanArray checkedItems = listView.getCheckedItemPositions();
                        for (int i = 0; i < checkedItems.size(); i++) {
                            int checkedPosition = checkedItems.keyAt(i);
                            //Check the selectable state and add to an ids List
                            if (checkedItems.valueAt(i)) {
                                String hwdId = ((IndexRowItem) listViewAdapter.getItem(checkedPosition)).getHwdId();
                                ids.add(hwdId);
                            }
                        }
                        idArray = ids.toArray(idArray);

                        //Bool to check for select all
                        boolean isEntryAllDeleted = false;

                        //check for the idArray and list view count
                        if (listView.getCount() == idArray.length)
                            isEntryAllDeleted = true;
                        //Handle Alert Box if select all option is enabled
                        if (isEntryAllDeleted) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                builder = new AlertDialog.Builder(getActivity(), android.R.style.Theme_Material_Light_Dialog_Alert);
                            } else {
                                builder = new AlertDialog.Builder(getActivity());
                            }
                            final String[] finalIdArray = idArray;
                            builder.setTitle(R.string.confirm_delete);
                            builder.setMessage(R.string.are_you_sure_you_want_to_delete_all_your_bookmarks);
                            builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    //Pass the entry ids to the onDeleteEntriesInList and refresh the favorites page
                                    boolean entriesDeleted = Utils.deleteItemFromFavorite(finalIdArray);
                                    if (entriesDeleted) {
                                        if (Utils.isTablet(getActivity().getApplicationContext()))
                                            favoriteButtonState(finalIdArray);
                                        updateFavoritesEntryList();
                                        Bundle params=new Bundle();
                                        params.putString("eventCategory","bookmark_button_action");
                                        params.putString("eventAction","delete_all_confirmation");
                                        params.putString("eventLabel","bookmark_cleared");
                                        FirebaseWrapperScreens.EventLogs(params);

//                                        t.send(new HitBuilders.EventBuilder().setCategory("favorite_page_actions").setAction("delete_all_confirmation").setLabel("favorites_cleared").build());
                                        final Button menuButton=(Button)getActivity().findViewById(R.id.fav_Menu_button);
                                        final ListView listView = (ListView) getActivity().findViewById(R.id.lv_favorites);
                                        if(listView.getAdapter().getCount() <= 0){
                                            menuButton.setAlpha(0.3f);
                                            menuButton.setEnabled(false);
                                        }else if (listView.getAdapter().getCount() >=1){
                                            menuButton.setAlpha(1.0f);
                                            menuButton.setEnabled(true);
                                        }
                                    }
                                }
                            });
                            builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    // User cancelled the dialog
                                    System.out.println();
                                    Bundle params=new Bundle();
                                    params.putString("eventCategory","view_bookmark_button_action");
                                    params.putString("eventAction","bookmarks_cancel_button_tapped");
                                    params.putString("eventLabel","");
                                    FirebaseWrapperScreens.EventLogs(params);
                                }
                            });
                            builder.show();

                        } else {
                            //Pass the entry ids to the onDeleteEntriesInList and refresh the favorites page
                            boolean entriesDeleted = Utils.deleteItemFromFavorite(idArray);
                            //Delete if multiple selected and refresh the favorites page
                            //onDeleteEntriesInList(idArray);
                            if (entriesDeleted) {
                                Bundle params=new Bundle();
                                params.putString("eventCategory","bookmark_button_action");
                                params.putString("eventAction","word_delete_button_tapped");
                                params.putString("eventLabel","bookmark_cleared");
                                FirebaseWrapperScreens.EventLogs(params);
                                if (Utils.isTablet(getActivity().getApplicationContext()))
                                    favoriteButtonState(idArray);
                                updateFavoritesEntryList();
                            }

                        }

                        mode.finish(); // Action picked, so close the CAB
                        return true;
                    case R.id.context_menu_selectAll:
                        /*
                        Get the fragment favorites list
                        Get the count of that list
                        Change the back ground of the selected item
                         */
                        if (listView.getCheckedItemCount() == listView.getCount()) {
                            for (int i = 0; i < listView.getCount(); i++) {
                                listView.setItemChecked(i, false);
                                listViewAdapter.removeSelectionAt(i);
                            }
                        }
                        //Update the selection state in listView and listViewAdapter
                        else {
                            for (int i = 0; i < listView.getCount(); i++) {
                                listView.setItemChecked(i, true);
                                listViewAdapter.setSelection(i);
                            }
                        }
                        //Set the no. of items in selected state
                        mode.setTitle(listView.getCheckedItemCount() + "/" + listView.getCount());
                        return true;
                    default:
                        return false;
                }
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
                //If Select All button is pressed, and Unselecting all - uncheck all the checked Items in Favorites ListView
                for (int i = 0; i < listView.getCount(); i++) {
                    IndexRowItem checkedRow = null;
                    checkedRow = ((IndexRowItem) listViewAdapter.getItem(i));
                    checkedRow.isSelected = false;
                    final Button menuButton=(Button)getActivity().findViewById(R.id.fav_Menu_button);
                    menuButton.setEnabled(true);
                    menuButton.setAlpha(1.0f);
                }
                //Enabling the swipe gesture for slide menu for phone alone
                if (!Utils.isTablet(getActivity()))
                    ((SlideMenuSearchAndIndex) getActivity()).getSlidingMenu().setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
                listViewAdapter.removeSelection();

            }
        });
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        updateFavoritesEntryList();
        styleElements();
        TextView emptyView = (TextView)getActivity().findViewById(R.id.tv_empty_view);
        emptyView.setTypeface(Singleton.getMundoSansPro());
        emptyView.setTextColor(getResources().getColor(R.color.white));
        ListView listView = (ListView)getActivity().findViewById(R.id.lv_favorites);
        listView.setEmptyView(emptyView);

        Typeface longmanFont = Typeface.createFromAsset(getActivity().getAssets(), "longman.ttf");
        final Button menuButton=(Button)getActivity().findViewById(R.id.fav_Menu_button);
        if(listView.getAdapter().getCount() <= 0){
            menuButton.setAlpha(0.3f);
            menuButton.setEnabled(false);
        }else if (listView.getAdapter().getCount() >=1){
            menuButton.setAlpha(1.0f);
            menuButton.setEnabled(true);
        }
        menuButton.setTypeface(longmanFont);
        menuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Creating the instance of PopupMenu
                PopupMenu popup = new PopupMenu(getActivity(), menuButton);

                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.popup_menu, popup.getMenu());
                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        SharedPreferences favoriteFilterPrefs = getActivity().getSharedPreferences("FavoritesPrefs", Context.MODE_PRIVATE);
                        SharedPreferences.Editor favoriteFilterEditor = favoriteFilterPrefs.edit();
                        switch (item.getItemId()) {
                            case R.id.one:
                                sort_atoz();
                                favoriteFilterEditor.putInt("favoriteLastFilter", 1);
                                favoriteFilterEditor.commit();
                                break;
                            case R.id.two:
                                sort_ztoa();
                                favoriteFilterEditor.putInt("favoriteLastFilter",2);
                                favoriteFilterEditor.commit();
                                break;
                            case R.id.three:
                                sort_date_oldest();
                                favoriteFilterEditor.putInt("favoriteLastFilter",3);
                                favoriteFilterEditor.commit();
                                break;
                            case R.id.four:
                                sort_date_recent();
                                favoriteFilterEditor.putInt("favoriteLastFilter",4);
                                favoriteFilterEditor.commit();
                                break;
                            case R.id.five:
                                delete_all();
                                break;
                            default:
                                break;
                        }

                        //Toast.makeText(getActivity(),"You Clicked : " + item.getTitle(), Toast.LENGTH_SHORT).show();
                        return true;
                    }
                });

                Menu menu=popup.getMenu();
                for (int i=0;i<menu.size();i++) {
                    MenuItem mi = menu.getItem(i);
                    applyFontToMenuItem(mi);
                }

                    popup.show();//showing popup menu
            }
        });

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void styleElements(){
        // Set the favorite page title font title to MundoSansPro
        Menu Fav_filterMenu = (Menu) getActivity().findViewById(R.id.fav_menu);
        MenuItem first_item = (MenuItem) getActivity().findViewById(R.id.one);
        TextView favoritesTitle = (TextView) getActivity().findViewById(R.id.favorites_title);
        favoritesTitle.setTypeface(Singleton.getMundoSansPro());
        SharedPreferences fontSizePreferences =  getActivity().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        int fontSizeValue = fontSizePreferences.getInt("FONT_VALUE", 0);
        favoritesTitle.setTextSize(20+fontSizeValue);
        TextView noFavoriteTextView = (TextView)getActivity().findViewById(R.id.tv_empty_view);
        noFavoriteTextView.setText(R.string.no_bookmarks);
        noFavoriteTextView.setTextSize(20+fontSizeValue);
    }

    //This method is mainly used to change the favorite Button state -  without refreshing the WebView
    /*
        Scenario when this method calls:
        Detail Page word is as same in Favorites List entry and trying to delete the word from favorites(Change the state(color) os
        favorites button without refreshing the webview)
    */
    //TODO: move to DetailPageFragment
    public void favoriteButtonState(String[] idArray) {
        //to pass string[] array from java to jQuery - convert string[] array into jSON Array
        JSONArray jsonArray = new JSONArray();

        if(idArray!=null) {
            for (int i = 0; i < idArray.length; i++) {
                jsonArray.put(idArray[i]);
            }
            Log.w("", idArray.toString());
            WebView webview = (WebView) getActivity().findViewById(R.id.wv_headCell);
            // to call the jQuery function from java code
            if(webview!=null)
            webview.loadUrl("javascript:validateFavoritesButtonState(" + jsonArray.toString() + ")");
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        // Sending Hit to GA as End of Activity
//        GoogleAnalytics.getInstance(getActivity()).reportActivityStop(getActivity());
        if (favoritesActionMode!=null)
            favoritesActionMode.finish();
    }
    public void getFavoritesItem(){
        SharedPreferences favoriteFilterPrefs = getActivity().getSharedPreferences("FavoritesPrefs", Context.MODE_PRIVATE);
        int value = favoriteFilterPrefs.getInt("favoriteLastFilter",0);
        switch (value){
            case 0:
                List<AlphabetListAdapter.Row> list = new ArrayList<AlphabetListAdapter.Row>();
                // Get the User DB Connection
                SQLiteDatabase db = DbConnection.getUserDbConnection();
                // Check if the current existence of DB is not null
                if (db != null) {
                    // Query to get the records from favorites table
                    String query = String.format("select id,hwd,homnum,frequent,entryDate from favorites order by rowid DESC");
                    Cursor cur = db.rawQuery(query, null);
                    while (cur.moveToNext()) {
                        boolean isFrequent= cur.getString(3).equalsIgnoreCase("1")?true:false;
                        // Adding the record to list
                        list.add(new RowWithHomnumForBookmark(cur.getString(1),cur.getString(0),cur.getString(2),isFrequent,cur.getString(4)));
                    }
                }
                // Close the DB Connection
                db.close();
                setFavoriteItems(list);
                break;
            case 1:
                sort_atoz();
                break;
            case 2:
                sort_ztoa();
                break;
            case 3:
                sort_date_oldest();
                break;
            case 4:
                sort_date_recent();
                break;
            default:
                break;
        }



        //return list;
    }

    // Method to add entries to favourites list
    public void updateFavoritesEntryList(){
        // Get the ListView from the XML
        final ListView listView = (ListView)getActivity().findViewById(R.id.lv_favorites);
        // Create the Adapter
        AlphabetListAdapter adapter = (AlphabetListAdapter)listView.getAdapter();

        //AlphabetListAdapter adapter = new AlphabetListAdapter();
        // Create the List
        getFavoritesItem();
//        if(adapter!=null){
//            adapter.setRows(list);
//            adapter.notifyDataSetChanged();
//        }
//        else{
//            adapter= new AlphabetListAdapter();
//            adapter.setRows(list);
//            listView.setAdapter(adapter);
//        }
    }

    public void refreshFavouritesEntryList(RowWithHomnumForBookmark wordItem,boolean isFavorited)
    {
        ListView listView = (ListView)getActivity().findViewById(R.id.lv_favorites);
        // Get the Adapter
        if(listView!=null) {
            final AlphabetListAdapter adapter = (AlphabetListAdapter) listView.getAdapter();
            final List<AlphabetListAdapter.Row> rowList = adapter.getRows();
            //if(adapter == null)
            //adapter=new AlphabetListAdapter();
            if (isFavorited) {
                //the word is favored..so add into list
                rowList.add(0, wordItem);
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        styleElements();
                        adapter.updateRows(rowList);
                        final Button menuButton=(Button)getActivity().findViewById(R.id.fav_Menu_button);
                        final ListView listView = (ListView) getActivity().findViewById(R.id.lv_favorites);
                        if(listView.getAdapter().getCount() <= 0){
                            menuButton.setAlpha(0.3f);
                            menuButton.setEnabled(false);
                        }else if (listView.getAdapter().getCount() >=1){
                            menuButton.setAlpha(1.0f);
                            menuButton.setEnabled(true);
                        }
                        //dynamically filtering the newly added bookmarks to the bookmarks list
                        updateFavoritesEntryList();
                    }
                });
            } else {
                //the word is unfavored..So remove it from list
                boolean succes = false;
                int i = 0;
                for (AlphabetListAdapter.Row obj : rowList) {
                    RowWithHomnumForBookmark word = (RowWithHomnumForBookmark) obj;
                    if (word.equals(wordItem)) {
                        rowList.remove(i);
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                adapter.updateRows(rowList);
                                final Button menuButton=(Button)getActivity().findViewById(R.id.fav_Menu_button);
                                final ListView listView = (ListView) getActivity().findViewById(R.id.lv_favorites);
                                if(listView.getAdapter().getCount() <= 0){
                                    menuButton.setAlpha(0.3f);
                                    menuButton.setEnabled(false);
                                }else if (listView.getAdapter().getCount() >=1){
                                    menuButton.setAlpha(1.0f);
                                    menuButton.setEnabled(true);
                                }
                            }
                        });
                        break;
                    }
                    i++;
                }
            }
        }
        return;
    }
    /**
     * refresh the UI element from settings page
     * Used in language change, Font size increase
     */
    public void refreshUIElements()
    {
        SharedPreferences fontSizePreferences =  getActivity().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        int fontSizeValue = fontSizePreferences.getInt("FONT_VALUE", 0);
        TextView noFavoriteTextView = (TextView)getActivity().findViewById(R.id.tv_empty_view);
        noFavoriteTextView.setText(R.string.no_bookmarks);
        noFavoriteTextView.setTextSize(20+fontSizeValue);
        TextView favouriteTitle = (TextView)getActivity().findViewById(R.id.favorites_title);
        favouriteTitle.setText(R.string.bookmarks);
        favouriteTitle.setTextSize(20+fontSizeValue);
        favouriteTitle.setTypeface(Singleton.getMundoSansPro());
        final Button menuButton=(Button)getActivity().findViewById(R.id.fav_Menu_button);
        final ListView listView = (ListView) getActivity().findViewById(R.id.lv_favorites);
        if(listView.getAdapter().getCount() <= 0){
            menuButton.setAlpha(0.3f);
            menuButton.setEnabled(false);
        }else if (listView.getAdapter().getCount() >=1){
            menuButton.setAlpha(1.0f);
            menuButton.setEnabled(true);
        }

    }

    //for sorting bookmarks a to z
    public void sort_atoz()
    {
        List<AlphabetListAdapter.Row> list = new ArrayList<AlphabetListAdapter.Row>();
        SQLiteDatabase db = DbConnection.getUserDbConnection();
        if (db != null) {
            String query = String.format("select id,hwd,homnum,frequent,entryDate from favorites order by lower(hwd) asc");
            Cursor cur = db.rawQuery(query, null);

            while (cur.moveToNext()) {
                boolean isFrequent= cur.getString(3).equalsIgnoreCase("1")?true:false;
                list.add(new RowWithHomnumForBookmark(cur.getString(1),cur.getString(0),cur.getString(2),isFrequent,cur.getString(4)));
            }
        }
        db.close();
        setFavoriteItems(list);
    }

    //for sorting bookmarks z to a
    public void sort_ztoa()
    {
        List<AlphabetListAdapter.Row> list = new ArrayList<AlphabetListAdapter.Row>();
        SQLiteDatabase db = DbConnection.getUserDbConnection();
        if (db != null) {
            String query = String.format("select id,hwd,homnum,frequent,entryDate from favorites order by lower(hwd) desc");
            Cursor cur = db.rawQuery(query, null);

            while (cur.moveToNext()) {
                boolean isFrequent= cur.getString(3).equalsIgnoreCase("1")?true:false;
                list.add(new RowWithHomnumForBookmark(cur.getString(1),cur.getString(0),cur.getString(2),isFrequent,cur.getString(4)));
            }
        }
        db.close();
        setFavoriteItems(list);
    }

    //for sorting bookmarks date oldest first
    public void sort_date_oldest()
    {
        List<AlphabetListAdapter.Row> list = new ArrayList<AlphabetListAdapter.Row>();
        SQLiteDatabase db = DbConnection.getUserDbConnection();
        if (db != null) {
            String query = String.format("select id,hwd,homnum,frequent, entryDate, SUBSTR(entrydate, 7,4)||'-'|| SUBSTR(entryDate, 4,2)||'-'||SUBSTR(entryDate, 1,2) as finalDate from favorites order by finalDate asc");
            Cursor cur = db.rawQuery(query, null);

            while (cur.moveToNext()) {
                boolean isFrequent= cur.getString(3).equalsIgnoreCase("1")?true:false;
                list.add(new RowWithHomnumForBookmark(cur.getString(1),cur.getString(0),cur.getString(2),isFrequent,cur.getString(4)));
            }
        }
        db.close();
        setFavoriteItems(list);
    }

    //for sorting bookmarks date recent first
    public void sort_date_recent()
    {
        List<AlphabetListAdapter.Row> list = new ArrayList<AlphabetListAdapter.Row>();
        SQLiteDatabase db = DbConnection.getUserDbConnection();
        if (db != null) {
            String query = String.format("select id,hwd,homnum,frequent, entryDate, SUBSTR(entrydate, 7,4)||'-'|| SUBSTR(entryDate, 4,2)||'-'||SUBSTR(entryDate, 1,2) as finalDate from favorites order by finalDate desc");
            Cursor cur = db.rawQuery(query, null);

            while (cur.moveToNext()) {
                boolean isFrequent= cur.getString(3).equalsIgnoreCase("1")?true:false;
                list.add(new RowWithHomnumForBookmark(cur.getString(1),cur.getString(0),cur.getString(2),isFrequent,cur.getString(4)));
            }
        }
        db.close();
        setFavoriteItems(list);
    }

    //for deleting all the entries inside bookmarks page
    public void delete_all()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(getActivity(), android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(getActivity());
        }
        // Title for Delete pop up in the favorites page
        builder.setTitle(R.string.confirm_delete);
        builder.setMessage(R.string.are_you_sure_you_want_to_delete_all_your_bookmarks);
        // If User accepted to delete all the bookmark entries
        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                //Pass the entry ids to the onDeleteEntriesInList and refresh the favorites page
                final ListView listView = (ListView) getActivity().findViewById(R.id.lv_favorites);
                AlphabetListAdapter adapter = (AlphabetListAdapter) listView.getAdapter();
                int count = listView.getAdapter().getCount();
                SQLiteDatabase db = DbConnection.getUserDbConnection();
                // String array to hold all the entry id in favorites page
                String[] hwdID = new String[count];
                if (db != null) {
                    String query = String.format("select id from favorites");
                    Cursor cur = db.rawQuery(query, null);
                    int i = 0;
                    while (cur.moveToNext()) {
                        if (i < count) {
                            hwdID[i] = cur.getString(0);
                            i++;
                        }
                    }
                }
                db.close();
                boolean isDeleted = Utils.deleteItemFromFavorite(hwdID);
                if (isDeleted) {
                    // Changing the Favorite Button State in the detail Page for the deleted words
                    favoriteButtonState(hwdID);
                    // UPdating the Favorites list to users
                    updateFavoritesEntryList();
                    Bundle params=new Bundle();
                    params.putString("eventCategory","bookmark_button_action");
                    params.putString("eventAction","delete_all_confirmation");
                    params.putString("eventLabel","bookmark_cleared");
                    FirebaseWrapperScreens.EventLogs(params);
                    // Changing the action and Visiblity of Menu button in Favorites
                    final Button menuButton=(Button)getActivity().findViewById(R.id.fav_Menu_button);
                    menuButton.setAlpha(0.3f);
                    menuButton.setEnabled(false);
                }
                else
                {
                    favoriteButtonState(hwdID);
                    // UPdating the Favorites list to users
                    updateFavoritesEntryList();
                    // Changing the action and Visiblity of Menu button in Favorites
                    final Button menuButton=(Button)getActivity().findViewById(R.id.fav_Menu_button);
                    menuButton.setAlpha(0.3f);
                    menuButton.setEnabled(true);
                }

            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
                System.out.println();
                Bundle params=new Bundle();
                params.putString("eventCategory","view_bookmark_button_action");
                params.putString("eventAction","bookmarks_cancel_button_tapped");
                params.putString("eventLabel","");
                FirebaseWrapperScreens.EventLogs(params);
            }
        });
        builder.show();
    }

    public void setFavoriteItems(List<AlphabetListAdapter.Row> list){
        final ListView listView = (ListView)getActivity().findViewById(R.id.lv_favorites);
        AlphabetListAdapter adapter = (AlphabetListAdapter) listView.getAdapter();
        if(adapter!=null){
            adapter.setRows(list);
            adapter.notifyDataSetChanged();
        }
        else {
            adapter = new AlphabetListAdapter();
            adapter.setRows(list);
            listView.setAdapter(adapter);
        }
    }

    @Override
    public void onResume() {
        SharedPreferences sharedPreferences =  getActivity().getSharedPreferences("LDOCE6PrefsFile", Context.MODE_PRIVATE);
        boolean isSearchOnClipboardOn=sharedPreferences.getBoolean("Search-on-clipboard", false);
        boolean isSaveSearchWordOn=sharedPreferences.getBoolean("SaveSearchWord",false);
        if(Utils.isTablet(getActivity())){

        if(isSaveSearchWordOn && !isSearchOnClipboardOn){
            ((SlideMenuSearchAndIndex)getActivity()).showLastSearchedWord();
        }
       }
//        else {
//            if(!isSaveSearchWordOn && !isSearchOnClipboardOn) {
//                ((SlideMenuSearchAndIndex) getActivity()).showWord();
//            }
//        }
        super.onResume();
    }

    private void applyFontToMenuItem(MenuItem mi) {
        Typeface font = Singleton.getMundoSansPro_Medium();

        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("" , font), 0 , mNewTitle.length(),  Spannable.SPAN_INCLUSIVE_INCLUSIVE);

        mi.setTitle(mNewTitle);
    }
}


