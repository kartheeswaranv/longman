package com.mobifusion.android.ldoce5.Util;

import android.app.AppOpsManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
//import android.support.v4.app.Fragment;
//import android.support.v4.app.FragmentActivity;
//import android.support.v4.app.FragmentManager;
//import android.support.v4.app.FragmentTransaction;
import android.util.DisplayMetrics;
import android.util.Log;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.mobifusion.android.ldoce5.Activity.DbConnection;
import com.mobifusion.android.ldoce5.Fragment.DetailPageFragment;
import com.mobifusion.android.ldoce5.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by vthanigaimani on 3/27/15.
 */
public class Utils {


    private static MediaPlayer mediaPlayer;
    public static String CoreDBFilePathAndName = "/data/data/com.mobifusion.android.ldoce5/files/ldoce.sqlite";
    public static String UserDBFileNameAndPath = "/data/data/com.mobifusion.android.ldoce5/files/UserDb.sqlite";

    public static float pxFromDp(final Context context, final float dp) {
        return dp * context.getResources().getDisplayMetrics().density;
    }

    public static int convertPxToDp(final Context context,final int input) { // Get the screen's density scale
     final float scale = context.getResources().getDisplayMetrics().density; // Convert the dps to pixels, based on density scale
      return (int) (input * scale + 0.5f); }

    public static boolean isTablet(Context context) {
        return context.getResources().getBoolean(R.bool.isTablet);
    }

    /**
     * Used to navigate to detail page fragment by passing the Main activity
     * Automatically added to the general back stack
     * Handle back separately
     * @param context
     * @param headword
     * @param hwdID
     * @param homnum
     * @param senseNum
     * @return true if successfully navigated
     */
    public static boolean navigateToDetailPage(FragmentActivity context, String headword, String hwdID, String homnum, String senseNum) {
        //Getting fragment manager and fragment transaction
        FragmentManager fragmentManager = context.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment currentFragment;
        DetailPageFragment detailPageFragment = new DetailPageFragment();
        detailPageFragment.setDetailPageArguments(headword,hwdID,homnum,senseNum);
        Boolean isNavigationSuccess = false;
        //Checking current device
        //Based on the device get the respective fragment ID
        //Replace the current fragment with Detail fragment

        if (headword != null && !hwdID.equals("") && Utils.CheckPageLoading(context)) {
            if (Utils.isTablet(context)) {
                currentFragment = fragmentManager.findFragmentById(R.id.detailPageFragment);
                fragmentTransaction.replace(R.id.detailPageFragment, detailPageFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                isNavigationSuccess = true;
            } else {
                currentFragment = fragmentManager.findFragmentById(R.id.indexResultfragment);
                fragmentTransaction.replace(R.id.indexResultfragment, detailPageFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                isNavigationSuccess = true;
            }
            return isNavigationSuccess;
        }
        return false;
    }

    /**
     * Setting application language based on the user selection
     * default language will be en_GB
     *
     * @param context
     * @param languageCode
     */
    public static void setApplicationLanguage(Context context, String languageCode) {
        Locale myLocale;
        String[] langWithCode = languageCode.split("_");
        if (langWithCode.length > 1)
            myLocale = new Locale(langWithCode[0], langWithCode[1]);//new Locale(lang);
        else
            myLocale = new Locale(langWithCode[0]);
        Resources res = context.getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }

    public static Boolean CheckPageLoading(FragmentActivity context) {
        Fragment currentFragment;
        if (Utils.isTablet(context))
            currentFragment = context.getSupportFragmentManager().findFragmentById(R.id.detailPageFragment);
        else
            currentFragment = context.getSupportFragmentManager().findFragmentById(R.id.indexResultfragment);

        if (currentFragment instanceof DetailPageFragment) {
            //check whether previously Tapped Word is fully Loaded or not -  taking from DetailPageFragment
            return ((DetailPageFragment) currentFragment).isLoaded;
        }
        return true;
    }
    //to get the HwdId and pass it to "navigateToDetailPage" function
    public static String getHwdId(String hwd,String homnum){
        String hwdID="";
        SQLiteDatabase db= DbConnection.getDbConnection();
        try{
            if(db!=null){
                String query;
                Cursor cur;
                if(homnum.isEmpty()) {
                    query = String.format("select id from core where HWD=? limit 1");
                    cur = db.rawQuery(query, new String[]{hwd});
                }
                else {
                    query = String.format("select id from core where HWD = ? and HOMNUM = ?");
                    cur = db.rawQuery(query, new String[]{hwd, homnum});
                }
                while(cur.moveToNext()){
                    //get id alone and return it
                    hwdID=cur.getString(0);
                }
            }
            //close the db connection
            db.close();
        }catch (Exception e){
            Log.w("Error", e.getStackTrace().toString());
        }
        return  hwdID;
    }
    //check the tapper word is present in DB or not, if present return true otherwise return false
    public static boolean checkHwd(String hwd){
        boolean isHwdAvailable=false;
        SQLiteDatabase db= DbConnection.getDbConnection();
        try
        {
            if(db!=null){
                String query=String.format("select * from core where hwd=?");
                Cursor cur=db.rawQuery(query, new String[]{hwd});
                if(cur.getCount()>0){
                    isHwdAvailable=true;
                }
            }
            //close the Db
            db.close();
        }

        catch(Exception e){
            Log.w("Error", e.getStackTrace().toString());
        }
        return isHwdAvailable;
    }
    /**
     * to check a word is favorite or not
     *
     * @param id the id of the word is used to check
     * @return true if word is already in favorites, false if word is not in favorites
     */
    public static boolean isFavorite(String id) {
        boolean isFavorite = false;
        SQLiteDatabase db = DbConnection.getUserDbConnection();
        // Check if the current existence of DB is not null
        try {
            if (db != null) {
                // Query to get the records from favorite table
                String query = String.format("select * from favorites where id=?");
                Cursor cur = db.rawQuery(query, new String[]{id});
                if (cur.getCount() > 0) {
                    isFavorite = true;
                }
            }
            // Close the DB Connection
            db.close();
        } catch (Exception e) {
            Log.w("Error", e.getStackTrace().toString());
        }
        return isFavorite;
    }

    //TODO: make function to delete a list if items from the favorite using array of IDs

    /**
     * to delete an entry from the favorite
     *
     * @param entriesIdArray array of id's to be deleted
     * @return true of word is deleted, or false if not deleted
     */
    public static boolean deleteItemFromFavorite(String[] entriesIdArray) {

        boolean isDeletedSuccess = false;
        //String to query the db
        String favoriteEntryDeletionQuery;

        // Get the User DB Connection
        SQLiteDatabase db = DbConnection.getUserDbConnection();

        //Transaction is used optimise the execution of queries
        db.beginTransaction();

        //When a db exists
        if (db != null) {

            for (String id : entriesIdArray) {
                //Query for deleting entries from history
                favoriteEntryDeletionQuery = "delete from favorites where id = ?";
                int count = db.delete("favorites", "id=?", new String[]{id});
                if (count > 0) {
                    isDeletedSuccess = true;
                } else {

                    isDeletedSuccess = false;
                }
            }
        }
        //Set the transaction as success
        db.setTransactionSuccessful();

        //End the transaction
        db.endTransaction();
        db.close();
        //to check whether the selected Entries are deleted from DB
        return isDeletedSuccess;
    }

    /**
     * to insert a entry to favorite
     *
     * @param wordInfo JSON object with info about the word like ID,hwd,homnum,frequency
     * @return true if successfully inserted, or false if error occures
     */
    public static boolean insertItemToFavourite(JSONObject wordInfo) {

        boolean status = false;
        SQLiteDatabase db = DbConnection.getUserDbConnection();
        try {
            if (db != null) {
                SimpleDateFormat dateFormat = new SimpleDateFormat(
                        "dd/MM/yyyy", Locale.getDefault());
                Date date = new Date();
                String currentDate=dateFormat.format(date);
                //getting values to insert in row
                ContentValues values = new ContentValues();
                values.put("id", wordInfo.getString("id"));
                values.put("hwd", wordInfo.getString("hwd"));
                values.put("homnum", wordInfo.getString("homnum"));
                values.put("frequent", wordInfo.getString("frequent"));
                values.put("entryDate",currentDate);
                //querying to insert
                long rowID = db.insert("favorites", null, values);
                status = rowID > -1 ? true : false;
            }
        } catch (SQLiteException e) {

        } catch (JSONException e) {
            e.printStackTrace();
        }
        db.close();
        return status;
    }

    public static String getMenuName (String item)
    {
        switch (item)
        {
            case "FavoritesFragment":
                return "favorites";
            case "HistoryFragment":
                return "history";
            case "CoreVocabularyFragment":
                return "core_vocabulary";
            case "AboutFragment":
                return "about_this_app";
            case "SettingsFragment":
                return "settings";
            default:
                return "";
        }
    }

    /***
     * Function to check the "show notification" check box is check or uncheck
     * Based on  the status of the check box Notification schedule will be enabled or disabled
     * Works in kitkat and above.
     * @param context
     * @return
     */
    public boolean isNotificationEnabled(Context context) {
        String CHECK_OP_NO_THROW = "checkOpNoThrow";
        String OP_POST_NOTIFICATION = "OP_POST_NOTIFICATION";

        AppOpsManager mAppOps = (AppOpsManager) context.getSystemService(Context.APP_OPS_SERVICE);
        ApplicationInfo appInfo = context.getApplicationInfo();
        String pkg = context.getApplicationContext().getPackageName();
        int uid = appInfo.uid;
        Class appOpsClass = null; /* Context.APP_OPS_MANAGER */
        try {
            appOpsClass = Class.forName(AppOpsManager.class.getName());
            Method checkOpNoThrowMethod = appOpsClass.getMethod(CHECK_OP_NO_THROW, Integer.TYPE, Integer.TYPE, String.class);
            Field opPostNotificationValue = appOpsClass.getDeclaredField(OP_POST_NOTIFICATION);
            int value = (int)opPostNotificationValue.get(Integer.class);
            return ((int)checkOpNoThrowMethod.invoke(mAppOps,value, uid, pkg) == AppOpsManager.MODE_ALLOWED);

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean detailPageNavigation(FragmentActivity context,String headword,String hwdID,String homnum,String senseNum, int cellHeight) {
        //Getting fragment manager and fragment transaction
        FragmentManager fragmentManager = context.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment currentFragment;

        DetailPageFragment detailPageFragment = new DetailPageFragment();
        detailPageFragment.setDetailPageArguments(headword,hwdID,homnum,senseNum);

        //detailPageFragment.setDetailPageArguments(headword,hwdID,homnum,senseNum,true,h1);
        Boolean isNavigationSuccess = false;
        //Checking current device
        //Based on the device get the respective fragment ID
        //Replace the current fragment with Detail fragment

        if (headword != null && !hwdID.equals("") && Utils.CheckPageLoading(context)) {
            if (Utils.isTablet(context)) {
                currentFragment = fragmentManager.findFragmentById(R.id.detailPageFragment);
                fragmentTransaction.replace(R.id.detailPageFragment, detailPageFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                isNavigationSuccess = true;
                detailPageFragment.thesRedirect=true;

            } else {
                detailPageFragment.thesRedirect=true;
                currentFragment = fragmentManager.findFragmentById(R.id.indexResultfragment);
                fragmentTransaction.replace(R.id.indexResultfragment, detailPageFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                isNavigationSuccess = true;
            }

            return isNavigationSuccess;
        }
        return false;
    }

    public static String getTodayDate(){
        String todayDate=null;
        todayDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        return todayDate;
    }

    public static boolean isNetworkAvailable(Context context) {
//        Runtime runtime = Runtime.getRuntime();
//        try {
//
//            Process ipProcess = runtime.exec("/system/bin/ping -c 1 8.8.8.8");
//            int     exitValue = ipProcess.waitFor();
//            return (exitValue == 0);
//
//        } catch (IOException e)          { e.printStackTrace(); }
//        catch (InterruptedException e) { e.printStackTrace(); }
//
//        return false;
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public static void getBookmarkFromXML(Context context) {

        final String PREF_FILE_NAME_LDOCE = "LDOCE6PrefsFile";
        SharedPreferences preferencesLDOCE = context.getSharedPreferences(PREF_FILE_NAME_LDOCE, Context.MODE_PRIVATE);
        boolean importedBookmark=preferencesLDOCE.getBoolean("importedBookmarks",false);
        if(!importedBookmark) {
            final String PREF_FILE_NAME = "PEARSONDictPreferences";
            SharedPreferences preferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
            String values = preferences.getString("BOOKMARKS", "");
            List<String> headwords = new ArrayList<>();
            if (!values.isEmpty()) {
                Pattern pattern = Pattern.compile("(\\W|\\w.*?)~~~~(\\d*)(,,,,)?");
                Matcher matcher = pattern.matcher(values);
                while (matcher.find()) {
                    headwords.add(matcher.group(1));
                }
            }

            String[] hwdArray = new String[headwords.size()];
            hwdArray = headwords.toArray(hwdArray);
            insertBookmarksIntoFavorite(hwdArray, context);
        }
        //set bookmarks are imported.
        SharedPreferences.Editor editor = preferencesLDOCE.edit();
        editor.putBoolean("importedBookmarks", true);
        // Commit the edits!
        editor.apply();
    }

    /**
     * Method to insert the previous app's bookmarks into favorites
     * @param bookmarksHwd
     */
    public static void insertBookmarksIntoFavorite(String[] bookmarksHwd, Context context) {
        if (bookmarksHwd.length > 0) {
            // Get the DB Connection
            SQLiteDatabase getDb = DbConnection.getDbConnection();
            // Get the User DB Connection
            SQLiteDatabase userDb = DbConnection.getUserDbConnection();
            // Check if the current existence of DB is not null
            if (getDb != null) {
                String query = String.format("select id as ID,hwd as HWD,frequent as FREQUENT, homnum as HOMNUM from lookup where hwd in (");
                for (int i = 0; i < bookmarksHwd.length; i++) {
                    query += "?";
                    if (i != bookmarksHwd.length - 1) {
                        query += ",";
                    }
                }
                query += ") and (HOMNUM=1 OR HOMNUM='')";
                long success = 0;
                Cursor cur = getDb.rawQuery(query, bookmarksHwd);
                // Transaction is used optimise the execution of queries
                userDb.beginTransaction();
                SimpleDateFormat dateFormat = new SimpleDateFormat(
                        "dd/MM/yyyy", Locale.getDefault());
                Date date = new Date();
                String currentDate=dateFormat.format(date);
                while (cur.moveToNext()) {
                    ContentValues values = new ContentValues();
                    values.put("id", cur.getString(cur.getColumnIndex("ID")));
                    values.put("hwd", cur.getString(cur.getColumnIndex("HWD")));
                    values.put("frequent", cur.getString(cur.getColumnIndex("FREQUENT")));
                    values.put("homnum", cur.getString(cur.getColumnIndex("HOMNUM")));
                    values.put("entryDate",currentDate);
                    // Inserting into Favorites table
                    success = userDb.insert("favorites", null, values);
                    System.out.println("The insertion is :" + success);
                }
                // Set the transaction as success
                userDb.setTransactionSuccessful();
                // End the Transaction
                userDb.endTransaction();
                userDb.close();
                getDb.close();
            }
        }
    }
}


