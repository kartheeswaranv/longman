package com.mobifusion.android.ldoce5.Util;

import android.app.Activity;
import android.graphics.Typeface;


/**
 * Created by sureshchandrababu on 06/02/15.
 */
public class Singleton {
    private static Singleton mInstance = null;
    private static Activity mContext;
    private Singleton(){
    }
    private static Typeface MundoSansPro;
    private static Typeface MundoSansPro_Bold;
    private static Typeface MundoSansPro_BoldItalic;
    private static Typeface MundoSansPro_Italic;
    private static Typeface MundoSansPro_Light;
    private static Typeface MundoSansPro_LightItalic;
    private static Typeface MundoSansPro_Medium;
    private static Typeface MundoSansPro_MediumItalic;
    private static Typeface CharisSILI;
    private static Typeface CharisSILR;
    private static Typeface Longman;

    public static Singleton getInstance(Activity context){
        if(mInstance == null)
        {
            mInstance = new Singleton();
            MundoSansPro=Typeface.createFromAsset(context.getAssets(),"MundoSansPro.otf");
            MundoSansPro_Bold=Typeface.createFromAsset(context.getAssets(),"MundoSansPro-Bold.otf");
            MundoSansPro_BoldItalic=Typeface.createFromAsset(context.getAssets(),"MundoSansPro-BoldItalic.otf");
            MundoSansPro_Italic=Typeface.createFromAsset(context.getAssets(),"MundoSansPro-Italic.otf");
            MundoSansPro_Light=Typeface.createFromAsset(context.getAssets(),"MundoSansPro-Light.otf");
            MundoSansPro_LightItalic=Typeface.createFromAsset(context.getAssets(),"MundoSansPro-LightItalic.otf");
            MundoSansPro_Medium=Typeface.createFromAsset(context.getAssets(),"MundoSansPro-Medium.otf");
            MundoSansPro_MediumItalic=Typeface.createFromAsset(context.getAssets(),"MundoSansPro-MediumItalic.otf");
            CharisSILI=Typeface.createFromAsset(context.getAssets(),"CharisSILI.ttf");
            CharisSILR=Typeface.createFromAsset(context.getAssets(),"CharisSILR.ttf");
            Longman=Typeface.createFromAsset(context.getAssets(),"longman.ttf");
        }
        return mInstance;
    }

    public static Typeface getMundoSansPro() {
        return MundoSansPro;
    }

    public static void setMundoSansPro(Typeface mundoSansPro) {
        MundoSansPro = mundoSansPro;
    }

    public static Typeface getMundoSansPro_Bold() {
        return MundoSansPro_Bold;
    }

    public static void setMundoSansPro_Bold(Typeface mundoSansPro_Bold) {
        MundoSansPro_Bold = mundoSansPro_Bold;
    }

    public static Typeface getMundoSansPro_BoldItalic() {
        return MundoSansPro_BoldItalic;
    }

    public static void setMundoSansPro_BoldItalic(Typeface mundoSansPro_BoldItalic) {
        MundoSansPro_BoldItalic = mundoSansPro_BoldItalic;
    }

    public static Typeface getMundoSansPro_Italic() {
        return MundoSansPro_Italic;
    }

    public static void setMundoSansPro_Italic(Typeface mundoSansPro_Italic) {
        MundoSansPro_Italic = mundoSansPro_Italic;
    }

    public static Typeface getMundoSansPro_Light() {
        return MundoSansPro_Light;
    }

    public static void setMundoSansPro_Light(Typeface mundoSansPro_Light) {
        MundoSansPro_Light = mundoSansPro_Light;
    }

    public static Typeface getMundoSansPro_LightItalic() {
        return MundoSansPro_LightItalic;
    }

    public static void setMundoSansPro_LightItalic(Typeface mundoSansPro_LightItalic) {
        MundoSansPro_LightItalic = mundoSansPro_LightItalic;
    }

    public static Typeface getMundoSansPro_Medium() {
        return MundoSansPro_Medium;
    }

    public static void setMundoSansPro_Medium(Typeface mundoSansPro_Medium) {
        MundoSansPro_Medium = mundoSansPro_Medium;
    }

    public static Typeface getMundoSansPro_MediumItalic() {
        return MundoSansPro_MediumItalic;
    }

    public static void setMundoSansPro_MediumItalic(Typeface mundoSansPro_MediumItalic) {
        MundoSansPro_MediumItalic = mundoSansPro_MediumItalic;
    }

    public static Typeface getCharisSILI() {
        return CharisSILI;
    }

    public static void setCharisSILI(Typeface charisSILI) {
        CharisSILI = charisSILI;
    }

    public static Typeface getCharisSILR() {
        return CharisSILR;
    }

    public static void setCharisSILR(Typeface charisSILR) {
        CharisSILR = charisSILR;
    }

    public static Typeface getLongman() {
        return Longman;
    }

    public static void setLongman(Typeface longman) {
        Longman = longman;
    }


}
