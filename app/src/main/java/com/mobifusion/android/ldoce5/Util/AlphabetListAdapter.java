package com.mobifusion.android.ldoce5.Util;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.text.Html;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mobifusion.android.ldoce5.R;
import com.mobifusion.android.ldoce5.model.AdditionalInfoItem;
import com.mobifusion.android.ldoce5.model.IndexRowItem;
import com.mobifusion.android.ldoce5.model.RowItemWithIcon;
import com.mobifusion.android.ldoce5.model.RowWithHomnum;
import com.mobifusion.android.ldoce5.model.RowWithFrequency;
import com.mobifusion.android.ldoce5.model.RowWithHomnumForBookmark;
import com.mobifusion.android.ldoce5.model.SearchRowItem;

import org.w3c.dom.Text;

import java.util.List;

public class AlphabetListAdapter extends BaseAdapter {
    private SparseBooleanArray mSelectedItemsIds;
    SharedPreferences fontSizePreferences;
    public static final String PREFS_NAME = "LDOCE6PrefsFile";

    public AlphabetListAdapter() {
        mSelectedItemsIds = new SparseBooleanArray();
    }

    public static abstract class Row {}
    
    public static final class Section extends Row {
        public final String text;

        public Section(String text) {
            this.text = text;
        }
    }
    
    public static final class Item extends Row {
        public final String text;

        public Item(String text) {
            this.text = text;
        }
    }
    private List<Row> rows;
    // used to keep selected position in ListView
    private int selectedPos = -1;
    public List<Row> getRows() {
        return rows;
    }
    public void setRows(List<Row> rows) {
        this.rows = rows;
    }

    public void updateRows(List<Row> rows){
        this.rows = rows;
        notifyDataSetChanged();
    }
    @Override
    public int getCount() {
        return rows.size();
    }

    @Override
    public Row getItem(int position) {
        return rows.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    
    @Override
    public int getViewTypeCount() {
        return 8;
    }
    
    @Override
    public int getItemViewType(int position) {
        if (getItem(position) instanceof Section) {
            return 1;
        }
        else if(getItem(position) instanceof RowItemWithIcon){
            return 2;
        }
        else if(getItem(position) instanceof SearchRowItem){
            return 3 ;
        }
        else if(getItem(position) instanceof RowWithHomnum){
            return 4;
        }
        else if (getItem(position) instanceof RowWithFrequency){
            return 5;
        }
        else if(getItem(position) instanceof AdditionalInfoItem){
            return 6;
        }
        else if (getItem(position)instanceof RowWithHomnumForBookmark){
            return 7;
        }
        else {
            return 0;
        }
    }
    public void removeSelection()
    {
        mSelectedItemsIds= new SparseBooleanArray();
        notifyDataSetChanged();
    }
    public SparseBooleanArray getSelectedIds(){
        return mSelectedItemsIds;
    }

    public void toggleSelection(int position) {
        selectView(position, !mSelectedItemsIds.get(position));

    }

    public void setSelection(int position){
        selectView(position,true);
    }
    public void removeSelectionAt(int position){
        selectView(position,false);
    }
    private void selectView(int position, boolean value) {
        if (value) {
            mSelectedItemsIds.put(position, value);
            System.out.println(position+""+value);
        }
        else {
            mSelectedItemsIds.delete(position);
        }
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        fontSizePreferences = parent.getContext().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        int fontSizeValue = fontSizePreferences.getInt("FONT_VALUE",0);
        //Checking what type of items should be shown
        if (getItemViewType(position) == 0) {
        //First level oor second level item
            if (view == null) {
                LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = (LinearLayout) inflater.inflate(R.layout.row_item, parent, false);
            }
            //Getting the item and setting the text in listview
            IndexRowItem item = (IndexRowItem) getItem(position);
            TextView textView = (TextView) view.findViewById(R.id.rowText);
            textView.setText(item.getHwd());
            textView.setTextColor((item.getIsFrequent())?Color.parseColor("#E45238"):Color.parseColor("#38678A"));
            textView.setTextSize(16+fontSizeValue);
            textView.setTypeface(Singleton.getMundoSansPro_Bold());
        }
        else if(getItemViewType(position) == 2)
        {
            //items is a slidemenu item with icon
            if (view == null) {
                LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = (LinearLayout) inflater.inflate(R.layout.row_item_with_icon, parent, false);
            }

            RowItemWithIcon item = (RowItemWithIcon) getItem(position);
            TextView textView = (TextView) view.findViewById(R.id.rowText);
            //setting the item title with mundo sans pro font
            textView.setText(item.getStringCode());
            textView.setTextSize(18+fontSizeValue);
            textView.setTypeface(Singleton.getMundoSansPro());
            //setting the icon with longman font
            TextView iconTextView = (TextView) view.findViewById(R.id.iconText);
            iconTextView.setText(item.getIconCode());
            iconTextView.setTextSize(20+fontSizeValue);
            iconTextView.setTypeface(Singleton.getLongman());
        }
        else if (getItemViewType(position)==3) {
            //First level or second level item
            if (view == null) {
                LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = (LinearLayout) inflater.inflate(R.layout.row_item, parent, false);
            }
            //Getting the item and setting the text in listview
            SearchRowItem item = (SearchRowItem) getItem(position);
            TextView textView = (TextView) view.findViewById(R.id.rowText);
            textView.setText(item.getHwd());
            textView.setTextColor((item.getIsFrequent()) ? Color.parseColor("#E45238") : Color.parseColor("#38678A"));
            textView.setTextSize(18+fontSizeValue);
            textView.setTypeface(Singleton.getMundoSansPro_Medium());
        }
        else if (getItemViewType(position)==4) {
            //Core Voc
            //Getting the item and setting the text in listview
            RowWithHomnum item = (RowWithHomnum) getItem(position);
            if(item.isSelected){
                LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = (LinearLayout) inflater.inflate(R.layout.selectedrow_item, parent, false);
            }
            else
            {
                LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = (LinearLayout) inflater.inflate(R.layout.row_item, parent, false);
            }
            TextView textView = (TextView) view.findViewById(R.id.rowText);
            textView.setText(Html.fromHtml(item.getHwd() + "<sup>" + item.getHomnum() + "</sup>"));
            textView.setTextSize(18 + fontSizeValue);
            textView.setTextColor((item.getIsFrequent()) ? Color.parseColor("#FF0000") : Color.parseColor("#FFFFFF"));
            textView.setTypeface(Singleton.getMundoSansPro());
        }
        else if (getItemViewType(position)==5) {
            // History
            // Getting the item and setting the text in ListView
            RowWithFrequency item = (RowWithFrequency) getItem(position);
            if(item.isSelected) {
                LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = (LinearLayout) inflater.inflate(R.layout.selectedrow_item, parent, false);
            }
            else{
                LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = (LinearLayout) inflater.inflate(R.layout.row_item, parent, false);
            }
            TextView textView  = (TextView) view. findViewById(R.id.rowText);
            textView.setText(item.getHwd());
            textView.setTextSize(18+fontSizeValue);
            textView.setTextColor((item.getIsFrequent()) ? Color.parseColor("#FF0000") : Color.parseColor("#FFFFFF"));
            textView.setTypeface(Singleton.getMundoSansPro());
        }
        else if (getItemViewType(position)==6) {
            //for additional info
            if (view == null) {
                LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = (LinearLayout) inflater.inflate(R.layout.row_item, parent, false);
            }
            //Getting the item and setting the text in listview
            AdditionalInfoItem item = (AdditionalInfoItem) getItem(position);
            TextView textView = (TextView) view.findViewById(R.id.rowText);
            textView.setText(item.text);
            textView.setTextSize(18 + fontSizeValue);
            textView.setTypeface(Singleton.getMundoSansPro());
        }
        else if (getItemViewType(position)==7){
            //Bookmark
            //Getting the item and setting the text in listview
            RowWithHomnumForBookmark item = (RowWithHomnumForBookmark) getItem(position);
            if(item.isSelected){
                LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = (LinearLayout) inflater.inflate(R.layout.selectedrow_item_bookmark, parent, false);
            }
            else
            {
                LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = (LinearLayout) inflater.inflate(R.layout.fav_row_item, parent, false);
            }
            TextView textView = (TextView) view.findViewById(R.id.favRowText);
            textView.setText(Html.fromHtml(item.getHwd() + "<sup>" + item.getHomnum() + "</sup>"));
            textView.setTextSize(18 + fontSizeValue);
            textView.setTextColor((item.getIsFrequent()) ? Color.parseColor("#FF0000") : Color.parseColor("#FFFFFF"));
            textView.setTypeface(Singleton.getMundoSansPro());

            TextView textView1 = (TextView)view.findViewById(R.id.favDateText);
            textView1.setText(item.getEntryDate());
            textView1.setTextSize(12 + fontSizeValue);
            textView1.setTextColor(Color.parseColor("#FFFFFF"));
            textView1.setTypeface(Singleton.getMundoSansPro_Medium());
        }
        else {
        // Section item currently we are not using
            if (view == null) {
                LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = (LinearLayout) inflater.inflate(R.layout.row_section, parent, false);
            }

            Section section = (Section) getItem(position);
            TextView textView = (TextView) view.findViewById(R.id.sectionText);
            textView.setText(section.text);
            //textView.setTextSize(10);
            //textView.setTextColor(Color.parseColor("#ffffff"));
            textView.setTypeface(Singleton.getMundoSansPro());
        }

        return view;
    }

}
