package com.mobifusion.android.ldoce5.Activity;

import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import androidx.fragment.app.FragmentActivity;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.widget.TextView;

//import com.google.android.gms.analytics.HitBuilders;
//import com.google.android.gms.analytics.Tracker;
import com.mobifusion.android.ldoce5.R;
import com.mobifusion.android.ldoce5.Util.FirebaseWrapperScreens;
import com.mobifusion.android.ldoce5.Util.LDOCEApp;
import com.mobifusion.android.ldoce5.Util.Singleton;
import com.mobifusion.android.ldoce5.Util.Utils;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

/**
 * Created by Bazi on 16/06/15.
 */
public class WordOfTheDayPopUp extends Dialog implements View.OnClickListener{

    FragmentActivity context;
    String hwdId;
//    Tracker t;
    public static final String PREFS_NAME = "LDOCE6PrefsFile";
    SharedPreferences fontSizePrefrences = getContext().getSharedPreferences(PREFS_NAME,Context.MODE_PRIVATE);
    int fontSizeValue = fontSizePrefrences.getInt("FONT_VALUE",0);

    public WordOfTheDayPopUp(FragmentActivity context) {
        super(context);
        this.context=context;
        this.hwdId="";
    }

    public WordOfTheDayPopUp(FragmentActivity context,String hwdId){
        super(context);
        this.context=context;
        this.hwdId=hwdId;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.word_popup);
        //Initialise the tracker
//        t = ((LDOCEApp) getContext().getApplicationContext()).getTracker(
//                LDOCEApp.TrackerName.APP_TRACKER);
        FirebaseWrapperScreens.ScreenViewEvent("Word of the Day Popup");

//        t.setScreenName("Word of the Day Popup");
//        // Sending Hit to GA When new Hits are tracked
//        t.send(new HitBuilders.AppViewBuilder().build());
    }

    private void populateContent() throws TransformerException, IOException {

        SQLiteDatabase getDb = DbConnection.getUserDbConnection();
        String hwdId="";
        String hwd="";
        String content="";
        if(getDb!=null) {
            Cursor cursor=getDb.rawQuery("select id from userNotifications",null);
            while (cursor.moveToNext()){
                hwdId=cursor.getString(0);
            }
            this.hwdId=hwdId;
            SQLiteDatabase coreDb=DbConnection.getDbConnection();
            Cursor cur=coreDb.rawQuery("select hwd,core from core where id = ?",new String[]{hwdId});
            while (cur.moveToNext()){
                hwd=cur.getString(0);
                content=cur.getString(1);
            }
        }
        content=content.replaceAll("\n","");
        //to remove tab space from the String
        content=content.replaceAll("\t", "");
        content=content.replaceAll("\r", "");
        content=content.replaceAll("<SYN>(.*?)</SYN>","");
        content=content.replaceAll("<OPP>(.*?)</OPP>","");
        content=content.replaceAll("<Thesref>(.*?)</Thesref>","");
        content=content.replaceAll("<ExpandableInformation>(.*?)</ExpandableInformation>","");
        content=content.replaceAll("<RELATEDWD>(.*?)</RELATEDWD>","");
        String patternString="(<Sense.*?>.*?</Sense>)";
        Pattern pattern= Pattern.compile(patternString);
        //Fetch the DEF Value
        Matcher matcher= pattern.matcher(content);
        String popUpContent="";
        while (matcher.find()) {

            String firstDefValue = matcher.group(1);
            //If the <DEF> tag value is not null add it it to List
            if(firstDefValue!=null){
                System.out.print(firstDefValue);
                popUpContent=firstDefValue;

            }
            break;

        }

        String subPatternString="(<Subsense>.*?</Subsense>)";
        Pattern subPattern= Pattern.compile(subPatternString);
        //Fetch the DEF Value
        Matcher subMatcher= subPattern.matcher(content);
        while (subMatcher.find()) {

            String firstDefValue = matcher.group(1);
            //If the <DEF> tag value is not null add it it to List
            if(firstDefValue!=null){
                System.out.print(firstDefValue);
                popUpContent=firstDefValue;
            }
            break;
        }
        String results="";
        String xlsToLoad ="href=\"masterxslpopup.xsl\"";
        String enclosingMeta = "<?xml-stylesheet type=\"text/xsl\""+xlsToLoad+"  ?>" +
                "<dict>" +
                "    <dictionary  xmlns:h=\"http://www.w3.org/1999/xhtml\" xmlns:e=\"urn:IDMEE\" xmlns:d=\"urn:LDOCE\" xmlns:p=\"urn:LDOCE\" xmlns=\"urn:LDOCE\"" +
                "        xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"" +
                "        xsi:schemaLocation=\"urn:DPS2-metadata \">%s</dictionary>" +
                "</dict>";
        results = String.format(enclosingMeta, popUpContent);
        results = getHTMLForXML(results, context);
        //to remove new Line the String
        results=results.replaceAll("\n","");
        //to remove tab space from the String
        results=results.replaceAll("\t", "");
        TextView hwdTv=(TextView)findViewById(R.id.tv_hwd_popup);
        hwdTv.setText(hwd);
        int length=results.length();
        WebView webView=(WebView)findViewById(R.id.wv_wordpopup);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDefaultFontSize(16 + fontSizeValue);
        webView.getSettings().setStandardFontFamily(String.valueOf(Singleton.getMundoSansPro()));
        webView.loadDataWithBaseURL("file:///android_asset/www/", results, "text/html", "UTF-8", null);
    }

    private void setView() {

//        RelativeLayout wordPopUp=(RelativeLayout)findViewById(R.id.rl_word_popup);
//        RelativeLayout.LayoutParams layoutParams= (RelativeLayout.LayoutParams)wordPopUp.getLayoutParams();
//
//        Display display = context.getWindowManager().getDefaultDisplay();
//        Point size = new Point();
//        display.getSize(size);
//        int width = (int)Math.round(size.x);
//        int height = (int)Math.round(size.y);
//        int value;
//        if(Utils.isTablet(context)){
//
//            value=600;
//        }
//        else {
//            value = width>height?height:width;
//        }
//        layoutParams.height= value;
//        layoutParams.width= value;
//        wordPopUp.setLayoutParams(layoutParams);
        Singleton.getInstance(context);

        //setting close button
        TextView closeButton=(TextView)findViewById(R.id.bt_popup_close);
        closeButton.setTextSize(20);
        closeButton.setTypeface(Singleton.getLongman());
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
//                t = ((LDOCEApp) getContext().getApplicationContext()).getTracker(
//                        LDOCEApp.TrackerName.APP_TRACKER);
//                t.setScreenName("Word of the Day Popup");

                Bundle params=new Bundle();
                params.putString("eventCategory","word_of_the_day_popup");
                params.putString("eventAction","wordoftheday_events");
                params.putString("eventLabel","close_Button_Clicked");
                FirebaseWrapperScreens.EventLogs(params);
//                t.send(new HitBuilders.EventBuilder().setCategory("word_of_the_day_popup").setAction("wordoftheday_events").setLabel("close_Button_Clicked").build());
            }
        });

        //setting title
        TextView title=(TextView)findViewById(R.id.tv_title_popup);
        title.setTypeface(Singleton.getMundoSansPro_Medium());
        title.setTextSize(18+fontSizeValue);

        //setting headword
        final TextView hwd=(TextView)findViewById(R.id.tv_hwd_popup);
        hwd.setTypeface(Singleton.getMundoSansPro_Medium());
        hwd.setTextSize(28+fontSizeValue);

        //setting more button
        TextView more = (TextView)findViewById(R.id.tv_more);
        //underline
        SpannableString content = new SpannableString(context.getString(R.string.more));
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        more.setText(content);
        more.setTypeface(Singleton.getMundoSansPro());
        more.setTextSize(18+fontSizeValue);
        //navigating to detail page on clicking more button
        more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                Utils.navigateToDetailPage(context, "", hwdId, "", "");
//                t = ((LDOCEApp) getContext().getApplicationContext()).getTracker(
//                        LDOCEApp.TrackerName.APP_TRACKER);
//                t.setScreenName("Word of the Day Popup");

                FirebaseWrapperScreens.ScreenViewEvent("Word of the Day Popup");

                Bundle params=new Bundle();
                params.putString("eventCategory","word_of_the_day_popup");
                params.putString("eventAction","wordoftheday_events");
                params.putString("eventLabel","more_Button_Clicked");
                FirebaseWrapperScreens.EventLogs(params);

//                t.send(new HitBuilders.EventBuilder().setCategory("word_of_the_day_popup").setAction("wordoftheday_events").setLabel("more_Button_Clicked").build());
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        setView();
        if(hwdId.equalsIgnoreCase(""))
            try {
                populateContent();
            } catch (TransformerException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        else
            try {
                showPopUpWithId(hwdId);
            } catch (TransformerException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        //setting seen word-of-the-day-popup
        updateDb();
        // TODO: Enable Session Management
        // Sending Hit to GA as Start of Activity
        // GoogleAnalytics.getInstance(WelcomeActivity.this).reportActivityStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        // TODO: Enable Session Management
        // Sending Hit to GA as End of Activity
        // GoogleAnalytics.getInstance(WelcomeActivity.this).reportActivityStop(this);

    }

    @Override
    public void onClick(View v) {

    }

    public void showPopUpWithId(String hwdId) throws TransformerException, IOException {
        String hwd="";
        String content="";
        SQLiteDatabase coreDb=DbConnection.getDbConnection();
        Cursor cur=coreDb.rawQuery("select hwd,core from core where id = ?",new String[]{hwdId});
        while (cur.moveToNext()){
            hwd=cur.getString(0);
            content=cur.getString(1);
        }

        content=content.replaceAll("\n","");
        //to remove tab space and unwanted tags from the String
        content=content.replaceAll("\t", "");
        content=content.replaceAll("\r", "");
        content=content.replaceAll("<SYN>(.*?)</SYN>","");
        content=content.replaceAll("<OPP>(.*?)</OPP>","");
        content=content.replaceAll("<Thesref>(.*?)</Thesref>","");
        content=content.replaceAll("<ExpandableInformation>(.*?)</ExpandableInformation>","");
        content=content.replaceAll("<RELATEDWD>(.*?)</RELATEDWD>","");
        String patternString="(<Sense.*?>.*?</Sense>)";
        Pattern pattern= Pattern.compile(patternString);
        //Fetch the DEF Value
        Matcher matcher= pattern.matcher(content);
        String popUpContent="";
        while (matcher.find()) {

            String firstDefValue = matcher.group(1);
            //If the <DEF> tag value is not null add it it to List
            if(firstDefValue!=null){
                System.out.print(firstDefValue);
                popUpContent=firstDefValue;

            }
            break;

        }
        String subPatternString="(<Subsense>.*?<\\/Subsense>)";
        Pattern subPattern= Pattern.compile(subPatternString);
        //Fetch the DEF Value
        Matcher subMatcher= subPattern.matcher(popUpContent);
        while (subMatcher.find()) {

            String firstDefValue = subMatcher.group(1);
            //If the <DEF> tag value is not null add it it to List
            if(firstDefValue!=null){
                System.out.print(firstDefValue);
                popUpContent=firstDefValue;
            }
            break;
        }
        String results="";
        String xlsToLoad ="href=\"masterxslpopup.xsl\"";
        String enclosingMeta = "<?xml-stylesheet type=\"text/xsl\""+xlsToLoad+"  ?>" +
                "<dict>" +
                "    <dictionary  xmlns:h=\"http://www.w3.org/1999/xhtml\" xmlns:e=\"urn:IDMEE\" xmlns:d=\"urn:LDOCE\" xmlns:p=\"urn:LDOCE\" xmlns=\"urn:LDOCE\"" +
                "        xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"" +
                "        xsi:schemaLocation=\"urn:DPS2-metadata \">%s</dictionary>" +
                "</dict>";
        results = String.format(enclosingMeta, popUpContent);
        results = getHTMLForXML(results, context);
        //to remove new Line the String
        results=results.replaceAll("\n","");
        //to remove tab space from the String
        results=results.replaceAll("\t", "");
        TextView hwdTv=(TextView)findViewById(R.id.tv_hwd_popup);
        hwdTv.setText(hwd);
        int length=results.length();
        WebView webView=(WebView)findViewById(R.id.wv_wordpopup);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadDataWithBaseURL("file:///android_asset/www/", results, "text/html", "UTF-8", null);

    }

    public String getHTMLForXML(String xml,Context ctx) throws TransformerException, IOException {
        Source xmlSource = new StreamSource(new StringReader(xml));
        Source xsltSource = new StreamSource(ctx.getAssets().open("www/masterxslpopup.xsl"));

        TransformerFactory tFactory = TransformerFactory.newInstance();
        Transformer transformer = tFactory.newTransformer(xsltSource);

        StringWriter writer = new StringWriter();
        StreamResult result = new StreamResult(writer);
        transformer.transform(xmlSource, result);
        String html = writer.toString();

        return html;
    }
    //update db as wordofthe day seen so that pop-up wont repeat
    public boolean updateDb(){
        SQLiteDatabase getDb = DbConnection.getUserDbConnection();
        if(getDb!=null) {
            ContentValues contentValues=new ContentValues();
            contentValues.put("isNotifSeen",1);
            int success=getDb.update("userNotifications",contentValues,"id=?",new String[]{hwdId});
            if(success>1)
                return true;
        }
        return false;
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        dismiss();
        return true;
    }
}
