package com.mobifusion.android.ldoce5.Activity;

/**
 * Created by MuraliKDharan on 15/09/15.
 */

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings.Secure;

import androidx.core.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.vending.licensing.AESObfuscator;
import com.google.android.vending.licensing.LicenseChecker;
import com.google.android.vending.licensing.LicenseCheckerCallback;
import com.google.android.vending.licensing.ServerManagedPolicy;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.mobifusion.android.ldoce5.R;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.zip.ZipInputStream;

import static com.mobifusion.android.ldoce5.Activity.WelcomeActivity.progress_bar_type;

public class LicenseCheck extends Activity {

    String filePath = File.separator + "sdcard" + File.separator + "Android" + File.separator + "obb" + File.separator + "com.mobifusion.android.ldoce5" + File.separator + "main.228.com.mobifusion.android.ldoce5.obb";
    boolean check;
    boolean ispermissionGranted;
    static boolean cancelButton = false;
    private static final int REQUEST_CODE_ASK_PERMISSIONS = 23;
    public ProgressDialog pDialog;
    public static FirebaseAnalytics mFirebaseAnalytics;

    /**
     * If the download isn't present, we initialize the download UI. This ties
     * all of the controls into the remote service calls.
     */
    //Alert for obb check
    AlertDialog.Builder adb;
    public void initializeDownloadUI() {
        setContentView(R.layout.obbdownloader_main);
        final String file_url = "http://eltapps.pearson.com/ldoce6app/ldoce6_obb/main.228.com.mobifusion.android.ldoce5.obb";
        adb = new AlertDialog.Builder(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            adb = new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            adb = new AlertDialog.Builder(this);
        }
        boolean isTablet = getResources().getBoolean(R.bool.isTablet);
        //checking whether the device is tablet or phone
        if (isTablet) {
            adb.setTitle(R.string.media_files);
            adb.setCancelable(false).setMessage("Media files missing, click OK to download them again");
            adb.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Boolean internet = isInternetConnection();
                    Log.d("internet connection", internet.toString());
                    if (internet) {
                        new DownloadFileFromURL().execute(file_url);
                        dialog.dismiss();
                    } else {
                        Toast.makeText(getApplicationContext(), "Please check for internet connection", Toast.LENGTH_LONG).show();
                        adb.show();
                    }
                    adb.show();
                }
            });
            adb.setNegativeButton("Download later", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
            adb.show();
        } else {
            adb.setTitle(R.string.media_files);
            adb.setCancelable(false)
                    .setMessage("Media files missing, click OK to download them again");
            adb.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {

                    Boolean internet = isInternetConnection();
                    Log.d("internet connection", internet.toString());
                    if (internet) {
                        new DownloadFileFromURL().execute(file_url);
                        dialog.dismiss();
                    } else {
                        Toast.makeText(getApplicationContext(), "Please check for internet connection", Toast.LENGTH_LONG).show();
                        adb.show();
                    }
                }
            });
            adb.setNegativeButton("Download later", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    cancelButton = true;
                    Log.d("slide", "slide");
                    finish();
                }
            });
            if (cancelButton) {
                cancelButton = false;
                finish();
            } else {
                adb.show();
            }
        }
    }
    public boolean isInternetConnection(){
        ConnectivityManager cn=(ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo nf=cn.getActiveNetworkInfo();
        boolean connection=false;
        if(nf != null && nf.isConnected()==true )
        {
            connection=true;
            Toast.makeText(this, "Network Available", Toast.LENGTH_LONG).show();}
        else
        {
            connection=false;
            Toast.makeText(this, "Network Not Available", Toast.LENGTH_LONG).show();
        }
        return connection;
    }

//    @Override
//    protected Dialog onCreateDialog(int id) {
//        switch (id) {
//            case progress_bar_type:
//                pDialog = new ProgressDialog(this);
//                pDialog.setMessage("Downloading file. Please wait...");
//                pDialog.setIndeterminate(false);
//                pDialog.setMax(100);
//                pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
//                pDialog.setCancelable(false);
//                pDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//
//                        File dir = new File(File.separator + "sdcard" + File.separator + "Android" + File.separator + "obb" + File.separator + "com.mobifusion.android.ldoce5" + File.separator + "main.228.com.mobifusion.android.ldoce5.obb");
//                        new DirectoryCleaner(dir).clean();
//                        dir.delete();
//                        dialog.dismiss();
//                        finish();
//                    }
//                });
//                pDialog.show();
//                return pDialog;
//            default:
//                return null;
//        }
//    }
    public class DirectoryCleaner {
        private final File mFile;

        public DirectoryCleaner(File file) {
            mFile = file;
        }

        public void clean() {
            if (null == mFile || !mFile.exists() || !mFile.isDirectory()) return;
            for (File file : mFile.listFiles()) {
                delete(file);
            }
        }

        private void delete(File file) {
            if (file.isDirectory()) {
                for (File child : file.listFiles()) {
                    delete(child);
                }
            }
            file.delete();

        }
    }
    class DownloadFileFromURL extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog(progress_bar_type);
        }

        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                URL url = new URL(f_url[0]);

                URLConnection conection = url.openConnection();
                conection.connect();
                int lenghtOfFile = conection.getContentLength();
                InputStream input = new BufferedInputStream(url.openStream(), 8192);
                String file=File.separator + "sdcard" + File.separator + "Android" + File.separator + "obb" + File.separator + "com.mobifusion.android.ldoce5" + File.separator;
                OutputStream output = null;
                File dir=new File(file);
                if (dir.exists() && dir.isDirectory()) {
                    check = true;
                    Log.d("directory","yes");
                    output = new FileOutputStream("/storage/emulated/0/Android/obb/com.mobifusion.android.ldoce5/main.228.com.mobifusion.android.ldoce5.obb");
                }
                else {
                    File directory = new File(file);
                    if(directory.mkdir()) {
                        System.out.println("Directory created");
                        output = new FileOutputStream("/storage/emulated/0/Android/obb/com.mobifusion.android.ldoce5/main.228.com.mobifusion.android.ldoce5.obb");
                    }
                    else
                    {
                        System.out.println("Directory not created");
                    }
                    Log.d("directory","no");
                }
//                OutputStream output = new FileOutputStream("/storage/emulated/0/main.228.com.mobifusion.android.ldoce5.obb");
                //OutputStream output = new FileOutputStream("/storage/emulated/0/Android/obb/com.mobifusion.android.ldoce5/main.228.com.mobifusion.android.ldoce5.obb");
                byte data[] = new byte[1024];
                long total = 0;
                while ((count = input.read(data)) != -1) {
                    total += count;
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));
                    output.write(data, 0, count);

                }
                output.flush();
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }
            return null;
        }
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            pDialog.setProgress(Integer.parseInt(progress[0]));
        }
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded
            dismissDialog(progress_bar_type);
            Intent intent = new Intent();
            intent.setClass(LicenseCheck.this, SlideMenuSearchAndIndex.class);
            startActivity(intent);
        }
    }
    boolean checkForFile() {
        Log.d("check for file","into it");
        if (!check) {
            try {
                FileInputStream fin = new FileInputStream(filePath);
                ZipInputStream zin = new ZipInputStream(fin);
                if (!zin.toString().isEmpty()) {
                    check = true;
                }
                zin.close();
            } catch (FileNotFoundException e) {
//                e.printStackTrace();
                Log.e("File not found ",e.getMessage());
            } catch (IOException e) {
//                e.printStackTrace();
                Log.e("IOException ",e.getMessage());
            }
        }
        return check;
    }


    private class MyLicenseCheckerCallback implements LicenseCheckerCallback {


        @Override
        public void allow(int reason) {
            if (isFinishing()) {
                Log.d("is finishing","yes");
                // Don't update UI if Activity is finishing.
                return;
            }
            // Should allow user access.
            if (checkForFile()) {
                Log.d("is check file","yes");
                return;
            } else {
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        initializeDownloadUI();
                    }
                });

            }
        }

        @Override
        public void dontAllow(int reason) {
            if (isFinishing()) {
                // Don't update UI if Activity is finishing.
                return;
            }

            // Should not allow access. In most cases, the app should assume
            // the user has access unless it encounters this. If it does,
            // the app should inform the user of their unlicensed ways
            // and then either shut down the app or limit the user to a
            // restricted set of features.
            // In this example, we show a dialog that takes the user to Market.
            showDialog(0);
        }

        @Override
        public void applicationError(int errorCode) {
            if (isFinishing()) {
                // Don't update UI if Activity is finishing.
                return;
            }
            // This is a polite way of saying the developer made a mistake
            // while setting up or calling the license checker library.
            // Please examine the error code and fix the error.
            toast("Error: ");
            startMainActivity();

        }
    }

    private static final String BASE64_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAihusOMorLeLuubYJ6Cp3eYYbeH6qqGh+zM8pOnX579XeZJbaiLPyNgPQ7dpvF43HrgrANUD6JQ+t76jCPlq0d9jTpcMCWUXYcKYr+6t6mLZL8avGQxjPuR0xeMH5OMBZiw3tvlfR4odmESMbPDTK+hsf77LjduaGP7nu+tAL5yW46pfD6jfuv5kzGlbzbbGF7VW0QxbzlBvqQh55/vBrKF5nmFAG+ZodENmrd4t+kxIrPCO0GvPnihV38wOCbDHcf35/vZtwQ0LeiVq6n2eILmRK+C4jL0X4SMhKywIAbEAHif0ptJ7tgXBW2NG7jlylHoMky964/beOlraTA+bOOQIDAQAB";
    private static final byte[] SALT = new byte[]{15, 97, 45, 94, 23, 12, 69, 22, 36, 52, 87, 14, 77, 9, 11, 55, 34, 59, 84, 76};

    private LicenseChecker mChecker;

    // A handler on the UI thread.

    private LicenseCheckerCallback mLicenseCheckerCallback;

    private void doCheck() {

        mChecker.checkAccess(mLicenseCheckerCallback);

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle screenViewParams= new Bundle();
        screenViewParams.putString("screenview","Licence check");
        mFirebaseAnalytics.logEvent("screenview",screenViewParams);
//        mFirebaseAnalytics.setCurrentScreen(this,"About this App",null);

        // Try to use more data here. ANDROID_ID is a single point of attack.
        String deviceId = Secure.getString(getContentResolver(),
                Secure.ANDROID_ID);

        // Library calls this when it's done.
        mLicenseCheckerCallback = new MyLicenseCheckerCallback();
        // Construct the LicenseChecker with a policy.
        mChecker = new LicenseChecker(this, new ServerManagedPolicy(this,
                new AESObfuscator(SALT, getPackageName(), deviceId)),
                BASE64_PUBLIC_KEY);
        doCheck();


        /**
         * Before we do anything, are the files we expect already here and
         * delivered (presumably by Market) For free titles, this is probably
         * worth doing. (so no Market request is necessary)
         */
    }

    @Override
    protected void onStart() {
        super.onStart();
        try {
            if (Build.VERSION.SDK_INT >= 23) {
                if (!ispermissionGranted) {
                    checkPermission();
//                  Thread.sleep(4000);
                }
                int result = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
                if (result != PackageManager.PERMISSION_GRANTED)
                    dialog();
                return;
            }
        } catch (Exception e) {
            Log.e("Permission Exception", e.getMessage());
        }
    }

    @Override
    protected void onStop() {

        super.onStop();
        if (Build.VERSION.SDK_INT >= 23) {
            int result = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (result != PackageManager.PERMISSION_GRANTED)
                dialog();
            return;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (Build.VERSION.SDK_INT >= 23) {
            int result = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (result != PackageManager.PERMISSION_GRANTED)
                dialog();
            return;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            if (Build.VERSION.SDK_INT >= 23) {
                if (!ispermissionGranted) {
                    //dialog();
                }
            } else {
                startMainActivity();
            }
        } catch (Exception e) {
            Log.e("File Check", e.getMessage());
        }
    }


//        @Override
//    protected Dialog onCreateDialog(int id) {
//        // We have only one dialog.
//        AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
//        } else {
//            builder = new AlertDialog.Builder(this);
//        }
//        return builder.setTitle("Application Not Licensed")
//                .setCancelable(false)
//                .setMessage(
//                        "This application is not licensed. Please purchase it from Android Market")
//                .setPositiveButton("Buy App",
//                        new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog,
//                                                int which) {
//                                Intent marketIntent = new Intent(
//                                        Intent.ACTION_VIEW,
//                                        Uri.parse("http://market.android.com/details?id="
//                                                + getPackageName()));
//                                startActivity(marketIntent);
//                                finish();
//                            }
//                        })
//                .setNegativeButton("Exit",
//                        new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog,
//                                                int which) {
//                                finish();
//                            }
//                        }).create();
//    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mChecker.onDestroy();
    }

    private void startMainActivity() {
        ispermissionGranted = true;
        try {
            if (Build.VERSION.SDK_INT >= 23) {
                if (ispermissionGranted) {
                    startActivity(new Intent(this, WelcomeActivity.class));  //REPLACE MainActivity.class WITH YOUR APPS ORIGINAL LAUNCH ACTIVITY
                    finish();
                }
            }
            startActivity(new Intent(this, WelcomeActivity.class));  //REPLACE MainActivity.class WITH YOUR APPS ORIGINAL LAUNCH ACTIVITY
            finish();
        } catch (Exception e) {
            Log.e("File Check", e.getMessage());
        }
    }

    public void toast(String string) {
        Toast.makeText(this, string, Toast.LENGTH_SHORT).show();
    }

    //Requesting permission
    private void checkPermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            int hasWriteContactsPermission = this.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
                if (!shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            REQUEST_CODE_ASK_PERMISSIONS);
                }
                return;
            }
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_CODE_ASK_PERMISSIONS);
        }
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
                    ispermissionGranted = true;
                    startMainActivity();
//                    Toast.makeText(getApplicationContext(),"granted", Toast.LENGTH_SHORT)
//                            .show();
                } else {
                    //if(!ispermissionGranted)
                        //dialog();
                    int result = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
                    if(result != PackageManager.PERMISSION_GRANTED){
                        // Permission Denied
                        dialog();
                    }

                }
                return;
            default:
                //super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }


    public void dialog() {

        AlertDialog.Builder adb = new AlertDialog.Builder(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            adb = new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            adb = new AlertDialog.Builder(this);
        }


        adb.setTitle(R.string.permission_denied);


        adb.setCancelable(false)
                .setMessage(
                        R.string.need_storage_permission);

        adb.setNegativeButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                LicenseCheck.this.finish();

            }
        });
        adb.show();
        return;
    }

//   public void grant() {
//        ispermissionGranted = true;
//    }

    //    @TargetApi(Build.VERSION_CODES.M)


}
