package com.mobifusion.android.ldoce5.Fragment;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.mobifusion.android.ldoce5.R;
import com.mobifusion.android.ldoce5.Util.Utils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

/**
 * Created by Bazi on 07/07/15.
 */
public class ImageFragment extends Fragment {

    public String id;
    public String fileName;
    Bitmap bitmap;
    ProgressBar pDialog;
    ImageView imageView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.image_fragment, null);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        try {
            styleElements();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    public void styleElements() throws IOException {

         imageView= (ImageView) getActivity().findViewById(R.id.iv_image);
//        try {
//            InputStream inputStream=getActivity().getAssets().open("thumbnail/"+fileName);
//            Bitmap bit=BitmapFactory.decodeStream(inputStream);
//            imageView.setImageBitmap(bit);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        String mediaFile = File.separator + "sdcard" + File.separator + "Android" + File.separator + "obb" + File.separator + ".media_files_228";
        File check = new File(mediaFile);
        if(check.exists()) {
            String filePath = File.separator + "sdcard" + File.separator + "Android" + File.separator + "obb" + File.separator + ".media_files_228" + File.separator + "ldoce6pics" + File.separator + fileName;

            FileInputStream in;
            BufferedInputStream buf;
            try {
                in = new FileInputStream(filePath);
                buf = new BufferedInputStream(in);
                Bitmap bMap = BitmapFactory.decodeStream(buf);
                imageView.setImageBitmap(bMap);
                if (in != null) {
                    in.close();
                }
                if (buf != null) {
                    buf.close();
                }
            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else{
            Toast.makeText(getContext(),R.string.unable_to_locate_media_files,Toast.LENGTH_SHORT).show();
        }
//        boolean isNetAvailable=Utils.isNetworkAvailable(getActivity().getApplicationContext());
//        if(isNetAvailable) {
//            new LoadImage().execute("http://eltapps.pearson.com/ldoce6app/ldoce6pics/" + fileName);
//        }
    }

    private class LoadImage extends AsyncTask<String, String, Bitmap> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if(Utils.isNetworkAvailable(getActivity().getApplicationContext())) {
                pDialog = (ProgressBar) getActivity().findViewById(R.id.pb_image);
                pDialog.getIndeterminateDrawable().setColorFilter(Color.rgb(103, 144, 177), PorterDuff.Mode.MULTIPLY);
                pDialog.setVisibility(View.VISIBLE);
            }
        }

        protected Bitmap doInBackground(String... args) {
            try {
                bitmap = BitmapFactory.decodeStream((InputStream) new URL(args[0]).getContent());
            }
            catch (Exception e) {
                e.printStackTrace();
                }
            return bitmap;
        }
        protected void onPostExecute(Bitmap image) {
            InputStream bitmap=null;
            if (image != null) {
                imageView.setImageBitmap(image);

            }
            else {
                try {
                    bitmap=getActivity().getAssets().open("thumbnail/"+fileName);
                    Bitmap bit=BitmapFactory.decodeStream(bitmap);
                    imageView.setImageBitmap(bit);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
            pDialog.setVisibility(View.INVISIBLE);
        }
    }

}

