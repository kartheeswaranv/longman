package com.mobifusion.android.ldoce5.Fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

//import com.google.android.gms.analytics.GoogleAnalytics;
//import com.google.android.gms.analytics.HitBuilders;
//import com.google.android.gms.analytics.Tracker;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.mobifusion.android.ldoce5.Activity.DbConnection;
import com.mobifusion.android.ldoce5.Activity.SlideMenuSearchAndIndex;
import com.mobifusion.android.ldoce5.R;
import com.mobifusion.android.ldoce5.Util.AlphabetListAdapter;
import com.mobifusion.android.ldoce5.Util.FirebaseWrapperScreens;
import com.mobifusion.android.ldoce5.Util.LDOCEApp;
import com.mobifusion.android.ldoce5.Util.Singleton;
import com.mobifusion.android.ldoce5.Util.Utils;
import com.mobifusion.android.ldoce5.model.IndexRowItem;
import com.mobifusion.android.ldoce5.model.RowWithFrequency;

import java.util.ArrayList;
import java.util.List;

import static android.view.View.INVISIBLE;


public class HistoryFragment extends Fragment {
    //Variable to handle Contextual actionbar show / hide
    private ActionMode historyActionMode;
    private FirebaseAnalytics mFirebaseAnalytics;
//    Tracker t;
    public static final String PREFS_NAME = "LDOCE6PrefsFile";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.activity_history,null);
        return v;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        styleElements();
        updateHistoryEntryList();
        TextView emptyView = (TextView)getActivity().findViewById(R.id.tv_empty_view);
        emptyView.setTypeface(Singleton.getMundoSansPro());
        emptyView.setTextColor(getResources().getColor(R.color.white));
        ListView listView = (ListView)getActivity().findViewById(R.id.lv_history);
        listView.setEmptyView(emptyView);
//        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
//        mFirebaseAnalytics.setCurrentScreen(getActivity(),"About this App",null);
        FirebaseWrapperScreens.ScreenViewEvent("History Page");

    }

    public void styleElements(){
        SharedPreferences fontSizePreferences =  getActivity().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        int fontSizeValue = fontSizePreferences.getInt("FONT_VALUE", 0);
        // Set the history page title font title to MundoSansPro
        TextView historyTitle = (TextView) getActivity().findViewById(R.id.history_title);
        historyTitle.setTypeface(Singleton.getMundoSansPro());
        historyTitle.setTextSize(20+fontSizeValue);
        TextView noHistoryTextView = (TextView)getActivity().findViewById(R.id.tv_empty_view);
        noHistoryTextView.setText(getString(R.string.no_history));
        noHistoryTextView.setTextSize(20+fontSizeValue);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Get the Tracker and Initialise it
//        t = ((LDOCEApp) getActivity().getApplication()).getTracker(
//                LDOCEApp.TrackerName.APP_TRACKER);
//        t.setScreenName("History Page");
        FirebaseWrapperScreens.ScreenViewEvent("History Page");

//        t.send(new HitBuilders.AppViewBuilder().build());
    }

    @Override
    public void onStart() {
        super.onStart();
        styleElements();
        // Sending Hit to GA as Start of new Activity
//        GoogleAnalytics.getInstance(getActivity()).reportActivityStart(getActivity());
        // Get the ListView from the XML
        final ListView listView = (ListView)getActivity().findViewById(R.id.lv_history);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //getting the clicked alphabet
                AlphabetListAdapter adapter = (AlphabetListAdapter) listView.getAdapter();
                RowWithFrequency item = (RowWithFrequency) adapter.getItem(position);
                androidx.fragment.app.FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                //Check BackStack entry
                Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.detailPageFragment);
                //Get the detail page


                //Get the detailPage fragment to handle back stack and check it is from detail page and check whether clicked word and previous word are equal
                if (fragmentManager.getBackStackEntryCount() > 0) {
                    //get the detail page fragment
                    Boolean isDetailPage = (fragment instanceof DetailPageFragment) ? true : false;
                    if (isDetailPage) {
                        DetailPageFragment detailPageFragment = (DetailPageFragment) fragment;
                        //Get the previous word check for the previous word
                        String previousWord = detailPageFragment.headword;
                        if (previousWord.equals(item.getHwd()))
                            return;
                        else
                            //Navigate to detail page
                            Utils.navigateToDetailPage(getActivity(),item.getHwd(),item.getHwdId(),"","");
                    } else
                        //Navigate tot detail page
                        Utils.navigateToDetailPage(getActivity(), item.getHwd(),item.getHwdId(),"","");
                }
                //Navigate to detail page
                else
                    Utils.navigateToDetailPage(getActivity(), item.getHwd(),item.getHwdId(),"","");
            }
        });
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        final AlphabetListAdapter listViewAdapter = (AlphabetListAdapter) listView.getAdapter();

        listView.setMultiChoiceModeListener(new ListView.MultiChoiceModeListener() {

            @Override
            public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
                /*
                  Scenario: When SearchView is in focus, and trying to Select an listView Item and Press Delete
                   1. Remove SearchView Focus
                   2. Make the SearchView to its normal position
                   3. Dismiss Keyboard if it is in enable state
                 */
                final ListView searchListView = (ListView) getActivity().findViewById(R.id.searchResultListView);
                Fragment searchFragment = getFragmentManager().findFragmentById(R.id.searchFragement);
                SearchView searchView = (SearchView) searchFragment.getActivity().findViewById(R.id.search_bar);
                if (!searchView.isIconified()) {
                    //Keyboard dismissal and remove the search focus
                    searchListView.setVisibility(INVISIBLE);
                    searchView.setQuery("", false);
                    searchView.setIconified(true);
                    searchView.setFocusable(false);
                }

                //get the count of checkedItems in History ListView
                int checkedCount = listView.getCheckedItemCount();
                //To display the number of Items selected in Action Bar
                if (checked) {
                    listViewAdapter.setSelection(position);
                } else
                    listViewAdapter.removeSelectionAt(position);
                mode.setTitle(checkedCount + "/" + listView.getCount());
                //To Select and UnSelect the items in History ListView
                historySelectAndUnSelect(listView);
            }

            private void historySelectAndUnSelect(ListView list) {
                SparseBooleanArray checkedItems = list.getCheckedItemPositions();
                for (int i = 0; i < checkedItems.size(); i++) {
                    IndexRowItem checkedRow = null;
                    int checkedPosition = checkedItems.keyAt(i);
                    checkedRow = ((IndexRowItem) listViewAdapter.getItem(checkedPosition));
                    //isSelected is a property for History Item - to change the background color
                    checkedRow.isSelected = checkedItems.valueAt(i);
                }
            }

            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                MenuInflater inflater = mode.getMenuInflater();
                inflater.inflate(R.menu.menu_context, menu);
                historyActionMode = mode;
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                //Disabling swipe guester for slidemenu for phone alone
                if (!Utils.isTablet(getActivity()))
                    ((SlideMenuSearchAndIndex) getActivity()).getSlidingMenu().setTouchModeAbove(SlidingMenu.TOUCHMODE_NONE);
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                // Respond to clicks on the actions in the CAB
                AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
                switch (item.getItemId()) {
                    case R.id.context_menu_delete:
                        //To retrieve the HWDid of corresponding selected rowItem in History ListView
                        List<String> ids = new ArrayList<String>();
                        String[] idArray = new String[ids.size()];
                        //This is mainly used to Store an array with Boolean Values -  it hte ListView Item is Checked, this array stores checkedItem position and bool value
                        SparseBooleanArray checkedItems = listView.getCheckedItemPositions();
                        for (int i = 0; i < checkedItems.size(); i++) {
                            int checkedPosition = checkedItems.keyAt(i);
                            //Check the selectable state and add to an ids List
                            if (checkedItems.valueAt(i)) {
                                String hwdId = ((IndexRowItem) listViewAdapter.getItem(checkedPosition)).getHwdId();
                                ids.add(hwdId);
                            }
                        }
                        idArray = ids.toArray(idArray);

                        //Bool to check for select all
                        boolean isEntryAllDeleted = false;

                        //check for the idArray and list view count
                        if (listView.getCount() == idArray.length)
                            isEntryAllDeleted = true;


                        //Handle Alert Box if select all option is enabled
                        if (isEntryAllDeleted) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                builder = new AlertDialog.Builder(getActivity(), android.R.style.Theme_Material_Light_Dialog_Alert);
                            } else {
                                builder = new AlertDialog.Builder(getActivity());
                            }
                            final String[] finalIdArray = idArray;
                            builder.setTitle(R.string.confirm_delete);
                            builder.setMessage(R.string.are_you_sure_you_want_to_delete_all_your_history);
                            builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //Pass the entry ids to the onDeleteEntriesInList and refresh the history page
                                    boolean entriesDeleted = onDeleteEntriesInList(finalIdArray);
                                    if (entriesDeleted)
                                        refreshHistoryPage();
                                    Bundle params=new Bundle();
                                    params.putString("eventCategory","history_page_actions");
                                    params.putString("eventAction","delete_all_confirmation");
                                    params.putString("eventLabel","history_cleared");
                                    FirebaseWrapperScreens.EventLogs(params);

//                                    t.send(new HitBuilders.EventBuilder().setCategory("history_page_actions").setAction("delete_all_confirmation").setLabel("history_cleared").build());

                                }
                            });
                            builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    // User cancelled the dialog
                                    System.out.println("User cancelled the dialog");
                                }
                            });
                            builder.show();
                        } else {
                            //Delete if multiple selected and refresh the history page
                            onDeleteEntriesInList(idArray);
                            Bundle params=new Bundle();
                            params.putString("eventCategory","history_page_actions");
                            params.putString("eventAction","word_delete_button_tapped");
                            params.putString("eventLabel","history_cleared");
                            FirebaseWrapperScreens.EventLogs(params);
                            refreshHistoryPage();
                        }

                        mode.finish(); // Action picked, so close the CAB
                        return true;
                    case R.id.context_menu_selectAll:
                        /*
                        Get the fragment history list
                        Get the count of that list
                        Change the back ground of the selected item
                         */
                        if (listView.getCheckedItemCount() == listView.getCount()) {
                            for (int i = 0; i < listView.getCount(); i++) {
                                listView.setItemChecked(i, false);
                                listViewAdapter.removeSelectionAt(i);
                            }
                        }
                        //Update the selection state in listView and listViewAdapter
                        else {
                            for (int i = 0; i < listView.getCount(); i++) {
                                listView.setItemChecked(i, true);
                                listViewAdapter.setSelection(i);
                            }
                        }
                        //Set the no. of items in selected state
                        mode.setTitle(listView.getCheckedItemCount() + "/" + listView.getCount());
                        return true;
                    default:
                        return false;
                }

            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
                //If Select All button is pressed, and Unselecting all - uncheck all the checked Items in History ListView
                for (int i = 0; i < listView.getCount(); i++) {
                    IndexRowItem checkedRow = null;
                    checkedRow = ((IndexRowItem) listViewAdapter.getItem(i));
                    checkedRow.isSelected = false;
                }
                //Enabling the swipe gesture for slide menu for phone alone
                if (!Utils.isTablet(getActivity()))
                    ((SlideMenuSearchAndIndex) getActivity()).getSlidingMenu().setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
                listViewAdapter.removeSelection();
            }
        });
    }

    // Method to add entries to history list
    public void updateHistoryEntryList(){
        // Get the ListView from the XML
        final ListView listView = (ListView)getActivity().findViewById(R.id.lv_history);
        // Create the Adapter
        AlphabetListAdapter adapter = (AlphabetListAdapter)listView.getAdapter();
        // Create the List
        List<AlphabetListAdapter.Row> list = getHistoryItems();
        if(adapter!=null) {
            // Populate the records to the list
            adapter.setRows(list);
            adapter.notifyDataSetChanged();
        }else{
            // Create the Adapter
            adapter = new AlphabetListAdapter();
            adapter.setRows(list);
            listView.setAdapter(adapter);
        }
    }

    public List<AlphabetListAdapter.Row> getHistoryItems(){
        List<AlphabetListAdapter.Row> list = new ArrayList<AlphabetListAdapter.Row>();
        // Get the User DB Connection
        SQLiteDatabase db = DbConnection.getUserDbConnection();
        // Check if the current existence of DB is not null
        if (db != null) {
            // Query to get the records from history table
            String query = String.format("select hwd,id,frequent from history order by timestamp desc");
            Cursor cur = db.rawQuery(query, null);

            while (cur.moveToNext()) {
                boolean isFrequent = cur.getString(2).equalsIgnoreCase("1") ? true : false;

                // Adding the record to list
                list.add(new RowWithFrequency(cur.getString(0), cur.getString(1), isFrequent));
            }
        }
        // Close the DB Connection
        db.close();
        return list;
    }


    public void updateHistoryWithoutReload(){
        // Get the ListView from the XML
        final ListView listView = (ListView)getActivity().findViewById(R.id.lv_history);
        // Create the Adapter
        AlphabetListAdapter adapter = (AlphabetListAdapter)listView.getAdapter();
        // Create the List
        List<AlphabetListAdapter.Row> list = getHistoryItems();
        if(adapter!=null) {
            // Populate the records to the list
            adapter.setRows(list);
            adapter.notifyDataSetChanged();
        }else{
            // Create the Adapter
            adapter = new AlphabetListAdapter();
            adapter.setRows(list);
            listView.setAdapter(adapter);
        }
    }

    // Method to delete entries from history list
    public boolean onDeleteEntriesInList(String[] entriesIdArray){
        boolean isDeletedSuccess=false;
        //String to query the db
        String historyEntryDeletionQuery;

        // Get the User DB Connection
        SQLiteDatabase db = DbConnection.getUserDbConnection();

        //Transaction is used optimise the execution of queries
        db.beginTransaction();

        //When a db exists
        if(db!=null) {

            for (String id: entriesIdArray) {
                 //Query for deleting entries from history
                 historyEntryDeletionQuery = "delete from history where id = ?";
                 int count  = db.delete("history","id=?",new String[]{id});
                if(count >=1){
                   isDeletedSuccess = true;
                }
                else {

                    isDeletedSuccess=false;
                }
            }
        }
        //Set the transaction as success
        db.setTransactionSuccessful();

        //End the transaction
        db.endTransaction();
        db.close();
        //to check whether the selected Entries are deleted from DB
       return isDeletedSuccess;
    }

    /**
     * Update the history table and refresh the data adapter
     * Used in Main activity to refresh the history page after the detail page loaded.
     */
    public void refreshHistoryPage()
    {
        updateHistoryEntryList();
    }

    @Override
    public void onStop() {
        super.onStop();
        // Sending Hit to GA as End of Activity
//        GoogleAnalytics.getInstance(getActivity()).reportActivityStop(getActivity());
        if (historyActionMode!=null)
            historyActionMode.finish();
    }

    /**
     * refresh the UI element from settings page
     * Used in language change, Font size increase
     */
    public void refreshUIElements()
    {
        SharedPreferences fontSizePreferences =  getActivity().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        int fontSizeValue = fontSizePreferences.getInt("FONT_VALUE", 0);
        TextView noHistoryTextView = (TextView)getActivity().findViewById(R.id.tv_empty_view);
        noHistoryTextView.setText(getString(R.string.no_history));
        noHistoryTextView.setTextSize(20+fontSizeValue);
        TextView historyTitle = (TextView)getActivity().findViewById(R.id.history_title);
        historyTitle.setText(getString(R.string.history));
        historyTitle.setTextSize(20+fontSizeValue);
    }

    @Override
    public void onResume() {
        SharedPreferences sharedPreferences =  getActivity().getSharedPreferences("LDOCE6PrefsFile", Context.MODE_PRIVATE);
        boolean isSearchOnClipboardOn=sharedPreferences.getBoolean("Search-on-clipboard", false);
        boolean isSaveSearchWordOn=sharedPreferences.getBoolean("SaveSearchWord",false);
        if(Utils.isTablet(getActivity())){

        if(isSaveSearchWordOn && !isSearchOnClipboardOn){
            ((SlideMenuSearchAndIndex)getActivity()).showLastSearchedWord();
        }
        }
//        else {
//            if(!isSaveSearchWordOn && !isSearchOnClipboardOn) {
//                ((SlideMenuSearchAndIndex) getActivity()).showWord();
//            }
//        }
        super.onResume();
    }
}
