package com.mobifusion.android.ldoce5.Fragment;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

//import com.google.android.gms.analytics.GoogleAnalytics;
//import com.google.android.gms.analytics.HitBuilders;
//import com.google.android.gms.analytics.Tracker;
import com.mobifusion.android.ldoce5.Activity.DbConnection;
import com.mobifusion.android.ldoce5.R;
import com.mobifusion.android.ldoce5.Util.AlphabetListAdapter;
import com.mobifusion.android.ldoce5.Util.FirebaseWrapperScreens;
import com.mobifusion.android.ldoce5.Util.LDOCEApp;
import com.mobifusion.android.ldoce5.Util.Singleton;
import com.mobifusion.android.ldoce5.Util.Utils;
import com.mobifusion.android.ldoce5.model.IndexRowItem;

import java.util.ArrayList;
import java.util.List;

public class AcademicWordList extends Fragment implements ListView.OnItemClickListener {

    public static final String PREFS_NAME = "LDOCE6PrefsFile";
//    Tracker t;
    List<AlphabetListAdapter.Row> listViewItems;
    boolean isSecondLevel;
    String clickedCharacter;
    public Parcelable currentState ;
    public Parcelable previousState ;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_academic_words, null);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
//        t = ((LDOCEApp) getActivity().getApplication()).getTracker(
//                LDOCEApp.TrackerName.APP_TRACKER);
//        t.setScreenName("About this App");
//        t.enableAutoActivityTracking(true);
        FirebaseWrapperScreens.ScreenViewEvent("About this App Page");

//        t.send(new HitBuilders.AppViewBuilder().build());
    }

    @Override
    public void onStart() {
        super.onStart();
        // Sending Hit to GA as new Activity
//        GoogleAnalytics.getInstance(getActivity()).reportActivityStart(getActivity());
        listViewItems=new ArrayList<>();
        final ListView listView=(ListView)getActivity().findViewById(R.id.academic_list);
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                currentState = listView.onSaveInstanceState();

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });
        // Set the About page title font title to MundoSansPro
        // Set the list view
        styleElement();
        //Checking currentstate, if it not null restore its state
        if (currentState!=null )
        {
            //If last word not null, which means we are in second level index.
            //Navigate to second level index with last word
            listView.onRestoreInstanceState(currentState);

        }
    }

    private void styleElement() {

        // Set the About page title font title to MundoSansPro
        TextView aboutPageTitle = (TextView) getActivity().findViewById(R.id.academic_title);
        aboutPageTitle.setTypeface(Singleton.getMundoSansPro());
        SharedPreferences fontSizePreferences =  getActivity().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        int fontSizeValue = fontSizePreferences.getInt("FONT_VALUE", 0);
        aboutPageTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20 + fontSizeValue);
        if(isSecondLevel){
            List<AlphabetListAdapter.Row> list = getAcademicItems(clickedCharacter);
            populateList(list);
        }
        else {
            List<AlphabetListAdapter.Row> list = getAcademicItems("");
            populateList(list);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        // Sending Hit to GA as End of Activity
//        GoogleAnalytics.getInstance(getActivity()).reportActivityStop(getActivity());
    }

    public List<AlphabetListAdapter.Row> getAcademicItems(String clickedAlphabet) {

        List<AlphabetListAdapter.Row> listItems=new ArrayList<>();
        listViewItems.clear();
        if(clickedAlphabet.equalsIgnoreCase("")){

            listItems.add(new IndexRowItem("A","",false));
            listItems.add(new IndexRowItem("B","",false));
            listItems.add(new IndexRowItem("C","",false));
            listItems.add(new IndexRowItem("D","",false));
            listItems.add(new IndexRowItem("E","",false));
            listItems.add(new IndexRowItem("F","",false));
            listItems.add(new IndexRowItem("G","",false));
            listItems.add(new IndexRowItem("H","",false));
            listItems.add(new IndexRowItem("I","",false));
            listItems.add(new IndexRowItem("J","",false));
            listItems.add(new IndexRowItem("L","",false));
            listItems.add(new IndexRowItem("M","",false));
            listItems.add(new IndexRowItem("N","",false));
            listItems.add(new IndexRowItem("O","",false));
            listItems.add(new IndexRowItem("P","",false));
            listItems.add(new IndexRowItem("Q","",false));
            listItems.add(new IndexRowItem("R","",false));
            listItems.add(new IndexRowItem("S","",false));
            listItems.add(new IndexRowItem("T","",false));
            listItems.add(new IndexRowItem("U","",false));
            listItems.add(new IndexRowItem("V","",false));
            listItems.add(new IndexRowItem("W","",false));
        }
        else {
            //frequency_acad
            SQLiteDatabase db = DbConnection.getDbConnection();
            String query;
            query = String.format("select hwd,id from frequency_acad where hwd like ? and AC='1' GROUP BY HWD collate nocase");
            Cursor cur = db.rawQuery(query, new String[]{clickedAlphabet+"%%"});
            while (cur.moveToNext())
            {
                String[] colun=cur.getColumnNames();
                String hwd=cur.getString(0);
                String hwdId=cur.getString(1);
                //Storing as IndexRowItem
                listItems.add(new IndexRowItem(hwd,hwdId,false));
            }
        }
        listViewItems=listItems;
        return listItems;
    }

    void populateList(List<AlphabetListAdapter.Row> listViewItems){

        AlphabetListAdapter adapter=new AlphabetListAdapter();
        adapter.setRows(listViewItems);
        //populating the content for about page
        ListView listView=(ListView)getActivity().findViewById(R.id.academic_list);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        final ListView listView=(ListView)getActivity().findViewById(R.id.academic_list);
        currentState=listView.onSaveInstanceState();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        final ListView listView=(ListView)getActivity().findViewById(R.id.academic_list);
        IndexRowItem clickedItem=(IndexRowItem)listViewItems.get(position);
        if(clickedItem.getHwdId().equalsIgnoreCase("")){
            clickedCharacter=clickedItem.getHwd();
            List<AlphabetListAdapter.Row> list= getAcademicItems(clickedItem.getHwd());
            populateList(list);
            isSecondLevel=true;
            //We are navigating to second level, so please save the current listview state to previous
            previousState = currentState;
        }
        else{
            //We are navigating to entry, save the current table view state.
            currentState = listView.onSaveInstanceState();
            Utils.navigateToDetailPage(getActivity(),clickedItem.getHwd(),clickedItem.getHwdId(),"","");
        }
    }

    public boolean onParentBackButtonPressed(){
        final ListView listView=(ListView)getActivity().findViewById(R.id.academic_list);
        if(isSecondLevel){
            //We are in second level
            List<AlphabetListAdapter.Row> list= getAcademicItems("");
            populateList(list);
            isSecondLevel=false;
            //Currrently AWL is in first level because of populate(list)
            //Change currentstate from previous state
            currentState = previousState;
            listView.onRestoreInstanceState(currentState);
            previousState = null;
            return true;
        }
        else{
            listView.onRestoreInstanceState(currentState);
            return false;
        }
    }
}