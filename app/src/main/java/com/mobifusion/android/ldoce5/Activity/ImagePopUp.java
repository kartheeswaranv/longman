package com.mobifusion.android.ldoce5.Activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.mobifusion.android.ldoce5.R;
import com.mobifusion.android.ldoce5.Util.Singleton;
import com.mobifusion.android.ldoce5.Util.Utils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

/**
 * Created by Bazi on 08/07/15.
 */
public class ImagePopUp extends Dialog{

    Context context;
    public String fileName;
    String hwdId;
    ImageView imageView;
    ProgressBar pDialog;
    AsyncTask<String, String, Bitmap> imageLoad;
    public ImagePopUp(Context context) {
        super(context);
        this.context=context;
        fileName="";
        hwdId="";
    }

    public ImagePopUp(Context context, int theme) {
        super(context, theme);
    }

    public ImagePopUp(Context context, String hwdId,String fileName) {
        super(context);
        this.context=context;
        this.fileName=fileName;
        this.hwdId=hwdId;
    }

    protected ImagePopUp(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.image_popup);

    }

    @Override
    protected void onStart() {
        super.onStart();
        TextView closeButton=(TextView)findViewById(R.id.bt_image_popup_close);
        closeButton.setTypeface(Singleton.getLongman());
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        imageView=(ImageView)findViewById(R.id.iv_imagepopup);
        pDialog=(ProgressBar)findViewById(R.id.pb_imagepopup);
        pDialog.getIndeterminateDrawable().setColorFilter(Color.rgb(103, 144, 177), PorterDuff.Mode.MULTIPLY);
        InputStream file= null;
//        try {
//            file = context.getAssets().open("thumbnail/"+fileName);
//            final Bitmap bit=BitmapFactory.decodeStream(file);
//            imageView.post(new Runnable() {
//                @Override
//                public void run() {
//                    imageView.setImageBitmap(bit);
//                }
//            });
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        String mediaFile = File.separator + "sdcard" + File.separator + "Android" + File.separator + "obb" + File.separator + ".media_files_228";
        File check = new File(mediaFile);
        if(check.exists()) {
            String filePath = File.separator + "sdcard" + File.separator + "Android" + File.separator + "obb" + File.separator + ".media_files_228" + File.separator + "ldoce6pics" + File.separator + fileName;

            FileInputStream in;
            BufferedInputStream buf;
            try {
                in = new FileInputStream(filePath);
                buf = new BufferedInputStream(in);
                Bitmap bMap = BitmapFactory.decodeStream(buf);
                imageView.setImageBitmap(bMap);
                if (in != null) {
                    in.close();
                }
                if (buf != null) {
                    buf.close();
                }
            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else
        {
            Toast.makeText(getContext(),R.string.unable_to_locate_media_files,Toast.LENGTH_SHORT).show();

        }


        OnAsyncImageTaskCompleted callback = new OnAsyncImageTaskCompleted() {

            @Override
            public void onPreExecution(Context context, Boolean isStarted) {
                if(isStarted){
                    pDialog.post(new Runnable() {
                        @Override
                        public void run() {
                            pDialog.setVisibility(View.VISIBLE);
                        }
                    });
                }

            }

            @Override
            public void onPostExecution(Context context, Boolean isStarted,final Bitmap bitmap) throws IOException {

                //bitmap=getActivity().getAssets().open("thumbnail/"+fileName);
                //Bitmap bit=BitmapFactory.decodeStream(bitmap);
                if(bitmap!=null){
                    imageView.post(new Runnable() {
                        @Override
                        public void run() {
                            imageView.setImageBitmap(bitmap);
                        }
                    });

                }
                else{
                    InputStream file=context.getAssets().open("thumbnail/"+fileName);
                    final Bitmap bit=BitmapFactory.decodeStream(file);
                    imageView.post(new Runnable() {
                        @Override
                        public void run() {
                            imageView.setImageBitmap(bit);
                        }
                    });
                }
                pDialog.post(new Runnable() {
                    @Override
                    public void run() {
                        pDialog.setVisibility(View.INVISIBLE);
                    }
                });
            }
        };
//        boolean isNetAvailable= Utils.isNetworkAvailable(context.getApplicationContext());
//        if(isNetAvailable) {
//            imageLoad = LoadImage.getInstance(context, callback);
//            imageLoad.execute("http://eltapps.pearson.com/ldoce6app/ldoce6pics/" + fileName);
//        }
    }

    private static class LoadImage extends AsyncTask<String, String, Bitmap> {

        static AsyncTask<String, String, Bitmap> myAsyncTaskInstance=null;
        Context iContext;
        private OnAsyncImageTaskCompleted listener;
        Bitmap bitmap;
        private LoadImage(Context iContext,OnAsyncImageTaskCompleted listener)
        {
            iContext = iContext;
            this.listener = listener;
        }

        public static AsyncTask<String, String, Bitmap> getInstance(Context iContext,OnAsyncImageTaskCompleted listener)
        {
            // if the current async task is already running, return null: no new async task
            // shall be created if an instance is already running
            if ((myAsyncTaskInstance != null) && myAsyncTaskInstance.getStatus() ==   Status.RUNNING)
            {
                // it can be running but cancelled, in that case, return a new instance
                if (myAsyncTaskInstance.isCancelled())
                {
                    myAsyncTaskInstance = new LoadImage(iContext,listener);
                }
                else
                {
                    // display a toast to say "try later"
                    // Toast.makeText(iContext, "A task is already running, try later", Toast.LENGTH_SHORT).show();
                    //TODO:// check why null was returned previously.
                    return myAsyncTaskInstance;
                }
            }

            //if the current async task is pending, it can be executed return this instance
            if ((myAsyncTaskInstance != null) && myAsyncTaskInstance.getStatus() == Status.PENDING)
            {
                return myAsyncTaskInstance;
            }

            //if the current async task is finished, it can't be executed another time, so return a new instance
            if ((myAsyncTaskInstance != null) && myAsyncTaskInstance.getStatus() == Status.FINISHED)
            {
                myAsyncTaskInstance = new LoadImage(iContext,listener);
            }


            // if the current async task is null, create a new instance
            if (myAsyncTaskInstance == null)
            {
                myAsyncTaskInstance = new LoadImage(iContext,listener);
            }
            // return the current instance
            return myAsyncTaskInstance;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            listener.onPreExecution(iContext,true);
        }

        protected Bitmap doInBackground(String... args) {
            try {
                bitmap = BitmapFactory.decodeStream((InputStream) new URL(args[0]).getContent());
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            return bitmap;
        }
        protected void onPostExecute(Bitmap image) {
            try {
                listener.onPostExecution(iContext,true,image);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }

    public interface OnAsyncImageTaskCompleted {
        public void onPreExecution(Context context, Boolean isStarted);
        public void onPostExecution(Context context, Boolean isStarted,Bitmap bitmap) throws IOException;

    }

}


