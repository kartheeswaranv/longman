package com.mobifusion.android.ldoce5.model;

import com.mobifusion.android.ldoce5.Util.AlphabetListAdapter;

/**
 * Created by Bazi on 03/06/15.
 */
public class AdditionalInfoItem extends AlphabetListAdapter.Row {
    public int id;
    public String text;
}
