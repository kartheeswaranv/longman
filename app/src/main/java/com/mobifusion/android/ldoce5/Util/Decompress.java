package com.mobifusion.android.ldoce5.Util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.Toast;

import com.mobifusion.android.ldoce5.Activity.DbHelper;
import com.mobifusion.android.ldoce5.Activity.LicenseCheck;
import com.mobifusion.android.ldoce5.Activity.WelcomeActivity;
import com.mobifusion.android.ldoce5.Fragment.DetailPageFragment;
import com.mobifusion.android.ldoce5.R;
import org.tukaani.xz.XZInputStream;

import java.io.BufferedOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Created by vthanigaimani on 1/18/15.
 */
public class Decompress extends AsyncTask<Void,Integer,Integer> {

    boolean unZipFileSize = false;
    int progress = 0;
    Activity context;
    private OnAsyncTaskCompleted listener;
    static AsyncTask<Void,Integer,Integer> myAsyncTaskInstance = null;
    String location = File.separator+"sdcard"+File.separator+"Android"+File.separator+"obb"+ File.separator;
    String fileToChange = File.separator+"sdcard"+File.separator+"Android"+File.separator+"obb"+ File.separator+"convertMp3ToDb";
    String rename = File.separator+"sdcard"+File.separator+"Android"+File.separator+"obb"+ File.separator+".media_files_228";

    //Decompressing the context
    private Decompress(Activity iContext,OnAsyncTaskCompleted listener)
    {
        context = iContext;
        this.listener = listener;
    }

    public static AsyncTask<Void, Integer, Integer> getInstance(Activity iContext,OnAsyncTaskCompleted listener)
    {
        // if the current async task is already running, return null: no new async task
        // shall be created if an instance is already running
        if ((myAsyncTaskInstance != null) && myAsyncTaskInstance.getStatus() ==   Status.RUNNING)
        {
            // it can be running but cancelled, in that case, return a new instance
            if (myAsyncTaskInstance.isCancelled())
            {
                myAsyncTaskInstance = new Decompress(iContext,listener);
            }
            else
            {
                // display a toast to say "try later"
               // Toast.makeText(iContext, "A task is already running, try later", Toast.LENGTH_SHORT).show();
                //TODO:// check why null was returned previously.
                return myAsyncTaskInstance;
            }
        }

        //if the current async task is pending, it can be executed return this instance
        if ((myAsyncTaskInstance != null) && myAsyncTaskInstance.getStatus() == Status.PENDING)
        {
            return myAsyncTaskInstance;
        }

        //if the current async task is finished, it can't be executed another time, so return a new instance
        if ((myAsyncTaskInstance != null) && myAsyncTaskInstance.getStatus() == Status.FINISHED)
        {
            myAsyncTaskInstance = new Decompress(iContext,listener);
        }


        // if the current async task is null, create a new instance
        if (myAsyncTaskInstance == null)
        {
            myAsyncTaskInstance = new Decompress(iContext,listener);
        }
        // return the current instance
        return myAsyncTaskInstance;
    }

    @Override
    public void onPreExecute()
    {
        System.out.println("onPreExecute");
        super.onPreExecute();
        listener.onPreExecution(context, true);
    }
    @Override
    protected Integer doInBackground(Void... params) {
        int bufferSize = 8192;
        byte[] buf = new byte[bufferSize];
        String name = null;

        try {
            System.out.println("doInBackground");
            listener.onDoInBackground(context,true);
            name = "ldoce_sqlite.7z";
            //InputStream of the sqlite file
            InputStream in = context.getResources().openRawResource(R.raw.ldoce_sqlite);
            //Original file size i.e size after extractation
            float fileSize = 160088064;
            FileOutputStream out = context.openFileOutput("ldoce.sqlite", 0);//Context.MODE_PRIVATE);
            try {
                // In contrast to other classes in org.tukaani.xz,
                // LZMAInputStream doesn't do buffering internally
                // and reads one byte at a time. BufferedInputStream
                // gives a huge performance improvement here but even
                // then it's slower than the other input streams from
                // org.tukaani.xz.
                //in = new BufferedInputStream(in);

                //check for extracted media file if exsist skip the process
                try{
                    File checkRenameFile = new File(rename);

                    if(!checkRenameFile.exists()) {
                        checkSize();
                    }
                }catch (Exception e){
                    Log.e("ExtractedFile Exception", e.getMessage());
                }

                in = new XZInputStream(in);
                int size;
                int counter=0;
                while ((size = in.read(buf)) != -1) {
                    if(this.isCancelled())
                        break;
                    out.write(buf, 0, size);
                    counter++;
                    //System.out.println((counter*8192)/fileSize);

                    progress = (int) (((counter*bufferSize)/fileSize)*100);
                        publishProgress(progress);
                }

                //delete the archive file if the extraction was cancelled,
                //because it was only partially extracted.
                if(this.isCancelled())
                {
                    File dir = context.getFilesDir();
                    File file = new File(dir,"ldoce.sqlite");
                    file.delete();
                }
                else
                {
                    //listener.onIndexCreation(context,true);

                    //Call to DbHelper
                    //Creating the Index and Frequency table to database
                    DbHelper db= new DbHelper();
                    //publishing as 101 to display indexing string
                    publishProgress(101);
                    db.IndexCreationAndFrequencyTableCreation();
                    System.out.println("Index Creation");
                    //Creating User Db
                    db.UserDbAndHistoryTableCreation();
                    //set favorite table updated.
                    final String PREF_FILE_NAME_LDOCE = "LDOCE6PrefsFile";
                    SharedPreferences preferencesLDOCE = context.getSharedPreferences(PREF_FILE_NAME_LDOCE, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferencesLDOCE.edit();
                    editor.putBoolean("FavTableUpdated", true);
                    // Commit the edits!
                    editor.apply();
                    System.out.println("User DB Creation");
                    //importing bookmarks from previous app
                    publishProgress(111);
                    Utils.getBookmarkFromXML(context);
                }

            }
            catch (Exception e)
            {
                System.err.println("Input Stream error: "+e.getMessage());
            }
            finally {
                // Close FileInputStream (directly or indirectly
                // via LZMAInputStream, it doesn't matter).
                in.close();
            }
        } catch (FileNotFoundException e) {
            System.err.println("LZMADecDemo: Cannot open " + name + ": "
                    + e.getMessage());
            System.exit(1);
        } catch (EOFException e) {
            System.err.println("LZMADecDemo: Unexpected end of input on "
                    + name);
            System.exit(1);

        } catch (IOException e) {
            System.err.println("LZMADecDemo: Error decompressing from "
                    + name + ": " + e.getMessage());
            System.exit(1);
        }
        return null;
    }

   //Progress bar updating
    @Override
    protected void onProgressUpdate(Integer... values) {
        //System.out.println("onProgressUpdate");
        super.onProgressUpdate(values);
        //if value is less than or equal to 100 means its extracting database
        //if value is 101 means its creating index
        //we can publish custome value and we can handle for some other label
        if(values[0]<=0){
            listener.unzipExtractionFile(context,true);
        }
       else if (values[0]<=100) {
            //showing progress of extraction
            listener.onProgressUpdation(context, true, values);

        }
        else if(values[0]<=110)
            //showing indexing
            listener.onIndexCreation(context,true);
        else
            //showing importing bookmarks
            listener.onImportingBookmark(context,true);

    }

    //After DB extracted
    @Override
    protected void onPostExecute(Integer integer) {
        System.out.println("onPostExecute");
        listener.onPostExecution(context,true);
        //Edit the system preferences
        SharedPreferences settings = context.getSharedPreferences(WelcomeActivity.PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean("dbExtracted", true);
        ApplicationInfo ai;
        int dbOverrideAppValue=0;
        try {
            ai = context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
            int value = ai.metaData.getInt("DbOverrideVersion",0);
            dbOverrideAppValue=value;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        editor.putInt("DbOverrideVersion",dbOverrideAppValue);
        // Commit the edits!
        editor.apply();
    }

    private long getFolderSize(File folder) {
        long length = 0;
        File[] files = folder.listFiles();

        int count = files.length;

        for (int i = 0; i < count; i++) {
            if (files[i].isFile()) {
                length += files[i].length();
            }
            else {
                length += getFolderSize(files[i]);
            }
        }
        return length;
    }

    public void whenGetFolderSizeRecursive_thenCorrect() {

        long expectedSize = 498672710;

        File file1 = new File(fileToChange);
        File file2 = new File(rename);
        boolean success = file1.renameTo(file2);
        if(success) {
            File folder = new File(File.separator + "sdcard" + File.separator + "Android" + File.separator + "obb" + File.separator + ".media_files_228");
            long size = getFolderSize(folder);

            if (expectedSize == size) {
                unZipFileSize = true;
            } else {
                folder.delete();
                checkSize();
            }
        }
    }
    public void checkSize(){
        StatFs statFs = new StatFs(Environment.getExternalStorageDirectory().getAbsolutePath());
        StatFs unSd = new StatFs(Environment.getRootDirectory().getAbsolutePath());
        long blockSize = statFs.getBlockSize();
        long totalSize = statFs.getBlockCount()*blockSize;
        long availableSize = statFs.getAvailableBlocks()*blockSize;
        long freeSize = statFs.getFreeBlocks()*blockSize;
        System.out.println("blockSize= "+blockSize);
        System.out.println("totalSize= "+totalSize);
        System.out.println("availableSize= "+availableSize);
        System.out.println("freeSize= "+freeSize );

        if (freeSize >= 1e+9) {
            unzip();
        } else {
            dialog();
        }
        whenGetFolderSizeRecursive_thenCorrect();
    }
    public void unzip(){
        String filePath = File.separator+"sdcard"+File.separator+"Android"+File.separator+"obb"+File.separator+"com.mobifusion.android.ldoce5"+File.separator+"main.228.com.mobifusion.android.ldoce5.obb";
// Unzip the file and place it in another location
        try
        {
            FileInputStream fin = new FileInputStream(filePath);
            ZipInputStream zin = new ZipInputStream(fin);
            ZipEntry ze = null;
            while ((ze = zin.getNextEntry()) != null)
            {
                Log.v("Decompress", "Unzipping " + ze.getName());

                if(ze.isDirectory())
                {
                    dirChecker(ze.getName());
                }
                else
                {

                    FileOutputStream fout = new FileOutputStream(location + ze.getName());
                    BufferedOutputStream bout = new BufferedOutputStream(fout);
                    byte[] buffer = new byte[8192];
                    int len;
                    while ((len = zin.read(buffer)) != -1)
                    {
                        bout.write(buffer, 0, len);
                        publishProgress(progress);
                    }
                    bout.close();
                    fout.close();

                    zin.closeEntry();

                }

            }
            zin.close();

        }
        catch(Exception e)
        {
            Log.e("Decompress", "unzip", e);
        }

    }
    private void dirChecker(String dir) {
        File f = new File(location + dir);

        if(!f.isDirectory()) {
            f.mkdirs();
        }
    }
    public void dialog(){

        AlertDialog.Builder adb = new AlertDialog.Builder(context);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            adb = new AlertDialog.Builder(context, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            adb = new AlertDialog.Builder(context);
        }

        adb.setCancelable(false)
                .setMessage(
                        R.string.insufficent_storage);

        adb.setNegativeButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                context.finish();
            }
        });
        adb.show();

    }

}
