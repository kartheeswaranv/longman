package com.mobifusion.android.ldoce5.Util;

//import com.google.android.gms.analytics.Logger;
//import com.google.android.gms.analytics.Tracker;
//import com.google.android.gms.analytics.GoogleAnalytics;
import com.mobifusion.android.ldoce5.R;

import android.app.Application;

import java.util.HashMap;

/**
 * Created by MuraliKDharan on 06/03/15.
 */


public class LDOCEApp extends Application {

    public enum TrackerName {
        APP_TRACKER // Tracker used only in this app.
    }

//    HashMap<TrackerName, Tracker> mTrackers = new HashMap<>();

    public LDOCEApp() {
        super();
    }

    @Override
    public void onCreate() {
        super.onCreate();
//        GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
//        analytics.setLocalDispatchPeriod(5);
//        analytics.getLogger().setLogLevel(Logger.LogLevel.VERBOSE);
    }

//    public synchronized Tracker getTracker(TrackerName trackerId) {
//        if (!mTrackers.containsKey(trackerId)) {
//            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
//            analytics.getLogger().setLogLevel(Logger.LogLevel.VERBOSE);
//            Tracker t = analytics.newTracker(R.xml.global_tracker);
//            mTrackers.put(trackerId, t);
//        }
//        return mTrackers.get(trackerId);
//    }
}