package com.mobifusion.android.ldoce5.Activity;

import android.content.ContentValues;
import android.content.Context;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.mobifusion.android.ldoce5.Util.Utils;
import com.mobifusion.android.ldoce5.model.SearchRowItem;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ramkumar on 24/03/15.
 */
public class DbHelper {

    public static String phrasalverbHwd="";
    //SearchInflections
    public static List<SearchRowItem> getSearchInflectionHWD(String enteredText) {
        SQLiteDatabase db;
        String query;
        DbConnection dbPath = new DbConnection();
        db = dbPath.getDbConnection();
        List<SearchRowItem> list = new ArrayList<>();
        //Check for db not equal to null and the entered text in the search bar is not equal null
        if (db != null && !enteredText.equalsIgnoreCase("")) {
            //Query to find SearchInflection Headword
            query = String.format("select id,hwd,frequent from searchinflections where InflectionWords = ? collate nocase");
            Cursor cur = db.rawQuery(query, new String[]{enteredText});
            while (cur.moveToNext()) {
                //Add to list
                list.add(new SearchRowItem(cur.getString(1), cur.getString(0), cur.getString(2).equalsIgnoreCase("1")));

            }
        }
        //Closing the db
        db.close();
        return list;
    }

    public static List<SearchRowItem> getSearchResult(String enteredText) {
        SQLiteDatabase db;
        String query;
        String textSpace = "% " + enteredText + " %";
        String textLeftSpace = "% " + enteredText;
        String textRightSpace = enteredText + " %";
        DbConnection dbPath = new DbConnection();
        db = dbPath.getDbConnection();
        List<SearchRowItem> list = new ArrayList<>();

        long startTime = System.currentTimeMillis();
        /* 4 types of Functions added
            1. Wild-card Search -  If the searched Word contains * and ?
            2. Normal Search (Whole Match) -  User Entered Text is searched in Database(Full Word)
            3. Normal Search (SearchInflections) -  ex. If User Entered "played" then corresponding Word "Play" is displayed
            4. Normal Search (Partial Match) -  ex. lo, Searched as "lo%" in the Database ans display the first 30 results of it
             */
        if (!enteredText.equalsIgnoreCase("")) {

            if (enteredText.contains("?") | enteredText.contains("*")) {
                enteredText = enteredText.replace('?', '_');
                //Replace * with % for query
                enteredText = enteredText.replace('*', '%');

                //Check for db not equal to null and the entered text in the search bar is not equal null
                if (db != null) {
                    //Query for wild card search and returning the list
                    query = String.format("SELECT HWD,id,HOMNUM,frequent FROM lookup WHERE HWD like ? GROUP BY HWD");
                    android.database.Cursor cur = db.rawQuery(query, new String[]{enteredText});
                    System.out.println(cur.getCount());
                    while (cur.moveToNext()) {
                        //Add to list
                        list.add(new SearchRowItem(cur.getString(0), cur.getString(1), cur.getString(3).equalsIgnoreCase("1")));
                    }
                }
            } else {
                //exact match
                query = String.format("select HWD,id,HOMNUM,frequent from LOOKUP where (HOMNUM=1 or HOMNUM='') and HWD=? collate nocase ORDER BY HWD ");
                android.database.Cursor cur = db.rawQuery(query, new String[]{enteredText});
                System.out.println(cur.getCount());
                while (cur.moveToNext()) {
                    //Add to list
                    list.add(new SearchRowItem(cur.getString(0), cur.getString(1), cur.getString(3).equalsIgnoreCase("1")));
                }
                //initialize phrasal verb cursor
                android.database.Cursor cursorPhrasal = null;
                //query to Find Phrasal Verb Words
                if (cur.getCount() == 0) {

                    query = String.format("SELECT hwd as HWD,id,frequent,HOMNUM from phrasalVerbs where phrasalVerbHwd = ? COLLATE NOCASE limit 1");
                    cursorPhrasal = db.rawQuery(query, new String[]{enteredText});
                    Log.d("phrasal verb count",cursorPhrasal.getCount()+"");

                    while (cursorPhrasal.moveToNext()) {
                        phrasalverbHwd=enteredText;
                        //Add to list
                        list.add(new SearchRowItem(cursorPhrasal.getString(0), cursorPhrasal.getString(1), cursorPhrasal.getString(2).equalsIgnoreCase("1")));
                    }
                }
                // phrasal verb null and if exact match is available move to search criteria
                if((cursorPhrasal == null || cursorPhrasal.getCount() == 0) && cur.getCount() >=0){
                    //query to Find Partial Matched Words
                    query = String.format("select HWD,id,HOMNUM,frequent from LOOKUP where (HOMNUM=1 or HOMNUM='') and (HWD like ? or HWD like ? or HWD like ?) ORDER BY HWD collate nocase ");
                    android.database.Cursor cursor = db.rawQuery(query, new String[]{textSpace, textLeftSpace, textRightSpace});
                    System.out.println(cursor.getCount());
                    Log.d("cursor count ", "search " + cursor.getCount());
                    while (cursor.moveToNext()) {
                        //Add to list
                        list.add(new SearchRowItem(cursor.getString(0), cursor.getString(1), cursor.getString(3).equalsIgnoreCase("1")));
                    }

                    //Query to find SearchInflection Headword
                    if (cursor.getCount() == 0) {
                        query = String.format("select id,hwd,frequent from searchinflections where InflectionWords = ? collate nocase ORDER BY HWD");
                        android.database.Cursor cursorInflection = db.rawQuery(query, new String[]{enteredText});
                        Log.d("cursor count ", "inflection " + cursorInflection.getCount());
                        while (cursorInflection.moveToNext()) {
                            //Add to list
                            list.add(new SearchRowItem(cursorInflection.getString(1), cursorInflection.getString(0), cursorInflection.getString(2).equalsIgnoreCase("1")));
                        }

                        //Query to find Subset  Headword
                        if (cursorInflection.getCount() == 0) {
                            query = String.format("select HWD,id,HOMNUM,frequent from LOOKUP where (HOMNUM=1 or HOMNUM='') and (HWD like ? and hwd <> ?) collate nocase ORDER BY HWD");
                            android.database.Cursor cursorSubset = db.rawQuery(query, new String[]{enteredText + "%%", enteredText});
                            Log.d("cursor count ", "sub words " + cursorSubset.getCount());
                            while (cursorSubset.moveToNext()) {
                                //Add to list
                                list.add(new SearchRowItem(cursorSubset.getString(0), cursorSubset.getString(1), cursorSubset.getString(3).equalsIgnoreCase("1")));
                            }
                        }
                    }
//                else
//                {

                }

            }
        }

        long stopTime = System.currentTimeMillis();
        double elapsedTime = stopTime - startTime;
        elapsedTime = elapsedTime / 1000.0;
        System.out.println("Total:" + elapsedTime + "secs");
        db.close();
        return list;

    }

    //Function to create index and frequency table
    public void IndexCreationAndFrequencyTableCreation() {
        //Default db connection variables
        SQLiteDatabase db;
        String createFrequencyTableQuery, addColumnLookupQuery, updateColumnFrequentLookupQuery, addColumnSearchInflectionsQuery, updateColumnFrequentSearchInflectionsQuery, indexCoreQuery, indexLookupQuery, indexEtymologyQuery, indexExaSoundQuery, indexPicsQuery, indexUKSoundQuery, indexUSSoundQuery, indexVerbTablesQuery, indexSearchInflectionsQuery,addColumnPhrasalVerb,updatePhrasalVerb;
        String updateColumnLookUp, alterTableLookup;
        //Connecting db
        DbConnection dbPath = new DbConnection();
        db = dbPath.getDbConnection();

        //Transaction is used optimise the execution of queries
        db.beginTransaction();
        //Starting the timer
        long startTime = System.currentTimeMillis();
        //Creating Indexes for all tables:
        indexCoreQuery = "create index if not exists core_id on core(id)";
        indexLookupQuery = "create index if not exists lookup_id on lookup(id)";
        indexEtymologyQuery = "create index if not exists etymology_id on etymology(id)";
        indexExaSoundQuery = "create index if not exists exasound_id on exasound(id)";
        indexPicsQuery = "create index if not exists pics_id on pics(id)";
        indexUKSoundQuery = "create index if not exists uksound_id on uksound(id)";
        indexUSSoundQuery = "create index if not exists ussound_id on ussound(id)";
        indexVerbTablesQuery = "create index if not exists verbtables_id on verbtables(id)";
        indexSearchInflectionsQuery = "create index if not exists search_inflections_inflectionWords on searchinflections(InflectionWords)";

        //Query for frequency table
        createFrequencyTableQuery = "create table if not exists frequency_acad as " +
                "select id,hwd,homnum," +
                "CASE WHEN CORE like '%<FREQ>S1</FREQ>%' THEN 'S1'" +
                "WHEN CORE like '%<FREQ>S2</FREQ>%' THEN 'S2' " +
                "WHEN  CORE like '%<FREQ>S3</FREQ>%' THEN 'S3' " +
                "ELSE '' END as freq_s," +
                "CASE WHEN  CORE like '%<FREQ>W1</FREQ>%' THEN 'W1'" +
                "WHEN  CORE like '%<FREQ>W2</FREQ>%' THEN 'W2'" +
                "WHEN  CORE like '%<FREQ>W3</FREQ>%' THEN 'W3'" +
                "ELSE '' END as freq_w," +
                "CASE WHEN  CORE like '%<LEVEL%value=\"1\"%' THEN '1'" +
                "WHEN  CORE like '%<LEVEL%value=\"2\"%' THEN '2'" +
                "WHEN  CORE like '%<LEVEL%value=\"3\"%' THEN '3'" +
                "ELSE '' END as level," +
                "CASE WHEN CORE like '%<AC>AWL</AC>%' THEN '1'" +
                "ELSE '' END as AC from CORE where CORE like '%<FREQ>S1</FREQ>%'" +
                "OR CORE like '%<FREQ>S2</FREQ>%'" +
                "OR CORE like '%<FREQ>S3</FREQ>%'" +
                "OR CORE like '%<FREQ>W1</FREQ>%'" +
                "OR CORE like '%<FREQ>W2</FREQ>%'" +
                "OR CORE like '%<FREQ>W3</FREQ>%'" +
                "OR CORE like '%<AC>AWL</AC>%'" +
                "OR CORE like '%<LEVEL%value=\"1\"%'" +
                "OR CORE like '%<LEVEL%value=\"2\"%'" +
                "OR CORE like '%<LEVEL%value=\"3\"%';";

        addColumnLookupQuery = "ALTER TABLE lookup ADD COLUMN frequent int default 0;";
        updateColumnFrequentLookupQuery = "update lookup set frequent=1 where hwd in (select hwd from frequency_acad where freq_s <> '' OR freq_w <> '' OR level <> '')";
        addColumnSearchInflectionsQuery = "ALTER TABLE searchinflections ADD COLUMN frequent int default 0;";
        updateColumnFrequentSearchInflectionsQuery = "update searchinflections set frequent=1 where hwd in (select hwd from frequency_acad)";
        alterTableLookup = "ALTER TABLE lookup ADD COLUMN useforWOD int default 0;";
        addColumnPhrasalVerb="ALTER TABLE phrasalVerbs ADD COLUMN frequent int default 0;";
        updatePhrasalVerb="update phrasalVerbs set frequent=1 where hwd in (select hwd from frequency_acad where freq_s <> '' OR freq_w <> '' OR level <> '')";
        //Words that should not be displayed for Local notifications and Word of the Day
        //created a seperate column in Lookup table and made it as 1
        updateColumnLookUp = "update lookup set useforWOD=1 where ID in (select ID from core where core not like '%<Sense%>%' or core like '%<Head>%<REGISTERLAB>taboo%</Head>%' or core like '%<Head>%<REGISTERLAB>not polite%</Head>%');";
        //Execution of multiple queries by a single string
        String[] statements = new String[]{indexCoreQuery, indexEtymologyQuery, indexExaSoundQuery, alterTableLookup, updateColumnLookUp, indexLookupQuery, indexPicsQuery, indexSearchInflectionsQuery, indexUKSoundQuery, indexUSSoundQuery, indexVerbTablesQuery, createFrequencyTableQuery, addColumnLookupQuery, updateColumnFrequentLookupQuery, addColumnSearchInflectionsQuery, updateColumnFrequentSearchInflectionsQuery,addColumnPhrasalVerb,updatePhrasalVerb};
        for (String sql : statements) {
            //Execution query
            db.execSQL(sql);
        }
        //Ending the transaction to make the query execution faster
        db.setTransactionSuccessful();
        db.endTransaction();
        //Calculation for the time to create the tables
        long stopTime = System.currentTimeMillis();
        double elapsedTime = stopTime - startTime;
        elapsedTime = elapsedTime / 1000.0;
        System.out.println(elapsedTime + "secs");
        //Closing the db
        db.close();

    }

    //Function to create UserDb For History and Favourites table
    public void UserDbAndHistoryTableCreation() {
        //Default db connection variables
        SQLiteDatabase db;
        String createHistoryTableQuery, createFavouritesTableQuery, createNotificationsTableQuery;
        //Connecting db
        DbConnection dbPath = new DbConnection();
        //Get UserDb Path
        db = dbPath.getUserDbConnection();
        //Transaction is used optimise the execution of queries
        db.beginTransaction();
        //Starting the timer
        long startTime = System.currentTimeMillis();
        //Creating History and Favourites tables:
        createHistoryTableQuery = "create table if not exists history (id text, hwd text, pos text, timestamp integer,frequent int);";
        createFavouritesTableQuery = "create table if not exists favorites (id text primary key, hwd text, homnum text,frequent int, entryDate DATETIME);";
        createNotificationsTableQuery = "create table if not exists userNotifications (id text, hwd text, notificationDate DATETIME primary key, isNotifSeen BIT );";
        //Execution of multiple queries by a single string
        String[] statements = new String[]{createHistoryTableQuery, createFavouritesTableQuery, createNotificationsTableQuery};
        for (String sql : statements) {
            //Execution query
            db.execSQL(sql);
        }
        //Ending the transaction to make the query execution faster
        db.setTransactionSuccessful();
        db.endTransaction();
        //Calculation for the time to create the tables
        long stopTime = System.currentTimeMillis();
        double elapsedTime = stopTime - startTime;
        elapsedTime = elapsedTime / 1000.0;
        System.out.println(elapsedTime + "secs");
        //Closing the db
        db.close();
    }

    //Function to Fetch HWD randomly for Word-Of-The-Day local notification
    public List<String[]> fetchHwd(Context mContext) {
        String hwd = null, hwdId = null, coreValue = null;
        List<String[]> wordofTheDay = new ArrayList<String[]>();
        String[] wordofTheDayDetail = new String[3];
        SQLiteDatabase db;
        String fetchHwdQuery;
        String unWantedWordsText = null;
        DbConnection dbPath = new DbConnection();
        db = dbPath.getDbConnection();
        AssetManager assetManager = mContext.getAssets();
        InputStream inputStream = null;
        try {
            //Filter Hwd which is not to be shown in Word of the Day
            inputStream = assetManager.open("WordsToAvoid.txt");
            unWantedWordsText = loadTextFile(inputStream);

        } catch (IOException e) {
            //should not find the .txt File
            System.out.println("Problem in opening WordsToAvoid.txt File");
        } finally {
            if (inputStream != null && db != null) {
                try {
                    fetchHwdQuery = String.format("SELECT id,hwd,CORE from core where id in (select id from lookup where  useforWOD !='1' and hwd not in (?) ORDER BY RANDOM() LIMIT 1)");
                    Cursor cur = db.rawQuery(fetchHwdQuery, new String[]{unWantedWordsText});
                    while (cur.moveToNext()) {
                        //ID
                        wordofTheDayDetail[0] = cur.getString(0);
                        //hwd
                        wordofTheDayDetail[1] = cur.getString(1);
                        //Core Value
                        wordofTheDayDetail[2] = cur.getString(2);
                    }
                    // Fetch the First <DEF> tag from Sense or Sub-sense to show it in Expanded View of Notification
                    coreValue = wordofTheDayDetail[2];
                    if (coreValue != null) {
                        //remove if there is any unwanted new-Lines

                        coreValue = coreValue.replaceAll("\n", "");
                        coreValue = coreValue.replaceAll("\r", "");
                        //remove if there if more that 2 spaces
                        coreValue = coreValue.replaceAll("\\s{2,}", "");
                        //Pattern to fetch first <DEF> tag
                        String patternString = "<DEF>(.*?)</DEF>";
                        Pattern pattern = Pattern.compile(patternString);
                        //Fetch the DEF Value
                        Matcher matcher = pattern.matcher(coreValue);
                        while (matcher.find()) {

                            String firstDefValue = matcher.group(1);
                            //If the <DEF> tag value is not null add it it to List
                            if (firstDefValue != null) {
                                firstDefValue = firstDefValue.replaceAll("<.*?>", "");
                                System.out.print(firstDefValue);
                                wordofTheDayDetail[2] = firstDefValue;
                                wordofTheDay.add(wordofTheDayDetail);
                            } else {
                                //If the Fetched Hwd does not have any tag as <DEF> again, fetch the new hwd and send it
                                wordofTheDay = fetchHwd(mContext);
                            }

                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }


        //Closing the db
        db.close();
        return wordofTheDay;
    }

    public String loadTextFile(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        byte[] bytes = new byte[4096];
        int len = 0;
        while ((len = inputStream.read(bytes)) > 0)
            byteStream.write(bytes, 0, len);
        return new String(byteStream.toByteArray(), "UTF8");
    }

    //fetch First Sense for Current Entry

    /**
     * Method for
     * Inserting values into Notification Table
     */
    public static long insertNotificationTable(String hwd, String hwdId) {
        // Inserting into notification table
        // Get the User DB Connection
        SQLiteDatabase getDb = DbConnection.getUserDbConnection();
        long success = 0;
        if (getDb != null) {
            //Transaction is used optimise the execution of queries
            getDb.beginTransaction();
            //Clear the table before inserting new values
            getDb.delete("userNotifications", null, null);
            //When a db exists
            ContentValues insertValues = new ContentValues();
            insertValues.put("id", hwdId);
            insertValues.put("hwd", hwd);
            String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
            insertValues.put("notificationDate", date);
            insertValues.put("isNotifSeen", 0);
            success = getDb.insert("userNotifications", null, insertValues);
            System.out.println("Value is:" + success);
            //Set the transaction as success
            getDb.setTransactionSuccessful();
            //End the transaction
            getDb.endTransaction();
            getDb.close();
        }
        return success;
    }

    /***
     * Funtion for
     * Updating the notification table
     */
    public static void updateNotificationTable(String hwd, String hwdId) {
        // Get the User DB Connection
        SQLiteDatabase db = DbConnection.getUserDbConnection();
        //Transaction is used optimise the execution of queries
        db.beginTransaction();
        ContentValues updateIsNotifSeen = new ContentValues();
        updateIsNotifSeen.put("isNotifSeen", "1");
        db.update("userNotifications", updateIsNotifSeen, "id=?", new String[]{hwdId});
        //Set the transaction as success
        db.setTransactionSuccessful();
        //End the transaction
        db.endTransaction();
        db.close();
    }

    /*
          This function is used to check whether Notification is seen or not
          To Show/disable Word-of-the-Day pop up when App opens
     */
    public static boolean checkIsNotifSeen() {
        String checkIfNotifSeenquery;
        int notifStatus = 0;
        boolean isNotifseen = false;
        // Get the User DB Connection
        SQLiteDatabase db = DbConnection.getUserDbConnection();
        if (db != null) {
            //query to Find Partial Matched Words
            String getTodayDate = Utils.getTodayDate();
            checkIfNotifSeenquery = String.format("select isNotifSeen from userNotifications where notificationDate = ?");
            Cursor cur = db.rawQuery(checkIfNotifSeenquery, new String[]{getTodayDate});
            System.out.println(cur.getCount());
            while (cur.moveToNext()) {
                notifStatus = Integer.parseInt(cur.getString(0));
                if (notifStatus == 1)
                    isNotifseen = true;
                return isNotifseen;
            }
        }
        //Closing the db
        db.close();

        return isNotifseen;
    }

    public static boolean checkNotificationEntry() {
        boolean notificationEntryPresent = false;
        String checkQuery;
        int notifStatus = 0;
        boolean isNotifseen = false;
        // Get the User DB Connection
        SQLiteDatabase db = DbConnection.getUserDbConnection();
        if (db != null) {
            //query to Find Partial Matched Words
            String getTodayDate = Utils.getTodayDate();
            checkQuery = String.format("select * from userNotifications where notificationDate = ?");
            Cursor cur = db.rawQuery(checkQuery, new String[]{getTodayDate});
            if (cur.getCount() > 0) {
                notificationEntryPresent = true;
                return notificationEntryPresent;
            }
        }
        db.close();
        return notificationEntryPresent;
    }

    /*
       Function is mainly used to get a Word from Db - and insert that entry in Usernotification Table
     */
    public String[] createAndInsertNotification(Context mContext) {
        //Array to hold hwd,hwdId and its sense Value(if needed)
        String[] wordofTheDayDetail = new String[3];
        SQLiteDatabase db;
        String fetchHwdQuery;
        String unWantedWordsText = null;
        DbConnection dbPath = new DbConnection();
        db = dbPath.getDbConnection();
        AssetManager assetManager = mContext.getAssets();
        InputStream inputStream = null;
        try {
            //Filter Hwd which is not to be shown in Word of the Day
            inputStream = assetManager.open("WordsToAvoid.txt");
            unWantedWordsText = loadTextFile(inputStream);

        } catch (IOException e) {
            //should not find the .txt File
            System.out.println("Problem in opening WordsToAvoid.txt File");
        } finally {
            if (inputStream != null && db != null) {
                try {
                    fetchHwdQuery = String.format("SELECT id,hwd,CORE from core where id in (select id from lookup where  useforWOD !='1' and hwd not in (?) ORDER BY RANDOM() LIMIT 1)");
                    Cursor cur = db.rawQuery(fetchHwdQuery, new String[]{unWantedWordsText});
                    while (cur.moveToNext()) {
                        //ID
                        wordofTheDayDetail[0] = cur.getString(0);
                        //hwd
                        wordofTheDayDetail[1] = cur.getString(1);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        //Closing the db
        db.close();
        //Check whether fethced entry is inserted correctly or not
        long isInsertionSucssess = insertNotificationTable(wordofTheDayDetail[1], wordofTheDayDetail[0]);
        if (isInsertionSucssess == 1) {
            //If inserted return the word details for creating notifications
            return wordofTheDayDetail;
        }
        //If insertion is not success - do not send the Word details for creating notifications
        wordofTheDayDetail = null;
        return wordofTheDayDetail;
    }

    public static String getLatestHistoryHwd() {
        String hwd = null;
        String latestHistoryHwdQuery;
        SQLiteDatabase db = DbConnection.getUserDbConnection();
        if (db != null) {
            latestHistoryHwdQuery = "select hwd from history order by timestamp desc limit 1";
            Cursor cur = db.rawQuery(latestHistoryHwdQuery, new String[]{});
            while (cur.moveToNext()) {
                hwd = cur.getString(0);
            }
        }
        //Close the Database
        db.close();
        return hwd;
    }
}