

       $(document).ready(function(){
                //GEO Tag Expansion
                    $.each($("GEO"),function(i,geoTag){
                            var geoValue = $(geoTag).find(".geoTagContent").text();
                            var transformedGEO=geoValue.replace(/\w+/g, function(match,contents,offset,s){
                                switch(match){
                                case 'BrE':
                                    return "British English";
                                case 'AmE':
                                    return "American English";
                                case 'AusE':
                                    return "Australian English";
                                default:
                                    return match;
                                }
                            });
                            $(geoTag).find(".geoTagContent").text(transformedGEO);
                        });

                //Expanding POS to their full forms.
            	$.each($(".posContent"),function(i,posTag){
            		var posValue = $(posTag).text();
            		var transformed=posValue.replace(/\w+/g, function(match,contents,offset,s){

            		switch(match){
            			case 'n':
            				return "noun";
            			case 'v':
            				return "verb";
            			case 'adj':
            				return "adjective";
            			case 'pron':
            				return "pronoun";
            			case 'adv':
            				return "adverb";
            			case 'prep':
            				return "preposition";
            			case 'phr':
                            return  "phrasal";
            			case 'interj':
            				return "interjection";
            			default:
            				return match;
            		}
            		});
            		$(posTag).text(transformed);
            	});


                //Gram Tag Expansion
                $.each($(".gramContent"),function(i,gramTag){
                        var gramValue = $(gramTag).text();
                        var transformed=gramValue.replace(/\w+/g, function(match,contents,offset,s){

                            switch(match){
                            case 'C':
                                return "countable";
                            case 'U':
                                return "uncountable";
                            case 'I':
                                return "intransitive";
                            case 'T':
                                return "transitive";
                            case 'P':
                                return "plural";
                            case 'prep':
                                return "preposition";
                            case 'adv':
                                return "adverb";
                            case 'adj':
                                return "adjective";
                            default:
                                return match;
                            }
                        });
                        $(gramTag).text(transformed);
                });
                //SbSth Tag Expansion
                //to replace Sb with Somebody and Sth with something
                    $('body').html( $('body').html().replace(/(\W|^)(sth)/gi,'$1something') );
                    $('body').html( $('body').html().replace(/(\W|^)(sb)/gi,'$1somebody') );
       });