<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://www.w3.org/1999/xhtml" xmlns:h="http://www.w3.org/1999/xhtml" xmlns:e="urn:IDMEE"
	xmlns:d="urn:LDOCE" xmlns:p="urn:LDOCE" version="1.0">

	<xsl:output method="html" version="1.0" encoding="utf-8" omit-xml-declaration="yes"/>
    <xsl:strip-space elements="*"/>
	<xsl:variable name="medium">book</xsl:variable>

	<xsl:template match="/">
		<xsl:apply-templates select="*"/>
	</xsl:template>

	<xsl:template match="@*">
		<xsl:copy-of select="."/>
	</xsl:template>

	<xsl:template match="*">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="text()">
		<xsl:copy><xsl:value-of select="normalize-space(.)"/></xsl:copy>
	</xsl:template>

	<xsl:template match="dict">
		<html>
			<head>
				<title>LDOCE TEST</title>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
				<link rel="stylesheet" type="text/css" href="popup.css"/>
				<link href="longman.css" rel="stylesheet"/>
			</head>
			<body class="noselect" bgcolor="#FFFFFF" style="color:#284D69;">
				<xsl:apply-templates select="node()"/>
			</body>
			<script src="jquery-2.1.3.min.js" type="text/javascript">//</script>
			<!-- <script src="lib/js/jquery.mobile-1.4.5.min.js">//</script> -->
			<script src="word-popup.js" type="text/javascript">//</script>
	    </html>
	</xsl:template>

	<xsl:template match="d:ABBR">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when
					test="./preceding-sibling::d:*[position()=1 and local-name(.)='DUMMYTEST']"/>
				<xsl:otherwise>
					<span class="neutral">
						<xsl:text> </xsl:text>
					</span>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:AC">
		<xsl:copy>
			
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:ACTIV">
	<xsl:copy>
		<xsl:apply-templates select="@*"/>
		<xsl:apply-templates select="node()"/>
	</xsl:copy>
</xsl:template>

	<xsl:template match="d:AMEQUIV">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when
					test="./preceding-sibling::d:*[position()=1 and (local-name(.)='AMEQUIV' or local-name(.)='BREQUIV' or local-name(.)='SYN')]">
					<span class="synonym normal">
						<xsl:text>, </xsl:text>
					</span>
				</xsl:when>
				<xsl:otherwise>
					<span class="synLabel">
						<xsl:text> SYN </xsl:text>
					</span>
				</xsl:otherwise>
			</xsl:choose>
            <span class="synContent">
			    <xsl:apply-templates select="node()"/>
            </span>
			<xsl:choose>
				<xsl:when test="./following-sibling::d:*[position()=1 and local-name(.)='AMEQUIV']"/>
				<xsl:otherwise>
					<span class="geo">
						<xsl:text> American English</xsl:text>
					</span>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="d:AmEVariant">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when test="./d:LINKWORD[contains(.,'also')]">
					<span class="neutral">
						<xsl:text> (</xsl:text>
					</span>
				</xsl:when>
				<xsl:when
					test="./preceding-sibling::d:*[position()=1 and (local-name(.)='Variant' or local-name(.)='BrEVariant'  or local-name(.)='AmEVariant')]">
					<span class="neutral">
						<xsl:text>, </xsl:text>
					</span>
				</xsl:when>
				<xsl:when
					test="./preceding-sibling::d:*[local-name(.)='DERIV'] and not (./d:*[local-name(.)='LINKWORD'])">
					<span class="neutral">
						<xsl:text>, </xsl:text>
					</span>
				</xsl:when>
				<xsl:when test="./preceding-sibling::d:*[local-name(.)='HYPHENATION'] ">
					<span class="neutral">
						<xsl:text>, </xsl:text>
					</span>
				</xsl:when>
				<xsl:when
					test="./preceding-sibling::d:*[local-name(.)='GEO'] and not (./d:*[local-name(.)='LINKWORD'])">
					<span class="neutral">
						<xsl:text>, </xsl:text>
					</span>
				</xsl:when>
				<xsl:otherwise>
					<span class="neutral">
						<xsl:text> </xsl:text>
					</span>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
			<xsl:choose>
				<xsl:when
					test="./following-sibling::d:*[position()=1 and local-name(.)='AmEVariant']"/>
				<xsl:otherwise>
					<span class="geo">
						<xsl:text> American English</xsl:text>
					</span>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:choose>
				<xsl:when
					test="following-sibling::d:*[position()=1 and (local-name(.)='AmEVariant' or local-name(.)='BrEVariant')]"/>
				<xsl:when
					test="preceding-sibling::d:*[position()=1 and ./d:LINKWORD[contains(.,'also')]]">
					<span class="neutral">
						<xsl:text>)</xsl:text>
					</span>
				</xsl:when>
				<xsl:when test="./d:LINKWORD[contains(.,'also')]">
					<span class="neutral">
						<xsl:text>)</xsl:text>
					</span>
				</xsl:when>
				<xsl:when test="./d:LINKWORD[contains(.,'also')]">
					<span class="neutral">
						<xsl:text>)</xsl:text>
					</span>
				</xsl:when>
			</xsl:choose>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:AMEVARPRON">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when
					test="./preceding-sibling::d:*[position()=1 and local-name(.)='DUMMYTEST']"/>
				<xsl:otherwise>
					<span class="neutral">
						<xsl:text> $ </xsl:text>
					</span>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:BOX">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when
					test="./preceding-sibling::d:*[position()=1 and local-name(.)='DUMMYTEST']"/>
				<xsl:otherwise>
					<span class="neutral">
						<xsl:text> </xsl:text>
					</span>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:BREQUIV">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when
					test="./preceding-sibling::d:*[position()=1 and (local-name(.)='AMEQUIV' or local-name(.)='BREQUIV' or local-name(.)='SYN')]">
					<span class="normal">
						<xsl:text>, </xsl:text>
					</span>
				</xsl:when>
				<xsl:otherwise>
					<span class="synLabel">
						<xsl:text> SYN </xsl:text>
					</span>
				</xsl:otherwise>
			</xsl:choose>
            <span class="synContent">
			    <xsl:apply-templates select="node()"/>
            </span>
			<xsl:choose>
				<xsl:when test="./following-sibling::d:*[position()=1 and local-name(.)='BREQUIV']"/>
				<xsl:otherwise>
					<span class="geo">
						<xsl:text> British English</xsl:text>
					</span>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:BrEVariant">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when test="./d:LINKWORD[contains(.,'also')]">
					<span class="neutral">
						<xsl:text> (</xsl:text>
					</span>
				</xsl:when>
				<xsl:when
					test="./preceding-sibling::d:*[position()=1 and (local-name(.)='Variant' or local-name(.)='BrEVariant'  or local-name(.)='AmEVariant')]">
					<span class="neutral">
						<xsl:text>, </xsl:text>
					</span>
				</xsl:when>
				<xsl:when
					test="./preceding-sibling::d:*[local-name(.)='DERIV'] and not (./d:*[local-name(.)='LINKWORD'])">
					<span class="neutral">
						<xsl:text>, </xsl:text>
					</span>
				</xsl:when>
				<xsl:when test="./preceding-sibling::d:*[local-name(.)='HYPHENATION'] ">
					<span class="neutral">
						<xsl:text>, </xsl:text>
					</span>
				</xsl:when>
				<xsl:when
					test="./preceding-sibling::d:*[position()=1 and local-name(.)='GEO'] and not (./d:*[local-name(.)='LINKWORD'])">
					<span class="neutral">
						<xsl:text>, </xsl:text>
					</span>
				</xsl:when>
				<xsl:otherwise>
					<span class="neutral">
						<xsl:text> </xsl:text>
					</span>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
			<xsl:choose>
				<xsl:when
					test="./following-sibling::d:*[position()=1 and local-name(.)='BrEVariant']"/>
				<xsl:otherwise>
					<span class="geo">
						<xsl:text> British English</xsl:text>
					</span>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:choose>
				<xsl:when
					test="following-sibling::d:*[position()=1 and (local-name(.)='AmEVariant' or local-name(.)='BrEVariant')]"/>
				<xsl:when
					test="preceding-sibling::d:*[position()=1 and ./d:LINKWORD[contains(.,'also')]]">
					<span class="neutral">
						<xsl:text>)</xsl:text>
					</span>
				</xsl:when>
				<xsl:when test="./d:LINKWORD[contains(.,'also')]">
					<span class="neutral">
						<xsl:text>)</xsl:text>
					</span>
				</xsl:when>
				<xsl:when test="./d:LINKWORD[contains(.,'also')]">
					<span class="neutral">
						<xsl:text>)</xsl:text>
					</span>
				</xsl:when>
			</xsl:choose>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:COLLO">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when test="./preceding-sibling::d:*">
					<span class="neutral">
						<xsl:text> </xsl:text>
					</span>
				</xsl:when>
				<xsl:otherwise/>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
			<xsl:choose>
				<xsl:when
					test="./preceding-sibling::d:*[position()=1 and local-name(.)='LINKWORD'] and not (./following-sibling::d:*[position()=1 and (local-name(.)='GEO' or local-name(.)='REGISTERLAB')])">
					<span class="neutral">
						<xsl:text>)</xsl:text>
					</span>
				</xsl:when>
			</xsl:choose>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:ColloExa">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when test="./preceding-sibling::d:*[position()=1 and local-name(.)='PROPFORM']">
					<span class="neutral">
						<xsl:text> </xsl:text>
					</span>
				</xsl:when>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:COLLOINEXA">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when test="./preceding-sibling::*[position()=1 and local-name(.)='GLOSS']">
					<span class="neutral">
						<xsl:text> </xsl:text>
					</span>
				</xsl:when>
				<xsl:otherwise/>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:COMMENT">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:COMP">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when test="./preceding-sibling::d:*[position()=1 and local-name(.)='LINKWORD']">
					<span class="neutral">
						<xsl:text> </xsl:text>
					</span>
				</xsl:when>
				<xsl:when test="./preceding-sibling::d:COMP">
					<span class="neutral">
						<xsl:text>, </xsl:text>
					</span>
				</xsl:when>
				<xsl:when test="./preceding-sibling::d:*">
					<span class="neutral">
						<xsl:text>, </xsl:text>
					</span>
					<span class="infllab">comparative </span>
				</xsl:when>
				<xsl:otherwise>
					<span class="infllab">comparative </span>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:Crossref">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<!--				<xsl:when test="./preceding-sibling::d:*[position()=1 and local-name(.)='Crossref' and ./d:Crossrefto[position()=1 and ./d:REFLEX]]"><span class="neutral"><xsl:text>, ➔</xsl:text></span></xsl:when> -->
				<xsl:when
					test="not (descendant::d:REFLEX) and (./preceding-sibling::d:*[position()=1 and local-name(.)='Crossref' and descendant::d:REFLEX])">
					<span class="neutral">
						<xsl:text>, ➔</xsl:text>
					</span>
				</xsl:when>
				<xsl:when test="./preceding-sibling::d:*[position()=1 and local-name(.)='Crossref']">
					<span class="neutral">
						<xsl:text>, </xsl:text>
					</span>
				</xsl:when>
				<xsl:otherwise>
					<span class="neutral">
						<xsl:text> ➔</xsl:text>
					</span>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:Thesref">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when test="./preceding-sibling::d:*[position()=1 and local-name(.)='Thesref']">
					<span class="neutral">
						<xsl:text>, </xsl:text>
					</span>
				</xsl:when>
				<xsl:otherwise>
					<span class="thesrefContent">
						<xsl:text>► see </xsl:text>
					</span>
                    <span class="thesrefLabel">
                        <xsl:text>THESAURUS </xsl:text>
                    </span>
                    <span class="thesrefContent">
                        <xsl:text>at </xsl:text>
                    </span>
				</xsl:otherwise>
			</xsl:choose>
            <span class="thesrefContent">
			    <xsl:apply-templates select="node()"/>
            </span>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="d:Gramref">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when test="./preceding-sibling::d:*[position()=1 and local-name(.)='Gramref']">
					<span class="neutral">
						<xsl:text>, </xsl:text>
					</span>
				</xsl:when>
				<xsl:otherwise>
					<span class="gramref">
						<xsl:text>Grammar guide</xsl:text>
					</span>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:Crossrefto">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when
					test="./preceding-sibling::d:*[position()=1 and local-name(.)='Crossrefto'] and ./d:REFLEX">
					<span class="neutral">
						<xsl:text>, ➔ </xsl:text>
					</span>
				</xsl:when>
				<xsl:when
					test="./preceding-sibling::d:*[position()=1 and local-name(.)='Crossrefto' and ./d:REFLEX]">
					<span class="neutral">
						<xsl:text>, ➔ </xsl:text>
					</span>
				</xsl:when>
				<xsl:when
					test="./preceding-sibling::d:*[position()=1 and local-name(.)='Crossrefto'] and ./d:REFLEX">
					<span class="neutral">
						<xsl:text>, ➔ </xsl:text>
					</span>
				</xsl:when>
				<xsl:when
					test="./preceding-sibling::d:*[position()=1 and local-name(.)='Crossrefto']">
					<span class="neutral">
						<xsl:text>, </xsl:text>
					</span>
				</xsl:when>
				<xsl:otherwise>
					<span class="neutral">
						<xsl:text> </xsl:text>
					</span>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:CROSSREFTYPE">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when
					test="./preceding-sibling::d:*[position()=1 and local-name(.)='DUMMYTEST']"/>
				<xsl:otherwise>
					<span class="neutral">
						<xsl:text> </xsl:text>
					</span>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:DEF">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when test="substring(.,1,1)=','"/>
				<xsl:when
					test="./preceding-sibling::d:*[local-name(.)='LEXUNIT' or local-name(.)='SIGNPOST' or local-name(.)='GRAM' or local-name(.)='REGISTERLAB' or local-name(.)='GEO' or local-name(.)='Variant' or local-name(.)='Inflections' or local-name(.)='BrEVariant' or local-name(.)='AmEVariant' or local-name(.)='DEF']">
					<span class="neutral">
						<xsl:text> </xsl:text>
					</span>
				</xsl:when>
				<xsl:when
					test="./preceding-sibling::d:*[position()=1 and (local-name(.)='FIELD' or local-name(.)='ACTIV')]"/>
				<xsl:when test="./preceding-sibling::d:*">
					<span class="neutral">
						<xsl:text> </xsl:text>
					</span>
				</xsl:when>
				<xsl:otherwise/>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:DERIV">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:value-of select="translate(.,'=#','ˈˌ')"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:Entry">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:if test="@online_only='yes'">
				<xsl:attribute name="class">supp</xsl:attribute>
			</xsl:if>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:EXAMPLE">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<!-- <xsl:if test="@online_only='yes'">
				<xsl:attribute name="class">supp</xsl:attribute>
			</xsl:if>
			<xsl:choose>
				<xsl:when test="parent::d:EXPL"/>
				<xsl:when test="./preceding-sibling::d:*[position()=1 and local-name(.)='EXAMPLE']">
					<span class="neutral">
						<xsl:text> | </xsl:text>
					</span>
				</xsl:when>
				<xsl:when test="./preceding-sibling::d:*[position()=1 and local-name(.)='GramExa']">
					<span class="neutral">
						<xsl:text> | </xsl:text>
					</span>
				</xsl:when>
				<xsl:when test="./preceding-sibling::d:*[position()=1 and local-name(.)='ColloExa']">
					<span class="neutral">
						<xsl:text> | </xsl:text>
					</span>
				</xsl:when>
				<xsl:when test="./preceding-sibling::d:*[position()=1 and local-name(.)='PROPFORM']">
					<span class="neutral">
						<xsl:text> </xsl:text>
					</span>
				</xsl:when>
				<xsl:when
					test="./preceding-sibling::d:*[position()=1 and local-name(.)='PROPFORMPREP']">
					<span class="neutral">
						<xsl:text> </xsl:text>
					</span>
				</xsl:when>
				<xsl:when test="./preceding-sibling::d:*[position()=1 and local-name(.)='EXPL']">
					<span class="neutral">
						<xsl:text> </xsl:text>
					</span>
				</xsl:when>
				<xsl:when test="./preceding-sibling::d:*[position()=1 and local-name(.)='COLLO']">
					<span class="neutral">
						<xsl:text> </xsl:text>
					</span>
				</xsl:when>
				<xsl:when test="./preceding-sibling::d:*[position()=1 and local-name(.)='GLOSS']">
					<span class="neutral">
						<xsl:text> </xsl:text>
					</span>
				</xsl:when>
				<xsl:when
					test="./preceding-sibling::d:*[position()=1 and local-name(.)='GRAM' and ./parent::d:GramExa]">
					<span class="neutral">
						<xsl:text> </xsl:text>
					</span>
				</xsl:when>

				<xsl:otherwise>
					<span class="neutral">
						<xsl:text>: </xsl:text>
					</span>
				</xsl:otherwise>
			</xsl:choose> -->
			<xsl:choose>
				<xsl:when test="(ancestor::d:GramBox) or (ancestor::d:F2NBox) or (ancestor::d:UsageBox)">
					<span class="neutral bullet">
						<xsl:text>&#x2022; </xsl:text>
						<span class="boxes-examples">
							<xsl:apply-templates select="node()"/>
						</span>
					</span>
				</xsl:when>
				<xsl:otherwise>
					<div>
						<span class='lg-uniF028 example-audio-icon'><xsl:comment></xsl:comment></span>
						<xsl:apply-templates select="node()"/>
					</div>
				</xsl:otherwise>
			</xsl:choose>

		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:EXPL">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:if test="preceding-sibling::d:EXPL and not (./d:BADEXA)">
				<xsl:attribute name="class">newline</xsl:attribute>
			</xsl:if>
			<xsl:choose>
				<xsl:when test="@type='smiley'">
					<span class="neutral largebullet">&#x2022; </span>

				</xsl:when>
				<xsl:when test="@type='frownie'">
					<span class="neutral redcolor"> &#x2717; </span>
				</xsl:when>
				<xsl:when
					test="parent::d:UsageBox and not (@type) and count(../d:EXPL[not (@type)]) &gt; 1">
					<span class="neutral largebullet">
						<xsl:text>&#x2022; </xsl:text>
					</span>
				</xsl:when>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
			<xsl:if
				test="./following-sibling::d:*[./parent::d:GramBox and position()=1 and local-name(.)='EXAMPLE']">
				<span class="neutral">
					<xsl:text>: </xsl:text>
				</span>
			</xsl:if>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:FIELD">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:FREQ">
		<div class='freq-info'>
			<xsl:copy>
				<xsl:apply-templates select="@*"/>
				<xsl:choose>
					<xsl:when
						test="./preceding-sibling::d:*[position()=1 and local-name(.)='DUMMYTEST']"/>
					<xsl:otherwise>
						<span class="neutral">
							<xsl:text> </xsl:text>
						</span>
					</xsl:otherwise>
				</xsl:choose>
                <span class='freq-level'>
                    <xsl:apply-templates select="node()"/>
                </span>
			</xsl:copy>
		</div>
	</xsl:template>

	<xsl:template match="d:FULLFORM">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when test="./preceding-sibling::text()">
					<span class="neutral">
						<xsl:text> </xsl:text>
					</span>
				</xsl:when>
				<xsl:when test="./preceding-sibling::d:DEF">
					<span class="neutral">
						<xsl:text> </xsl:text>
					</span>
				</xsl:when>
				<xsl:when test="./preceding-sibling::d:*[position()=1 and local-name(.)='LINKWORD']">
					<span class="neutral">
						<xsl:text> </xsl:text>
					</span>
				</xsl:when>
				<xsl:when test="./preceding-sibling::d:*">
					<span class="neutral">
						<xsl:text> (</xsl:text>
					</span>
				</xsl:when>
				<xsl:otherwise>
					<span class="neutral">
						<xsl:text>(</xsl:text>
					</span>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
			<xsl:choose>
				<xsl:when test="./preceding-sibling::text()"/>
				<xsl:when test="./preceding-sibling::d:DEF"/>
				<xsl:when test="./following-sibling::d:*[position()=1 and local-name(.)='LINKWORD']"/>
				<xsl:otherwise>
					<span class="neutral">
						<xsl:text>)</xsl:text>
					</span>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:GEO">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when test="./preceding-sibling::d:*">
					<span class="neutral">
						<xsl:text> </xsl:text>
					</span>
				</xsl:when>
				<xsl:otherwise/>
			</xsl:choose>
            <span class="geoTagContent">
			    <xsl:apply-templates select="node()"/>
            </span>
			<xsl:choose>
				<xsl:when
					test="./preceding-sibling::d:*[position()=1 and local-name(.)='COLLO'] and (./preceding-sibling::d:*[position()=2 and (local-name(.)='LINKWORD')])">
					<span class="neutral">
						<xsl:text>)</xsl:text>
					</span>
				</xsl:when>
			</xsl:choose>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:GLOSS">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when
					test="./preceding-sibling::d:*[position()=1 and local-name(.)='DUMMYTEST']"/>
				<xsl:otherwise>
					<span class="neutral">
						<xsl:text> (=</xsl:text>
					</span>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
			<xsl:choose>
				<xsl:when
					test="./preceding-sibling::d:*[position()=1 and local-name(.)='DUMMYTEST']"/>
				<xsl:otherwise>
					<span class="neutral">
						<xsl:text>)</xsl:text>
					</span>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:GRAM">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when
					test="./preceding-sibling::d:*[local-name(.)='LEXUNIT' or local-name(.)='SIGNPOST' or local-name(.)='REGISTERLAB' or local-name(.)='GEO' or local-name(.)='Variant']">
					<span class="neutral gramTag">
						<xsl:text> [</xsl:text>
					</span>
				</xsl:when>
				<xsl:when test="./preceding-sibling::d:*[position()=1 and local-name(.)='GRAM']">
					<span class="neutral gramTag">
						<xsl:text>, </xsl:text>
					</span>
				</xsl:when>
				<xsl:when
					test="./preceding-sibling::d:*[position()=1 and (local-name(.)='FIELD' or local-name(.)='ACTIV')]">
					<span class="neutral gramTag">
						<xsl:text>[</xsl:text>
					</span>
				</xsl:when>
				<xsl:when test="./preceding-sibling::d:*">
					<span class="neutral gramTag">
						<xsl:text> [</xsl:text>
					</span>
				</xsl:when>
				<xsl:otherwise>
					<span class="neutral gramTag">
						<xsl:text>[</xsl:text>
					</span>
				</xsl:otherwise>
			</xsl:choose>
            <span class="gramContent gramTag">
			    <xsl:apply-templates select="node()"/>
            </span>
			<xsl:choose>
				<xsl:when test="./following-sibling::d:*[position()=1 and local-name(.)='GRAM']"/>
				<xsl:otherwise>
					<span class="neutral gramTag">
						<xsl:text>]</xsl:text>
					</span>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:GramExa">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:Head">
		<div class="head-cell no-select">
			<xsl:if test="./parent::d:PhrVbEntry">
				<xsl:attribute name="class">head-cell no-select phrvbhead-cell</xsl:attribute>
			</xsl:if>
			<xsl:copy>
				<xsl:apply-templates select="@*"/>
				<xsl:apply-templates select="node()"/>
			</xsl:copy>
			<xsl:if test="./parent::d:Entry">
                <div class='level-container'><xsl:comment></xsl:comment></div>
				<div class='buttons-container'>
                    <span class="white-button  waves-effect wave-light uk-button"><i class="lg-uniF02A"><xsl:comment></xsl:comment></i></span>
					<span class="white-button  waves-effect wave-light us-button"><i class="lg-uniF029"><xsl:comment></xsl:comment></i></span>
					<span class="white-button lg-uniF20D awl-icon"><i><xsl:comment></xsl:comment></i></span>
					<span class="btn-floating btn waves-effect waves-light blue-button"><i class="lg-uniF0c5"><xsl:comment></xsl:comment></i></span>
			        <span class="btn-floating btn waves-effect waves-light blue-button favorite-button"><i class="lg-uniF004"><xsl:comment></xsl:comment></i></span>
					<span class="btn-floating btn waves-effect waves-light blue-button info-button"><i class="lg-uniF129"><xsl:comment></xsl:comment></i></span>
				</div>
			</xsl:if>
		</div>
		<hr class="frequency-container-hr"/>
		<div class="frequency-container-phone">
					<span class='freq-level-prefix local'>frequency</span>
	                    <div class="hori-row-title">
	                        <div class="cell word"><xsl:comment></xsl:comment></div>
	                        <div class="cell local top1000">top_one_thousand</div>
	                        <div class="cell local top2000">top_two_thousand</div>
	                        <div class="cell local top3000" >top_three_thousand</div>
	                    </div>
	                    <div class="hori-row">
	                        <div class=" local title" id="spoken" style="font-size:14px">spoken</div>
	                        <div class="cell-sec">
	                        	<div class="cell-cont s1" ><xsl:comment></xsl:comment></div>
	                        	<div class="cell-cont s2" ><xsl:comment></xsl:comment></div>
	                        	<div class="cell-cont s3" ><xsl:comment></xsl:comment></div>
	                        </div>
	                    </div>
	                    <div class="hori-row" id="written" style="padding-top: 5px;">
	                        <div class=" local title" style="font-size:14px">written</div>
	                        <div class="cell-sec">
	                        	<div class="cell-cont w1"><xsl:comment></xsl:comment></div>
	                        	<div class="cell-cont w2"><xsl:comment></xsl:comment></div>
	                        	<div class="cell-cont w3"><xsl:comment></xsl:comment></div>
	                        </div>
	                    </div>
	                </div>
	</xsl:template>
	<xsl:template match="d:ExpandableInformation">
		<div class="expandable-information" style="display:none;">
			<xsl:copy>
				<xsl:apply-templates select="@*"/>
				<xsl:apply-templates select="node()"/>
			</xsl:copy>
		</div>
	</xsl:template>
	<xsl:template match="d:Hint">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when
					test="./preceding-sibling::d:*[position()=1 and local-name(.)='DUMMYTEST']"/>
				<xsl:otherwise>
					<span class="warning">
						<xsl:text> ► </xsl:text>
					</span>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:HINTBOLD">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when
					test="./preceding-sibling::d:*[position()=1 and local-name(.)='DUMMYTEST']"/>
				<xsl:otherwise/>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:HINTITALIC">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when
					test="./preceding-sibling::d:*[position()=1 and local-name(.)='HINTITALIC']">
					<span class="neutral">
						<xsl:text> | </xsl:text>
					</span>
				</xsl:when>
				<xsl:when
					test="./preceding-sibling::d:*[position()=1 and (local-name(.)='HINTBOLD' or local-name(.)='PronCodes')]">
					<span class="neutral">
						<xsl:text>: </xsl:text>
					</span>
				</xsl:when>
				<xsl:when test="./preceding-sibling::text()">
					<span class="neutral">
						<xsl:text> </xsl:text>
					</span>
				</xsl:when>
				<xsl:otherwise/>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:HINTROMAN">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when
					test="./preceding-sibling::d:*[position()=1 and local-name(.)='DUMMYTEST']"/>
				<xsl:otherwise>
					<span class="neutral">
						<xsl:text> </xsl:text>
					</span>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:HINTTITLE">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when
					test="./preceding-sibling::d:*[position()=1 and local-name(.)='DUMMYTEST']"/>
				<xsl:otherwise>
					<span class="neutral">
						<xsl:text> </xsl:text>
					</span>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:HOMNUM">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:if test="$medium='book' and @pub='online'">
				<xsl:attribute name="class">supp</xsl:attribute>
			</xsl:if>
			<span class="super-script">
				<xsl:apply-templates select="node()"/>
			</span>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:HWD">
			<xsl:copy>
				<xsl:apply-templates select="@*"/>
                <span class="hidden headword">
				    <xsl:apply-templates select="node()"/>
                </span>
			</xsl:copy>
	</xsl:template>

	<xsl:template match="d:HYPHENATION">
			<xsl:copy>
                <span class="hyphenation">
                    <xsl:apply-templates select="@*"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="translate(.,'=#','ˈˌ')"/>
                </span>
			</xsl:copy>

	</xsl:template>

	<xsl:template match="d:Inflections">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when
					test="./preceding-sibling::d:*[position()=1 and local-name(.)='DUMMYTEST']">
					<span class="neutral">
						<xsl:text>, </xsl:text>
					</span>
				</xsl:when>
				<xsl:otherwise>
					<span class="neutral">
						<xsl:text> (</xsl:text>
					</span>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
			<xsl:choose>
				<xsl:when
					test="./following-sibling::d:*[position()=1 and local-name(.)='DUMMYTEST']"/>
				<xsl:otherwise>
					<span class="neutral">
						<xsl:text>)</xsl:text>
					</span>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:LEXUNIT">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when test="./preceding-sibling::d:*">
					<span class="neutral">
						<xsl:text> </xsl:text>
					</span>
				</xsl:when>
				<xsl:otherwise/>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:LEXVAR">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when test="./preceding-sibling::d:LEXVAR">
					<span class="neutral">
						<xsl:text>, </xsl:text>
					</span>
				</xsl:when>
				<xsl:when test="./preceding-sibling::d:*">
					<span class="neutral">
						<xsl:text> </xsl:text>
					</span>
				</xsl:when>
				<xsl:otherwise/>
			</xsl:choose>
			<xsl:value-of select="translate(.,'=#','ˈˌ')"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:LINKWORD">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when test="substring(.,1,1)=','"/>
				<xsl:when test="./preceding-sibling::d:*[position()=1 and local-name(.)='COLLO']">
					<span class="neutral">
						<xsl:text> (</xsl:text>
					</span>
				</xsl:when>
				<xsl:when test="./preceding-sibling::d:*">
					<span class="neutral">
						<xsl:text> </xsl:text>
					</span>
				</xsl:when>

				<xsl:otherwise/>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
    <xsl:template match="d:LINKWORD/text()[.='abbrev.']">
        <span class="LINKWORD">abbreviation</span>
    </xsl:template>

	<xsl:template match="d:NonDV">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when
					test="./preceding-sibling::text()[not (substring(.,string-length(.),1)=' ' or substring(.,string-length(.),1)='(')]">
					<span class="neutral">
						<xsl:text> </xsl:text>
					</span>
				</xsl:when>
				<xsl:otherwise/>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:BOOKFILM">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when
					test="./preceding-sibling::text()[not (substring(.,string-length(.),1)=' ' or substring(.,string-length(.),1)='(')]">
					<span class="neutral">
						<xsl:text> </xsl:text>
					</span>
				</xsl:when>
				<xsl:otherwise/>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:NOTE">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when
					test="./preceding-sibling::d:*[position()=1 and local-name(.)='DUMMYTEST']"/>
				<xsl:otherwise>
					<span class="neutral">
						<xsl:text> </xsl:text>
					</span>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:Noteprompt">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:OBJECT">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when
					test="./preceding-sibling::d:*[position()=1 and local-name(.)='DUMMYTEST']"/>
				<xsl:otherwise>
					<span class="neutral">
						<xsl:text> </xsl:text>
					</span>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:OPP">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when test="./preceding-sibling::d:*[position()=1 and local-name(.)='OPP']">
					<span class="neutral">
						<xsl:text>, </xsl:text>
					</span>
				</xsl:when>
				<xsl:otherwise>
					<span class="oppLabel">
						<xsl:text> OPP </xsl:text>
					</span>
				</xsl:otherwise>
			</xsl:choose>
            <span class="oppContent">
			    <xsl:apply-templates select="node()"/>
            </span>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:ORTHVAR">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when test="./preceding-sibling::d:*">
					<span class="neutral">
						<xsl:text> </xsl:text>
					</span>
				</xsl:when>
				<xsl:otherwise/>
			</xsl:choose>
			<xsl:value-of select="translate(.,'=#','ˈˌ')"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:PASTPART">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when test="./preceding-sibling::d:*[position()=1 and local-name(.)='LINKWORD']">
					<span class="neutral">
						<xsl:text> </xsl:text>
					</span>
				</xsl:when>
				<xsl:when test="./preceding-sibling::d:PASTPART">
					<span class="neutral">
						<xsl:text>, </xsl:text>
					</span>
				</xsl:when>
				<xsl:when test="./preceding-sibling::d:*">
					<span class="neutral">
						<xsl:text>, </xsl:text>
					</span>
                    <span class="infllab">
                        <xsl:text>past participle </xsl:text>
                    </span>
				</xsl:when>
				<xsl:otherwise>
                    <span class="infllab">
                        <xsl:text>past participle </xsl:text>
                    </span>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:PASTTENSE">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when test="./preceding-sibling::d:*[position()=1 and local-name(.)='LINKWORD']">
					<span class="neutral">
						<xsl:text> </xsl:text>
					</span>
				</xsl:when>
				<xsl:when test="./preceding-sibling::d:PASTTENSE">
					<span class="neutral">
						<xsl:text>, </xsl:text>
					</span>
				</xsl:when>
				<xsl:when test="./preceding-sibling::d:*">
					<span class="neutral">
						<xsl:text>, </xsl:text>
					</span>
					<span class="infllab">past tense </span>
				</xsl:when>
				<xsl:otherwise>
					<span class="infllab">past tense </span>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:PhrVbEntry">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when
					test="./preceding-sibling::d:*[position()=1 and local-name(.)='DUMMYTEST']"/>
				<xsl:otherwise/>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:PHRVBHWD">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when
					test="./preceding-sibling::d:*[position()=1 and local-name(.)='DUMMYTEST']"/>
				<xsl:otherwise/>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:PIC">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:PICCAL">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:PLURALFORM">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when test="./preceding-sibling::d:*[local-name(.)='LINKWORD']">
					<span class="neutral">
						<xsl:text> </xsl:text>
					</span>
				</xsl:when>
				<xsl:when test="./preceding-sibling::d:*[local-name(.)='PLURALFORM']">
					<span class="neutral">
						<xsl:text>, </xsl:text>
					</span>
				</xsl:when>
				<xsl:otherwise>
					<span class="italic">
						<xsl:text>plural </xsl:text>
					</span>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:POS">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when test="./preceding-sibling::d:*[local-name(.)='POS']">
					<span class="neutral">
						<xsl:text>, </xsl:text>
					</span>
				</xsl:when>
				<xsl:otherwise>
					<span class="neutral">
						<xsl:text> </xsl:text>
					</span>
				</xsl:otherwise>
			</xsl:choose>
			<span class='pos-text'>
				<xsl:apply-templates select="node()"/>
			</span>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:PRESPART">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when test="./preceding-sibling::d:*[position()=1 and local-name(.)='LINKWORD']">
					<span class="neutral">
						<xsl:text> </xsl:text>
					</span>
				</xsl:when>
				<xsl:when test="./preceding-sibling::d:PRESPART">
					<span class="neutral">
						<xsl:text>, </xsl:text>
					</span>
				</xsl:when>
				<xsl:when test="./preceding-sibling::d:*">
					<span class="neutral">
						<xsl:text>, </xsl:text>
					</span>
					<span class="infllab">present participle </span>
				</xsl:when>
				<xsl:otherwise>
					<span class="infllab">present participle </span>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:PRESPARTX">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when test="./preceding-sibling::d:*[position()=1 and local-name(.)='LINKWORD']">
					<span class="neutral">
						<xsl:text> </xsl:text>
					</span>
                    <span class="infllab">present participle </span>
				</xsl:when>
				<xsl:when test="./preceding-sibling::d:*">
					<span class="neutral">
						<xsl:text>, </xsl:text>
					</span>
                    <span class="infllab">present participle </span>
				</xsl:when>
                <xsl:otherwise>
                    <span class="infllab">present participle </span>
                </xsl:otherwise>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:PRON">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when test="./preceding-sibling::d:EXP">
					<span class="neutral">
						<xsl:text> /</xsl:text>
					</span>
				</xsl:when>
				<xsl:when test="./preceding-sibling::d:*">
					<span class="neutral">
						<xsl:text> </xsl:text>
					</span>
				</xsl:when>
				<xsl:otherwise/>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
			<xsl:choose>
				<xsl:when test="./preceding-sibling::d:EXP">
					<span class="neutral">
						<xsl:text>/</xsl:text>
					</span>
				</xsl:when>
				<xsl:otherwise/>
			</xsl:choose>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:PronCodes">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when
					test="./preceding-sibling::d:*[position()=1 and local-name(.)='DUMMYTEST']"/>
				<xsl:otherwise>
					<span class="neutral">
						<xsl:text> /</xsl:text>
					</span>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
			<xsl:choose>
				<xsl:when
					test="./following-sibling::d:*[position()=1 and local-name(.)='DUMMYTEST']"/>
				<xsl:otherwise>
					<span class="neutral">
						<xsl:text>/</xsl:text>
					</span>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:PROPFORM">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when test="./preceding-sibling::d:GEO">
					<span class="neutral">
						<xsl:text>, </xsl:text>
					</span>
				</xsl:when>
				<xsl:when test="./preceding-sibling::d:*">
					<span class="neutral">
						<xsl:text> </xsl:text>
					</span>
				</xsl:when>
				<xsl:otherwise/>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:PROPFORMPREP">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when
					test="./preceding-sibling::d:*[position()=1 and local-name(.)='PROPFORMPREP']">
					<span class="neutral">
						<xsl:text>, </xsl:text>
					</span>
				</xsl:when>
				<xsl:when test="./preceding-sibling::d:*">
					<span class="bold expandHWD">
					</span>
				</xsl:when>
				<xsl:otherwise>
					<span class="bold expandHWD">
					</span>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:PTandPP">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when test="./preceding-sibling::d:*[position()=1 and local-name(.)='LINKWORD']">
					<span class="neutral">
						<xsl:text> </xsl:text>
					</span>
				</xsl:when>
				<xsl:when test="./preceding-sibling::d:PTandPP">
					<span class="neutral">
						<xsl:text>, </xsl:text>
					</span>
				</xsl:when>
				<xsl:when test="./preceding-sibling::d:*">
					<span class="neutral">
						<xsl:text>, </xsl:text>
					</span>
					<span class="infllab">past tense and past participle </span>
				</xsl:when>
				<xsl:otherwise>
					<span class="infllab">past tense and past participle </span>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:PTandPPX">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when test="./preceding-sibling::d:*[position()=1 and local-name(.)='LINKWORD']">
					<span class="neutral">
						<xsl:text> </xsl:text>
					</span>
				</xsl:when>
				<xsl:when test="./preceding-sibling::d:*">
					<span class="neutral">
						<xsl:text>, </xsl:text>
					</span>
                    <span class="infllab">past tense and past participle </span>
				</xsl:when>
                <xsl:otherwise>
                    <span class="infllab">past tense and past participle </span>
                </xsl:otherwise>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:REFHOMNUM">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:if test="not (ancestor::d:Thesref)">
				<xsl:apply-templates select="node()"/>
			</xsl:if>
		</xsl:copy>
	</xsl:template>


	<xsl:template match="d:REFHWD">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when test="../preceding-sibling::d:*[position()=1 and local-name(.)='REFLEX']">
					<span class="neutral lowercase">
						<xsl:text>at </xsl:text>
					</span>
				</xsl:when>
				<xsl:otherwise/>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:REFLEX">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when test="./parent::d:Crossrefto">
					<xsl:attribute name="class">supp</xsl:attribute>
				</xsl:when>
				<xsl:when test="./preceding-sibling::d:Crossrefto">
					<span class="neutral">
						<xsl:text>, ➔ </xsl:text>
					</span>
				</xsl:when>
				<xsl:otherwise>
					<span class="neutral">
						<xsl:text> </xsl:text>
					</span>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:REFSENSENUM">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when
					test="./preceding-sibling::d:*[position()=1 and local-name(.)='REFSENSENUM']">
					<span class="neutral">
						<xsl:text>, </xsl:text>
					</span>
				</xsl:when>
				<xsl:otherwise>
					<span class="refsensenum">
						<xsl:text>(</xsl:text>
					</span>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
			<xsl:choose>
				<xsl:when
					test="./following-sibling::d:*[position()=1 and local-name(.)='REFSENSENUM']"/>
				<xsl:otherwise>
					<span class="refsensenum">
						<xsl:text>)</xsl:text>
					</span>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:REGISTERLAB">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when test="./preceding-sibling::d:*">
					<span class="neutral">
						<xsl:text> </xsl:text>
					</span>
				</xsl:when>
				<xsl:otherwise/>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
			<xsl:choose>
				<xsl:when
					test="./preceding-sibling::d:*[position()=1 and local-name(.)='COLLO'] and (./preceding-sibling::d:*[position()=2 and (local-name(.)='LINKWORD')])">
					<span class="neutral">
						<xsl:text>)</xsl:text>
					</span>
				</xsl:when>
			</xsl:choose>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:RELATEDWD">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when
					test="./preceding-sibling::d:*[position()=1 and local-name(.)='RELATEDWD']">
					<span class="neutral">
						<xsl:text>, </xsl:text>
					</span>
				</xsl:when>
				<xsl:otherwise>
					<span class="neutral">
						<xsl:text> ➔ </xsl:text>
					</span>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:RunOn">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when
					test="./preceding-sibling::d:*[position()=1 and local-name(.)='DUMMYTEST']"/>
				<xsl:otherwise>
					<span class="neutral">
						<xsl:text> —</xsl:text>
					</span>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:Sense">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when
					test="count(../d:*[local-name(.)='Sense']) + count(../d:SpokenSect/*[local-name(.) = 'Sense'])>1">
					<xsl:choose>
						<xsl:when
							test="ancestor::d:Entry[descendant::*[local-name(.)='FREQ' or local-name(.)='SIGNPOST']]">
							<xsl:attribute name="class">newline</xsl:attribute>
						</xsl:when>
						<xsl:otherwise/>
					</xsl:choose>
					<span class="sensenum">
						<xsl:if
							test="preceding-sibling::d:Sense[position()=1 and descendant::*[local-name(.)='F2NBox' or local-name(.)='ColloBox']]">
							<xsl:attribute name="newline">yes</xsl:attribute>
						</xsl:if>

						<xsl:choose>
							<xsl:when test="parent::*[local-name(.)='SpokenSect']">
								<xsl:value-of
									select="count(preceding-sibling::*[local-name(.) = 'Sense']) + count(./preceding-sibling::d:SpokenSect/*[local-name(.) = 'Sense']) + count(../preceding-sibling::*[local-name(.) = 'Sense']) + 1"
									/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of
									select="count(preceding-sibling::*[local-name(.) = 'Sense']) + count(./preceding-sibling::d:SpokenSect/*[local-name(.) = 'Sense']) + 1"
									/>
							</xsl:otherwise>
						</xsl:choose>
					</span>
				</xsl:when>
				<xsl:otherwise>
					<span class="neutral">
						<xsl:text> </xsl:text>
					</span>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:SIGNPOST">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when
					test="./preceding-sibling::d:*[position()=1 and local-name(.)='DUMMYTEST']"/>
				<xsl:otherwise/>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:SpokenSect">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when
					test="./preceding-sibling::d:*[position()=1 and local-name(.)='DUMMYTEST']"/>
				<xsl:otherwise>
					<hr/><span class="spokensect">SPOKEN PHRASES</span>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:STRONG">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when test="./preceding-sibling::d:*[position()=1 and local-name(.)='PRON']">
					<span class="neutral">
						<xsl:text>; </xsl:text>
					</span>
				</xsl:when>
				<xsl:otherwise>
					<span class="neutral">
						<xsl:text> </xsl:text>
					</span>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:Subsense">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when test="count(../d:*[local-name(.)='Subsense'])>1">
					<!-- <xsl:if test="preceding-sibling::d:*">
						<span class="neutral">
							<xsl:text> </xsl:text>
						</span>
					</xsl:if> -->
					<xsl:variable name="count"
						select="count(preceding-sibling::*[local-name(.) = 'Subsense']) + 1"/>
					<span class="sensenum">
						<xsl:number value="$count" format="a)"/>
					</span>
				</xsl:when>
				<xsl:otherwise>
					<span class="neutral">
						<xsl:text> </xsl:text>
					</span>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:SUFFIX">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when
					test="./preceding-sibling::d:*[position()=1 and local-name(.)='DUMMYTEST']"/>
				<xsl:otherwise/>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:SUPERL">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when test="./preceding-sibling::d:*[position()=1 and local-name(.)='LINKWORD']">
					<span class="neutral">
						<xsl:text> </xsl:text>
					</span>
				</xsl:when>
				<xsl:when test="./preceding-sibling::d:SUPERL">
					<span class="neutral">
						<xsl:text>, </xsl:text>
					</span>
				</xsl:when>
				<xsl:when test="./preceding-sibling::d:*">
					<span class="neutral">
						<xsl:text>, </xsl:text>
					</span>
					<span class="infllab">superlative </span>
				</xsl:when>
				<xsl:otherwise>
					<span class="infllab">superlative </span>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:SYN">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when test="./preceding-sibling::d:*[position()=1 and local-name(.)='SYN']">
					<span class="neutral">
						<xsl:text>, </xsl:text>
					</span>
				</xsl:when>
				<xsl:when test="./preceding-sibling::d:*[position()=1 and local-name(.)='BREQUIV']">
					<span class="neutral">
						<xsl:text>, </xsl:text>
					</span>
				</xsl:when>
				<xsl:when test="./preceding-sibling::d:*[position()=1 and local-name(.)='AMEQUIV']">
					<span class="neutral">
						<xsl:text>, </xsl:text>
					</span>
				</xsl:when>
				<xsl:otherwise>
					<span class="synLabel">
						<xsl:text> SYN </xsl:text>
					</span>
				</xsl:otherwise>
			</xsl:choose>
            <span class="synContent">
			    <xsl:apply-templates select="node()"/>
            </span>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:T3PERSSING">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when test="./preceding-sibling::d:*[position()=1 and local-name(.)='LINKWORD']">
					<span class="neutral">
						<xsl:text> </xsl:text>
					</span>
				</xsl:when>
				<xsl:when test="./preceding-sibling::d:T3PERSSING">
					<span class="neutral">
						<xsl:text>, </xsl:text>
					</span>
				</xsl:when>
				<xsl:when test="./preceding-sibling::d:*">
					<span class="neutral">
						<xsl:text>, </xsl:text>
					</span>
					<span class="infllab">third person singular </span>
				</xsl:when>
				<xsl:otherwise>
					<span class="infllab">third person singular </span>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:T3PERSSINGX">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when test="./preceding-sibling::d:*">
					<span class="neutral">
						<xsl:text>, </xsl:text>
					</span>
                    <span class="infllab">third person singular </span>
				</xsl:when>
                <xsl:otherwise>
                    <span class="infllab">third person singular </span>
                </xsl:otherwise>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:Tail">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:UNCLASSIFIED">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when
					test="./preceding-sibling::d:*[position()=1 and local-name(.)='DUMMYTEST']"/>
				<xsl:otherwise>
					<span class="neutral">
						<xsl:text> </xsl:text>
					</span>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:USAGE">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when
					test="./preceding-sibling::d:*[position()=1 and local-name(.)='DUMMYTEST']"/>
				<xsl:otherwise>
					<span class="neutral">
						<xsl:text> </xsl:text>
					</span>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:Variant">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when test="./d:LINKWORD[contains(.,'also')]">
					<span class="neutral">
						<xsl:text> (</xsl:text>
					</span>
				</xsl:when>
				<xsl:when test="./d:*[local-name(.)='ABBR'] ">
					<span class="neutral">
						<xsl:text> (</xsl:text>
					</span>
				</xsl:when>
				<xsl:when
					test="./preceding-sibling::d:*[position()=1 and (local-name(.)='Variant' or local-name(.)='BrEVariant'  or local-name(.)='AmEVariant')]">
					<span class="neutral">
						<xsl:text>, </xsl:text>
					</span>
				</xsl:when>
				<xsl:when
					test="./preceding-sibling::d:*[local-name(.)='DERIV'] and not (./d:*[local-name(.)='LINKWORD'])">
					<span class="neutral">
						<xsl:text>, </xsl:text>
					</span>
				</xsl:when>
				<xsl:when
					test="./preceding-sibling::d:*[local-name(.)='HYPHENATION'] and ./d:*[local-name(.)='GEO']">
					<span class="neutral">
						<xsl:text>, </xsl:text>
					</span>
				</xsl:when>
				<xsl:when
					test="./preceding-sibling::d:*[local-name(.)='HYPHENATION'] and not (./d:*[local-name(.)='LINKWORD'])">
					<span class="neutral">
						<xsl:text>, </xsl:text>
					</span>
				</xsl:when>
				<xsl:when
					test="./preceding-sibling::d:*[position()=1 and local-name(.)='GEO'] and not (./d:*[local-name(.)='LINKWORD'])">
					<span class="neutral">
						<xsl:text>, </xsl:text>
					</span>
				</xsl:when>
				<xsl:when
					test="./preceding-sibling::d:*[position()=1 and local-name(.)='REGISTERLAB'] and not (./d:*[local-name(.)='LINKWORD'])">
					<span class="neutral">
						<xsl:text>, </xsl:text>
					</span>
				</xsl:when>
				<xsl:otherwise>
					<span class="neutral">
						<xsl:text> </xsl:text>
					</span>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
			<xsl:choose>
				<xsl:when test="./d:*[local-name(.)='ABBR'] ">
					<span class="neutral">
						<xsl:text>)</xsl:text>
					</span>
				</xsl:when>
				<xsl:when
					test="following-sibling::d:*[position()=1 and (local-name(.)='AmEVariant' or local-name(.)='BrEVariant')]"/>
				<xsl:when test="./d:LINKWORD[contains(.,'also')]">
					<span class="neutral">
						<xsl:text>)</xsl:text>
					</span>
				</xsl:when>
				<xsl:otherwise/>
			</xsl:choose>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:F2NBox">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:if test="@online_only='yes'">
				<xsl:attribute name="class">supp</xsl:attribute>
			</xsl:if>
			<span class="boxheader">Register</span>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:ColloBox">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:if test="@online_only='yes'">
				<xsl:attribute name="class">supp</xsl:attribute>
			</xsl:if>
			<span class="heading">COLLOCATIONS</span>
			<xsl:apply-templates select="node()"/>
			<xsl:apply-templates select="descendant::d:Collocate[@book='truncated']"
				mode="truncated"/>
			<xsl:apply-templates select="descendant::d:ErrorBox" mode="display"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:ThesBox">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:if test="@online_only='yes'">
				<xsl:attribute name="class">supp</xsl:attribute>
			</xsl:if>
			<xsl:if test="@fullpage='yes'">
				<xsl:attribute name="headword">
					<xsl:value-of select="ancestor::d:Entry/d:Head/d:HWD"/>
				</xsl:attribute>
			</xsl:if>
			<span class="heading">THESAURUS</span>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:Section">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:if test="@online_only='yes'">
				<xsl:attribute name="class">supp</xsl:attribute>
			</xsl:if>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:ErrorBox"> </xsl:template>

	<xsl:template match="d:ErrorBox" mode="display">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:if test="@online_only='yes'">
				<xsl:attribute name="class">supp</xsl:attribute>
			</xsl:if>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:LearnerItem">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:if test="@online_only='yes'">
				<xsl:attribute name="class">supp</xsl:attribute>
			</xsl:if>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:Error">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when test="preceding-sibling::d:Error"/>
				<xsl:otherwise>
					<span class="warning">
						<xsl:text> ► </xsl:text>
					</span>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:Collocate">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:attribute name="class">
				<xsl:choose>
					<xsl:when test="@online_only='yes'">supp</xsl:when>
					<xsl:when
						test="$medium='book' and (not (descendant::d:COLLEXA[not (@online_only='yes')])) and (preceding-sibling::d:*[position()=1 and local-name(.)='Collocate' and not (descendant::d:COLLEXA[not (@online_only='yes')])])"
						>inline</xsl:when>
					<xsl:otherwise>newline</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:choose>
				<xsl:when
					test="($medium='book' and not (descendant::d:COLLEXA[not (@online_only='yes')])) and (preceding-sibling::d:*[position()=1 and local-name(.)='Collocate' and not (descendant::d:COLLEXA[not (@online_only='yes')])])">
					<span class="neutral">
						<xsl:text></xsl:text>
					</span>
				</xsl:when>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:Exponent">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:if test="@online_only='yes'">
				<xsl:attribute name="class">supp</xsl:attribute>
			</xsl:if>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:COLLOC">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:attribute name="class">
				<xsl:choose>
					<xsl:when test="../d:KEY">key</xsl:when>
					<xsl:when test="@online_only='yes'">supp</xsl:when>
					<xsl:otherwise>collo</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:EXP">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:attribute name="class">
				<xsl:choose>
					<xsl:when test="../d:KEY">key</xsl:when>
					<xsl:when test="@online_only='yes'">supp</xsl:when>
					<xsl:otherwise>display</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:choose>
				<xsl:when
					test="./preceding-sibling::d:*[position()=1 and local-name(.)='REGISTERLAB']">
					<span class="neutral">
						<xsl:text>, </xsl:text>
					</span>
				</xsl:when>
				<xsl:when test="./preceding-sibling::d:*[position()=1 and local-name(.)='GEO']">
					<span class="neutral">
						<xsl:text>, </xsl:text>
					</span>
				</xsl:when>
				<xsl:when test="./preceding-sibling::d:*[position()=1 and local-name(.)='DEF']">
					<span class="neutral">
						<xsl:text> </xsl:text>
					</span>
				</xsl:when>
				<xsl:otherwise/>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:COLLEXA">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:if test="@online_only='yes'">
				<xsl:attribute name="class">supp</xsl:attribute>
			</xsl:if>
			<xsl:choose>
				<xsl:when test="./preceding-sibling::d:*[position()=1 and local-name(.)='COLLEXA']">
					<span class="neutral">
						<xsl:text></xsl:text>
					</span>
				</xsl:when>
				<xsl:otherwise>
					<span class="neutral">
						<xsl:text> </xsl:text>
					</span>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:THESEXA">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:if test="@online_only='yes'">
				<xsl:attribute name="class">supp</xsl:attribute>
			</xsl:if>
			<xsl:choose>
				<xsl:when test="./preceding-sibling::d:*[position()=1 and local-name(.)='THESEXA']">
					<span class="neutral"> | </span>
				</xsl:when>
				<xsl:otherwise>
					<span class="neutral">
						<xsl:text>: </xsl:text>
					</span>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:THESPROPFORM">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:if test="@online_only='yes'">
				<xsl:attribute name="class">supp</xsl:attribute>
			</xsl:if>
			<xsl:choose>
				<xsl:when test="./preceding-sibling::d:*[position()=1 and local-name(.)='THESEXA']">
					<span class="neutral"> | </span>
				</xsl:when>
				<xsl:otherwise>
					<span class="neutral">
						<xsl:text>: </xsl:text>
					</span>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:COLLGLOSS">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:if test="@online_only='yes'">
				<xsl:attribute name="class">supp</xsl:attribute>
			</xsl:if>
			<xsl:choose>
				<xsl:when
					test="./preceding-sibling::d:*[position()=1 and local-name(.)='DUMMYTEST']"/>
				<xsl:otherwise>
					<span class="neutral">
						<xsl:text> (=</xsl:text>
					</span>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
			<xsl:choose>
				<xsl:when
					test="./preceding-sibling::d:*[position()=1 and local-name(.)='DUMMYTEST']"/>
				<xsl:otherwise>
					<span class="neutral">
						<xsl:text>)</xsl:text>
					</span>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:HEADING">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when test=".='Sense 1' and not (../following-sibling::d:ColloBox)">
					<xsl:attribute name="class">supp</xsl:attribute>
					<xsl:apply-templates select="node()"/>
				</xsl:when>
				<xsl:when test="./parent::d:F2NBox">
					<span class="neutral">
						<xsl:text> </xsl:text>
					</span>
					<xsl:apply-templates select="node()"/>
				</xsl:when>
				<xsl:when test="./parent::d:GramBox and count(../d:HEADING)=1">
					<span class="heading">
						<xsl:text>GRAMMAR: </xsl:text>
						<xsl:apply-templates select="node()"/>
					</span>
				</xsl:when>
				<xsl:when test="./parent::d:UsageBox and not (preceding-sibling::d:*)">
					<span class="heading">
						<xsl:text>USAGE: </xsl:text>
						<xsl:apply-templates select="node()"/>
					</span>
				</xsl:when>
				<xsl:when test="./parent::d:GramBox|parent::d:UsageBox">
					<xsl:attribute name="class">newline</xsl:attribute>
					<xsl:apply-templates select="node()"/>
				</xsl:when>
				<xsl:otherwise>
					<span class="neutral">
						<xsl:text> – </xsl:text>
					</span>
					<xsl:choose>
						<xsl:when test="contains(.,'sense')">
							<xsl:value-of
								select="concat(substring-before(.,'sense'),'Meaning',substring-after(.,'sense'))"
								/>
						</xsl:when>
						<xsl:when test="contains(.,'Sense')">
							<xsl:value-of
								select="concat(substring-before(.,'Sense'),'Meaning',substring-after(.,'Sense'))"
								/>
						</xsl:when>
					</xsl:choose>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:BADCOLLO">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:GOODCOLLO">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<span class="neutral">
				<xsl:text> </xsl:text>
			</span>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:SUBHEADING">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:SECHEADING">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:if test="contains(.,'+')">
				<xsl:attribute name="convert">no</xsl:attribute>
			</xsl:if>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:BADEXA">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
                <xsl:when test="./preceding-sibling::d:*[position()=1 and local-name(.)='GOODEXA']">
                    <span class="neutral">
                        <xsl:text></xsl:text>
                    </span>
                </xsl:when>
                <xsl:when test="./preceding-sibling::d:*[position()=1 and local-name(.)='BADEXA']">
                    <span class="neutral">
                        <xsl:text> </xsl:text>
                    </span>
                </xsl:when>
                <xsl:otherwise>
					<span class="neutral">
						<xsl:text> </xsl:text>
					</span>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:GOODEXA">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when test="./preceding-sibling::d:*[position()=1 and local-name(.)='GOODEXA']">
					<span class="neutral">
						<xsl:text></xsl:text>
					</span>
				</xsl:when>
				<xsl:when test="./preceding-sibling::d:*[position()=1 and local-name(.)='BADEXA']">
					<xsl:text> ➔ </xsl:text>
				</xsl:when>
				<xsl:otherwise/>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:GramBox">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
            <xsl:if test="EXAMPLE">

            </xsl:if>
			<xsl:choose>
				<xsl:when test="@type='comparison'">
					<xsl:attribute name="class">
						<xsl:text>header</xsl:text>
					</xsl:attribute>
					<span class="boxheader">
						<xsl:text>GRAMMAR: Comparison</xsl:text>
					</span>
				</xsl:when>
				<xsl:when test="@type='patterns'">
					<xsl:attribute name="class">
						<xsl:text>header</xsl:text>
					</xsl:attribute>
					<span class="boxheader">
						<xsl:text>GRAMMAR: Patterns with </xsl:text>
						<xsl:value-of select="ancestor::d:Entry/descendant::d:HWD[1]"/>
					</span>
				</xsl:when>
				<xsl:when test="@type='nobox'">
					<xsl:attribute name="class">
						<xsl:text>nobox</xsl:text>
					</xsl:attribute>
					<span class="heading"> GRAMMAR </span>
				</xsl:when>
				<xsl:otherwise>
					<xsl:choose>
						<xsl:when test="./d:HEADING">
							<xsl:attribute name="class">
								<xsl:text>header</xsl:text>
							</xsl:attribute>
							<xsl:if test="count(./d:HEADING) &gt;1">
								<span class="boxheader">
									<xsl:text>GRAMMAR</xsl:text>
								</span>
							</xsl:if>
						</xsl:when>
						<xsl:otherwise>
							<xsl:attribute name="class">
								<xsl:text>normal</xsl:text>
							</xsl:attribute>
							<span class="boxheader">GRAMMAR</span>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:UsageBox">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:ThesColloBox">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:COLLORANGE">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<span class="neutral">
				<xsl:text> </xsl:text>
			</span>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:WARNING">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<span class="warning">
				<xsl:text> ► </xsl:text>
			</span>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="d:LEVEL">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<span class="level">
				<xsl:choose>
					<xsl:when test="@value=1">
						<xsl:text>●○○</xsl:text>
					</xsl:when>
					<xsl:when test="@value=2">
						<xsl:text>●●○</xsl:text>
					</xsl:when>
					<xsl:when test="@value=3">
						<xsl:text>●●●</xsl:text>
					</xsl:when>
				</xsl:choose>
			</span>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:CROSS">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:choose>
				<xsl:when test="@type='smiley'">
					<span class="neutral">&#x22022;</span>
				</xsl:when>
				<xsl:otherwise>
					<span class="neutral redcolor">&#x2717; </span>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="d:SearchInflections">
		<span class="hidden">
			<xsl:copy>
				<xsl:apply-templates select="@*"/>
				<xsl:apply-templates select="node()"/>
			</xsl:copy>
		</span>
	</xsl:template>
</xsl:stylesheet>
