<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output omit-xml-declaration="yes"/>
    <xsl:variable name="vEmbDoc">
    </xsl:variable>
    <xsl:template match="Collocations">
        <html>
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                <style>
                    body
                    {
                        font-family:"MundoSansPro";
                        background-color:#EDF4FA;
                        color:rgb(56,103,138);
                        padding:0;
                        margin:10px;
                        font-size:14pt;
                    }
                    .section-heading
                    {
                        text-align:left;
                        font-family:"MundoSansPro-Bold"
                        color:rgb(56,103,138);
                        text-transform: capitalize;
                        line-height:1.5;
                        font-size:14pt;
                        }
                    .colloc
                    {
                        font-family:"MundoSansPro-Bold";
                        color:rgb(174,145,106);
                        font-size:14pt;
                        line-height:1.5;
                    }
                    .pronunciation
                    {
                        font-family:CharisSIL;
                        Color:rgb(56,103,138);
                    }
                    .sec-heading
                    {
                        text-transform: uppercase;
                    }
                    secheading[convert="no"] .sec-heading
                    {
                        text-transform: none;
                    }
                    .text-italic
                    {
                        font-family:"MundoSansPro-Italic";
                    }
                    .text-bold
                    {
                        font-family:"MundoSansPro-Bold";
                    }
                    @font-face {
                    font-family: 'MundoSansPro';
                    src: url('../MundoSansPro.otf')
                    }
                    @font-face {
                    font-family: 'MundoSansPro-Bold';
                    src: url('../MundoSansPro-Bold.otf')
                    }
                    @font-face {
                    font-family: 'MundoSansPro-Medium';
                    src: url('../MundoSansPro-Medium.otf')
                    }
                    @font-face {
                    font-family: 'MundoSansPro-MediumItalic';
                    src: url('../MundoSansPro-MediumItalic.otf')
                    }
                    @font-face {
                    font-family: 'MundoSansPro-BoldItalic';
                    src: url('../MundoSansPro-BoldItalic.otf')
                    }
                    @font-face {
                    font-family: 'MundoSansPro-Italic';
                    src: url('../MundoSansPro-Italic.otf')
                    }
                    @font-face {
                    font-family: 'MundoSansPro-Light';
                    src: url('../MundoSansPro-Light.otf')
                    }
                    @font-face {
                    font-family: 'MundoSansPro-LightItalic';
                    src: url('../MundoSansPro-LightItalic.otf')
                    }
                    @font-face {
                    font-family: 'CharisSILR';
                    src: url('../CharisSILR.ttf')
                    }

                    @font-face {
                    font-family: 'CharisSILI';
                    src: url('../CharisSILI.ttf')
                    }
                </style>
            </head>
            <body>
                    <xsl:for-each select="ColloBox">
                        <xsl:apply-templates/>
                    </xsl:for-each>
            </body>
            <script src="jquery-2.1.3.min.js" type="text/javascript">//</script>
            <script src="additional_info.js" type="text/javascript">//</script>
        </html>
    </xsl:template>
    
    <!--Templates-->
    <xsl:template match="xsl:template"/>
    
    <!-- HEADING Formatting tags-->
    <xsl:template match="HEADING">
        <span class="text-bold sense-heading">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="EXPR">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="UNDERLINE">
        <xsl:apply-templates/>
    </xsl:template>
    
    <!-- Section Formatting tags-->
    <xsl:template match="Section">
        <xsl:apply-templates/><br/>
    </xsl:template>
    <xsl:template match="addNewLine">
        <xsl:apply-templates/><br/>
    </xsl:template>

    <xsl:template match="SECHEADING">
        <div class="section-heading">
            <span class="text-bold">
                <xsl:copy>
                    <xsl:apply-templates select="@*"/>
                    <xsl:if test="contains(.,'+')">
                        <xsl:attribute name="convert">no</xsl:attribute>
                    </xsl:if>
                    <span class="sec-heading">
                        <xsl:apply-templates select="node()"/>
                    </span>
                </xsl:copy>
            </span>
            <br/>
        </div>
    </xsl:template>
    <xsl:template match="Collocate">
        <xsl:apply-templates/><br/>
    </xsl:template>
    <xsl:template match="COLLOC">
        <span class="colloc">
            <xsl:apply-templates/>
            <xsl:text>    </xsl:text>
        </span>
    </xsl:template>
    <xsl:template match="Variant">
        (<xsl:apply-templates/>)
    </xsl:template>
    <xsl:template match="LINKWORD">
        <span class="text-italic">
            <xsl:apply-templates/>
        </span>
        <xsl:text>    </xsl:text>
    </xsl:template>
    <xsl:template match="LEXVAR">
        <span class="colloc">
            <xsl:apply-templates/>
        </span>
            <xsl:text>    </xsl:text>
    </xsl:template>
    <xsl:template match="OBJECT">
            <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="ORTHVAR">
            <span class="text-bold">
                <xsl:apply-templates/>
            </span>
             <xsl:text>    </xsl:text>
    </xsl:template>
    <xsl:template match="ABBR">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="GRAM">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="GEO/text()[.='AmE']">American English</xsl:template>
    <xsl:template match="GEO/text()[.='BrE']">British English</xsl:template>
    <xsl:template match="GEO/text()[.='AusE']">Australian English</xsl:template>
    <xsl:template match="GEO/text()[.='especially AmE']">especially American English</xsl:template>
    <xsl:template match="GEO/text()[.='especially BrE']">especially British English</xsl:template>
    <xsl:template match="GEO/text()[.='especially AusE']">especially Australian English</xsl:template>
    <xsl:template match="GEO">
        <xsl:element name="GEO">
            <span class="text-italic geoTagContent">
                <xsl:apply-templates/>
            </span>
            <xsl:choose>
                <xsl:when test="./following-sibling::*[1][self::LEXVAR]''">
                    <xsl:text>,</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text> </xsl:text>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:element>
    </xsl:template>

    <xsl:template match="REGISTERLAB">
        <span class="text-italic">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="PronCodes">
        <span class="pronunciation">
            /<xsl:apply-templates/>/
        </span>
    </xsl:template>
    <xsl:template match="PRON">
        <span class="pronunciation">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="i">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="STRONG">
        <span class="text-italic">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="AMEVARPRON">
        <span class="pronunciation">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="BrEVariant">
        (<xsl:apply-templates/><span class="text-italic"> British English</span>)
    </xsl:template>
    <xsl:template match="AmEVariant">
        (<xsl:apply-templates/><span class="text-italic"> American English</span>)
    </xsl:template>
    <xsl:template match="POS">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="COLLGLOSS">
        (=<xsl:apply-templates/>)
    </xsl:template>
    <xsl:template match="COLLEXA">
        <br/>• <span class="text-italic"><xsl:apply-templates/></span>
    </xsl:template>
    
    <!-- ErrorBox Formatting tags-->
    <xsl:template match="Error">
            <xsl:apply-templates/><br/>
    </xsl:template>
    <xsl:template match="BADCOLLO">
            <strike><xsl:apply-templates/></strike>
    </xsl:template>
    <xsl:template match="GOODCOLLO">
            <xsl:text>    </xsl:text>
            <span class="text-bold">
                <xsl:apply-templates/>
            </span>
    </xsl:template>
    <xsl:template match="newLine">
        <br/>
    </xsl:template>
</xsl:stylesheet>