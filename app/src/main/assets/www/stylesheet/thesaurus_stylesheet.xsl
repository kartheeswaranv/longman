<?xml-stylesheet type="text/xsl" href="thesaurus_stylesheet.xml"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output omit-xml-declaration="yes"/>
    <xsl:variable name="vEmbDoc">
    </xsl:variable>
    <xsl:template match="Thesaurus">
        <html>
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                <style>
                    body
                    {
                    font-family:"MundoSansPro";
                    background-color:#EDF4FA;
                    color:rgb(56,103,138);
                    margin:10px;
                    font-size:14pt;
                    }
                    .color
                    {
                    color:rgb(56,103,138);
                    }
                    .exp
                    {
                    font-family:MundoSansPro-Bold;
                    color:rgb(174,145,106);
                    font-size:14pt;
                    line-height:1.5;
                    }
                    .section-heading
                    {
                    text-align:left;
                    font-family:MundoSansPro-Bold;
                    color:rgb(56,103,138);
                    text-transform: uppercase;
                    line-height:1.5;
                    font-size:14pt;
                    }
                    .ital_word
                    {
                    font-family : "MundoSansPro-Italic";
                    }
                    .bold
                    {
                    font-family : "MundoSansPro-Bold";
                    }
                    .bold_ital
                    {
                    font-family : "MundoSansPro-BoldItalic";
                    }
                    .refhwd
                    {
                    text-transform: uppercase;
                    color:rgb(56,103,138);
                    }
                    .pronunciation
                    {
                        font-family:CharisSIL;
                        Color:rgb(56,103,138);
                    }
                    .gloss
                    {
                        color:rgb(56,103,138)!important;
                        font-family:"MundoSansPro" !important;
                        font-weight:normal !important;
                    }
                    @font-face {
                    font-family: 'MundoSansPro';
                    src: url('../MundoSansPro.otf')
                    }
                    @font-face {
                    font-family: 'MundoSansPro-Bold';
                    src: url('../MundoSansPro-Bold.otf')
                    }
                    @font-face {
                    font-family: 'MundoSansPro-Medium';
                    src: url('../MundoSansPro-Medium.otf')
                    }
                    @font-face {
                    font-family: 'MundoSansPro-MediumItalic';
                    src: url('../MundoSansPro-MediumItalic.otf')
                    }
                    @font-face {
                    font-family: 'MundoSansPro-BoldItalic';
                    src: url('../MundoSansPro-BoldItalic.otf')
                    }
                    @font-face {
                    font-family: 'MundoSansPro-Italic';
                    src: url('../MundoSansPro-Italic.otf')
                    }
                    @font-face {
                    font-family: 'MundoSansPro-Light';
                    src: url('../MundoSansPro-Light.otf')
                    }
                    @font-face {
                    font-family: 'MundoSansPro-LightItalic';
                    src: url('../MundoSansPro-LightItalic.otf')
                    }
                    @font-face {
                    font-family: 'CharisSILR';
                    src: url('../CharisSILR.ttf')
                    }

                    @font-face {
                    font-family: 'CharisSILI';
                    src: url('../CharisSILI.ttf')
                    }
                    
                </style>
            </head>
            <body>
                <xsl:for-each select="ThesBox">
                    <xsl:apply-templates/>
                </xsl:for-each>
            </body>
            <script src="jquery-2.1.3.min.js" type="text/javascript">//</script>
            <script src="additional_info.js" type="text/javascript">//</script>
        </html>
    </xsl:template>
    
    <!--Formatting tags-->
    <xsl:template match="xsl:template"/>
    
    <!-- HEADING Formatting tags-->
    <xsl:template match="HEADING">
        <span class="bold"><xsl:apply-templates/><br/></span>
    </xsl:template>
    <xsl:template match="EXPR">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="UNDERLINE">
        <xsl:apply-templates/>
    </xsl:template>
    
    <!-- Section Formatting tags-->
    <xsl:template match="Section">
        <xsl:apply-templates/><br/>
    </xsl:template>
    <xsl:template match="SECHEADING">
        <span class="section-heading">
            <xsl:apply-templates/><br/>
        </span>
    </xsl:template>
    <xsl:template match="EXP">
        <span class="exp">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="Exponent">
        <xsl:apply-templates/><br/>
    </xsl:template>
    <xsl:template match="POS">
        <xsl:text>    </xsl:text>
        <span class="color ital_word posContent">
           <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="GRAM">
        <xsl:text>    </xsl:text>
        <span class="gramContent color">
            [<xsl:apply-templates/>]
        </span>
    </xsl:template>
    <xsl:template match="REGISTERLAB">
        <xsl:text>    </xsl:text>
        <span class="color ital_word">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="GEO">
        <xsl:element name="GEO">
        <xsl:text>    </xsl:text>

        <span class="color ital_word geoTagContent">
            <xsl:apply-templates/>
        </span>
        <xsl:if test="./following-sibling::* and (./following-sibling!='Variant')">
        <xsl:text>    </xsl:text>
        </xsl:if>
        </xsl:element>
    </xsl:template>
    <xsl:template match="DEF">
        <span class="color">
            <xsl:text>    </xsl:text>
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    
    <!-- DATE,BOOKFILM,PERSON,FULLFORM,ABBR, and NONDV child tags have no formatting so default color will be showing-->
    
    <xsl:template match="DEFBOLD">
        <span class="color bold">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="GLOSS">
        <span class="gloss">
            (=<xsl:apply-templates/>)
        </span>
    </xsl:template>
    <xsl:template match="PronCodes">
        <span class="pronunciation"> /<xsl:apply-templates/>/</span>
    </xsl:template>
    <xsl:template match="i">
        <span class="color ital_word">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="AMEVARPRON">
        <xsl:text>    </xsl:text>
        <span class="pronunciation">
            $ <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="Variant">
            <xsl:copy>
                <xsl:apply-templates select="@*"/>
                <xsl:choose>
                    <xsl:when test="./LINKWORD[contains(.,'also')]">
                        <span class="neutral">
                            <xsl:text> (</xsl:text>
                        </span>
                    </xsl:when>
                    <xsl:when test="./*[local-name(.)='ABBR'] ">
                        <span class="neutral">
                            <xsl:text> (</xsl:text>
                        </span>
                    </xsl:when>
                    <xsl:when
                        test="./preceding-sibling::*[position()=1 and (local-name(.)='Variant' or local-name(.)='BrEVariant'  or local-name(.)='AmEVariant')]">
                        <span class="neutral">
                            <xsl:text>, </xsl:text>
                        </span>
                    </xsl:when>
                    <xsl:when
                        test="./preceding-sibling::*[local-name(.)='DERIV'] and not (./*[local-name(.)='LINKWORD'])">
                        <span class="neutral">
                            <xsl:text>, </xsl:text>
                        </span>
                    </xsl:when>
                    <xsl:when
                        test="./preceding-sibling::*[local-name(.)='HYPHENATION'] and ./*[local-name(.)='GEO']">
                        <span class="neutral">
                            <xsl:text>, </xsl:text>
                        </span>
                    </xsl:when>
                    <xsl:when
                        test="./preceding-sibling::*[local-name(.)='HYPHENATION'] and not (./*[local-name(.)='LINKWORD'])">
                        <span class="neutral">
                            <xsl:text>, </xsl:text>
                        </span>
                    </xsl:when>
                    <xsl:when
                        test="./preceding-sibling::*[position()=1 and local-name(.)='GEO'] and not (./*[local-name(.)='LINKWORD'])">
                        <span class="neutral">
                            <xsl:text>, </xsl:text>
                        </span>
                    </xsl:when>
                    <xsl:when
                        test="./preceding-sibling::*[position()=1 and local-name(.)='REGISTERLAB'] and not (./*[local-name(.)='LINKWORD'])">
                        <span class="neutral">
                            <xsl:text>, </xsl:text>
                        </span>
                    </xsl:when>
                    <xsl:otherwise>
                        <span class="neutral">
                            <xsl:text> </xsl:text>
                        </span>
                    </xsl:otherwise>
                </xsl:choose>
                <xsl:apply-templates select="node()"/>
                <xsl:choose>
                    <xsl:when test="./*[local-name(.)='ABBR'] ">
                        <span class="neutral">
                            <xsl:text>)</xsl:text>
                        </span>
                    </xsl:when>
                    <xsl:when
                    test="following-sibling::*[position()=1 and (local-name(.)='AmEVariant' or local-name(.)='BrEVariant')]"/>
                    <xsl:when test="./LINKWORD[contains(.,'also')]">
                        <span class="neutral">
                            <xsl:text>)</xsl:text>
                        </span>
                    </xsl:when>
                    <xsl:otherwise/>
                </xsl:choose>
            </xsl:copy>
    </xsl:template>
    <xsl:template match="LINKWORD">
        <span class="color ital_word">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="LEXVAR">
        <span class="exp">
            <xsl:if test="./preceding-sibling::*">
                <xsl:text> </xsl:text>
            </xsl:if>
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="ORTHVAR">
        <xsl:text>    </xsl:text>
        <span class="color bold">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="BrEVariant">
        (<xsl:apply-templates/><span class="ital_word"> British English</span>)
    </xsl:template>
    <xsl:template match="AmEVariant">
        (<xsl:apply-templates/><span class="ital_word"> American English</span>)
    </xsl:template>
    <xsl:template match="THESPROPFORM">
        <span class="exp">
            <br/><xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="THESEXA">
        <xsl:copy>
            <xsl:apply-templates select="@*"/>
            <xsl:if test="@online_only='yes'">
                <xsl:attribute name="h:class">supp</xsl:attribute>
            </xsl:if>
            <xsl:choose>
                <xsl:when test="./preceding-sibling::d:*[position()=1 and local-name(.)='THESEXA']">
                    <!--<span class="neutral"> | </span>-->
                </xsl:when>
                <xsl:otherwise>
                    <span class="neutral">
                        <xsl:text>: </xsl:text>
                    </span>
                </xsl:otherwise>
            </xsl:choose>
            <span class="ital_word color">
                <br/>• <xsl:apply-templates select="node()"/>
            </span>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="COLLOINEXA">
        <span class="color bold_ital">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="Crossref">
        <span class="color">
            <br/>→<xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="CROSSREFTYPE">
        <span class="color">
            <xsl:apply-templates/>
            <xsl:text>    </xsl:text>
        </span>
    </xsl:template>
    <xsl:template match="Crossrefto">
        <span class="color">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="REFHWD">
        <span class="refhwd">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="REFHOMNUM">
        <span class="refhwd">
            <sup><xsl:apply-templates/></sup>
        </span>
    </xsl:template>
    <xsl:template match="COLLORANGE">
        <xsl:text>    </xsl:text>
        <span class="color">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="newLine">
        <br/>
    </xsl:template>
</xsl:stylesheet>


