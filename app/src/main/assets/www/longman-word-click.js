
(function ($) {
	'use strict';

	/**
	 * @param {Event} event
	 * @returns {String}
	 */
	function getWordFromEvent(event) {
		var range, word;
		// Firefox
		if (event.rangeParent && document.createRange) {
			range = document.createRange();
			range.setStart(event.rangeParent, event.rangeOffset);
			range.setEnd(event.rangeParent, event.rangeOffset);
			expandRangeToWord(range);
			word = range.toString();
			var arr = word.split('/');
			if (arr.length>0) {
			word = arr[0];
			};
			word=jQuery.trim(word);
			var param2,param3;
			if (/[^\w\s]/gi.test(word.substring(0,1))) {
				word = word.substring(1,word.length);
			}
			if (/[^\w\s]/gi.test(word.substring(word.length-1,word.length))) 
			{
				word = word.substring(0,word.length-1);
			}
			console.log(word);
			var outputJSON = {};
			var firstSibling = event.target.nextSibling;
			var secondSibling =   (firstSibling==null)?null:event.target.nextSibling.nextSibling;
			if (firstSibling!=null && firstSibling.nodeName == "REFHOMNUM") {
				param2 = firstSibling.innerText.replace(/[^\w\s]/gi,'');
				if (secondSibling!=null&&secondSibling.nodeName =="REFSENSENUM") {
					param3 = secondSibling.innerText.replace(/[^\w\s]/gi, '');
				} else
				{
					param3 = 0;
				}
			}else if (firstSibling!=null&&firstSibling.nodeName =="REFSENSENUM")
			{
			 param3 = firstSibling.innerText.replace(/[^\w\s]/gi,'');
			 param2 = 1;
			}else
			{
				param2=1;
				param3=0;
			}
			outputJSON.text = word;
			outputJSON.tappedTag = event.target;
			outputJSON.homnum = param2;
			outputJSON.sensenum = param3;
			return outputJSON;
		// Webkit
		} else if (document.caretRangeFromPoint) {
			//console.error('caretRangeFromoint');
			range = document.caretRangeFromPoint(event.clientX, event.clientY);
			//console.error(range);
			expandRangeToWord(range);
			word = range.toString();
			var arr = word.split('/');
			if (arr.length>0) {
			word = arr[0];
			};
			word=jQuery.trim(word);
			var param2,param3;
			var i = 0;
            //start
			while (/[^\w\s]/gi.test(word.substring(i,i+1))) {
			word = word.substring(1,word.length);
			}
			i=word.length
			//end
			while (/[^\w\s]/gi.test(word.substring(i-1,i))) {
			word = word.substring(0,i-1);
			i = word.length;
			}
			var outputJSON = {};
			var firstSibling = event.target.nextSibling;
			var secondSibling =   (firstSibling==null)?null:event.target.nextSibling.nextSibling;
			if (firstSibling!=null && firstSibling.nodeName == "REFHOMNUM") {
				param2 = firstSibling.innerText.replace(/[^\w\s]/gi,'');
				if (secondSibling!=null&&secondSibling.nodeName =="REFSENSENUM") {
					param3 = secondSibling.innerText.replace(/[^\w\s]/gi, '');
				} else
				{
					param3 = 0;
				}
			}else if (firstSibling!=null&&firstSibling.nodeName =="REFSENSENUM")
			{
			 param3 = firstSibling.innerText.replace(/[^\w\s]/gi,'');
			 param2 = '';
			}else
			{
				param2='';
				param3='';
			}
			outputJSON.text = word;
			outputJSON.tappedTag = event.target;
			outputJSON.homnum = param2;
			outputJSON.sensenum = param3;
			return outputJSON;	
		// Firefox for events without rangeParent
		} else if (document.caretPositionFromPoint) {
			var caret = document.caretPositionFromPoint(event.clientX, event.clientY);
			range = document.createRange();
			range.setStart(caret.offsetNode, caret.offset);
			range.setEnd(caret.offsetNode, caret.offset);
			expandRangeToWord(range);
			word = range.toString();
			var arr = word.split('/');
			if (arr.length>0) {
			word = arr[0];
			};
			word=jQuery.trim(word);
			var param2,param3;
			if (/[^\w\s]/gi.test(word.substring(0,1))) {
				word = word.substring(1,word.length);
			}
			if (/[^\w\s]/gi.test(word.substring(word.length-1,word.length))) 
			{
				word = word.substring(0,word.length-1);
			}
			console.log(word);
			var outputJSON = {};
			var firstSibling = event.target.nextSibling;
			var secondSibling =   (firstSibling==null)?null:event.target.nextSibling.nextSibling;
			if (firstSibling!=null && firstSibling.nodeName == "REFHOMNUM") {
				param2 = firstSibling.innerText.replace(/[^\w\s]/gi,'');
				if (secondSibling!=null&&secondSibling.nodeName =="REFSENSENUM") {
					param3 = secondSibling.innerText.replace(/[^\w\s]/gi, '');
				} else
				{
					param3 = 0;
				}
			}else if (firstSibling!=null&&firstSibling.nodeName =="REFSENSENUM")
			{
			 param3 = firstSibling.innerText.replace(/[^\w\s]/gi,'');
			 param2 = '';
			}else
			{
				param2='';
				param3='';
			}
			outputJSON.text = word;
			outputJSON.tappedTag = event.target;
			outputJSON.homnum = param2;
			outputJSON.sensenum = param3;
			return outputJSON;
		} else {
			return null;
		}
	}

	/**
	 *
	 * @param {Range} range
	 * @returns {String}
	 */
	function expandRangeToWord(range) {
		while (range.startOffset > 0) {
			if (range.toString().indexOf(' ') === 0 || range.toString().indexOf('/') === 0|| range.toString().indexOf('.') === 0) {
				range.setStart(range.startContainer, range.startOffset + 1);
				break;
			}
			range.setStart(range.startContainer, range.startOffset - 1);
		}
		while (range.endOffset < range.endContainer.length && range.toString().indexOf(' ') == -1) {
			range.setEnd(range.endContainer, range.endOffset + 1);
		}
		return [range.toString().trim(),event.target];
	}

	/**
	 * Attach event(s) to element and call handler when event is triggered
	 * @param {String} event
	 * @param {Function} handler function(Event event, String word)
	 */
	$.fn.getWordByEvent = function(event, handler) {
		// Save last coordinates for events which does not have coordinates info (such as taphold)
		var coordinates = {};
		this.on('mousedown', function(e) {
			coordinates = { x: e.clientX, y: e.clientY }
		});

		this.on(event, function(e) {
			e = e.originalEvent || e;
			if (!e.clientX) {
				e.clientX = coordinates.x;
				e.clientY = coordinates.y;
			}
			handler.call(this, e, getWordFromEvent(e));
		});
	};
})(jQuery);