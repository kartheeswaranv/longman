
//flag to check pagination button is clicked
var isScrolledByButton=false;
$(document).ready(function(){

	//to replace Sb with Somebody and Sth with something
    $('dictionary').html( $('dictionary').html().replace(/(\W|^)(sth)/gi,'$1something') );
    $('dictionary').html( $('dictionary').html().replace(/(\W|^)(sb)/gi,'$1somebody') );
   
    function getSelectedText() {
        if (window.getSelection) {
            return window.getSelection().toString();
        } else if (document.selection) {
            return document.selection.createRange().text;
        }
        return '';
    }

	var $entries = $("entry");

	/*
	* Function to get the tapped word (Word linking)
	*/
	$('sense,tail,phrvbentry').getWordByEvent('click', function(event, word) {
			var closestCrossreftoTag = $(word.tappedTag).closest("crossrefto");
			if($(word.tappedTag).hasClass("example-audio-icon")){
				return;
			}
			if (word.tappedTag.nodeName == "REFHWD") {
				if (closestCrossreftoTag.find('refhwd').text()==hwdContent) {
					$("html, body").animate({ scrollTop: 0 }, "slow");
					return;
				}
			}
			else if (word.text == hwdContent) {
				$("html, body").animate({ scrollTop: 0 }, "slow");
				return;
			}
			// If the Tapped word is Opp - do not redirect to detail Page
			if ($(word.tappedTag).hasClass("oppLabel"))
			    if((word.text == "O") || (word.text == "Op")){
			    {
                    return;
			    }
			}

			// If the Tapped word is SYN - do not redirect to detail Page
			if (($(word.tappedTag).hasClass("synLabel") && (word.text) == "S")){
                return;
            }



			if ($(word.tappedTag).closest("thesref").length>0) {
                var headHeight=$(this).closest('entry').find('.head-cell').height();
                if($('.pagination-button').length>0)
                   headHeight+=40;
				word.text=$(closestCrossreftoTag).find("refhwd").text();
				word.id = $(closestCrossreftoTag).attr('refid');
				word.cellHeight=headHeight;
				word.isThesrefTapped=1;
				if (word.tappedTag.nextSibling!=null && word.tappedTag.nextSibling.nodeName == "REFHOMNUM")
					word.homnum = word.tappedTag.nextSibling.innerHTML;
				else
				word.homnum = "";
				word.tappedTag = 0;
				jsInterface.tappedThesref(JSON.stringify(word));
			}
			else if ($(closestCrossreftoTag).length>0) {
				console.log("crossrefto");
				word.tappedTag = 0;
				word.text=$(closestCrossreftoTag).find("refhwd").clone().children().remove().end().text()
				word.id = $(closestCrossreftoTag).attr('refid');
				jsInterface.tappedCrossrefto(JSON.stringify(word));
			}
			else if (word.text!="") {
			word.tappedTag = 0;
			        if(word.text=="OP"||word.text=="O")
                     {
                        return;
                     }
                     else if(word.text=="S")
                      {
                        return;
                      }
                 else{
				jsInterface.tappedWord(JSON.stringify(word))
			        }
			 }
			else{
				return;
								
			}
	});
	var hwdContent=$entries.find('.headword').text();
	//Adding pagination buttons based on the count of entries
	if($entries.length>1){
		for (var i = 0; i < $entries.length ; i++) {
			//setting the id of each entry as attribute for easy scrolling
			$(".pagination-buttons-container").append("<span class='pagination-button' btn-id='"+$($entries[i]).attr("id")+"'>"+(i+1)+"</span>");
		}
	}
	else{
		$(".pagination-header").css("display", "none");
		$("dictionary").css("padding-top", "0px");
	}
	
	$(".pagination-button").bind("click",paginationBtnClick);

	$.each($entries,function(i,entry){

			
		//to check whether HYPHENATION entry is present or not
        	if(!($(entry).find("hyphenation").length)){
                hwdContent=$(entry).find(".headword").text();
                $(entry).find("hwd").after("<hyphenation><span class='hyphenation'>"+hwdContent+"</span></hyphenation>");
        	}

		//to color the Headword and its homnum in red color, if the entry has LEVEL information
		if($(entry).find("level").length){
			$(entry).find(".hyphenation").addClass("frequent");
			$(entry).find("homnum").addClass("frequent");
		}
		else{
		    //there is no level tag, so hide the frequency container.
		    $(entry).find(".frequency-container").addClass("hidden");
		}
		//hide the phone frequency container initially
		$(entry).find(".frequency-container-phone").css("display","none");
		$(entry).find(".frequency-container-hr").css("display","none");
		    
		//to enclose specific tags into a separate tag so that they appear in a new line.
		$levelContainer = $(entry).find(".level-container");
		if($levelContainer)
		    $(entry).find(".level-container").append($(entry).find("level"));
		//to include a level info in frequency chart - phone
		var levelContainerPhone = $(entry).find(".frequency-container-phone");
		var levelInfo=$(entry).find("level");
		if($(levelContainerPhone).length>0 && $(levelInfo).length>0){
			$(levelInfo).clone().insertAfter($(entry).find('.freq-level-prefix'));
		}
		//$(entry).find(".head-cell").first().find("level,ac").wrap("<div class='second-line-content'></div>");
        //To change the XML Position of LEVEL -  above the frequency graph
        $levelTag = $(entry).find("level");
        $(entry).find(".frequency-container").prepend($levelTag);

        $(entry).find(".frequency-container").css('display','inline-block');
		//hide or show the AWL icon if AC tag is present
		$acTag = $(entry).find("ac");
		if($acTag.length <= 0){
			//AC tag is not present. Hide the icon. 
			$(entry).find(".awl-icon").addClass("hidden");
		}
		else{
			//Change the order of AC tag, to the end of the second line content. 
			//Note: this is only for tablet. In Phone, since we are not showing any content inside AC tag, the order of AC
			//in phone doesn't matter.
			$(entry).find(".second-line-content").append($acTag);
		}


		// To show the frequency level (S1,W1 etc) in frequency chart
		//Showing/hiding frequency chart based freq tag
		$freqTags = $(entry).find("FREQ");
        if($freqTags.length){
            $.each($freqTags,function(i,freqTag){
                var freqValue=$(freqTag).find(".freq-level").text();
                if(freqValue){
                    $(entry).find('.'+freqValue).addClass("selected");
                }
            });
        }

        //Checking for the expandableinformation tag, if so show the chevoran 
        $.each($(entry).find("sense,subsense"),function(index,sense){

    		//Wrap content except sensenumber, expandableinfo and subsense inside a div, so that we 
    		//can indent the content alone.
        	$(sense).children().not(".sensenum,expandableinformation,subsense").wrapAll("<div class='sense-content'></div>");
        	$mainTable = null;
        	if($(sense).prop("tagName") == "SENSE"){
        		//selected tag is SENSE
        		//Wrap sense number and content inside a div.table so that we can create a three column layout
        		$(sense).children(".sensenum,.sense-content").wrapAll("<div class='sense-main-table'></div>");
        		//Add a third colum, with chevron span to the sense-main-table
        		$mainTable = $(sense).find(".sense-main-table");
        		$mainTable.append("<div class='chevron-button-container'><span class='lg-uniF078 chevron-button'></span></div>");

        	}else if($(sense).prop("tagName")== "SUBSENSE")
        	{
        		//selected tag is Subsense
        		//add table class to the subsense tag
        		$(sense).addClass("sense-main-table");
        		//add a third colum, with chevron span to the table. 
        		$(sense).append("<div class='chevron-button-container'><span class='lg-uniF078 chevron-button'></span></div>");
        		$mainTable = $(sense);
        	}

    		if ($mainTable.find('expandableinformation').length>0){
				//console.log("present"+sense);
    		}else
    		{
    			//console.log("Not present");
    			$mainTable.find('.chevron-button').hide();
    		}	
        	
        });
		
		//To show the status of button based of word is favorite or not
		$favButton = $(entry).find(".favorite-button");
		var id=$(entry).attr("id");
		if(jsInterface && jsInterface.isWordFavorite(id)){
			//giving favorite state
            $favButton.find("i").removeClass("lg-uniF20e");
            $favButton.find("i").addClass("lg-uniF212");
		}
		else{
			//word is not favorite,
			// Change to add bookmarked icon
			 //removing favorite state
             $favButton.find("i").removeClass("lg-uniF212");
             $favButton.find("i").addClass("lg-uniF20e");
		}
		//To show the state of Additional Info button
		$additionalInfoButton=$(entry).find(".info-button");
		var id=$(entry).attr("id");
		if(jsInterface && !jsInterface.isAdditionalInfoAvailable(id)){

            $additionalInfoButton.addClass('right-button disabled');
            $additionalInfoButton.css("box-shadow","none");
            $additionalInfoButton.css("color","lightgray");
            $additionalInfoButton.removeClass("blue-button"); 
            //btn-floating btn waves-effect waves-light blue-button info-button   
		}
		// TO show the state of US Audio button - if audio not available
        $usButto=$(entry).find(".us-button");
        var id =$(entry).attr("id");
        if(jsInterface && jsInterface.isExtractedFilesAvailable()){
            if(jsInterface && !jsInterface.isUSSoundAvailable(id)){
                $usButto.addClass('button-disabled disabled');
                $usButto.disabled=true;
                $usButto.css("pointer-events","none");
            }else{
                $usButto.removeClass('button-disabled');
                $usButto.enabled=true;
            }
        }
        else
        {
            $usButto.addClass('button-disabled disabled');
            $usButto.disabled=true;
            $usButto.css("pointer-events","none");
        }

        // TO show the state of UK Audio Button if uk audio not available
        $ukButto=$(entry).find(".uk-button");
        var id = $(entry).attr("id");
        if(jsInterface && jsInterface.isExtractedFilesAvailable()){
            if(jsInterface && !jsInterface.isUKSoundAvailable(id)){
                 $ukButto.addClass('button-disabled disabled');
                 $ukButto.disabled=true;
                 $ukButto.css("pointer-events","none");
            }else{
                $ukButto.removeClass('button-disabled');
                $ukButto.enabled=true;
            }
        }
        else
        {
            $ukButto.addClass('button-disabled disabled');
            $ukButto.disabled=true;
            $ukButto.css("pointer-events","none");
        }

		//function to handle expand and collapse for sense
        $.each($(entry).find("sense,subsense"),function(index,sense)
        {
        	var senseMainTable; 
        	if($(sense).prop("tagName") == "SENSE"){
        		senseMainTable = $(sense).children(".sense-main-table")[0];
        	}
        	else{
        		senseMainTable = sense;
        	}
			var expandable = $(senseMainTable).find('.expandable-information');
    		       	

        	$(senseMainTable).find('.chevron-button').bind('click',function(){
        		var that = this;
        		$(expandable).toggle(100,function(){
		            if($(that).hasClass("element-rotate-up")){
	        			$(that).removeClass("element-rotate-up").addClass("element-rotate-down");
	        		}
	        		else{
	        			$(that).removeClass("element-rotate-down").addClass("element-rotate-up");
	        		}
				}); 
        		
        		
        	});
        });
	});
  $expandPropFormperp = $(".expandHWD").each(function(i,propForm){
  	var closestPhrParent  = $(propForm).closest("phrvbentry");
  	var headword;
  	if(closestPhrParent.length >0 ){
  		headWord = $($(closestPhrParent).get(0)).find("phrvbhwd").text();
  	}
  	else {
  		headWord =  hwdContent;
  	}
  	$(propForm).text(headWord+" "+$(propForm).text());
  });
  var phrasalBool=true;
var phrentriesArray=[];
  // Divider between each SENSE
  $("SENSE").before("<HR/>");
  //Divider before each TAIL tag
  $("TAIL").before("<HR/>");
  //Divider before each phrvbentry tag
  $("phrvbentry").before("<HR/>");
var value ;
var phrasalverbhwd= jsInterface.PhrasalVerb();
  var head;
  var objectTag;
  // finding the current phrasalverb entry
  $phrVb=$('entry').find("phrvbentry");
  $.each($phrVb,function(index,phrVbEntry){
        // finding phrasalverb headword
        var phrhead=$(this).closest('phrvbentry');
        // getting the text of current pharasal verb
        head=phrhead.find('phrvbhwd').text();
        // find the object tag -  something/somebody etc.........
        objectTag = phrhead.find('object').text();
        // If object tag is present, then remove it temproarily to check the search word
        if(objectTag!=null)
        {
            head=  head.replace(objectTag,'');
            // this space is maily removing due to double space in OBJECT Tag
            head = head.replace('  ',' ');
        }

         var formatString = ' '+phrasalverbhwd

        // if user searched phrasal verb and phrasal verb entry is matched, then scroll to that entry
        if(head == phrasalverbhwd || head == formatString)
        {
            phrasalBool=false;
            value = $(phrhead);
            // scroll to specific phrasal verb entry
            var scrollOffset= typeof value.offset().top == 'undefined' ? value.offsetTop : value.offset().top
                $('html,body').animate({
                    scrollTop: scrollOffset - 40
                },500);
        }
  	$(phrVbEntry).find('hr').remove();
  });

  //Remove GramRef tags and its contents
  $("GramRef").remove();

  //To remove the separator between spoken pharses title and following sense
  $spokenSect= $('entry').find("spokensect");
  if($spokenSect.find('.spokensect').next().is('hr')){
  	$spokenSect.find('.spokensect').next().remove();
  }

    //Expanding POS to their full forms.
            	$.each($("POS"),function(i,posTag){
            		var posValue = $(posTag).find(".pos-text").text();
            		var transformed=posValue.replace(/\w+/g, function(match,contents,offset,s){

            		switch(match){
            			case 'n':
            				return "noun";
            			case 'v':
            				return "verb";
            			case 'adj':
            				return "adjective";
            			case 'pron':
            				return "pronoun";
            			case 'adv':
            				return "adverb";
            			case 'prep':
            				return "preposition";
            			case 'phr':
                            return  "phrasal";
            			case 'interj':
            				return "interjection";
            			default:
            				return match;
            		}
            		});
            		$(posTag).find(".pos-text").text(transformed);
            	});


	//Gram Tag Expansion
	$.each($("GRAM"),function(i,gramTag){
			var gramValue = $(gramTag).find(".gramContent").text();

			if(!gramValue.indexOf(" ")>=0 && (gramValue.indexOf(","))){
				gramValue=gramValue.replace(",",", ");
			}
			var transformed=gramValue.replace(/\w+/g, function(match,contents,offset,s){

				switch(match){
				case 'C':
					return "countable";
				case 'U':
					return "uncountable";
				case 'I':
					return "intransitive";
				case 'T':
					return "transitive";
				case 'P':
					return "plural";
				case 'prep':
					return "preposition";
				case 'adv':
					return "adverb";
				case 'adj':
					return "adjective";
				default:
			    	return match;
			    }
		    });

			$(gramTag).find(".gramContent").text(transformed);
		});

	//GEO Tag Expansion
	$.each($("GEO"),function(i,geoTag){
			var geoValue = $(geoTag).find(".geoTagContent").text();
			var transformedGEO=geoValue.replace(/\w+/g, function(match,contents,offset,s){
				switch(match){
				case 'especially':
					return "especially";
				case 'BrE':
					return "British English";
				case 'AmE':
					return "American English";
				case 'AusE':
					return "Australian English";
				default:
				    break; 
				}
			});
			$(geoTag).find(".geoTagContent").text(transformedGEO);
		});

		//to handle click event of favorite button
		$(".favorite-button").click(function(){

			var info_Object={};
			//fetching the entry details
			$entry=$(this).closest("entry");
			var id=$entry.attr("id");
			var hwd=$entry.find(".headword").text();
			var homnum=$entry.find("homnum").text();
			var frequency;
			if($entry.find("level").length>0)
				frequency=1;
			else
				frequency=0;
			info_Object["id"]=id;
			info_Object["hwd"]=hwd;
			info_Object["homnum"]=homnum;
			info_Object["frequent"]=frequency;
			//checking if word is already in favorite
			//if word is already in favorite : deleting from favorites
			//if else : inserting into favorites
			var isFavorite=jsInterface == undefined ? false : jsInterface.toggleFavorite(JSON.stringify(info_Object));
			if (isFavorite) {
				//giving favorite state
				$(this).find("i").removeClass("lg-uniF20e");
				$(this).find("i").addClass("lg-uniF212");
			}
			else{
				//removing favorite state
				$(this).find("i").removeClass("lg-uniF212");
                $(this).find("i").addClass("lg-uniF20e");
			}
		});


		//US Button click event
        $(".us-button").click(function(){
			//Check for disabled class
			if (!$(this).hasClass("btn-disabled")) {
				$(this).find("i").addClass("button-selected");
				//Add class disabled to UK US Example audio icon Button
				$(".us-button").addClass("btn-disabled");
				$(".uk-button").addClass("btn-disabled");
				$(".example-audio-icon").addClass("btn-disabled");
				var info_Object={};
				//Fetching the entry details
				$entry=$(this).closest("entry");
				var id=$entry.attr("id");
				var buttonPressed="us";
				//Insert values in the object
				info_Object["id"]=id;
				info_Object["buttonChoose"]=buttonPressed;
				//Call to us uk sound with info object
				if(jsInterface){
				    jsInterface.checkUsUkExampleSoundButton(JSON.stringify(info_Object));
				}
			}
						    			
		});

		//UK Button click event
        		$(".uk-button").click(function(){
        			//Check for disabled class
        			if (!$(this).hasClass("btn-disabled")) {
        				$(this).find("i").addClass("button-selected");
        				//Add class disabled to UK US Example audio icon Button
        				$(".us-button").addClass("btn-disabled");
        				$(".uk-button").addClass("btn-disabled");
        				$(".example-audio-icon").addClass("btn-disabled");
        				var info_Object={};
        				//Fetching the entry details
        				$entry=$(this).closest("entry");
        				var id=$entry.attr("id");
        				var buttonPressed="uk";
        				//Insert values in the object
        				info_Object["id"]=id;
        				info_Object["buttonChoose"]=buttonPressed;
        				//Call to us uk sound with info object
        				if(jsInterface){
        				    jsInterface.checkUsUkExampleSoundButton(JSON.stringify(info_Object));

        				}
        			}
        		});

        //Example Audio Button click event
    		$(".example-audio-icon").click(function(){
    			//Check for disabled class
    			if (!$(this).hasClass("btn-disabled")) {
    				$(this).addClass("button-selected");
    				//Add class disabled to UK US Example audio icon Button
                    $(".us-button").addClass("btn-disabled");
                   	$(".uk-button").addClass("btn-disabled");
                   	$(".example-audio-icon").addClass("btn-disabled");
    				var info_Object={};
    				//Fetching the example details
    				var id = $(this).closest('example')[0].getAttribute('id');
    				var buttonPressed ="example";
    				//Insert values in the object
    				info_Object["id"]=id;
    				info_Object["buttonChoose"]= buttonPressed;    			           	
    				//Call to us uk sound with info object
                    if(jsInterface){
                       jsInterface.exSound(JSON.stringify(info_Object));

                    }}
    		});

    		//Info Button click event
    		$(".info-button").click(function(){
    			//Info object
    			var info_Object={};
				//Fetching the entry details
				$entry=$(this).closest("entry");
				var id=$entry.attr("id");
				var buttonPressed="info";
				var headHeight=$($(this).closest('.head-cell')).height();
				if($('.pagination-button').length>0)
					headHeight+=40;
				//Insert values in the object
				info_Object["id"]=id;
				info_Object["buttonChoose"]=buttonPressed;
				info_Object["headCellHeight"]=headHeight;
				if (jsInterface)
					jsInterface.infoButtonTapped(JSON.stringify(info_Object));
                
    		});
    		//copy-to-clipboard Button click event
       		 $(".copy-button").click(function(){
        		//Initialise the info object
				var info_Object={};
				//Fetching the entry details
				$entry=$(this).closest("entry");
				var id=$entry.attr("id");
				var hwd=$entry.find(".headword").text();
				var buttonPressed="copy-to-clipboard-button";
				//Insert values in the object
				info_Object["id"]=id;
				info_Object["buttonChoose"]=buttonPressed;
				info_Object["hwd"]=hwd;
				//Call to callForCopyToClipboard with info object
				if(jsInterface)
					jsInterface.callForCopyToClipboard(JSON.stringify(info_Object));		    			
			});

       		$("img").click(function(){
       			var id=$(this).attr("id");
       			var path=$(this).attr("src")
       			var name= path.replace("file:///android_asset/thumbnail/","");
       			var detail={};
       			detail.id=id;
       			detail.fileName=name;
       			jsInterface.openImage(JSON.stringify(detail));
       		}); 
    		//to show/hide frequency chart in phone
    		$(".phone .head-cell .level").click(function(){
    			var frequen=$(this).closest("entry").find(".frequency-container-phone")[0];
    			var supperator=$(this).closest('entry').find('.frequency-container-hr');
    			if($(frequen).length>0){

    				$(frequen).toggle(100);
    			
    				$(supperator).toggle(50);
    			}
				if(jsInterface)
                	jsInterface.callForAnalyticsTracking();
    		});

		$.each($(".local"),function(index,localItem){
              var resKey = $(localItem).text();
              if(jsInterface)
              	$(localItem).text(jsInterface.getLocalizedString(resKey));

        });


	//withinviewport.defaults["top"]=40;
	$(window).bind("scrollstart", function() {
		console.log('scroll-started');
	});

//scroll till end
	$(window).bind("scrollstop", function() {
	    if(!isScrolledByButton){
		    //clear all active buttons
		    $('.pagination-button').removeClass('selected');
		    //add active class to timeline
		    var itemsToCheckForVisibility = $(".head-cell, sense, tail");
			var firstVisibleItem = null;
			for(var i=0;i<itemsToCheckForVisibility.length;i++){
				if($(itemsToCheckForVisibility[i]).isOnScreen(1,1,40)){
					firstVisibleItem = itemsToCheckForVisibility[i];

					break;
				}
			}
			if ((window.innerHeight + window.scrollY) >= document.body.scrollHeight) {
                    // you're at the bottom of the page
                      console.log("Bottom of page",$('.pagination-button').length);
                      $('.pagination-button').removeClass('selected');
                      		    //add active class to timeline
                      		    var itemsToCheckForVisibility = $(".head-cell, sense, tail");
                                		    navigateToHomnumOrSense($('.pagination-button').length,null,null);
                     }
                     else{

		    $entryOnTop = $(firstVisibleItem).closest("entry");
		    var entriD=$entryOnTop.attr("id");
		    console.log('pagination till end ',entriD);
			$(".pagination-button[btn-id='"+entriD+"']").addClass("selected");
}
	    }
	 });
	populateImage(imageInfo.ImageDetails);
	$(window).bind("touchstart",function(){
		isScrolledByButton = false;
	});
	setTimeout(function(){
		$('body').css("visibility","visible");
	},0);
	setTimeout(function(){
	if(phrasalBool)
		navigateToHomnumOrSense(navigationOpts.navhomnum,navigationOpts.navsensenum,navigationOpts.navid);
	},500);

	 if(thesNavigation.IsthesTapped=="ThesTapped"){
	 		getHeadcellHeight(thesNavigation.thesHwdId);
	 }
});

/*used to change the color of favorites button
idArray - Array which is obtained from FavoritesFragment.java, contains the set of id needs to be deleted
*/
var validateFavoritesButtonState=function(idArray){
	console.log(idArray);
	for(var i=0;i<idArray.length;i++){
	    //check for each entry id with each array position
		var entry = $("entry[id='"+idArray[i]+"']");
		console.log(entry);
		//To show the status of button based of word is favorite or not
		$favButton = $(entry).find(".favorite-button");
        //word is unfavored,
        //removing favorite state
        $favButton.find("i").removeClass("lg-uniF212");
        $favButton.find("i").addClass("lg-uniF20e");
	}
}

// Change the state of the sound  button
var restoreSoundButtonState=function(jsonObject){	
	/* If it is us, uk button will be enabled and the selected state will be cancelled */
	if (jsonObject.btn=="us") {
	 	//Getting the id and btn which is selected from the jsonobject
		 var id=$("entry[id='" +jsonObject.id +"']");
	 	//Find the us button for the respective entry	
		 $usButton = $(id).find(".us-button");
		 //Remove button-selected and disabled class for us button
	 	 $usButton.find("i").removeClass("button-selected");
		 //Remove class from us, uk and example audio icon to enable to play the sound
		 $(".us-button").removeClass("btn-disabled");
		 $(".uk-button").removeClass("btn-disabled");
		 $(".example-audio-icon").removeClass("btn-disabled");
	}
	/* If it is uk, us button will be enabled and the selected state will be cancelled */
	else if (jsonObject.btn=="uk")
	{
	 	//Getting the id and btn which is selected from the jsonobject
	 	var id=$("entry[id='" +jsonObject.id +"']");
	 	//Find the uk button for the respective entry	
     	$ukButton = $(id).find(".uk-button");
    	 //Remove button-selected and disabled class for uk button
	 	$ukButton.find("i").removeClass("button-selected");
	 	//Remove class from us, uk and example audio icon to enable to play the sound
	  	 $(".us-button").removeClass("btn-disabled");
		 $(".uk-button").removeClass("btn-disabled");
		 $(".example-audio-icon").removeClass("btn-disabled");
	}
	else if (jsonObject.btn=="example") {
		 $exampleId=$("example[id='"+jsonObject.id+"']");
		//Remove button-selected and disabled class for example audio icon button
		 //$exampleId.find("i").removeClass("button-selected");
		 $(".example-audio-icon").removeClass("button-selected");
		//Remove class from us, uk and example audio icon to enable to play the sound
		 $(".us-button").removeClass("btn-disabled");
		 $(".uk-button").removeClass("btn-disabled");
		 $(".example-audio-icon").removeClass("btn-disabled");
	}
	else
	{
		 $(".us-button").removeClass("button-selected");
		 $f(".uk-button").removeClass("button-selected");
		 $(".example-audio-icon").removeClass("button-selected");
		 $(".us-button").removeClass("btn-disabled");
		 $(".uk-button").removeClass("btn-disabled");
		 $(".example-audio-icon").removeClass("btn-disabled");
	}
	 	
}

/**
* This function is used to navigate to specific homenum with sense number. or to a particular element by using ID
* homnum is the entry number
* sensenum is the particular sense number
* scroll_ID  anEntry id to which the pge should be scrolled.
* Given high priority to scroll_ID, so that it will not consider homnum and sensenum
* if homnum is null("") it is considered as 0
* if you dont want any argument, assign it as null or ""
*/
var navigateToHomnumOrSense = function(homnum,sensenum,scroll_ID){

	//scrolling to a tag with specific id is provided.so directly scrolling to that tag with ID
	//now currently did specifically for 'entry' tag..if you want to scroll to other tag,remove the entry

	//removing selected class of buttons
	$('.pagination-button').removeClass("selected");
	if(!(scroll_ID =="" || scroll_ID =="##" || scroll_ID == null || typeof scroll_ID == 'undefined')){
		//found that some webkit use offset().top and other use offsetTop .
		var scrolloffset= typeof $($("entry[id='"+scroll_ID+"']")[0]).offset().top == 'undefined'? $($("entry[id='"+scroll_ID+"']")[0]).offsetTop : $($("entry[id='"+scroll_ID+"']")[0]).offset().top;
		$('body').animate({
			scrollTop: scrolloffset - 40
		},500);
		$(".pagination-button[btn-id='"+scroll_ID+"']").addClass("selected");
	}
	//no scrolling id is provided, so scrolling to specific sense number with homenum
	else{
		//To avoid removing selected state of pagination button because of scrolling function 
		isScrolledByButton=true;
		//checking if homnum is null...then setting as 0..else..decrement the value
	   if(homnum == "" || homnum == null || typeof homnum == 'undefined' || homnum == 0) 
	   	homnum = 0;
	   else
	   	homnum--;
	   //checking if sensenum is null...set -1 or decrement the value
	   if(sensenum == null || typeof sensenum == 'undefined' || sensenum == 0)
	   	sensenum=-1;
	   else
	   	sensenum--;
	   //getting the entry to which scrolled
	   $entryToScroll=$($('entry')[homnum]);
	   //getting the specific pagination button
	   var btniD=$entryToScroll.attr("id");
	   //getting sense item if it is not null to be scrolled
	   if(sensenum>=0 && $entryToScroll.length>0){
	   	$entryToScroll=$($entryToScroll.find('sense')[sensenum]); //$entryToScroll.getElementsByTagName('sense')[sensenum];
	   }
	   //scrolling to the specific item
	   if($entryToScroll.length>0){
	   	var scrollOffset= typeof $entryToScroll.offset().top == 'undefined' ? $entryToScroll.offsetTop : $entryToScroll.offset().top
			$('html,body').animate({
				scrollTop: scrollOffset - 40

			},500);

console.log('button id',btniD);
			//giving selected state for pagination button once scrolled
			$(".pagination-button[btn-id='"+btniD+"']").addClass("selected");
			
	    }
	}
}

// Change the state of the sound  button
var addDivTagForAdditionalInfo=function(jsonObject){	
	/* If it is us, uk button will be enabled and the selected state will be cancelled */
	if (jsonObject.btn=="info") {
	 	//Getting the id and btn which is selected from the jsonobject
		 var id=$("entry[id='" +jsonObject.id +"']");
	 	//Find the us button for the respective entry	
		 // $usButton = $(id).find(".newline");
		 //Remove button-selected and disabled class for us button
	 	 // $usButton.addClass("overlay-box");
		
	} 	
}	
//To Add dummy div to end of html body to make it to scroll to top
var addDummyDivAndScrollToTop = function(scroll_ID){
	$('dictionary').after("<div class='dummy-cell'></div>");
	var Idd=scroll_ID;
	navigateToHomnumOrSense(null,null,scroll_ID);
	$('body').bind('touchmove', function(e){e.preventDefault()});
	$(".pagination-button").unbind("click",paginationBtnClick);
}

//To remove added dummy divs
var removeDummyDiv = function(scroll_ID){
	var $dummyCell=$('.dummy-cell');
	if($dummyCell.length>0){
		$dummyCell.remove();
	}
	$('body').unbind('touchmove');
	$(".pagination-button").bind("click",paginationBtnClick);
}

//to navigate to the clicked entry using the pagination buttons
	var paginationBtnClick=function(){
		//setting the flag for denoting button scroll
		isScrolledByButton=true;
		//getting the ID of entry
		var entryID = $(this).attr("btn-id");
		//Removing the selected state of pagination buttons
		var $paginationbtns=$(".pagination-button");
		$.each($paginationbtns,function(i,button){
			$(button).removeClass("selected");
		});
		//setting selected state for current button
		$(this).addClass("selected");
		//scrolling to the clicked entry
		navigateToHomnumOrSense(null,null,entryID);
	};
	/* used to get the Headcell height - redirect for Thesaurus page
	*/
	var getHeadcellHeight=function(hwdId){
		console.log(hwdId);
		var entry =$("entry[id='"+hwdId+"']");
		console.log(entry);
		var heightOfHeadCell=$(entry).find(".head-cell").height();
		if($('.pagination-button').length>0)
						   heightOfHeadCell+=40;
		console.log(heightOfHeadCell);
		var x = {};
		x.headHeight = heightOfHeadCell;
		jsInterface.setHeadcellHeight(JSON.stringify(x));
	}
	var getSelectedString=function(isCopy){
		var text = "";
            if (window.getSelection) {
                text = window.getSelection().toString();
            }
            else if (document.selection && document.selection.type != "Control") {
                text = document.selection.createRange().text;
            }
            console.log(text);
            var x = {};
            x.selectedString=text;
            x.actionToPerform = isCopy;
            jsInterface.getSelectedWord(JSON.stringify(x));
	};


var populateImage=function(imageInfo){

	data = $.parseJSON(imageInfo);
	$.each(data, function(i, item) {

		var curspndEntry= $("entry[id='"+item.id+"']");
		if(curspndEntry.length>0){

	    	var imagetag=curspndEntry.find('img');
	    	imagetag.attr("src",item.fileName);
	    	imagetag.attr("id",item.id);
	    	var diTag=imagetag.closest('.img-phone');
	    	diTag.removeAttr("style");
	    	diTag.addClass("imgsDiv");
    	}
	});
}

