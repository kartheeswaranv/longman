package com.mobifusion.android.ldoce5.Activity;

import android.content.res.AssetManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.test.ActivityInstrumentationTestCase2;

import com.mobifusion.android.ldoce5.Activity.DbConnection;
import com.mobifusion.android.ldoce5.Activity.SlideMenuSearchAndIndex;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by ramkumar on 23/06/15.
 */
public class WordOfTheDayTest extends ActivityInstrumentationTestCase2<SlideMenuSearchAndIndex> {
    //Create object for SlideMenuSearchAndIndex
    SlideMenuSearchAndIndex slideMenuObject;
    public WordOfTheDayTest(){
        super(SlideMenuSearchAndIndex.class);
    }

    @Override
    public void setUp() throws Exception {
        super.setUp();
        slideMenuObject = getActivity();
        assertNotNull("SlideMenuActivity is Null", slideMenuObject);
    }

    @Override
    public void tearDown() throws Exception {
        super.tearDown();
    }

    public void testFilterWords() throws Exception {
        SQLiteDatabase db;
        String fetchHwdQuery;
        String unWantedWordsText=null;
        DbConnection dbPath= new DbConnection();
        db=dbPath.getDbConnection();
        AssetManager assetManager=slideMenuObject.getAssets();
        InputStream inputStream=null;
        try{
            //Filter Hwd which is not to be shown in Word of the Day
            inputStream = assetManager.open("WordsToAvoid.txt");
            unWantedWordsText = loadTextFile(inputStream);

        }catch (IOException e){
            //should not find the WordsToAvoid.txt File
            System.out.println("Problem in opening WordsToAvoid.txt File");
        }finally {
            if(inputStream!=null && db!=null){
                try {
                    fetchHwdQuery = String.format("SELECT hwd from core where id in (select id from lookup where  useforWOD !='1' and hwd not in (?) ORDER BY RANDOM())");
                    Cursor cur = db.rawQuery(fetchHwdQuery, new String[]{unWantedWordsText});
                    int totalWords = cur.getCount();
                    String[] filteredHwd = new String[totalWords];
                    int i=0;
                    while (cur.moveToNext()) {
                        String hwd=cur.getString(0);
                        filteredHwd[i] = hwd;
                        i++;
                    }
                    //get the List of Words in Array that has <REGISTERLAB>taboo or not polite</REGISTERLAB>
                    String[] unWantedWords=new String[200];
                    unWantedWords=getUnWantedWords();
                    assertNotSame("Filtering is not Working properly",filteredHwd,unWantedWords);
                }catch (Exception e){
                    System.out.println("Problem in Fetching Words from Db");
                }
            }
        }
        //Closing the db
        db.close();
    }
    public String loadTextFile(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        byte[] bytes = new byte[4096];
        int len = 0;
        while ((len = inputStream.read(bytes)) > 0)
            byteStream.write(bytes, 0, len);
        return new String(byteStream.toByteArray(), "UTF8");
    }

    private String[] getUnWantedWords(){
        SQLiteDatabase db;
        String[] unWantedWords = null;
        DbConnection dbPath= new DbConnection();
        db=dbPath.getDbConnection();
        String fetchUnwantdsQuery;
        if (db != null) {
            fetchUnwantdsQuery="select hwd from lookup where  useforWOD ='1'";
            Cursor cur = db.rawQuery(fetchUnwantdsQuery, new String[]{});
            int i=0;
            while(cur.moveToNext()){
                String hwd=cur.getString(0);
                unWantedWords = new String[cur.getCount()];
                unWantedWords[i]=hwd;
                i++;
            }
            if(unWantedWords.length>0){
                return unWantedWords;
            }
        }
        db.close();
        return unWantedWords;
    }
}
