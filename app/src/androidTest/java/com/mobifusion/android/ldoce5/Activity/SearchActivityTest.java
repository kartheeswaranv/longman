package com.mobifusion.android.ldoce5.Activity;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.test.ActivityInstrumentationTestCase2;

import com.mobifusion.android.ldoce5.Fragment.SearchFragment;
import com.mobifusion.android.ldoce5.R;
import com.mobifusion.android.ldoce5.Activity.DbConnection;
import com.mobifusion.android.ldoce5.Activity.SlideMenuSearchAndIndex;
import com.mobifusion.android.ldoce5.model.SearchRowItem;

import java.util.LinkedHashSet;

/**
 * Created by sureshchandrababu on 30/03/15.
 */
public class SearchActivityTest extends ActivityInstrumentationTestCase2<SlideMenuSearchAndIndex>{
    SlideMenuSearchAndIndex searchActivityObject;
    SearchFragment searchFragment;
    DbConnection dbPath;
    SQLiteDatabase db;
    public SearchActivityTest()
    {
        super(SlideMenuSearchAndIndex.class);
    }

    @Override
    public void setUp() throws Exception {
        super.setUp();
        searchActivityObject = getActivity();
        searchFragment = (SearchFragment)searchActivityObject.getSupportFragmentManager().findFragmentById(R.id.searchFragement);
        dbPath= new DbConnection();
        db=dbPath.getDbConnection();
    }

    public void testToPassSetup() throws Exception {

        assertNotNull(searchFragment);
    }

    public void testSearchResultFetching() throws Exception {
        LinkedHashSet<SearchRowItem> result = searchFragment.searchRetrievalResults("welcome");
        assertNotNull(result);
    }

    public void testSearchResultCount() throws Exception {
        LinkedHashSet<SearchRowItem> result = searchFragment.searchRetrievalResults("welcome");
        assertEquals(2, result.size());
    }

    public void testPartialMatchWord() throws Exception {
        LinkedHashSet<SearchRowItem> result = searchFragment.searchRetrievalResults("wel");
        Boolean isWordPresent = false;
        for ( SearchRowItem item : result )
        {
            isWordPresent = (item.getHwd().equals("welcome"))?true:false;
            if (isWordPresent)
                return;
        }
        assertEquals(Boolean.TRUE,isWordPresent);
    }

    public void testSearchInflection() throws Exception {
        LinkedHashSet<SearchRowItem> result = searchFragment.searchRetrievalResults("played");
        Boolean isWordPresent = false;
        for ( SearchRowItem item : result )
        {
            isWordPresent = (item.getHwd().equals("play"))?true:false;
            if (isWordPresent)
                return;
        }
        assertEquals(Boolean.TRUE,isWordPresent);
    }

    public void testExactMatch() throws Exception {
        LinkedHashSet<SearchRowItem> result = searchFragment.searchRetrievalResults("exact");
        int position = 0;
        Boolean isWordPresent = false;
        for (Object item : result) {
            isWordPresent = (((SearchRowItem) item).getHwd().equals("exact")) ? true : false;
            if (isWordPresent)
                return;
            position++;
        }
        assertEquals(1, position);
    }
    public void testNoResult() throws Exception {
        LinkedHashSet<SearchRowItem> result = searchFragment.searchRetrievalResults("sdvshdkvgsdvgsdv");
        String headWord = null;
        for(Object item : result)
        {
            headWord = ((SearchRowItem)item).getHwd();
        }
        assertEquals("No Matches Found",headWord);
    }

    //Testing for '?'
    public void testAdvancedSearchWithQuestionMark() throws Exception{
        LinkedHashSet<SearchRowItem> actualResult = new LinkedHashSet<>();
        actualResult=searchFragment.searchRetrievalResults("D?g");
        LinkedHashSet<SearchRowItem> expectedResult= new LinkedHashSet<>();
        LinkedHashSet<SearchRowItem> result= searchFragment.searchRetrievalResults("D?g");
        int actualCount=actualResult.size();
        Cursor cur = null;
        String enteredText="d_g";
        db=dbPath.getDbConnection();
        Boolean isWordPresent = false;
        if(db!=null) {
            String query = String.format("SELECT HWD,id,HOMNUM,frequent FROM lookup WHERE HWD like '%s' GROUP BY HWD LIMIT 30 ", enteredText);
            cur = db.rawQuery(query, null);
            while(cur.moveToNext())
            {
                //Add to list
                expectedResult.add(new SearchRowItem(cur.getString(0),"",cur.getString(3).equalsIgnoreCase("1")));

            }
            for (SearchRowItem item : result) {
                isWordPresent = (item.getHwd().equals("dog")) ? true : false;
                if (isWordPresent)
                    return;
            }
            assertEquals(Boolean.TRUE,isWordPresent);
        }
        boolean isTestEqual=expectedResult.containsAll(actualResult);
        if(isTestEqual)
        {
            System.out.println("Equal Set of Words -  Test Passed");
        }
        assertEquals(expectedResult,actualResult);
        int expectedCount=cur.getCount();
        assertEquals(expectedCount,actualCount);

    }

    //Testing for '*'
    public void testAdvancedSearchWithAsterikMark() throws Exception
    {
        LinkedHashSet<SearchRowItem> actualResult = new LinkedHashSet<>();
        actualResult=searchFragment.searchRetrievalResults("D*g");
        LinkedHashSet<SearchRowItem> expectedResult= new LinkedHashSet<>();
        LinkedHashSet<SearchRowItem> result= searchFragment.searchRetrievalResults("D*g");
        int actualCount=actualResult.size();
        Cursor cur = null;
        Boolean isWordPresent = false;
        String enteredText="d%g";
        db=dbPath.getDbConnection();
        if(db!=null)
        {
            String query=String.format("SELECT HWD,id,HOMNUM,frequent FROM lookup WHERE HWD like '%s' GROUP BY HWD LIMIT 30 ",enteredText);
            cur = db.rawQuery(query, null);

            while(cur.moveToNext())
            {
                //Add to list
                expectedResult.add(new SearchRowItem(cur.getString(0),"",cur.getString(3).equalsIgnoreCase("1")));
            }
        }
        boolean isTestEqual=expectedResult.containsAll(actualResult);
        if(isTestEqual)
        {
            System.out.println("Equal Set of Words -  Test Passed");
        }
//        assertEquals(expectedResult,actualResult);
// //       int expectedCount=cur.getCount();
// //       assertEquals(expectedCount,actualCount);

        for (SearchRowItem item : result) {
            isWordPresent = (item.getHwd().equals("deafening")) ? true : false;
            if (isWordPresent)
                return;
        }
        assertEquals(Boolean.TRUE,isWordPresent);
    }
    // testing for ? and *
    public void testAdvancedSearchWithAsterikAndQuestionMark() throws Exception
    {
        SearchFragment searchFragment= new SearchFragment();
        LinkedHashSet<SearchRowItem> actualResult = new LinkedHashSet<>();
        actualResult=searchFragment.searchRetrievalResults("A*e?");
        LinkedHashSet<SearchRowItem> result= searchFragment.searchRetrievalResults("A*e?");
        LinkedHashSet<SearchRowItem> expectedResult= new LinkedHashSet<>();
        int actualCount=actualResult.size();
        DbConnection dbPath= new DbConnection();
        SQLiteDatabase db;
        Cursor cur = null;
        Boolean isWordPresent = false;
        String enteredText="A%e_";
        db=dbPath.getDbConnection();
        if(db!=null)
        {
            String query=String.format("SELECT HWD,id,HOMNUM,frequent FROM lookup WHERE HWD like '%s' GROUP BY HWD LIMIT 30 ",enteredText);
            cur = db.rawQuery(query, null);

            while(cur.moveToNext())
            {
                //Add to list
                expectedResult.add(new SearchRowItem(cur.getString(0),"",cur.getString(3).equalsIgnoreCase("1")));
            }
        }
        boolean isTestEqual=expectedResult.containsAll(actualResult);
        if(isTestEqual)
        {
            System.out.println("Equal Set of Words -  Test Passed");
        }
//        assertEquals(expectedResult,actualResult);
//        int expectedCount=cur.getCount();
//        assertEquals(expectedCount,actualCount);
        for (SearchRowItem item : result) {
            isWordPresent = (item.getHwd().equals("Abel")) ? true : false;
            if (isWordPresent)
                return;
        }
        assertEquals(Boolean.TRUE,isWordPresent);
    }


}

