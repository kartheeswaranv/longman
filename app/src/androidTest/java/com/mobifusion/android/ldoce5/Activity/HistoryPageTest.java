package com.mobifusion.android.ldoce5.Activity;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import android.test.ActivityInstrumentationTestCase2;

import com.mobifusion.android.ldoce5.Fragment.HistoryFragment;
import com.mobifusion.android.ldoce5.R;

/**
 * Created by MuraliKDharan on 07/04/15.
 */
public class HistoryPageTest extends ActivityInstrumentationTestCase2<SlideMenuSearchAndIndex> {

    SlideMenuSearchAndIndex searchActivityObject;
    HistoryFragment historyFragmentObject;

    //Zero Argument Constructor
    public HistoryPageTest()
    {
        super(SlideMenuSearchAndIndex.class);
    }



    @Override
    public void setUp() throws Exception {
        super.setUp();

        // Setup for test class
        searchActivityObject = getActivity();
        FragmentTransaction ft = searchActivityObject.getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.indexResultfragment, new HistoryFragment());
        ft.commit();

    }
    //Check whether History fragment presents
    public void testHistoryFragment() throws Exception {
        Thread.sleep(900);
        Fragment fragment = searchActivityObject.getSupportFragmentManager().findFragmentById(R.id.indexResultfragment);
        Boolean isHistory = (fragment instanceof HistoryFragment)?true:false;
        assertEquals(Boolean.TRUE,isHistory);
    }

//    public void testDeletingEntries() throws Exception {
//
//        String[] array= new String[]{"u2fc098491a42200a.262cc60a.1180415e23b.3f88"};
//        // Added the word Rotarian for deleting an entry in history list
//
//        HistoryFragment historyFragment=new HistoryFragment();
//        boolean isExist=historyFragment.onDeleteEntriesInList(array); //returns true when the entry is deleted in db.
//        boolean isEntryInDB=false;
//        SQLiteDatabase db;
//        String query;
//        db = DbConnection.getUserDbConnection();
//        //Boolean isExist = true;
//        Thread.sleep(3000);
//        // Query the DB for checking if the word is deleted or not
//        if (db != null) {
//            query = String.format("select * from history where id = ?");
//            Cursor cur = db.rawQuery(query, new String[]{"u2fc098491a42200a.262cc60a.1180415e23b.3f88"});
//            if(cur.getCount()>0) {
//                isEntryInDB = true;
//            }
//            else
//            {
//                isEntryInDB=false;
//            }
//            assertNotSame(isEntryInDB,isExist);
//            db.close();
//        }
//    }

    public void testDeleteAlreadyDeletedEntry() throws Exception{

        String[] array= new String[]{"u2fc098491a42200a.262cc60a.1180415e23b.3f88"};
        // Added the word Rotarian as an entry in history list
        HistoryFragment historyFragment=new HistoryFragment();
        boolean isExist=historyFragment.onDeleteEntriesInList(array); //returns true when the entry is deleted in db.
        boolean isEntryInDB=true;
        SQLiteDatabase db;
        String query;
        db = DbConnection.getUserDbConnection();
        //Boolean isExist = true;
        Thread.sleep(3000);
        // Query the DB for checking if the word is deleted or not
        if (db != null) {
            query = String.format("delete from history where id = ?");
            Cursor cur = db.rawQuery(query, new String[]{"u2fc098491a42200a.262cc60a.1180415e23b.3f88"});
            while (cur.moveToNext())
            {
                isEntryInDB = false;
            }
        }
        assertNotSame(isEntryInDB, isExist);
        db.close();
    }

    public void testDeleteEntryNotInDb() throws Exception{

        String[] array= new String[]{"u2fc098491a42200a.262cc60a.1180415e23b.3f88"};
        // Added the word Rotarian as an entry in history list

        HistoryFragment historyFragment=new HistoryFragment();
        boolean isExist=historyFragment.onDeleteEntriesInList(array); //returns true when the entry is deleted in db.
        boolean isEntryInDB=true;
        SQLiteDatabase db;
        String query;
        db = DbConnection.getUserDbConnection();
        //Boolean isExist = true;
        Thread.sleep(3000);
        // Query the DB for checking if the word is deleted or not
        if (db != null) {
            query = String.format("delete from history where id = ?");
            Cursor cur = db.rawQuery(query, new String[]{"u2fc098491a42200a.6e2b450a.114ee8912f5.-2d7f"}); // A's id added
            while (cur.moveToNext())
            {
                isEntryInDB = false;
            }
        }
        assertNotSame(isEntryInDB,isExist);
        db.close();
    }

    public void testDeletingAEntryByPosition() throws Exception{

        String[] array= new String[]{"u2fc098491a42200a.262cc60a.1180415e23b.3f88","u2fc098491a42200a.6e2b450a.114ee8912f5.-2d7f","u2fc098491a42200a.6e2b450a.114ee8912f5.-2d35","u2fc098491a42200a.6e2b450a.114ee8912f5.-2d2c","u2fc098491a42200a.6e2b450a.114ee8912f5.-2cb4","u2fc098491a42200a.6e2b450a.114ee8912f5.-2c9d","u2fc098491a42200a.-5a72f42d.118c6f3dec0.-7ba1"};

        int count = array.length;

        int newCount = 0;

        String[] deletionItems = new String[]{"u2fc098491a42200a.262cc60a.1180415e23b.3f88","u2fc098491a42200a.-5a72f42d.118c6f3dec0.-7ba1","u2fc098491a42200a.6e2b450a.114ee8912f5.-2d35"};
        int deletionItemsCount = deletionItems.length;

        HistoryFragment historyFragment=new HistoryFragment();
        historyFragment.onDeleteEntriesInList(deletionItems);
        SQLiteDatabase db;
        String query;
        db = DbConnection.getUserDbConnection();
        Thread.sleep(3000);

        // Query the DB for checking if the word is deleted or not
        if (db != null) {
            query= String.format("select * from history");
            Cursor cur = db.rawQuery(query,new String[]{});
            while (cur.moveToNext()){
                newCount=cur.getCount();
            }
        }
        db.close();
        assertNotSame(count, newCount);
    }

    public void testCheckPositionAfterDeletion() throws Exception{
        
    }


    //Check Test Passing or not
    public void testPass() throws Exception {
        assertNotNull(searchActivityObject);
    }


}
