package com.mobifusion.android.ldoce5.Activity;

import android.content.ClipboardManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import androidx.core.app.Fragment;
import androidx.core.app.FragmentManager;
import android.test.ActivityInstrumentationTestCase2;

import com.mobifusion.android.ldoce5.Fragment.DetailPageFragment;
import com.mobifusion.android.ldoce5.R;
import com.mobifusion.android.ldoce5.Util.Utils;
import com.mobifusion.android.ldoce5.model.IndexRowItem;

import static android.content.Context.CLIPBOARD_SERVICE;

/**
 * Created by sureshchandrababu on 07/04/15.
 */
public class DetailPageTest extends ActivityInstrumentationTestCase2<SlideMenuSearchAndIndex> {

    SlideMenuSearchAndIndex homeActivityObject;
    Fragment detailPageFragment;

    public DetailPageTest()
    {
        super(SlideMenuSearchAndIndex.class);
    }

    @Override
    public void setUp() throws Exception {
        super.setUp();
        homeActivityObject = getActivity();
    }

    public void testToPassSetup() throws Exception {
        assertEquals(Boolean.TRUE,Boolean.TRUE);
    }
    /*
    * To check the Navigation
    */
    public void testNavigationToDetailPage() throws Exception {

        //Creating the sample Index row item. i.e clicked item
        IndexRowItem item = new IndexRowItem();
        item.setHwd("Rotarian");
        item.setHwdId("u2fc098491a42200a.262cc60a.1180415e23b.3f88");
        item.setIsFrequent(Boolean.FALSE);
        Utils.navigateToDetailPage(homeActivityObject,item.getHwd(),item.getHwdId(),"1","0");
        //Making thread to sleep for 9sec. Just allowing the fragment to update its UI
        Thread.sleep(3000);
        Fragment currentFragment;
        //Checking the Device and get the appropriate fragment ID
        if (Utils.isTablet(homeActivityObject))
        currentFragment= homeActivityObject.getSupportFragmentManager().findFragmentById(R.id.detailPageFragment);
        else
        currentFragment= homeActivityObject.getSupportFragmentManager().findFragmentById(R.id.indexResultfragment);
        Boolean isDetailFragment;
        //Check the fragment Instance type
        if (currentFragment instanceof DetailPageFragment)
            isDetailFragment = true;
        else
            isDetailFragment = false;
        assertEquals(Boolean.TRUE,isDetailFragment);
    }

    public void testNavigationBackStackCount() throws Exception {
        //Creating the sample Index row item. i.e clicked item
        IndexRowItem item = new IndexRowItem();
        item.setHwd("Rotarian");
        item.setHwdId("u2fc098491a42200a.262cc60a.1180415e23b.3f88");
        item.setIsFrequent(Boolean.FALSE);
        Utils.navigateToDetailPage(homeActivityObject,item.getHwd(),item.getHwdId(),"1","0");
        //Making thread to sleep for 9sec. Just allowing the fragment to update its UI
        Thread.sleep(3000);

        Utils.navigateToDetailPage(homeActivityObject,item.getHwd(),item.getHwdId(),"1","0");
        //Making thread to sleep for 9sec. Just allowing the fragment to update its UI
        Thread.sleep(3000);

        FragmentManager fragmentManager = homeActivityObject.getSupportFragmentManager();
        assertEquals(1,fragmentManager.getBackStackEntryCount());

    }

    //test for updating history table on navigation to a word


    public void testUpdateHistoryTable() throws Exception {

        //checking initial insertion
        IndexRowItem item = new IndexRowItem();
        item.setHwd("Rotarian");
        item.setHwdId("u2fc098491a42200a.262cc60a.1180415e23b.3f88");
        item.setIsFrequent(Boolean.FALSE);
//        Utils.navigateToDetailPage(homeActivityObject,item.getHwd(),item.getHwdId(),"1","0");
//        //Making thread to sleep for 9sec. Just allowing the fragment to update its UI
//        Thread.sleep(3000);
//        Fragment currentFragment;
//        //Checking the Device and get the appropriate fragment ID
//        if (Utils.isTablet(homeActivityObject))
//            currentFragment= homeActivityObject.getSupportFragmentManager().findFragmentById(R.id.detailPageFragment);
//        else
//            currentFragment= homeActivityObject.getSupportFragmentManager().findFragmentById(R.id.indexResultfragment);
//        Boolean isDetailFragment;
//        //Check the fragment Instance type
//        if (currentFragment instanceof DetailPageFragment)
//            isDetailFragment = true;
//        else
//            isDetailFragment = false;
//        assertEquals(Boolean.TRUE,isDetailFragment);

        SQLiteDatabase db;
        String query;
//        db = DbConnection.getUserDbConnection();
//        // Query the DB for checking if the word is already exist
        boolean isExist=false;
        String timeStamp = "";
        String id=item.getHwdId();
//        if(db!=null) {
//            query = String.format("select * from history where id = ?");
//            Cursor cur = db.rawQuery(query, new String[]{id});
//            while (cur.moveToNext()) {
//                isExist = true;
//                timeStamp= cur.getString(cur.getColumnIndex("timestamp"));
//            }
//        }
//        db.close();
//        assertEquals(true,isExist);
//        Thread.sleep(3000);
        //checking if timestamp is updating on re entry
//        Utils.navigateToDetailPage(homeActivityObject,item.getHwd(),item.getHwdId(),"1","0");
//        db = DbConnection.getUserDbConnection();
//        // Query the DB for checking if the word is already exist
//        isExist=false;
//        String timeStamp2="";
//        if(db!=null) {
//            query = String.format("select * from history where id = ?");
//            Cursor cur = db.rawQuery(query, new String[]{id});
//            while (cur.moveToNext()) {
//                isExist = true;
//                timeStamp2= cur.getString(cur.getColumnIndex("timestamp"));
//            }
//        }
//        db.close();
//        boolean check=!timeStamp.equalsIgnoreCase(timeStamp2);
//        assertEquals(true,check);
//
//        //checking if history table is updating on navigation to detail page
        db = DbConnection.getUserDbConnection();
        int count=0;
        if(db!=null) {
            query = String.format("select * from history");
            Cursor cur = db.rawQuery(query,null);
            count= cur.getCount();
        }
        db.close();
        //Navigating two other words
        item.setHwd("aback");
        item.setHwdId("u2fc098491a42200a.6e2b450a.114ee8912f5.-2c11");
        item.setIsFrequent(Boolean.FALSE);
        Utils.navigateToDetailPage(homeActivityObject,item.getHwd(),item.getHwdId(),"1","0");
        Thread.sleep(3000);
        item.setHwd("acclaim");
        item.setHwdId("u2fc098491a42200a.6e2b450a.114ee8912f5.-1e11");
        item.setIsFrequent(Boolean.FALSE);
        Utils.navigateToDetailPage(homeActivityObject,item.getHwd(),item.getHwdId(),"1","0");

        Thread.sleep(3000);
        db = DbConnection.getUserDbConnection();
        int count_afterInsertion=0;
        if(db!=null) {
            query = String.format("select * from history");
            Cursor cur = db.rawQuery(query,null);
            count_afterInsertion= cur.getCount();
        }
        db.close();
        assertEquals(count,count_afterInsertion);
    }

    public void testCopyToClipBoard() throws Exception {
            DetailPageFragment detailPageFragment = new DetailPageFragment();
            detailPageFragment.copyToClipBoard("eager",homeActivityObject);
            ClipboardManager clipboardManager = (ClipboardManager) getActivity().getSystemService(CLIPBOARD_SERVICE);
            String ccp=  (String)clipboardManager.getText();
            assertEquals("eager",ccp);
    }

    @Override
    public void tearDown() throws Exception {
        homeActivityObject.finish();
        super.tearDown();
    }
}
