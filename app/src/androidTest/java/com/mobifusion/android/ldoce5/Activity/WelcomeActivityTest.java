package com.mobifusion.android.ldoce5.Activity;

import android.content.SharedPreferences;
import android.test.ActivityInstrumentationTestCase2;
import android.widget.TextView;

import com.mobifusion.android.ldoce5.R;

/**
 * Created by Manikandan on 17/03/15.
 */
public class WelcomeActivityTest extends ActivityInstrumentationTestCase2<WelcomeActivity> {
    //Activity Object
    WelcomeActivity welcomeActivityObject;
    public SharedPreferences settings;
    public static final String PREFS_NAME = "LDOCE6PrefsFile";
    public WelcomeActivityTest() {
        super(WelcomeActivity.class);
    }
    @Override
    public void setUp() throws Exception {
        //Clearing App data helps to restart the app
        settings = getInstrumentation().getTargetContext().getSharedPreferences(PREFS_NAME,0);
        settings.edit().clear().commit();
        super.setUp();
        welcomeActivityObject = getActivity();
        assertNotNull("WelcomeActivity is Null", welcomeActivityObject);
    }
    @Override
    public void tearDown() throws Exception {
        //Cancelling the welcome task to re-test the welcome activity
        welcomeActivityObject.finish();
        if(welcomeActivityObject.task!=null)
            welcomeActivityObject.task.cancel(true);
        super.tearDown();
    }
    //Test for Compare Strings method
    public void testCompareStrings() throws Exception {
    TextView welcomeTextView =(TextView)welcomeActivityObject.findViewById(R.id.tv_welcome);
    assertEquals("Welcome!", (String) welcomeTextView.getText());
    }
    //Test for addLanguages method
    public void testAddLanguages() throws Exception
    {
       assertEquals(7,welcomeActivityObject.addLanguages().size());
    }
    //Test for getLanguageByCode method
    public void testGetLanguageByCode() throws Exception
    {
        assertNotSame("日本の",welcomeActivityObject.getLanguageByCode("ja").toString());
    }
}


