package com.mobifusion.android.ldoce5.Activity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import androidx.core.app.Fragment;
import androidx.core.app.FragmentManager;
import android.test.ActivityUnitTestCase;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;

import com.mobifusion.android.ldoce5.Fragment.HomePageFragment;
import com.mobifusion.android.ldoce5.Fragment.IndexFragment;
import com.mobifusion.android.ldoce5.R;
import com.mobifusion.android.ldoce5.Util.AlphabetListAdapter;
import com.mobifusion.android.ldoce5.model.IndexContent;
import com.mobifusion.android.ldoce5.model.IndexRowItem;
import com.mobifusion.android.ldoce5.model.RowItemWithIcon;
import com.mobifusion.android.ldoce5.model.SideIndexBarItem;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bazi on 17/03/15.
 */
public class SlideMenuSearchAndIndexTest extends ActivityUnitTestCase<SlideMenuSearchAndIndex> {

    private SlideMenuSearchAndIndex slideMenuSearchAndIndex;
    public SlideMenuSearchAndIndexTest(Class<SlideMenuSearchAndIndex> activityClass) {
        super(SlideMenuSearchAndIndex.class);
    }

    public SlideMenuSearchAndIndexTest()
    {
        super(SlideMenuSearchAndIndex.class);
    }

    @Override
    public void setUp() throws Exception {
        super.setUp();


    }

    public void testIndexActivity() throws Exception {
        //creating index activity to unit test
        startActivity(new Intent(getInstrumentation().getTargetContext(), SlideMenuSearchAndIndex.class), null, null);
        slideMenuSearchAndIndex=(SlideMenuSearchAndIndex)getActivity();
        IndexFragment indexActivity = new IndexFragment();
        androidx.core.app.FragmentManager manager = slideMenuSearchAndIndex.getSupportFragmentManager();
        manager.executePendingTransactions();
        Fragment fragment=(Fragment)manager.findFragmentById(R.id.indexResultfragment);
        Fragment currentDisplayingFragment = (Fragment) slideMenuSearchAndIndex.getSupportFragmentManager().findFragmentById(R.id.indexResultfragment);
        if (currentDisplayingFragment instanceof IndexFragment) {

            //expected values
            String[] expectedValues={"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"};

            //Generating first level index
            IndexContent indexContent=indexActivity.generateContentForIndex("");
            List<IndexRowItem> rowItems= indexContent.indexViewContent;
            int i=0;
            for(IndexRowItem obj:rowItems)
            {
                assertEquals(obj.getHwd(),expectedValues[i]);
                i++;
            }

            //checking for side index values
            String[] expectedValuesforIndexBar={"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"};
            List<SideIndexBarItem> indexBarItems= indexContent.sideIndexBarContent;
            i=0;
            for(SideIndexBarItem obj:indexBarItems)
            {
                assertEquals(obj.getText(),expectedValuesforIndexBar[i]);
                i++;
            }

            /**
             * ###################################################
             */

            //Assing a alphabet as clicked character
            String clickedAlphabet="U";
            //Generating Expected values
            List<String> expectedValue=new ArrayList<>();
            try {
                //Data base connection
                SQLiteDatabase db = SQLiteDatabase.openDatabase(DBFilePathaAndName, null, 0);
                String query;
                //checking if its a first #section
                if(clickedAlphabet.equalsIgnoreCase("#"))
                {
                    query = String.format("SELECT  DISTINCT(HWD),id,frequency as 0 from core where (SUBSTR (upper(HWD),1,1)NOT BETWEEN 'A' AND 'Z')");
                }
                else
                    query = String.format("select DISTINCT(hwd)as hw ,id from core where hwd like '%s%%' order by hw collate nocase", clickedAlphabet);
                Cursor cur = db.rawQuery(query, null);
                if (cur.getCount() <= 0) {

                }
                else
                {
                    //traversing through all the items
                    expectedValue.add("A-Z");
                    while (cur.moveToNext())
                    {
                        String hwd=cur.getString(0);
                        String hwdId=cur.getString(1);
                        //Storing as IndexRowItem
                        expectedValue.add(hwd);
                    }
                }
                db.close();
            }
            catch (Exception e) {

            }

            //Expected values
            List<String> expectedSideIndexBarValues=new ArrayList<>();

            String currentCharacter,previousCharacter;
            previousCharacter="";
            currentCharacter="";
            int start=1;
            int end=1;
            SideIndexBarItem indexItem;
            //Adding 'A-Z' section to the indexbar and listview
            //Adding element to the side index bar
            expectedSideIndexBarValues.add("A-Z");
            //Adding section title  and its starting position
            //Data base connection
            SQLiteDatabase db = SQLiteDatabase.openDatabase(DBFilePathaAndName, null, 0);
            String query="select  REPLACE(SUBSTR(sortname,1,2),'_','') AS KEY from lookup where sortname like ? group by hwd order by sortname  collate nocase";

            Cursor cur = db.rawQuery(query,new String[]{clickedAlphabet+"%"});
            if (cur.getCount() <= 0) {
                Log.e("IndexFragment:", "Database empty");

            }
            else
            {
                //Adding section title  and its starting position
                int count=0;
                while (cur.moveToNext())
                {
                    //Sub index title like AA,AB,AC...
                    currentCharacter = cur.getString(0);
                    //Count for each section like AA,AB,AC....
                    count++;
                    //calculating the starting and ending location of each section(on clicking the side index bar).
                    if(!currentCharacter.equalsIgnoreCase(previousCharacter)){
                        //Since we are not using the end value,it is put as dummy. did not calculated its value
                        previousCharacter=currentCharacter;
                        //Adding element to the side index bar
                        expectedSideIndexBarValues.add(previousCharacter);
                    }
                }

            }
            db.close();

            //Generating second level index
            IndexContent indexContents=indexActivity.generateContentForIndex(clickedAlphabet);
            List<IndexRowItem> rowItemss= indexContents.indexViewContent;
            i=0;
            //checking if two values are equal
            for(IndexRowItem obj:rowItemss)
            {
                assertEquals(expectedValue.get(i), obj.getHwd());
                i++;
            }


            //checking if indexbar values are same as expected
            List<SideIndexBarItem> indexBarItemss= indexContents.sideIndexBarContent;
            i=0;
            for(SideIndexBarItem obj:indexBarItemss)
            {
                assertEquals(obj.getText(), expectedSideIndexBarValues.get(i));
                i++;
            }

        }
    }

    public void testSlideMenuContent() throws Exception {

        //Starting the default activity
        startActivity(new Intent(getInstrumentation().getTargetContext(), SlideMenuSearchAndIndex.class), null, null);
        slideMenuSearchAndIndex=(SlideMenuSearchAndIndex)getActivity();
        if(!slideMenuSearchAndIndex.isTablet(slideMenuSearchAndIndex)) {
            Button menuButton = (Button) slideMenuSearchAndIndex.findViewById(R.id.slideMenuButton);
            menuButton.performClick();
            androidx.core.app.FragmentManager manager = slideMenuSearchAndIndex.getSupportFragmentManager();
            manager.executePendingTransactions();

            //TouchUtils.clickView(this, menuButton);

            ListView list = (ListView) slideMenuSearchAndIndex.findViewById(R.id.listItems);
            AlphabetListAdapter adapter = (AlphabetListAdapter) list.getAdapter();

            List<RowItemWithIcon> expectedValues = new ArrayList<>();
            expectedValues.add(new RowItemWithIcon("favorites", "\uf004", R.string.favorites));
            expectedValues.add(new RowItemWithIcon("history", "\uf017", R.string.history));
            expectedValues.add(new RowItemWithIcon("core_vocabulary", "\uf081", R.string.core_vocabulary));
            expectedValues.add(new RowItemWithIcon("academic_word_list", "\uf19d", R.string.academic_word_list));
            expectedValues.add(new RowItemWithIcon("about_this_app", "\uf05a", R.string.about_this_app));
            expectedValues.add(new RowItemWithIcon("settings", "\uf013", R.string.settings));
            //List<RowItemWithIcon> rowItemWithIcons = slideMenuSearchAndIndex. .generateContentForSlideMenu();
            int i = 0;
            for (i = 0; i < adapter.getCount(); i++) {
                RowItemWithIcon actualValues = (RowItemWithIcon) adapter.getItem(i);
                assertEquals("Checking for Slide Menu", expectedValues.get(i).getStringCode(), actualValues.getStringCode());
            }
        }
    }

    public void testHomePageButtonTapSettingsThenHome() throws Exception
    {
        //Starting the default activity
        startActivity(new Intent(getInstrumentation().getTargetContext(), SlideMenuSearchAndIndex.class), null, null);
        slideMenuSearchAndIndex = getActivity();
        if(slideMenuSearchAndIndex.isTablet(slideMenuSearchAndIndex))
        {
            //tap on Settings button
            Button settings = (Button)slideMenuSearchAndIndex.findViewById(R.id.settings_btn);
            settings.performClick();

            //tap on the home button.
            ImageButton homeButton = (ImageButton)slideMenuSearchAndIndex.findViewById(R.id.home_button);
            homeButton.performClick();

            FragmentManager fragmentManager=slideMenuSearchAndIndex.getSupportFragmentManager();
            fragmentManager.executePendingTransactions();
            Fragment currentFragment=fragmentManager.findFragmentById(R.id.detailPageFragment);


            //check the backstack count. it should be 1 (only home page fragment)
            int backstackCount = fragmentManager.getBackStackEntryCount();
            assertEquals("BackStack count should be 0, after clicking home page button",0,backstackCount);

            //check the current fragment instance type.
            assertTrue("Current Fragment should be Home fragment",currentFragment instanceof HomePageFragment);
        }
    }

    public void testHomePageButtonTapSettingsAboutThenHome() throws Exception
    {
        //Starting the default activity
        startActivity(new Intent(getInstrumentation().getTargetContext(), SlideMenuSearchAndIndex.class), null, null);
        slideMenuSearchAndIndex = getActivity();
        if(slideMenuSearchAndIndex.isTablet(slideMenuSearchAndIndex))
        {
            //tap on Settings button
            Button settings = (Button)slideMenuSearchAndIndex.findViewById(R.id.settings_btn);
            settings.performClick();

            //tap on About button
            Button about = (Button)slideMenuSearchAndIndex.findViewById(R.id.about_btn);
            about.performClick();

            //tap on the home button.
            ImageButton homeButton = (ImageButton)slideMenuSearchAndIndex.findViewById(R.id.home_button);
            homeButton.performClick();

            FragmentManager fragmentManager=slideMenuSearchAndIndex.getSupportFragmentManager();
            fragmentManager.executePendingTransactions();
            Fragment currentFragment=fragmentManager.findFragmentById(R.id.detailPageFragment);


            //check the backstack count. it should be 1 (only home page fragment)
            int backstackCount = fragmentManager.getBackStackEntryCount();
            assertEquals("BackStack count should be 0, after clicking home page button",0,backstackCount);

            //check the current fragment instance type.
            assertTrue("Current Fragment should be Home fragment",currentFragment instanceof HomePageFragment);
        }
    }
}
