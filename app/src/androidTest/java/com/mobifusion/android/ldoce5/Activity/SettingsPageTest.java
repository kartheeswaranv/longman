package com.mobifusion.android.ldoce5.Activity;

import androidx.core.app.Fragment;
import androidx.core.app.FragmentTransaction;
import android.test.ActivityInstrumentationTestCase2;

import com.mobifusion.android.ldoce5.Fragment.SettingsFragment;
import com.mobifusion.android.ldoce5.R;
import com.mobifusion.android.ldoce5.model.Language;

import java.util.ArrayList;

/**
 * Created by MuraliKDharan on 08/06/15.
 */
public class SettingsPageTest extends ActivityInstrumentationTestCase2<SlideMenuSearchAndIndex> {

    SlideMenuSearchAndIndex searchActivityObject;
    //Zero Argument Constructor
    public SettingsPageTest() {
        super(SlideMenuSearchAndIndex.class);
    }

    @Override
    public void setUp() throws Exception {
        //Setup for test cases
        super.setUp();
        searchActivityObject=getActivity();
        //Replacing the indexResultFragment with core vocabulary fragment
        FragmentTransaction ft = searchActivityObject.getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.indexResultfragment, new SettingsFragment());
        ft.commit();
    }

    //Check Test Passing or not
    public void testPass() throws Exception {
        Thread.sleep(900);
        assertNotNull(searchActivityObject);
    }
    //Check whether settings fragment presents
    public void testSettingsFragment() throws Exception
    {
        Thread.sleep(900);
        Fragment fragment = searchActivityObject.getSupportFragmentManager().findFragmentById(R.id.indexResultfragment);
        Boolean isSettingsFragment = (fragment instanceof SettingsFragment)?true:false;
        assertEquals(Boolean.TRUE, isSettingsFragment);
    }
    public void testLanguageDropdown() throws Exception {
        Thread.sleep(900);
        Fragment fragment = searchActivityObject.getSupportFragmentManager().findFragmentById(R.id.indexResultfragment);
        ArrayList<Language> arrayList = ((SettingsFragment)fragment).getLanguages();
        assertEquals(arrayList.size(), 7);
    }
}
