package com.mobifusion.android.ldoce5.Activity;

import android.database.sqlite.SQLiteDatabase;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import android.test.ActivityInstrumentationTestCase2;

import com.mobifusion.android.ldoce5.Fragment.CoreVocabularyFragment;
import com.mobifusion.android.ldoce5.R;
import com.mobifusion.android.ldoce5.model.RowWithHomnum;

import java.util.ArrayList;

/**
 * Created by Manikandan on 02/04/15.
 */
public class CoreVocabularyTest extends ActivityInstrumentationTestCase2<SlideMenuSearchAndIndex> {
    SlideMenuSearchAndIndex searchActivityObject;
    CoreVocabularyFragment coreVocabularyFragmentObject;
    DbConnection dbPath;
    SQLiteDatabase db;
    //Zero Argument Constructor
    public CoreVocabularyTest()
    {
        super(SlideMenuSearchAndIndex.class);
    }

    @Override
    public void setUp() throws Exception {
        //Setup for test cases
        super.setUp();
        searchActivityObject=getActivity();
        dbPath= new DbConnection();
        db=dbPath.getDbConnection();
        //Replacing the indexResultFragment with core vocabulary fragment
        FragmentTransaction ft = searchActivityObject.getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.indexResultfragment, new CoreVocabularyFragment());
        ft.commit();
    }

    //Check Test Passing or not
    public void testPass() throws Exception {
        assertNotNull(searchActivityObject);
    }

    //Check whether core vocabulary fragment presents
    public void testCoreVocabularyFragment() throws Exception
    {
        Thread.sleep(900);
        Fragment fragment = searchActivityObject.getSupportFragmentManager().findFragmentById(R.id.indexResultfragment);
        Boolean isCoreVocabulary = (fragment instanceof CoreVocabularyFragment)?true:false;
        assertEquals(Boolean.TRUE,isCoreVocabulary);
    }


    //Filter All 9000 Words Testcase
    public void testAll9000Words() throws Exception
    {
        Thread.sleep(900);
        Fragment fragment = searchActivityObject.getSupportFragmentManager().findFragmentById(R.id.indexResultfragment);
        //Fetch the query result of All 9000 words
        ArrayList<RowWithHomnum> arrayList = ((CoreVocabularyFragment)fragment).getRows(0);
        //Check the id of word 'a' present inside the all 9000 word filter
        String checkingString= "u2fc098491a42200a.6e2b450a.114ee8912f5.-2d2c";
        ArrayList<String> row = new ArrayList<String>();
        for(RowWithHomnum rowWithHomnum : arrayList)
        {
            //Assigning row id's to a string array
           row.add(rowWithHomnum.getHwdId().toString());
        }
        //Check with the String array which contains the checking string or not
        assertEquals(true,row.contains(checkingString));
    }
    //Filter High Frequency Words Testcase
    public void testHighFrequencyWords() throws Exception
    {
        Thread.sleep(900);
        Fragment fragment = searchActivityObject.getSupportFragmentManager().findFragmentById(R.id.indexResultfragment);
        //Fetch the query result of High Frequency words
        ArrayList<RowWithHomnum> arrayList = ((CoreVocabularyFragment)fragment).getRows(1);
        //Check the id of word 'a' present inside the High frequency word filter
        String checkingString= "u2fc098491a42200a.6e2b450a.114ee8912f5.-2d2c";
        ArrayList<String> row = new ArrayList<String>();
        for(RowWithHomnum rowWithHomnum : arrayList)
        {
            //Assigning row id's to a string array
            row.add(rowWithHomnum.getHwdId().toString());
        }
        //Check with the String array which contains the checking string or not
        assertEquals(true,row.contains(checkingString));
    }
    //Filter Medium Frequency Words Testcase
    public void testMediumFrequencyWords() throws Exception
    {
        Thread.sleep(900);
        Fragment fragment = searchActivityObject.getSupportFragmentManager().findFragmentById(R.id.indexResultfragment);
        //Fetch the query result of Medium Frequency words
        ArrayList<RowWithHomnum> arrayList = ((CoreVocabularyFragment)fragment).getRows(2);
        //Check the id of word 'abandon' present inside the Medium Frequency word filter
        String checkingString= "u2fc098491a42200a.6e2b450a.114ee8912f5.-2bea";
        ArrayList<String> row = new ArrayList<String>();
        for(RowWithHomnum rowWithHomnum : arrayList)
        {
            //Assigning row id's to a string array
            row.add(rowWithHomnum.getHwdId().toString());
        }
        //Check with the String array which contains the checking string or not
        assertEquals(true,row.contains(checkingString));
    }
    //Filter Low Frequency Words Testcase
    public void testLowFrequencyWords() throws Exception
    {
        Thread.sleep(900);
        Fragment fragment = searchActivityObject.getSupportFragmentManager().findFragmentById(R.id.indexResultfragment);
        //Fetch the query result of Low Frequency words
        ArrayList<RowWithHomnum> arrayList = ((CoreVocabularyFragment)fragment).getRows(3);
        //Check the id of word 'abbreviate' present inside the Low Frequency word filter
        String checkingString= "u2fc098491a42200a.6e2b450a.114ee8912f5.-2b13";
        ArrayList<String> row = new ArrayList<String>();
        for(RowWithHomnum rowWithHomnum : arrayList)
        {
            //Assigning row id's to a string array
            row.add(rowWithHomnum.getHwdId().toString());
        }
        //Check with the String array which contains the checking string or not
        assertEquals(true,row.contains(checkingString));
    }
    //Filter All Top Spoken Words Testcase
    public void testAllTopSpokenWords() throws Exception
    {
        Thread.sleep(900);
        Fragment fragment = searchActivityObject.getSupportFragmentManager().findFragmentById(R.id.indexResultfragment);
        //Fetch the query result of All Spoken words
        ArrayList<RowWithHomnum> arrayList = ((CoreVocabularyFragment)fragment).getRows(4);
        //Check the id of word 'half' present inside the All Spoken word filter
        String checkingString= "u2fc098491a42200a.6e2b450a.11503f8242d.3509";
        ArrayList<String> row = new ArrayList<String>();
        for(RowWithHomnum rowWithHomnum : arrayList)
        {
            //Assigning row id's to a string array
            row.add(rowWithHomnum.getHwdId().toString());
        }
        //Check with the String array which contains the checking string or not
        assertEquals(true,row.contains(checkingString));
    }
    //Filter Top 1000 Spoken Words Testcase
    public void testTop1000SpokenWords() throws Exception
    {
        Thread.sleep(900);
        Fragment fragment = searchActivityObject.getSupportFragmentManager().findFragmentById(R.id.indexResultfragment);
        //Fetch the query result of Top 1000 Spoken words
        ArrayList<RowWithHomnum> arrayList = ((CoreVocabularyFragment)fragment).getRows(5);
        //Check the id of word 'half' present inside the Top 1000 Spoken word filter
        String checkingString= "u2fc098491a42200a.6e2b450a.11503f8242d.3509";
        ArrayList<String> row = new ArrayList<String>();
        for(RowWithHomnum rowWithHomnum : arrayList)
        {
            //Assigning row id's to a string array
            row.add(rowWithHomnum.getHwdId().toString());
        }
        //Check with the String array which contains the checking string or not
        assertEquals(true,row.contains(checkingString));
    }
    //Filter Top 2000 Spoken Words Testcase
    public void testTop2000SpokenWords() throws Exception
    {
        Thread.sleep(900);
        Fragment fragment = searchActivityObject.getSupportFragmentManager().findFragmentById(R.id.indexResultfragment);
        //Fetch the query result of Top 2000 Spoken words
        ArrayList<RowWithHomnum> arrayList = ((CoreVocabularyFragment)fragment).getRows(6);
        //Check the id of word 'yellow' present inside the Top 2000 Spoken word filter
        String checkingString= "u2fc098491a42200a.6e2b450a.115045a11b6.-2daa";
        ArrayList<String> row = new ArrayList<String>();
        for(RowWithHomnum rowWithHomnum : arrayList)
        {
            //Assigning row id's to a string array
            row.add(rowWithHomnum.getHwdId().toString());
        }
        //Check with the String array which contains the checking string or not
        assertEquals(true,row.contains(checkingString));
    }
    //Filter Top 3000 Spoken Words Testcase
    public void testTop3000SpokenWords() throws Exception
    {
        Thread.sleep(900);
        Fragment fragment = searchActivityObject.getSupportFragmentManager().findFragmentById(R.id.indexResultfragment);
        //Fetch the query result of Top 3000 Spoken words
        ArrayList<RowWithHomnum> arrayList = ((CoreVocabularyFragment)fragment).getRows(7);
        //Check the id of word 'magic' present inside the Top 3000 Spoken word filter
        String checkingString= "u2fc098491a42200a.6e2b450a.1150415cba4.259";
        ArrayList<String> row = new ArrayList<String>();
        for(RowWithHomnum rowWithHomnum : arrayList)
        {
            //Assigning row id's to a string array
            row.add(rowWithHomnum.getHwdId().toString());
        }
        //Check with the String array which contains the checking string or not
        assertEquals(true,row.contains(checkingString));
    }
    //Filter All Written Words Testcase
    public void testAllWrittenWords() throws Exception
    {
        Thread.sleep(900);
        Fragment fragment = searchActivityObject.getSupportFragmentManager().findFragmentById(R.id.indexResultfragment);
        //Fetch the query result of All Written words
        ArrayList<RowWithHomnum> arrayList = ((CoreVocabularyFragment)fragment).getRows(8);
        //Check the id of word 'magic' present inside the All Written word filter
        String checkingString= "u2fc098491a42200a.6e2b450a.1150415cba4.259";
        ArrayList<String> row = new ArrayList<String>();
        for(RowWithHomnum rowWithHomnum : arrayList)
        {
            //Assigning row id's to a string array
            row.add(rowWithHomnum.getHwdId().toString());
        }
        //Check with the String array which contains the checking string or not
        assertEquals(true,row.contains(checkingString));
    }
    //Filter Top 1000 Written Words Testcase
    public void testTop1000WrittenWords() throws Exception
    {
        Thread.sleep(900);
        Fragment fragment = searchActivityObject.getSupportFragmentManager().findFragmentById(R.id.indexResultfragment);
        //Fetch the query result of Top 1000 Written words
        ArrayList<RowWithHomnum> arrayList = ((CoreVocabularyFragment)fragment).getRows(9);
        //Check the id of word 'name' present inside the Top 1000 Written word filter
        String checkingString= "u2fc098491a42200a.6e2b450a.1150433a334.-4460";
        ArrayList<String> row = new ArrayList<String>();
        for(RowWithHomnum rowWithHomnum : arrayList)
        {
            //Assigning row id's to a string array
            row.add(rowWithHomnum.getHwdId().toString());
        }
        //Check with the String array which contains the checking string or not
        assertEquals(true,row.contains(checkingString));
    }
    //Filter Top 2000 Written Words Testcase
    public void testTop2000WrittenWords() throws Exception
    {
        Thread.sleep(900);
        Fragment fragment = searchActivityObject.getSupportFragmentManager().findFragmentById(R.id.indexResultfragment);
        //Fetch the query result of Top 2000 Written words
        ArrayList<RowWithHomnum> arrayList = ((CoreVocabularyFragment)fragment).getRows(10);
        //Check the id of word 'package' present inside the Top 2000 Written word filter
        String checkingString= "u2fc098491a42200a.6e2b450a.1150433a334.63c7";
        ArrayList<String> row = new ArrayList<String>();
        for(RowWithHomnum rowWithHomnum : arrayList)
        {
            //Assigning row id's to a string array
            row.add(rowWithHomnum.getHwdId().toString());
        }
        //Check with the String array which contains the checking string or not
        assertEquals(true,row.contains(checkingString));
    }
    //Filter Top 3000 Written Words Testcase
    public void testTop3000WrittenWords() throws Exception
    {
        Thread.sleep(900);
        Fragment fragment = searchActivityObject.getSupportFragmentManager().findFragmentById(R.id.indexResultfragment);
        //Fetch the query result of Top 3000 words
        ArrayList<RowWithHomnum> arrayList = ((CoreVocabularyFragment)fragment).getRows(11);
        //Check the id of word 'sad' present inside the Top 3000 Written word filter
        String checkingString= "u2fc098491a42200a.6e2b450a.115043d5839.5dbd";
        ArrayList<String> row = new ArrayList<String>();
        for(RowWithHomnum rowWithHomnum : arrayList)
        {
            //Assigning row id's to a string array
            row.add(rowWithHomnum.getHwdId().toString());
        }
        //Check with the String array which contains the checking string or not
        assertEquals(true,row.contains(checkingString));
    }
}
